package bgu.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;

import java.net.URL;

/**
 * User: zruya
 * Date: 13/12/12
 * Time: 15:18
 */
public class TestParent {
    @Rule
    public TestName testName = new TestName();

    private long time = 0;

    @Before
    public void writeTestNameBefore() throws NoSuchMethodException {
        System.out.println();
        System.out.println();
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Executing test: " + getClass().getSimpleName() + "." + testName.getMethodName());
        System.out.println("------------------------------------------------------------------------");
        System.out.println();
        time = System.currentTimeMillis();
    }

    @After
    public void writeTestNameAfter() throws NoSuchMethodException {
        if (time != 0){
            time = System.currentTimeMillis() - time;
            System.out.println();
            System.out.println("------------------------------------------------------------------------");
            System.out.println("Finished test: " + getClass().getSimpleName() + "." + testName.getMethodName() + ", Time: " + time + "ms");
            System.out.println("------------------------------------------------------------------------");
            System.out.println();
        } else {
            System.out.println(getClass().getSimpleName() + "." + testName.getMethodName() + " was skipped");
            System.out.println();
        }
    }


    public static URL findResource(String resourcePath) {
        return TestParent.class.getClassLoader().getResource(resourcePath);
    }
}
