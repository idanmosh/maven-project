package bgu.tests.runner;

import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.annotations.ParamsConverter;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

/**
 * User: zruya
 * Date: 13/12/12
 * Time: 17:41
 */
public class ModelCustomTestRunner extends BlockJUnit4ClassRunner {

    public ModelCustomTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected Statement methodInvoker(FrameworkMethod method, Object test) {
        return new ModelStatement(method, test);
    }


    private class ModelStatement extends Statement {
        private FrameworkMethod method;
        private Object test;

        public ModelStatement(FrameworkMethod method, Object test) {
            this.method = method;
            this.test = test;
        }

        @Override
        public void evaluate() throws Throwable {
            Field modelField = getModelField(test.getClass());
            Field paramsField = getParamObjField(test.getClass());
            Method convertMethod = getParamsConverterMethod(test.getClass());
            CompileModels compileModels = method.getAnnotation(CompileModels.class);

            if (modelField != null && compileModels != null) {
                String[] useFiles = compileModels.useFiles();
                assertNotSame("Error, u must specify at least one use spec file in the annotation", useFiles.length, 0);

                String[] paramFiles = compileModels.paramFiles();

                List<MModel> compiledModel = compileUseModels(useFiles);
                List<String> loadedParams = loadParams(paramFiles);

                assertEquals("Error, error while compiling, got a different amount of compiled models then expected", useFiles.length, compiledModel.size());

                if (loadedParams.size() > 0) {
                    assertEquals("Error, expected the same amount of param and use files", compiledModel.size(), loadedParams.size());
                    runTestsWithParams(modelField, paramsField, compiledModel, loadedParams);
                } else {
                    runTestsWithoutParams(modelField, compiledModel);
                }

            } else {
                method.invokeExplosively(test);
            }
        }

        private void runTestsWithParams(Field modelField, Field paramsField, List<MModel> compiledModel, List<String> loadedParams) throws Throwable {
            for (int i = 0; i < compiledModel.size(); i++) {
                modelField.set(test, compiledModel.get(i));
                paramsField.set(test, loadedParams.get(i));
                method.invokeExplosively(test);
            }
        }

        private void runTestsWithoutParams(Field modelField,List<MModel> compiledModel) throws Throwable {
            for (MModel aCompiledModel : compiledModel) {
                modelField.set(test, aCompiledModel);
                method.invokeExplosively(test);
            }
        }

        private List<MModel> compileUseModels(String[] useFiles) {
            ArrayList<MModel> compiledModels = new ArrayList<MModel>();

            for (String useFile : useFiles) {
                Reader r = null;

                URL resource = TestParent.findResource(useFile);
                if (resource == null) {
                    assertTrue("couldn't find file " + useFile, false);
                }

                try {
                    r = new BufferedReader(new FileReader(resource.getFile()));
                } catch (FileNotFoundException e) {
                    assertTrue(e.getMessage(), false);
                }

                MModel mModel = USECompiler.compileSpecification(r,
                        useFile, new PrintWriter(System.err),
                        new ModelFactory());

                if (mModel == null) {
                    assertTrue("error compiling " + useFile, false);
                }

                compiledModels.add(mModel);
            }

            return compiledModels;
        }

        private List<String> loadParams(String[] paramFiles) throws IOException {
            ArrayList<String> params = new ArrayList<String>();

            for (String paramFile : paramFiles) {
                BufferedReader br = null;

                URL resource = TestParent.findResource(paramFile);
                if (resource == null) {
                    assertTrue("couldn't find file " + paramFile, false);
                }

                try {
                    StringBuilder sb = new StringBuilder();
                    br = new BufferedReader(new FileReader(resource.getFile()));
                    String line = br.readLine();
                    while (line != null) {
                        sb.append(line);
                        sb.append("\n");
                        line = br.readLine();
                    }
                    String param = sb.toString();
                    params.add(param);
                } catch (FileNotFoundException e) {
                    assertTrue(e.getMessage(), false);
                } catch (IOException e) {
                    assertTrue(e.getMessage(), false);
                } finally {
                    if (br != null) {
                        br.close();
                    }
                }
            }

            return params;
        }


        private Field getModelField(Class<?> klass) {
            for (Field f : klass.getDeclaredFields()) {
                if (f.isAnnotationPresent(InjectCompiledModel.class)) {
                    f.setAccessible(true);
                    return f;
                }
            }

            return null;
        }

        private Field getParamObjField(Class<?> klass) {
            for (Field f : klass.getDeclaredFields()) {
                if (f.isAnnotationPresent(InjectSpecParamObject.class)) {
                    f.setAccessible(true);
                    return f;
                }
            }

            return null;
        }

        private Method getParamsConverterMethod(Class<?> klass) {

            for (Method m : klass.getDeclaredMethods()) {
                if (m.isAnnotationPresent(ParamsConverter.class)) {
                    m.setAccessible(true);
                    return m;
                }
            }

            return null;
        }
    }
}
