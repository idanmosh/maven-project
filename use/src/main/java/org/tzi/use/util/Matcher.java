package org.tzi.use.util;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MNavigableElement;

import java.util.Iterator;
import java.util.List;

public class Matcher {
    
	/**
	 * This function searches for a permutation of 'subs' such that each class referenced by 'supers'
	 * is a superclass of a class referenced by 'subs' 
	 * @param supers list of MNavigableElement
	 * @param subs list of MNavigableElement
	 * @return permutation of 'subs' such that 
	 * for each i: 
	 * 		supers.elementAt(i).cls().isSuperClassOf(subs.elementAt(i).cls())
	 * 		and subs.elementAt(i).cls().isSubClassOf(supers.elementAt(i).cls())
	 * or <code>null</code> if such permutation doesn't exist
	 */
	public static List<? extends MNavigableElement> findMatchByHierarchy
	(List<? extends MNavigableElement> supers, List<? extends MNavigableElement> subs ) {

    	List<MNavigableElement> resultLst;

    	PermutationGenerator<MNavigableElement> permGen = 
    		new PermutationGenerator<MNavigableElement>((List<MNavigableElement>)subs);

    	while (permGen.hasMore()) {
    		resultLst = permGen.getNextList();
    		if (matches((List<MNavigableElement>)supers, (List<MNavigableElement>)resultLst)) {
    			return resultLst;
    		}
    	}	
    	return null;
    }

	/**
	 * Tests if each class referenced by 'supers' is a super class of classes referenced by 'subs'
	 * @param supers
	 * @param subs
	 * @return 
	 */
    private static boolean matches(List<MNavigableElement> supers, List<MNavigableElement> subs ) {
    	if (supers.size() != subs.size()) return false;
    	Iterator<MNavigableElement> supIt, subIt;
    	MClass sup, sub;
    	supIt = supers.iterator();
    	subIt = subs.iterator();
    	while(supIt.hasNext() && subIt.hasNext()) {
    		sup = supIt.next().cls(); sub = subIt.next().cls();
    		if (!sub.isSubClassOf(sup)) {
    			return false;
    		}
    	}
    	return true;    	
    }
}
