/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: ASTAssociation.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.parser.use;

import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.parser.AST;
import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MAggregationKind;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MConform;
import org.tzi.use.uml.mm.MGeneralization;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MRedefine;
import org.tzi.use.uml.mm.MSubset;
import org.tzi.use.util.Matcher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Node of the abstract syntax tree constructed by the parser.
 *
 * @version     $ProjectVersion: 0.393 $
 * @author  Mark Richters
 */
public class ASTAssociation extends AST {
    private MyToken fKind;
    private MyToken fName;
    private List<ASTAssociationEnd> fAssociationEnds; // (ASTAssociationEnd)
    private List <MyToken> fparentAssociations;

    public ASTAssociation(MyToken kind, MyToken name) {
        fKind = kind;
        fName = name;
        fAssociationEnds = new ArrayList<ASTAssociationEnd>();
        fparentAssociations = new ArrayList<MyToken>();
    }
    
    public String getName() {
    	return fName.getText();
    }

    public void addEnd(ASTAssociationEnd ae) {
        fAssociationEnds.add(ae);
    }
    
    public void addParentAssociations(List<MyToken> parentAssociations) {
    	fparentAssociations = parentAssociations;
    }
    
    public List<MyToken> getParentAssociations() {
    	return fparentAssociations;
    }
    
    public List<ASTAssociationEnd> getAssociationEnds() {
    	return fAssociationEnds;
    }

    public MAssociation gen(Context ctx, MModel model) 
        throws SemanticException 
    {
        MAssociation assoc = ctx.modelFactory().createAssociation(fName.getText());
        // sets the line position of the USE-Model in this association
        assoc.setPositionInModel( fName.getLine() );
        String kindname = fKind.getText();
        int kind = MAggregationKind.NONE;
        if (kindname.equals("association") ) {
        }
        else if (kindname.equals("aggregation") )
            kind = MAggregationKind.AGGREGATION;
        else if (kindname.equals("composition") )
            kind = MAggregationKind.COMPOSITION;

        Iterator it = fAssociationEnds.iterator();
        try {
            while (it.hasNext() ) {
                ASTAssociationEnd ae = (ASTAssociationEnd) it.next();

                // kind of association determines kind of first
                // association end
                
                MAssociationEnd aend = ae.gen(ctx, kind);
                
                //System.out.println(aend.name() + " " + aend.isQualified());
                
               
                assoc.addAssociationEnd(aend);

                // further ends are plain ends
                kind = MAggregationKind.NONE;
            }
            
            model.addAssociation(assoc);
        } 
        catch (MInvalidModelException ex) {
            throw new SemanticException(fName,
                                        "In " + MAggregationKind.name(assoc.aggregationKind()) + " `" +
                                        assoc.name() + "': " + 
                                        ex.getMessage());
        }

        model.generalizationGraph().add(assoc);
        model.redefineAssociationsGraph().add(assoc);
        model.subsetAssociationsGraph().add(assoc);
        	
        return assoc;
    }
    
    
   /**
    * This function is responsible for generation of {@link MRedefine}, {@link MSubset} and {@link MConform}
    * for each association end that belongs to this association.
    * @param ctx - Context object.
    */
    public void genChanges(Context ctx) {
    	Iterator it = fAssociationEnds.iterator();
    	ASTAssociationEnd end = null;
    	boolean subsets = false;
    	while(it.hasNext()) {
    		end = (ASTAssociationEnd)it.next();
    		if (end.getRedefine() != null) {
    			end.getRedefine().genRedefenitions(ctx);
    		}
    		if (end.getSubset() != null) {
    			end.getSubset().genSubsets(ctx);
    			subsets = true;
    		}
    		if (end.getConform() != null) {
    			end.getConform().genConforms(ctx);
    		}
    	}
    	//assert that all associationEnds subset ends of all associatio
    	//FIXME: If there is an asspociation end without subsets then add a subset constraint to it instead of throwing an exeption
    
    	if (subsets) {
    		assertAllEndsSubset(ctx);
    	}
    	
    	//update undefined fields for redefine
    	it = fAssociationEnds.iterator();
    	while (it.hasNext()) {
    		end = (ASTAssociationEnd)it.next();
    		end.updateUndefinedMultiplicity(ctx);
    	}
    }

   
	private void assertAllEndsSubset(Context ctx) {
		MAssociation assoc = ctx.model().getAssociation(fName.getText());
		List<MAssociationEnd> ends = assoc.associationEnds();
		for (MAssociationEnd end1 : ends) {
			for (MAssociationEnd end2 : ends) {
				if (!end1.subset().getSubsettedAssociations().containsAll(end2.subset().getSubsettedAssociations())) {
					ctx.reportError(fName, "There is an inconsistensy in association '" 
							+ fName.getText() 
							+ "' between associationEnds '" 
							+ end1.name()
							+"' and '"
							+ end2.name()
							+"' subset should be defined on both ends.");
				}
			}	
		}
	}

	public String toString() {
        return "FIXME";
    }
   

	public void genAssociationsHierarchy(Context ctx) {
		MModel model = ctx.model();
		MAssociation parentAssoc;
		
		MAssociation thisAssociation = model.getAssociation(fName.getText());
		
		//this association was already created, if it wasn't then error flags were raised
		//and there is no point moving on
		if (thisAssociation == null) return;
		
		for (MyToken parentAssocTok : fparentAssociations) {
			if ((parentAssoc = model.getAssociation(parentAssocTok.getText())) == null) {
				ctx.reportError(parentAssocTok, "Assocation named '" + parentAssocTok.getText() + "' " +
						"doesn't exist.");
				continue;
			}
			
			if (null == Matcher.findMatchByHierarchy
					(parentAssoc.associationEnds(), thisAssociation.associationEnds())) {
				ctx.reportError(parentAssocTok, "Association '"+  parentAssoc.name()
						+ "' can't generalize association '" 
						+ thisAssociation.name() 
						+ "' because association ends can't be matched.");
				continue;
			}
			
			MGeneralization gen = 
				ctx.modelFactory().createGeneralization(thisAssociation, parentAssoc);
			try {
				ctx.model().addGeneralization(gen);
			} catch (MInvalidModelException e) {
				ctx.reportError(fName, e);
			}
		}
	}

	public void addImplicitConforms(Context ctx) {
		MModel model = ctx.model();
		MAssociation assoc = model.getAssociation(fName.getText());
		DirectedGraph graph = model.generalizationGraph();
		
		
	}


}
