/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: ASTMultiplicity.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.parser.use;

import org.tzi.use.parser.AST;
import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.uml.mm.MMultiplicity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Node of the abstract syntax tree constructed by the parser.
 *
 * @version     $ProjectVersion: 0.393 $
 * @author  Mark Richters
 */
public class ASTMultiplicity extends AST {
    private MyToken fStartToken; // for error position
    private List fRanges;   // (ASTMultiplicityRange)

    public ASTMultiplicity(MyToken t) {
        fStartToken = t;
        fRanges = new ArrayList();
    }

    public void addRange(ASTMultiplicityRange mr) {
        fRanges.add(mr);
    }

    public MMultiplicity gen(Context ctx) {
        MMultiplicity mult = ctx.modelFactory().createMultiplicity();
        
        
        try {
        	//vitalyb: "if" added by me
        	if (fRanges.isEmpty()) {
        		addRange(new ASTMultiplicityRange(MMultiplicity.UNDEFINED));
        	}
      
            Iterator it = fRanges.iterator();
            while (it.hasNext() ) {
                ASTMultiplicityRange mr = (ASTMultiplicityRange) it.next();
                mult.addRange(mr.fLow, mr.fHigh);
            }
        } catch (IllegalArgumentException ex) {
            ctx.reportError(fStartToken, ex);
        }
        return mult;
    }

    public String toString() {
        return "FIXME";
    }
}
