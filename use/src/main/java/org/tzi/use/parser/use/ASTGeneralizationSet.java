package org.tzi.use.parser.use;

import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.List;
/**
 * Class for representing the constrained generalization set in UML class diagram.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 17/10/2008
 */
public class ASTGeneralizationSet {

	private String fGSName;
	private MyToken fSuperClass;
	private String fGSType;
	private List<MyToken> fSubClasses;
	private static int defaultNameGenerator = 0;
	
	public ASTGeneralizationSet(){
		fGSName = "DefaultName";
		fSubClasses = new ArrayList<MyToken>();
		fGSType = "overlapping";
	}

	public String getGSName(){
		return fGSName;
	}
	
	public String getSuperClassName(){
		return fSuperClass.getText();
	}
	
	public List<MyToken> getSubClasses(){
		return fSubClasses;
	}
	
	public void setSuperClassName(MyToken name){
		fSuperClass = name;
	}
	
	public void setGSName(MyToken name){
		fGSName = name.getText();		
	}
	
	public void setGSType(String gsType){
		//setType receives only valid strings from the parser - type assertion not necessary 
		fGSType = gsType;
	}
	
	public String getGsType(){
		return fGSType;
	}
	
	public void addClasses(List<MyToken> classesList){
		fSubClasses = classesList;
	}
	
	public void gen(Context ctx) {
		fGSName = (fGSName.equals("DefaultName")) ? 
				fGSName + ASTGeneralizationSet.defaultNameGenerator++ : fGSName;

		MModel model = ctx.model();
		MClass cls;
		MClass SuperClass = model.getClass(fSuperClass.getText());
		if (null == SuperClass) {
			ctx.reportError(fSuperClass, "Class '" + fSuperClass.getText() + "' dosesn't exist " +
					"and can't be a superClass in GS." );
			return;
		}
		//create GS - add name type and super class
		MGeneralizationSet gs = new MGeneralizationSet(fGSName, fGSType, SuperClass);
		
		//add sub classes to GS
		for(MyToken t: fSubClasses) {
			try {
				cls = findClass(t, model);
				assertInheritance(cls, t);
				gs.addSubClass(cls);
				
			} catch (SemanticException e) {
				ctx.reportError(e);
			}
		}
		//add GS to model
		model.addGeneralizationSet(gs);
	}


	private void assertInheritance(MClass cls, MyToken t) throws  SemanticException {
		//if parent not found throw exception
		for (Object o : cls.allParents()) {
			MClass c = (MClass)o;
			
			if (0 == c.name().compareToIgnoreCase(fSuperClass.getText())) {
				//c is a child of fSuperClassName - inheritance exists
				return;
			}
		}
		throw new SemanticException(t, "Class '" + cls.name() + "' has no parents and can't be part of GS");
		
	}

	private MClass findClass(MyToken className, MModel model) throws SemanticException {
		MClass cls;
		if (null == (cls = model.getClass(className.getText()))) {
			throw new SemanticException(className, "Can't find class named '" + className.getText() +"'.");
		}
		
		return cls;
	}
}