package org.tzi.use.parser.use;


import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MInvalidModelException;

import java.util.Set;


/**
 * Class that holds tokens from ANTLR that describe the redefined set.
 * Also responsible for creating redefine sets in the model.
 * @author Vitaly Bichov <br>
 * Created on 21/09/10
 */
public class ASTRedefine extends ASTModify {
	
	/** Set of  ASTModifyUnit objects, each of which contains pairs of className.roleName that 
	 * this association redefines ("parents") */
    private Set<ASTModifyUnit> fredefinedSet;

    
    ASTRedefine(Set<ASTModifyUnit> redefinedSet, MyToken assocEndName) {
		super(assocEndName);
		fredefinedSet = redefinedSet;
	}
	
	
	/**
	 * Getter for {@link #fredefinedSet} 
	 * @return Set(ASTModifyUnit)
	 */
	public Set<ASTModifyUnit> getRedefinedSet() {
		return fredefinedSet;
	}
	
	/**
	 * Iterate over redefined set, find the redefined associationEnds, check that
	 * this associationEnd can redefine them.
	 * @param ctx - Context object
	 */
	public void genRedefenitions(Context ctx) {		
		MAssociationEnd redefinedAssEnd = null; 
		
		for  (ASTModifyUnit redef : fredefinedSet) {
			try {
				
				redefinedAssEnd = getModifiedAssociationEnd(redef, ctx);
			
				fMassocEnd.redefine().validateRedefenition(redefinedAssEnd);
			} catch (MInvalidModelException inv) {
				ctx.reportError(fassocEndName ,inv);
			} catch (SemanticException e) {
				ctx.reportError(e);
				continue;
			}
		
			fMassocEnd.redefine().addRedefinedAssocEnd(redefinedAssEnd);
		}
	}	
}
