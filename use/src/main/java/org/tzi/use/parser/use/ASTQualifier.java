package org.tzi.use.parser.use;

import java.util.ArrayList;
import java.util.List;
/**
 * Created on 10/10/2008
 * @author Victor Makarenkov
 * Extension of the USE UML model to handle qualified associations.
 * Qualifier constraint is built as a property of the association end  
 */
public class ASTQualifier {

	protected List<ASTAttribute> fAttributes;   // (ASTAttribute)
	
	public ASTQualifier(){
		
		fAttributes = new ArrayList<ASTAttribute>();
	}
	
	  public void addAttribute(ASTAttribute a) {
	        fAttributes.add(a);
	    }
	  
	  public List<ASTAttribute> getQualifierAttributes(){
		  
		  return fAttributes;
	  }
	  
}
