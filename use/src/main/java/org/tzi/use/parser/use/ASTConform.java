package org.tzi.use.parser.use;

import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MInvalidModelException;

import java.util.Set;

/**
 * Class that holds tokens from ANTLR that describe the conform set.
 * Also responsible for creating conform sets in the model.
 * This class is deprecated use subsetTrunsformator instead.
 * @author Vitaly Bichov <br>
 * Created on 21/09/10
 */
@Deprecated
public class ASTConform  extends ASTModify {
	
	/** Set of ASTModifyUnit objects, each of which contains pairs of associationClass.roleName that 
	 * this association conforms to. ("parents") */
	private final Set<ASTModifyUnit> fConformedSet;
	@Deprecated
	ASTConform(Set<ASTModifyUnit> conformSet, MyToken assocEndName) {
		super(assocEndName);
		fConformedSet = conformSet;
	}
		
	/**
	 * Getter for {@link #fConformSet}
	 * @return Set(ASTModifyUnit)
	 */
	@Deprecated
	public Set<ASTModifyUnit> getConformedSet() {
		return fConformedSet;
	}

	/**
	 * Iterate over conformed set, find the conformed associationEnds, check that
	 * this associationEnd can conform to them and update the model.
	 * @param ctx - Context object
	 */
	@Deprecated
	public void genConforms(Context ctx) {
		MAssociationEnd conformedAssEnd = null;
		
		
		for (ASTModifyUnit redef : fConformedSet ) {
			try {
				//find association end that this association end conforms to
				conformedAssEnd = getModifiedAssociationEnd(redef, ctx);
				
				fMassocEnd.conform().validateConform(conformedAssEnd);
			} catch (MInvalidModelException inv) {
				ctx.reportError(fassocEndName ,inv);
			} catch (SemanticException e) {
				ctx.reportError(e);
				continue;
			}
			fMassocEnd.conform().addConformedAssocEnd(conformedAssEnd);
		}
	}
}
