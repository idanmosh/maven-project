header { 
/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  
 */

package org.tzi.use.parser.use; 
}
{
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.ParseErrorHandler;
import java.util.Set;
import java.util.HashSet;


import org.tzi.use.parser.ocl.*;
}class GUSEParser extends Parser;

options {
	exportVocab= GUSE;
	defaultErrorHandler= true;
	buildAST= false;
	k= 5;
	importVocab=GOCL;
}

{  
    final static String Q_COLLECT  = "collect";
    final static String Q_SELECT   = "select";
    final static String Q_REJECT   = "reject";
    final static String Q_FORALL   = "forAll";
    final static String Q_EXISTS   = "exists";
    final static String Q_ISUNIQUE = "isUnique";
    final static String Q_SORTEDBY = "sortedBy";
    final static String Q_ANY      = "any";
    final static String Q_ONE      = "one";

    final static int Q_COLLECT_ID  = 1;
    final static int Q_SELECT_ID   = 2;
    final static int Q_REJECT_ID   = 3;
    final static int Q_FORALL_ID   = 4;
    final static int Q_EXISTS_ID   = 5;
    final static int Q_ISUNIQUE_ID = 6;
    final static int Q_SORTEDBY_ID = 7;
    final static int Q_ANY_ID      = 8;
    final static int Q_ONE_ID      = 9;

    final static HashMap queryIdentMap = new HashMap();

    static {
        queryIdentMap.put(Q_COLLECT,  new Integer(Q_COLLECT_ID));
        queryIdentMap.put(Q_SELECT,   new Integer(Q_SELECT_ID));
        queryIdentMap.put(Q_REJECT,   new Integer(Q_REJECT_ID));
        queryIdentMap.put(Q_FORALL,   new Integer(Q_FORALL_ID));
        queryIdentMap.put(Q_EXISTS,   new Integer(Q_EXISTS_ID));
        queryIdentMap.put(Q_ISUNIQUE, new Integer(Q_ISUNIQUE_ID));
        queryIdentMap.put(Q_SORTEDBY, new Integer(Q_SORTEDBY_ID));
        queryIdentMap.put(Q_ANY,      new Integer(Q_ANY_ID));
        queryIdentMap.put(Q_ONE,      new Integer(Q_ONE_ID));
    }

    protected boolean isQueryIdent(Token t) {
        return queryIdentMap.containsKey(t.getText());
    }
    
    private int fNest = 0;
    
    public void traceIn(String rname) throws TokenStreamException {
        for (int i = 0; i < fNest; i++)
            System.out.print(" ");
        super.traceIn(rname);
        fNest++;
    }

    public void traceOut(String rname) throws TokenStreamException {
        fNest--;
        for (int i = 0; i < fNest; i++)
            System.out.print(" ");
        super.traceOut(rname);
    }
    
    public void init(ParseErrorHandler handler) {
        fParseErrorHandler = handler;
    }

    /* Overridden methods. */
	private ParseErrorHandler fParseErrorHandler;
    
    public void reportError(RecognitionException ex) {
        fParseErrorHandler.reportError(
	        ex.getLine() + ":" +ex.getColumn() + ": " + ex.getMessage());
    }
}
model returns [ASTModel n]{ 
    ASTEnumTypeDefinition e = null;
    ASTAssociation a = null;
    ASTConstraintDefinition cons = null;
    ASTPrePost ppc = null;
    ASTGeneralizationSet gs = null;
    n = null;
}
:"model" name:IDENT { n = new ASTModel((MyToken) name); }
    ( e=enumTypeDefinition { n.addEnumTypeDef(e); } )*
    (   ( generalClassDefinition[n] )
      | ( gs=generalizationSetDefinition { n.addGeneralizationSet(gs); } )
      | ( a=associationDefinition { n.addAssociation(a); } )
      | ( "constraints"
          (   cons=invariant { n.addConstraint(cons); } 
            | ppc=prePost { n.addPrePost(ppc); } 
          )*  
        )
      | (xorDefenition[n])
    )*
    EOF
    ;

generalizationSetDefinition returns [ASTGeneralizationSet n]{ List idList; n = new ASTGeneralizationSet();}
:"gs" 
("GSname" gsName:IDENT { n.setGSName((MyToken)gsName); })?
(
   ("type" "overlapping" { n.setGSType("overlapping"); })
  |("type" "complete" { n.setGSType("complete"); })
  |("type" "incomplete" { n.setGSType("incomplete"); })
  |("type" "disjoint" { n.setGSType("disjoint"); })
  |("type" "disjoint_complete" { n.setGSType("disjoint_complete"); })
  |("type" "disjoint_incomplete" { n.setGSType("disjoint_incomplete"); })
  |("type" "overlapping_complete" { n.setGSType("overlapping_complete"); })
  |("type" "overlapping_incomplete" { n.setGSType("overlapping_incomplete"); })
   )?
("super" sname:IDENT { n.setSuperClassName((MyToken)sname); })
("subClasses" idList=idList { n.addClasses(idList); } )
;

enumTypeDefinition returns [ASTEnumTypeDefinition n]{ List idList; n = null; }
:"enum" name:IDENT LBRACE idList=idList RBRACE ( SEMI )?
        { n = new ASTEnumTypeDefinition((MyToken) name, idList); }
    ;

generalClassDefinition[ASTModel n] { boolean isAbstract = false;
  ASTClass c = null;
  ASTAssociationClass ac = null;}
:( "abstract" { isAbstract = true; } )? 
    ( ( c=classDefinition[isAbstract] { n.addClass(c); } ) | 
      ( ac=associationClassDefinition[isAbstract] { n.addAssociationClass(ac); } ) )
       
;

classDefinition[boolean isAbstract] returns [ASTClass n]{ List idList; n = null; }
:"class" name:IDENT { n = new ASTClass((MyToken) name, isAbstract); }
    ( LESS idList=idList { n.addSuperClasses(idList); } )?
    ( "attributes" 
      { ASTAttribute a; }
      ( a=attributeDefinition { n.addAttribute(a); } )* 
    )?
    ( "operations" 
      { ASTOperation op; }
      ( op=operationDefinition { n.addOperation(op); } )* 
    )?
    ( "constraints"
      ( { ASTInvariantClause inv; } 
        inv=invariantClause { n.addInvariantClause(inv); }
      )*
    )?
    "end"
    ;

associationClassDefinition[boolean isAbstract] returns [ASTAssociationClass n]{List idList; n = null; ASTAssociationEnd ae; }
:{ MyToken t1 = (MyToken) LT(1); } ("associationClass"|"associationclass") 
        { 
            if (t1.getText().equals("associationClass")) {
               reportWarning("the 'associationClass' keyword is deprecated and will " +
                             "not be supported in the future, use 'associationclass' instead");
            }  
        }
    name:IDENT { n = new ASTAssociationClass((MyToken) name, isAbstract); }
    ( LESS idList=idList { n.addSuperClasses(idList); } )?
    "between"
    ae=associationEnd { n.addEnd(ae); }
    ( ae=associationEnd { n.addEnd(ae); } )+
    ( "attributes" 
      { ASTAttribute a; }
      ( a=attributeDefinition { n.addAttribute(a); } )* 
    )?
    ( "operations" 
      { ASTOperation op; }
      ( op=operationDefinition { n.addOperation(op); } )* 
    )?
    ( "constraints"
      ( { ASTInvariantClause inv; } 
        inv=invariantClause { n.addInvariantClause(inv); }
      )*
    )?
    ( { MyToken t = (MyToken) LT(1); }
      ( "aggregation" | "composition" )
      { n.setKind(t); }
    )?
    "end"
    ;

attributeDefinition returns [ASTAttribute n]{ ASTType t; n = null; }
:name:IDENT COLON t=type ( SEMI )?
        { n = new ASTAttribute((MyToken) name, t); }
    ;

operationDefinition returns [ASTOperation n]{ List pl; ASTType t = null; ASTExpression e = null; 
  ASTPrePostClause ppc = null; n = null; 
  ASTALActionList al = null;
}
:name:IDENT
    pl=paramList
    ( COLON t=type )?
    (EQUAL e=expression )? 
  ("begin" al=alActionList "end" )?
    { n = new ASTOperation((MyToken) name, pl, t, e,al); }
    ( ppc=prePostClause { n.addPrePostClause(ppc); } )*
    ( SEMI )?
    ;

associationDefinition returns [ASTAssociation n]{List idList; ASTAssociationEnd ae; n = null; }
:{ MyToken t = (MyToken) LT(1); }
    ( "association" | "aggregation" | "composition" )
    //    ( classDefinition | (name:IDENT { n = new ASTAssociation(t, (MyToken) name); }) )
    name:IDENT { n = new ASTAssociation(t, (MyToken) name); }
    ( LESS idList=idList { n.addParentAssociations(idList); } )?
    "between"
    ae=associationEnd { n.addEnd(ae); }
    ( ae=associationEnd { n.addEnd(ae); } )+
    "end"
    ;

associationEnd returns [ASTAssociationEnd n]{ ASTMultiplicity m; n = null; ASTQualifier q; Set redef = null, subset = null, conform = null;}
:name:IDENT LBRACK m=multiplicity RBRACK 
    { n = new ASTAssociationEnd((MyToken) name, m); } 
    ( "role" rn:IDENT { n.setRolename((MyToken) rn); } )?
    ( "ordered" { n.setOrdered(); } )?
    ( "qualifier" q=qualifier { n.setQualifier(q); } )?
    (
     LBRACE
      ( 
           ("redefines" redef=modifySet {n.setRedefine(new ASTRedefine(redef, (MyToken)name));} ) 
        |  ("subsets" subset=modifySet {n.setSubset(new ASTSubset(subset, (MyToken)name));} )
        |  ("conforms" conform=modifySet {n.setConform(new ASTConform(conform, (MyToken)name));} ) 
      )
     RBRACE
    )*
    ( SEMI )?
    ;

protected modifySet returns [Set s]{ s = new HashSet();}
://      (roleName:IDENT (COMMA)? {s.add(roleName);})+
    (className:IDENT DOT propName:IDENT (COMMA)? {s.add(new ASTModifyUnit((MyToken)className, (MyToken)propName)); } )+

    ;

qualifier returns [ASTQualifier n]{n = new ASTQualifier();}
:( "attributes" 
      { ASTAttribute a; }
      ( a=attributeDefinition { n.addAttribute(a); } )+ 
    )?
    ;

multiplicity returns [ASTMultiplicity n]{ ASTMultiplicityRange mr; n = null; }
:{ 
  MyToken t = (MyToken) LT(1); // remember start position of expression
  n = new ASTMultiplicity(t); 
    }
    (mr=multiplicityRange { n.addRange(mr); })?
    ( COMMA mr=multiplicityRange  { n.addRange(mr); } )*
    ;

multiplicityRange returns [ASTMultiplicityRange n]{ int ms1, ms2; n = null; }
:ms1=multiplicitySpec { n = new ASTMultiplicityRange(ms1); }
    ( DOTDOT ms2=multiplicitySpec { n.setHigh(ms2); } )?
    ;

multiplicitySpec returns [int m]{ m = -1; }
:i:INT { m = Integer.parseInt(i.getText()); }
    | STAR  { m = -1; }
    ;

invariant returns [ASTConstraintDefinition n]{ n = null; ASTType t = null; ASTInvariantClause inv = null; }
:{ n = new ASTConstraintDefinition(); }
    "context"
    ( v:IDENT COLON { n.setVarName((MyToken) v); } )? // requires k = 2

    t=simpleType { n.setType(t); }
    ( inv=invariantClause { n.addInvariantClause(inv); } )* //+
    ;

invariantClause returns [ASTInvariantClause n]{ ASTExpression e; n = null; }
:"inv" ( name:IDENT )? COLON e=expression
    { n = new ASTInvariantClause((MyToken) name, e); }
    ;

prePost returns [ASTPrePost n]{ n = null; List pl = null; ASTType rt = null; ASTPrePostClause ppc = null; }
:"context" classname:IDENT COLON_COLON opname:IDENT pl=paramList ( COLON rt=type )?
    { n = new ASTPrePost((MyToken) classname, (MyToken) opname, pl, rt); }
    ( ppc=prePostClause { n.addPrePostClause(ppc); } )+
    ;

prePostClause returns [ASTPrePostClause n]{ ASTExpression e; n = null; }
:{ MyToken t = (MyToken) LT(1); }
    ( "pre" | "post" )  ( name:IDENT )? COLON e=expression
    { n = new ASTPrePostClause(t, (MyToken) name, e); }
    ;

alActionList returns [ASTALActionList al]{
  al = null;
  ASTALAction action = null;
  al = new ASTALActionList();
}
:( action=alAction {al.add(action);})*
;

alAction returns [ASTALAction action]{
  action = null;
}
:action = alCreateVar
| action = alDelete
| action = alSet
| action = alSetCreate
|   action = alInsert
| action = alDestroy
| action = alIf
| action = alWhile
|   action = alFor
|   action = alExec
;

alCreateVar returns [ASTALCreateVar var]{
  var = null;
  ASTType type = null;
}
:("var"|"declare") name:IDENT COLON type=type 
  { var = new ASTALCreateVar(name,type); }
;

alSet returns [ASTALSet set]{ 
    set = null;
    ASTExpression lval = null;
    ASTExpression rval = null;
}
:"set" lval=expression COLON_EQUAL rval=expression
    { set = new ASTALSet(lval, rval); }
;

alSetCreate returns [ASTALSetCreate setcreate]{
    setcreate = null;
    ASTExpression lval = null;
    ASTExpression nameExpr = null;
}
:"create" lval=expression COLON_EQUAL { LT(1).getText().equals("new") }? new_:IDENT cls:IDENT 
    ( "namehint" nameExpr=expression )?
    { setcreate = new ASTALSetCreate(lval, (MyToken)cls, nameExpr);}
;

alInsert returns [ASTALInsert insert]{ ASTExpression e; List exprList = new ArrayList(); insert = null; }
:"insert" LPAREN 
    e=expression { exprList.add(e); } COMMA
    e=expression { exprList.add(e); } ( COMMA e=expression { exprList.add(e); } )* 
    RPAREN "into" id:IDENT
    { insert = new ASTALInsert(exprList, (MyToken) id); }
    ;

alDelete returns [ASTALDelete n]{ ASTExpression e; List exprList = new ArrayList(); n = null; }
:"delete" LPAREN
    e=expression { exprList.add(e); } COMMA
    e=expression { exprList.add(e); } ( COMMA e=expression { exprList.add(e); } )*
    RPAREN "from" id:IDENT
    { n = new ASTALDelete(exprList, (MyToken) id); }
    ;

alDestroy returns [ASTALDestroy n]{ ASTExpression e = null;  n = null; }
:"destroy" e=expression
    { n = new ASTALDestroy(e); }
    ;

alIf returns [ASTALIf i]{
  i = null;
  ASTExpression ifexpr;
  ASTALActionList thenlist;
  ASTALActionList elselist=null;
}
:"if" ifexpr=expression 
  "then" thenlist=alActionList
  ("else" elselist=alActionList)?
  "endif" 
  { i = new ASTALIf(ifexpr,thenlist,elselist); }
;

alWhile returns [ASTALWhile w]{
  w = null;
  ASTExpression expr;
  ASTALActionList body;
}
:"while" expr=expression 
  "do" 
  body=alActionList
  "wend"
  { w = new ASTALWhile(expr,body); }
;

alFor returns [ASTALFor f]{
  f = null;
  ASTExpression expr;
  ASTALActionList body;
  ASTType type;
}
:"for" var:IDENT COLON type=type "in" expr=expression 
  "do" 
  body=alActionList
  { LT(1).getText().equals("next") }? next:IDENT
  { f = new ASTALFor(var,type,expr,body); }
;

alExec returns [ASTALExecute c]{ 
    ASTExpression op;
    c=null;
}
:"execute" op=expression
    { c = new ASTALExecute(op); }
;

xorDefenition[ASTModel n] {List lst;}
:"xor" className:IDENT COMMA lst=idList SEMI
{n.addXor(new ASTXor(className, lst));}
;

// inherited from grammar GOCLParser
paramList returns [List paramList]{ ASTVariableDeclaration v; paramList = new ArrayList(); }
:LPAREN
    ( 
      v=variableDeclaration { paramList.add(v); }
      ( COMMA v=variableDeclaration  { paramList.add(v); } )* 
    )?
    RPAREN
    ;

// inherited from grammar GOCLParser
idList returns [List idList]{ idList = new ArrayList(); }
:id0:IDENT { idList.add((MyToken) id0); }
    ( COMMA idn:IDENT { idList.add((MyToken) idn); } )*
    ;

// inherited from grammar GOCLParser
variableDeclaration returns [ASTVariableDeclaration n]{ ASTType t; n = null; }
:name:IDENT COLON t=type
    { n = new ASTVariableDeclaration((MyToken) name, t); }
    ;

// inherited from grammar GOCLParser
expressionOnly returns [ASTExpression n]{ n = null; }
:n=expression EOF
    ;

// inherited from grammar GOCLParser
expression returns [ASTExpression n]{ ASTLetExpression prevLet = null, firstLet = null; ASTType t = null; 
  ASTExpression e1, e2; n = null; 
}
:{ MyToken tok = (MyToken) LT(1); /* remember start of expression */ }
    ( 
      "let" name:IDENT ( COLON t=type )? EQUAL e1=expression "in"
       { ASTLetExpression nextLet = new ASTLetExpression((MyToken) name, t, e1);
         if ( firstLet == null ) 
             firstLet = nextLet;
         if ( prevLet != null ) 
             prevLet.setInExpr(nextLet);
         prevLet = nextLet;
       }
    )*

    n=conditionalImpliesExpression
    { if ( n != null ) 
         n.setStartToken(tok);
      if ( prevLet != null ) { 
         prevLet.setInExpr(n);
         n = firstLet;
         n.setStartToken(tok);
      }
    }
    ;

// inherited from grammar GOCLParser
conditionalImpliesExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=conditionalOrExpression
    ( op:"implies" n1=conditionalOrExpression 
        { n = new ASTBinaryExpression((MyToken) op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
conditionalOrExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=conditionalXOrExpression
    ( op:"or" n1=conditionalXOrExpression
        { n = new ASTBinaryExpression((MyToken) op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
conditionalXOrExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=conditionalAndExpression
    ( op:"xor" n1=conditionalAndExpression
        { n = new ASTBinaryExpression((MyToken) op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
conditionalAndExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=equalityExpression
    ( op:"and" n1=equalityExpression
        { n = new ASTBinaryExpression((MyToken) op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
equalityExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=relationalExpression 
    ( { MyToken op = (MyToken) LT(1); }
      (EQUAL | NOT_EQUAL) n1=relationalExpression
        { n = new ASTBinaryExpression(op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
relationalExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=additiveExpression 
    ( { MyToken op = (MyToken) LT(1); }
      (LESS | GREATER | LESS_EQUAL | GREATER_EQUAL) n1=additiveExpression 
        { n = new ASTBinaryExpression(op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
additiveExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=multiplicativeExpression 
    ( { MyToken op = (MyToken) LT(1); }
      (PLUS | MINUS) n1=multiplicativeExpression
        { n = new ASTBinaryExpression(op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
multiplicativeExpression returns [ASTExpression n]{ ASTExpression n1; n = null; }
:n=unaryExpression 
    ( { MyToken op = (MyToken) LT(1); }
      (STAR | SLASH | "div") n1=unaryExpression
        { n = new ASTBinaryExpression(op, n, n1); } 
    )*
    ;

// inherited from grammar GOCLParser
unaryExpression returns [ASTExpression n]{ n = null; }
:( { MyToken op = (MyToken) LT(1); }
        ("not" | MINUS | PLUS ) 
        n=unaryExpression { n = new ASTUnaryExpression((MyToken) op, n); }
      )
    | n=postfixExpression
    ;

// inherited from grammar GOCLParser
postfixExpression returns [ASTExpression n]{ boolean arrow; n = null; }
:n=primaryExpression 
    ( 
     ( ARROW { arrow = true; } | DOT { arrow = false; } ) 
     n=propertyCall[n, arrow] 
    )*
    ;

// inherited from grammar GOCLParser
primaryExpression returns [ASTExpression n]{ n = null; }
:n=literal
    | n=propertyCall[null, false]
    | LPAREN n=expression RPAREN
    | n=ifExpression
    // HACK: the following requires k=3
    | id1:IDENT DOT "allInstances" ( LPAREN RPAREN )?
      { n = new ASTAllInstancesExpression((MyToken) id1); }
      ( AT "pre" { n.setIsPre(); } ) ? 
    ;

// inherited from grammar GOCLParser
propertyCall[ASTExpression source, boolean followsArrow] returns [ASTExpression n]{ n = null; }
:// this semantic predicate disambiguates operations from
      // iterate-based expressions which have a different syntax (the
      // OCL grammar is very loose here).
      { isQueryIdent(LT(1)) }? 
      n=queryExpression[source]
    | n=iterateExpression[source]
    | n=operationExpression[source, followsArrow]
    | n=typeExpression[source, followsArrow]
    ;

// inherited from grammar GOCLParser
queryExpression[ASTExpression range] returns [ASTExpression n]{ 
    ASTElemVarsDeclaration decls = new ASTElemVarsDeclaration(); 
    n = null; 
}
:op:IDENT 
    LPAREN 
    ( decls=elemVarsDeclaration BAR )? // requires k=2 
    n=expression
    RPAREN
    { n = new ASTQueryExpression((MyToken) op, range, decls, n); }
    ;

// inherited from grammar GOCLParser
iterateExpression[ASTExpression range] returns [ASTExpression n]{ 
    ASTElemVarsDeclaration decls = null; 
    ASTVariableInitialization init = null; 
    n = null;
}
:i:"iterate"
    LPAREN
    decls=elemVarsDeclaration SEMI
    init=variableInitialization BAR
    n=expression
    RPAREN
    { n = new ASTIterateExpression((MyToken) i, range, decls, init, n); }
    ;

// inherited from grammar GOCLParser
operationExpression[ASTExpression source, boolean followsArrow] returns [ASTOperationExpression n]{ ASTExpression e; n = null; }
:name:IDENT 
    { n = new ASTOperationExpression((MyToken) name, source, followsArrow); }

    ( LBRACK rolename:IDENT RBRACK { n.setExplicitRolename((MyToken) rolename); })?

    ( AT "pre" { n.setIsPre(); } ) ? 
    (
      LPAREN { n.hasParentheses(); }
      ( 
	     e=expression { n.addArg(e); }
	     ( COMMA e=expression { n.addArg(e); } )* 
	  )?
      RPAREN
    )?
    ;

// inherited from grammar GOCLParser
typeExpression[ASTExpression source, boolean followsArrow] returns [ASTTypeArgExpression n]{ ASTType t = null; n = null; }
:{ MyToken opToken = (MyToken) LT(1); }
	( "oclAsType" | "oclIsKindOf" | "oclIsTypeOf" )
	LPAREN t=type RPAREN 
      { n = new ASTTypeArgExpression(opToken, source, t, followsArrow); }
    ;

// inherited from grammar GOCLParser
elemVarsDeclaration returns [ASTElemVarsDeclaration n]{ List idList; ASTType t = null; n = null; }
:idList=idList
    ( COLON t=type )?
    { n = new ASTElemVarsDeclaration(idList, t); }
    ;

// inherited from grammar GOCLParser
variableInitialization returns [ASTVariableInitialization n]{ ASTType t; ASTExpression e; n = null; }
:name:IDENT COLON t=type EQUAL e=expression
    { n = new ASTVariableInitialization((MyToken) name, t, e); }
    ;

// inherited from grammar GOCLParser
ifExpression returns [ASTExpression n]{ ASTExpression cond, t, e; n = null; }
:i:"if" cond=expression "then" t=expression "else" e=expression "endif"
        { n = new ASTIfExpression((MyToken) i, cond, t, e); } 
    ;

// inherited from grammar GOCLParser
literal returns [ASTExpression n]{ n = null; }
:t:"true"   { n = new ASTBooleanLiteral(true); }
    | f:"false"  { n = new ASTBooleanLiteral(false); }
    | i:INT    { n = new ASTIntegerLiteral((MyToken) i); }
    | r:REAL   { n = new ASTRealLiteral((MyToken) r); }
    | s:STRING { n = new ASTStringLiteral((MyToken) s); }
    | HASH enumLit:IDENT { n = new ASTEnumLiteral((MyToken) enumLit); }
    | n=collectionLiteral
    | n=emptyCollectionLiteral
    | n=undefinedLiteral
    | n=tupleLiteral
    ;

// inherited from grammar GOCLParser
collectionLiteral returns [ASTCollectionLiteral n]{ ASTCollectionItem ci; n = null; }
:{ MyToken op = (MyToken) LT(1); } 
    ( "Set" | "Sequence" | "Bag" ) 
    { n = new ASTCollectionLiteral(op); }
    LBRACE 
    ci=collectionItem { n.addItem(ci); } 
    ( COMMA ci=collectionItem { n.addItem(ci); } )* 
    RBRACE
    ;

// inherited from grammar GOCLParser
collectionItem returns [ASTCollectionItem n]{ ASTExpression e; n = new ASTCollectionItem(); }
:e=expression { n.setFirst(e); } 
    ( DOTDOT e=expression { n.setSecond(e); } )?
    ;

// inherited from grammar GOCLParser
emptyCollectionLiteral returns [ASTEmptyCollectionLiteral n]{ ASTType t = null; n = null; }
:"oclEmpty" LPAREN t=collectionType RPAREN
    { n = new ASTEmptyCollectionLiteral(t); }
    ;

// inherited from grammar GOCLParser
undefinedLiteral returns [ASTUndefinedLiteral n]{ ASTType t = null; n = null; }
:"oclUndefined" LPAREN t=type RPAREN
    { n = new ASTUndefinedLiteral(t); }
    ;

// inherited from grammar GOCLParser
tupleLiteral returns [ASTTupleLiteral n]{ ASTTupleItem ti; n = null; List tiList = new ArrayList(); }
:"Tuple"
    LBRACE
    ti=tupleItem { tiList.add(ti); } 
    ( COMMA ti=tupleItem { tiList.add(ti); } )*
    RBRACE
    { n = new ASTTupleLiteral(tiList); }
    ;

// inherited from grammar GOCLParser
tupleItem returns [ASTTupleItem n]{ ASTExpression e; n = null; }
:name:IDENT (COLON|EQUAL) e=expression 
    { n = new ASTTupleItem((MyToken) name, e); } 
    ;

// inherited from grammar GOCLParser
type returns [ASTType n]{ n = null; }
:{ MyToken tok = (MyToken) LT(1); /* remember start of type */ }
    (
      n=simpleType
    | n=collectionType
    | n=tupleType
    )
    { n.setStartToken(tok); }
    ;

// inherited from grammar GOCLParser
typeOnly returns [ASTType n]{ n = null; }
:n=type EOF
    ;

// inherited from grammar GOCLParser
simpleType returns [ASTSimpleType n]{ n = null; }
:name:IDENT { n = new ASTSimpleType((MyToken) name); }
    ;

// inherited from grammar GOCLParser
collectionType returns [ASTCollectionType n]{ ASTType elemType = null; n = null; }
:{ MyToken op = (MyToken) LT(1); } 
    ( "Collection" | "Set" | "Sequence" | "Bag" ) 
    LPAREN elemType=type RPAREN
    { n = new ASTCollectionType(op, elemType); n.setStartToken(op);}
    ;

// inherited from grammar GOCLParser
tupleType returns [ASTTupleType n]{ ASTTuplePart tp; n = null; List tpList = new ArrayList(); }
:"Tuple" LPAREN 
    tp=tuplePart { tpList.add(tp); } 
    ( COMMA tp=tuplePart { tpList.add(tp); } )* 
    RPAREN
    { n = new ASTTupleType(tpList); }
    ;

// inherited from grammar GOCLParser
tuplePart returns [ASTTuplePart n]{ ASTType t; n = null; }
:name:IDENT COLON t=type
    { n = new ASTTuplePart((MyToken) name, t); }
    ;

{
  import java.io.PrintWriter;
  import org.tzi.use.util.Log;
  import org.tzi.use.util.StringUtil;
  import org.tzi.use.parser.ParseErrorHandler;
  import org.tzi.use.parser.MyToken;
}class GUSELexer extends Lexer;

options {
	importVocab=GUSE;
	defaultErrorHandler= true;
	testLiterals= false;
	k= 2;
}

{
  protected int fTokColumn = 1;
    private PrintWriter fErr;
    private ParseErrorHandler fParseErrorHandler;
  
      public void consume() throws CharStreamException {
        if (inputState.guessing == 0 ) {
            if (text.length() == 0 ) {
                // remember token start column
                fTokColumn = getColumn();
            }
        }
        super.consume();
    }

    public String getFilename() {
        return fParseErrorHandler.getFileName();
    }
    
    protected Token makeToken(int t) {
        MyToken token = 
            new MyToken(getFilename(), getLine(), fTokColumn);
        token.setType(t);
        if (t == EOF )
            token.setText("end of file or input");
        return token;
    }

    public void reportError(RecognitionException ex) {
        fParseErrorHandler.reportError(
                ex.getLine() + ":" + ex.getColumn() + ": " + ex.getMessage());
    }
 

    /**
     * Returns true if word is a reserved keyword. 
     */
    public boolean isKeyword(String word) {
        ANTLRHashString s = new ANTLRHashString(word, this);
        boolean res = literals.get(s) != null;
        Log.trace(this, "keyword " + word + ": " + res);
        return res;
    }

    public void traceIn(String rname) throws CharStreamException {
        traceIndent();
        traceDepth += 1;
        System.out.println("> lexer " + rname + ": c == '" + 
                           StringUtil.escapeChar(LA(1), '\'') + "'");
    }

    public void traceOut(String rname) throws CharStreamException {
        traceDepth -= 1;
        traceIndent();
        System.out.println("< lexer " + rname + ": c == '" +
                           StringUtil.escapeChar(LA(1), '\'') + "'");
    }
    
    public void init(ParseErrorHandler handler) {
        fParseErrorHandler = handler;
    }
}
WS :( ' '
    | '\t'
    | '\f'
    | (   "\r\n"
    | '\r'
  | '\n'
      )
      { newline(); }
    )
    { $setType(Token.SKIP); }
    ;

SL_COMMENT :("//" | "--")
    (~('\n'|'\r'))* ('\n'|'\r'('\n')?)
    { $setType(Token.SKIP); newline(); }
    ;

ML_COMMENT :"/*"
    ( options { generateAmbigWarnings = false; } : { LA(2)!='/' }? '*'
    | '\r' '\n' { newline(); }
    | '\r'  { newline(); }
    | '\n'  { newline(); }
    | ~('*'|'\n'|'\r')
    )*
    "*/"
    { $setType(Token.SKIP); }
    ;

ARROW 
options {
	paraphrase= "'->'";
}
:"->";

AT 
options {
	paraphrase= "'@'";
}
:'@';

BAR 
options {
	paraphrase= "'|'";
}
:'|';

COLON 
options {
	paraphrase= "':'";
}
:':';

COLON_COLON 
options {
	paraphrase= "'::'";
}
:"::";

COLON_EQUAL 
options {
	paraphrase= "':='";
}
:":=";

COMMA 
options {
	paraphrase= "','";
}
:',';

DOT 
options {
	paraphrase= "'.'";
}
:'.';

DOTDOT 
options {
	paraphrase= "'..'";
}
:"..";

EQUAL 
options {
	paraphrase= "'='";
}
:'=';

GREATER 
options {
	paraphrase= "'>'";
}
:'>';

GREATER_EQUAL 
options {
	paraphrase= "'>='";
}
:">=";

HASH 
options {
	paraphrase= "'#'";
}
:'#';

LBRACE 
options {
	paraphrase= "'{'";
}
:'{';

LBRACK 
options {
	paraphrase= "'['";
}
:'[';

LESS 
options {
	paraphrase= "'<'";
}
:'<';

LESS_EQUAL 
options {
	paraphrase= "'<='";
}
:"<=";

LPAREN 
options {
	paraphrase= "'('";
}
:'(';

MINUS 
options {
	paraphrase= "'-'";
}
:'-';

NOT_EQUAL 
options {
	paraphrase= "'<>'";
}
:"<>";

PLUS 
options {
	paraphrase= "'+'";
}
:'+';

RBRACE 
options {
	paraphrase= "'}'";
}
:'}';

RBRACK 
options {
	paraphrase= "']'";
}
:']';

RPAREN 
options {
	paraphrase= "')'";
}
:')';

SEMI 
options {
	paraphrase= "';'";
}
:';';

SLASH 
options {
	paraphrase= "'/'";
}
:'/';

STAR 
options {
	paraphrase= "'*'";
}
:'*';

protected INT :('0'..'9')+
    ;

protected REAL :INT ( '.' INT )? 
    ( ('e'|'E') ('+'|'-')? INT )?
    ;

RANGE_OR_INT :( INT ".." )    => INT    { $setType(INT); }
    | ( INT '.' INT)  => REAL   { $setType(REAL); }
    | ( INT ('e'|'E'))  => REAL { $setType(REAL); }
    |   INT                     { $setType(INT); }
    ;

STRING { char c1; StringBuffer s = new StringBuffer(); }
:'\'' { s.append('\''); } 
    (   c1=ESC { s.append(c1); } 
      | 
        c2:~('\''|'\\') { s.append(c2); } 
    )* 
    '\'' { s.append('\''); } 
    { $setText(s.toString()); }
    ;

protected ESC returns [char c]{ c = 0; int h0,h1,h2,h3; }
:'\\'
     (  'n' { c = '\n'; }
     | 'r' { c = '\r'; }
     | 't' { c = '\t'; }
     | 'b' { c = '\b'; }
     | 'f' { c = '\f'; }
     | '"' { c = '"'; }
     | '\'' { c = '\''; }
     | '\\' { c = '\\'; }
     | ('u')+ 
       h3=HEX_DIGIT h2=HEX_DIGIT h1=HEX_DIGIT h0=HEX_DIGIT 
   { c = (char) (h0 + h1 * 16 + h2 * 16 * 16 + h3 * 16 * 16 * 16); }
     | o1:'0'..'3' { c = (char) Character.digit(o1, 8); } 
        ( options { warnWhenFollowAmbig = false; } : o2:'0'..'7' 
          { c = (char) (c * 8 + Character.digit(o2, 8)); } 
          ( options { warnWhenFollowAmbig = false; } : o3:'0'..'7' 
            { c = (char) (c * 8 + Character.digit(o3, 8)); } 
          )? 
        )?
     | d1:'4'..'7' { c = (char) Character.digit(d1, 8); } 
       ( options { warnWhenFollowAmbig = false; } : d2:'0'..'7' 
         { c = (char) (c * 8 + Character.digit(d2, 8)); } 
       )?
     )
     ;

protected HEX_DIGIT returns [int n]{ n = 0; }
:(   c1:'0'..'9' { n = Character.digit(c1, 16); }
      | c2:'A'..'F' { n = Character.digit(c2, 16); }
      | c3:'a'..'f' { n = Character.digit(c3, 16); }
    )
    ;

IDENT 
options {
	testLiterals= true;
	paraphrase= "an identifier";
}
:('$' |'a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*
    ;

protected VOCAB :'\3'..'\377'
    ;


