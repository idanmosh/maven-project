package org.tzi.use.parser.use;


import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MInvalidModelException;

import java.util.Set;



/**
 * Class that holds tokens from ANTLR that describe the subset set.
 * Also responsible for creating subset sets in the model.
 * @author Vitaly Bichov <br>
 * Created on 21/09/10
 */
public class ASTSubset extends ASTModify {
	
	/** Set of  ASTModifyUnit objects, each of which contains pairs of className.roleName that 
	 * this association subsets ("parents") */
    private final Set<ASTModifyUnit> fsubsettingSet;

    
    ASTSubset(Set<ASTModifyUnit> subsettingSet, MyToken assocEndName) {
		super(assocEndName);
		fsubsettingSet = subsettingSet;
	}


	/**
	 * Getter for {@link #fsubsettingSet} 
	 * @return Set(ASTModifyUnit)
	 */
	public Set<ASTModifyUnit> getSubsettingSet() {
		return fsubsettingSet;
	}
	
	/**
	 * Iterate over subseted set, find the subseted associationEnds, check that
	 * this associationEnd can subset them.
	 * @param ctx - Context object
	 */
	public void genSubsets(Context ctx) {
		MAssociationEnd subsettedAssEnd = null; 
		
		for (ASTModifyUnit subset : fsubsettingSet) {
			try { 			

				subsettedAssEnd = getModifiedAssociationEnd(subset, ctx);

				fMassocEnd.subset().validateSubset(subsettedAssEnd);
			} catch (MInvalidModelException inv) {
				ctx.reportError(fassocEndName ,inv);
			} catch (SemanticException e) {
				ctx.reportError(e);
				continue;
			}
			
			
			fMassocEnd.subset().addSubsettedAssocEnd(subsettedAssEnd);
		}
	}
}