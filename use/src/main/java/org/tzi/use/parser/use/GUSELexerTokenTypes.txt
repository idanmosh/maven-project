// $ANTLR 2.7.4: expandeduse.g -> GUSELexerTokenTypes.txt$
GUSELexer    // output token vocab name
LPAREN("'('")=4
COMMA("','")=5
RPAREN("')'")=6
IDENT("an identifier")=7
COLON("':'")=8
LITERAL_let="let"=9
EQUAL("'='")=10
LITERAL_in="in"=11
LITERAL_implies="implies"=12
LITERAL_or="or"=13
LITERAL_xor="xor"=14
LITERAL_and="and"=15
NOT_EQUAL("'<>'")=16
LESS("'<'")=17
GREATER("'>'")=18
LESS_EQUAL("'<='")=19
GREATER_EQUAL("'>='")=20
PLUS("'+'")=21
MINUS("'-'")=22
STAR("'*'")=23
SLASH("'/'")=24
LITERAL_div="div"=25
LITERAL_not="not"=26
ARROW("'->'")=27
DOT("'.'")=28
LITERAL_allInstances="allInstances"=29
AT("'@'")=30
LITERAL_pre="pre"=31
BAR("'|'")=32
LITERAL_iterate="iterate"=33
SEMI("';'")=34
LBRACK("'['")=35
RBRACK("']'")=36
LITERAL_oclAsType="oclAsType"=37
LITERAL_oclIsKindOf="oclIsKindOf"=38
LITERAL_oclIsTypeOf="oclIsTypeOf"=39
LITERAL_if="if"=40
LITERAL_then="then"=41
LITERAL_else="else"=42
LITERAL_endif="endif"=43
LITERAL_true="true"=44
LITERAL_false="false"=45
INT=46
REAL=47
STRING=48
HASH("'#'")=49
LITERAL_Set="Set"=50
LITERAL_Sequence="Sequence"=51
LITERAL_Bag="Bag"=52
LBRACE("'{'")=53
RBRACE("'}'")=54
DOTDOT("'..'")=55
LITERAL_oclEmpty="oclEmpty"=56
LITERAL_oclUndefined="oclUndefined"=57
LITERAL_Tuple="Tuple"=58
LITERAL_Collection="Collection"=59
LITERAL_model="model"=60
LITERAL_constraints="constraints"=61
LITERAL_gs="gs"=62
LITERAL_GSname="GSname"=63
LITERAL_type="type"=64
LITERAL_overlapping="overlapping"=65
LITERAL_complete="complete"=66
LITERAL_incomplete="incomplete"=67
LITERAL_disjoint="disjoint"=68
LITERAL_disjoint_complete="disjoint_complete"=69
LITERAL_disjoint_incomplete="disjoint_incomplete"=70
LITERAL_overlapping_complete="overlapping_complete"=71
LITERAL_overlapping_incomplete="overlapping_incomplete"=72
LITERAL_super="super"=73
LITERAL_subClasses="subClasses"=74
LITERAL_enum="enum"=75
LITERAL_abstract="abstract"=76
LITERAL_class="class"=77
LITERAL_attributes="attributes"=78
LITERAL_operations="operations"=79
LITERAL_end="end"=80
LITERAL_associationClass="associationClass"=81
LITERAL_associationclass="associationclass"=82
LITERAL_between="between"=83
LITERAL_aggregation="aggregation"=84
LITERAL_composition="composition"=85
LITERAL_begin="begin"=86
LITERAL_association="association"=87
LITERAL_role="role"=88
LITERAL_ordered="ordered"=89
LITERAL_qualifier="qualifier"=90
LITERAL_redefines="redefines"=91
LITERAL_subsets="subsets"=92
LITERAL_conforms="conforms"=93
LITERAL_context="context"=94
LITERAL_inv="inv"=95
COLON_COLON("'::'")=96
LITERAL_post="post"=97
LITERAL_var="var"=98
LITERAL_declare="declare"=99
LITERAL_set="set"=100
COLON_EQUAL("':='")=101
LITERAL_create="create"=102
LITERAL_namehint="namehint"=103
LITERAL_insert="insert"=104
LITERAL_into="into"=105
LITERAL_delete="delete"=106
LITERAL_from="from"=107
LITERAL_destroy="destroy"=108
LITERAL_while="while"=109
LITERAL_do="do"=110
LITERAL_wend="wend"=111
LITERAL_for="for"=112
LITERAL_execute="execute"=113
WS=114
SL_COMMENT=115
ML_COMMENT=116
RANGE_OR_INT=117
ESC=118
HEX_DIGIT=119
VOCAB=120
