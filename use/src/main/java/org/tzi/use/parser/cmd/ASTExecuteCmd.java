/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: ASTExecuteCmd.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.parser.cmd;

import org.tzi.use.parser.Context;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.parser.ocl.ASTExpression;
import org.tzi.use.uml.ocl.expr.Expression;
import org.tzi.use.uml.sys.MCmd;
import org.tzi.use.uml.sys.MCmdExecute;

/**
 * Node of the abstract syntax tree constructed by the parser.
 *
 * @version     $ProjectVersion: 0.393 $
 * @author  Mark Richters
 */
public class ASTExecuteCmd extends ASTCmd {
    private ASTExpression fExpr;

    public ASTExecuteCmd(ASTExpression source) {
        fExpr = source;
    }

    public MCmd gen(Context ctx) throws SemanticException {
        MCmdExecute res = null;

        ctx.setIsSideEffectFree(false);
        Expression exp = fExpr.gen(ctx);
        ctx.setIsSideEffectFree(true);
        return new MCmdExecute(ctx.systemState(), exp);
    }
}
