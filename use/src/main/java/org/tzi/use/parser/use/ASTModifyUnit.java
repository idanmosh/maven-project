package org.tzi.use.parser.use;

import org.tzi.use.parser.MyToken;

/**
 * ANTLR tokens container. Used in redefine, subset and conform.
 * @author Vitaliy Bichov <br>
 *	Created on 21/09/10
 */

public final class ASTModifyUnit {
	private MyToken f_className, f_roleName;
	
	
	public ASTModifyUnit(MyToken className, MyToken roleName) {
		f_className = className;
		f_roleName = roleName;
	}
	
	public void setClassName(MyToken className) {
		f_className = className;
	}

	public MyToken getRoleName() {
		return f_roleName;
	}
	public MyToken getClassName() {
		return f_className;
	}
}
