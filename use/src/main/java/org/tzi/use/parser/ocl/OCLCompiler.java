/*
 * OCL - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: OCLCompiler.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.parser.ocl;

import org.tzi.use.parser.Context;
import org.tzi.use.parser.ParseErrorHandler;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.ocl.expr.Expression;
import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.value.VarBindings;

import java.io.PrintWriter;
import java.io.Reader;

public class OCLCompiler {

    private OCLCompiler() {} // no instances
    
    /**
     * Compiles an expression.
     *
     * @param  in the source to be compiled
     * @param  inName name of the source stream
     * @param  err output stream for error messages
     * @return Expression null if there were any errors
     */
    public static Expression compileExpression(MModel model, 
                                               Reader in, 
                                               String inName, 
                                               PrintWriter err,
                                               VarBindings globalBindings) {
        Expression expr = null;
        ParseErrorHandler errHandler = new ParseErrorHandler(inName, err);
        GOCLLexer lexer = new GOCLLexer(in);
        GOCLParser parser = new GOCLParser(lexer);
        lexer.init(errHandler);
        parser.init(errHandler);
        try {
            // Parse the input expression
            ASTExpression astExpr = parser.expressionOnly();
            //System.err.println("***" + astExpr.toString());
            if (errHandler.errorCount() == 0 ) {
    
                // Generate code
                Context ctx = new Context(inName, err, globalBindings, null);
                ctx.setModel(model);
                expr = astExpr.gen(ctx);
    
                // check for semantic errors
                if (ctx.errorCount() > 0 )
                    expr = null;
            }
        } catch (antlr.RecognitionException e) {
            err.println(parser.getFilename() +":" + 
                        e.getLine() + ":" +
                        e.getColumn() + ": " + 
                        e.getMessage());
        } catch (SemanticException e) {
            err.println(e.getMessage());
        } catch (antlr.TokenStreamRecognitionException e) {
            err.println(parser.getFilename() +":" + 
                        e.recog.getLine() + ":" + e.recog.getColumn() + ": " + 
                        e.recog.getMessage());
        } catch (antlr.TokenStreamException ex) {
            err.println(ex.getMessage());
        }
        err.flush();
        return expr;
    }

    /**
     * Compiles a type.
     *
     * @param  in the source to be compiled
     * @param  inName name of the source stream
     * @param  err output stream for error messages
     * @return Type null if there were any errors
     */
    public static Type compileType(MModel model, 
                                   Reader in, 
                                   String inName, 
                                   PrintWriter err) {
        Type type = null;
        ParseErrorHandler errHandler = new ParseErrorHandler(inName, err);
        GOCLLexer lexer = new GOCLLexer(in);
        GOCLParser parser = new GOCLParser(lexer);
        lexer.init(errHandler);
        parser.init(errHandler);
        try {
            // Parse the input expression
            ASTType astType = parser.typeOnly();
            //System.err.println("***" + astExpr.toString());
            if (errHandler.errorCount() == 0 ) {
    
                // Generate code
                Context ctx = new Context(inName, err, null, null);
                ctx.setModel(model);
                type = astType.gen(ctx);
    
                // check for semantic errors
                if (ctx.errorCount() > 0 )
                    type  = null;
            }
        } catch (antlr.RecognitionException e) {
            err.println(parser.getFilename() +":" + 
                        e.getLine() + ":" +
                        e.getColumn() + ": " + 
                        e.getMessage());
        } catch (SemanticException e) {
            err.println(e.getMessage());
        } catch (antlr.TokenStreamRecognitionException e) {
            err.println(parser.getFilename() +":" + 
                        e.recog.getLine() + ":" + e.recog.getColumn() + ": " + 
                        e.recog.getMessage());
        } catch (antlr.TokenStreamException ex) {
            err.println(ex.getMessage());
        }
        err.flush();
        return type;
    }
}
