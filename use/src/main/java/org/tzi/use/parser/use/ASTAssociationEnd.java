/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: ASTAssociationEnd.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.parser.use;

import org.tzi.use.parser.AST;
import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MNavigableElement;
import org.tzi.use.uml.mm.MQualifier;
import org.tzi.use.util.Matcher;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Node of the abstract syntax tree constructed by the parser.
 *
 * @version     $ProjectVersion: 0.393 $
 * @author  Mark Richters
 */
public class ASTAssociationEnd extends AST {
    private MyToken fName;
    private ASTMultiplicity fMultiplicity;
    private MyToken fRolename;  // optional: may be null!
    private boolean fOrdered;
    private ASTQualifier fQualifier; //optional: may be null
    
    private ASTRedefine fRedefine;
    private ASTSubset fSubset;
    private ASTConform fConform;
    private MAssociationEnd fMassocEnd;

    public ASTAssociationEnd(MyToken name, ASTMultiplicity mult) {
        fName = name;
        fMultiplicity = mult;
        fOrdered = false;
    }

    public void setRolename(MyToken rolename) {
        fRolename = rolename;
    }

    public void setOrdered() {
        fOrdered = true;
    }

    public MAssociationEnd gen(Context ctx, int kind) throws SemanticException {
        // lookup class at association end in current model
        MClass cls = ctx.model().getClass(fName.getText());
        if (cls == null )
            // this also renders the rest of the association useless
            throw new SemanticException(fName, "Class `" + fName.getText() +
                                        "' does not exist in this model.");
    
        MMultiplicity mult = fMultiplicity.gen(ctx);
        if (fOrdered && ! mult.isCollection() ) {
            ctx.reportWarning(fName, "Specifying `ordered' for " +
                              "an association end targeting single objects has no effect.");
            fOrdered = false;
        }
        
       
        MAssociationEnd aend;
   
         aend = ctx.modelFactory().createAssociationEnd(cls, 
        	            ( fRolename != null ) ? fRolename.getText() : cls.nameAsRolename(),
        	            mult, kind, fOrdered); 
        
        	            
        	
        	
        //Adding the qualifier to association end
        
        //Added by Azzam

        
       
       
        if (isQualified()){ //check if the qualifier exists
        	MQualifier qualifier = ctx.modelFactory().createQualifier("Qualifier");
        	
        	List<ASTAttribute> attributes_list = fQualifier.getQualifierAttributes();
        	for(int i=0;i<attributes_list.size();i++){                          //Adding the qualifier attributes 
        		ASTAttribute ast_attribute = attributes_list.get(i);  
        		MAttribute attribute =  ast_attribute.gen(ctx);
        		qualifier.addAttribute(attribute);
        		//System.out.println(aend.name() + " Size: " + ((EnumType) attribute.type()).getEnumerationSize());
        	}
        	aend.setQualifier(qualifier);
        }
        
        fMassocEnd = aend; 
        updateModifiers(aend);
        //System.out.println(fMassocEnd.name() + " Size: " + fMassocEnd.isQualified());
        
        return aend;
    }

    private void updateModifiers(MAssociationEnd aend) {
		if (fSubset != null) {
			fSubset.setMAssociationEnd(aend);
		}
		if (fConform != null) {
			fConform.setMAssociationEnd(aend);
		}
		if (fRedefine != null) {
			fRedefine.setMAssociationEnd(aend);
		}
	}

	public String toString() {
        return "FIXME";
    }
    
    //Adding methods to handle qualifiers at association end
    
    /**
     * Setting the qualifier at the association end
     */
    public void setQualifier(ASTQualifier qualifier){
    	fQualifier = qualifier;
    	
    }
    /**
     * Getter for the qualifier. Should be invoked after isQualified method
     * @return the qualifier at the association end
     */
    public ASTQualifier getQualifier(){
    	return fQualifier;
    }
    /**
     * Checks whether the association end is qualified
     * @return true if the qualifier exists at this association end
     */
    public boolean isQualified(){
    	return (fQualifier != null);
    }

    
    /**
     * Setter to a ASTConform object - used by ANTLR. 
     * @param conform
     */
	public void setConform(ASTConform conform) {
		this.fConform = conform;
	}

	//TODO vitalyb fix comments and javadocs
	/**
	 * Getter to ASTConform object. If <code>null</code> is returned then this association end doesn't conform.
	 * If association end dosn't belong to an associationClass should be <code>null</code>.
	 * @return - ASTConform object or null.
	 */
	public ASTConform getConform() {
		return fConform;
	}

	/**
	 * Setter to a ASTSubset object - used by ANTLR. 
	 * @param fSubset
	 */
	public void setSubset(ASTSubset fSubset) {
		this.fSubset = fSubset;
	}

	/**
	 * Getter to ASTSubset object. If <code>null</code> is returned then this association end
	 * doesn't subset another association end.
	 * @return ASTSubset or <code>null</code>.
	 */
	public ASTSubset getSubset() {
		return fSubset;
	}

	/**
	 * Setter to a ASTRedefine object - used by ANTLR. 
	 */
	public void setRedefine(ASTRedefine fRedefine) {
		this.fRedefine = fRedefine;
	}

	/**
	 * Getter to ASTRedefine object. If <code>null</code> is returned then this association end wasn't subseted.
	 * @return ASTRedefine or <code>null</code>.
	 */
	public ASTRedefine getRedefine() {
		return fRedefine;
	}
	
	
	/**
	 * Updates multiplicity in meta model association end that was created by this class.  
	 * @param ctx
	 */
	public void updateUndefinedMultiplicity(Context ctx) {
		if (fMassocEnd.multiplicity().getRange().getUpper() != MMultiplicity.UNDEFINED) return;
		MModel model = ctx.model();
		MAssociationEnd assocEnd;
		String errStr = "Can't infer multiplicity for association end '" + fName.getText() + "' ";
		int minUpper = Integer.MAX_VALUE;
		Set<MAssociation> redefinedAssocs = model.redefineAssociationsGraph().
													targetNodeClosureSet(fMassocEnd.association());
		
		if (redefinedAssocs == null || redefinedAssocs.isEmpty()) {
			ctx.reportError(fName, errStr 
					+ "association that contanis it doesn't have any redefinig association end");
			return;
		}
		
		for (MAssociation assoc : redefinedAssocs) {
			assocEnd = findMatchingParentIn(assoc);
			minUpper = MMultiplicity.getSmallerNumber(minUpper, 
												assocEnd.multiplicity().getRange().getUpper());
		}
		
		if (Integer.MAX_VALUE == minUpper) {
			ctx.reportError(fName, errStr + 
					"multiplicities in all matching association ends are also undefined.");
			return;
		}
		
		fMassocEnd.multiplicity().getRange().setUpper(minUpper);		
	}
	

	/**
	 * Finds an association end in association 'assoc' that matches this association end. 
	 * The match is by hierarchy of referenced classes. 
	 * @param assoc association in which the search should be executed.
	 * @return an association end that matches this (metaModel association end) association end
	 * or <code>null</code> if one is ont found. 
	 */
	private MAssociationEnd findMatchingParentIn (MAssociation assoc) {
    	List<MNavigableElement> lst = (List<MNavigableElement>) Matcher.findMatchByHierarchy(
    			assoc.associationEnds(), fMassocEnd.association().associationEnds());
    	
    	Iterator<MNavigableElement> supIt, subIt;
    	MNavigableElement sup, sub;
    	supIt = lst.iterator();
    	subIt = fMassocEnd.association().associationEnds().iterator();
    	while (subIt.hasNext()) {
    		sup = supIt.next();
    		sub = subIt.next();
    		if (fMassocEnd == sub) return (MAssociationEnd) sup;
    	}
    	return null;
    }

}












