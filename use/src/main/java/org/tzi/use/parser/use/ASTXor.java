package org.tzi.use.parser.use;

import antlr.Token;
import bgu.cmd.Options;
import org.tzi.use.parser.Context;
import org.tzi.use.parser.MyToken;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MXor;

import java.util.ArrayList;
import java.util.List;

/**
* Class that holds tokens from ANTLR that describe 'xor' set.
* Also responsible for creating 'xor' sets in the model.
*/
public class ASTXor {
	
	MyToken fSourceName, fxorName;
	
	List<MyToken> fEnds;
	
	public ASTXor(Token sourceName, List ends, Token xorName) {
		fSourceName = (MyToken)sourceName;
		fEnds = ends;
		fxorName = (MyToken) xorName;
	}


	
	public void gen(Context ctx) {
		MClass sourceClass;
		MAssociationEnd end;
		
		List<MAssociationEnd> ends = new ArrayList<MAssociationEnd>(fEnds.size()); 
		MModel model = ctx.model();
	
		//find the source class
		if ((sourceClass = model.getClass(fSourceName.getText())) == null) {
			ctx.reportError(fSourceName, "In xor: Class '" + fSourceName.getText() + "' not found");
			return;
		}
		
		for (MyToken tok : fEnds) {

			try {
				if ((end = ((MAssociationEnd)sourceClass.navigableEnd(tok.getText()))) == null) {
					ctx.reportError(tok, "In xor association end '"+ tok.getText() + "' " +
							"is not navigble from class '"+sourceClass.name()+"'.");
					return;
				}
				if (!Options.getXorModelIsOveriding()) {
					if (end.multiplicity().getRange().getLower() != 0) {
						ctx.reportError(tok, "In xor multiplicity of association end " 
								+ end.name() + " is not comatible with the selected xor model.");
						return;
					}
				}
				ends.add(end);
			}catch (ClassCastException e) {
				ctx.reportError(tok, "In xor '" + tok.getText() +"' is not an association end.");
				return;
			}
		}
		//TODO vitalyb change to MXor creation factory
		try {
			String name = fxorName == null ? null : fxorName.getText();
			model.addXor(new MXor(name, sourceClass, ends));
		} catch (IllegalArgumentException e) {
			ctx.reportError(fSourceName, e);
		}
	}
}
