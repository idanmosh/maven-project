package org.tzi.use.uml.mm;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created on 10/10/2008
 * @author Victor Makarenkov
 *  Interface added in order to support the qualifier in USE UML Model.
 *  Ben-Gurion University of the Negev, Department of Computer Science
 */
public class MQualifier extends MModelElementImpl{
	
	private List<MAttribute> fAttributes;
	private MAssociationEnd fOwner;
	
	protected MQualifier(String name) {
		super(name);        //What qualifier name should be ? 
		fAttributes = new ArrayList<MAttribute>();
	}
	
	protected MQualifier (String name, List<MAttribute> attributes){
		super(name);
		fAttributes = attributes;
	}

	public void addAttribute(MAttribute attribute){
		fAttributes.add(attribute);
	}
	
	public List<MAttribute> getAttributes(){
		return fAttributes;
	}
	
	public void setOwner(MAssociationEnd associationEnd){
		fOwner = associationEnd;
	}
	
	public MAssociationEnd getAssociationEnd(){
		return fOwner;
	}

	public String toString(){
		return "Qualifier with "+ fAttributes.size()+" attributes";
	}
	
	@Override
	public void processWithVisitor(MMVisitor v) {
		// TODO Auto-generated method stub
		
	}
	
   
	
}
