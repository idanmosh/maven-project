package org.tzi.use.uml.mm;

import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.util.Matcher;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;



public abstract class MModifyAssociationEnd {
	private final MAssociationEnd fAssocEnd;

    
    public MModifyAssociationEnd(MAssociationEnd assocEnd) {
    	fAssocEnd = assocEnd;
	}
    
    protected MAssociationEnd assocEnd() {
    	return fAssocEnd;
    }

    protected Set<MAssociation> getAssociationsFromSetOfAssociationEnds(Set<MAssociationEnd> assocEnds) {
		Set<MAssociation> returnSet = new TreeSet<MAssociation>();
		Iterator<MAssociationEnd> iter = assocEnds.iterator();
		MAssociationEnd ae = null;
		while (iter.hasNext()) {
			ae = iter.next();
			returnSet.add(ae.association());
		}
		return returnSet;
    }
    
    
    
    protected boolean thisSideInheretanceTest(MAssociationEnd modifiedEnd) {
    	return assocEnd().cls().isSubClassOf(modifiedEnd.cls()); 
    }
    
	protected boolean associationInheirtanceTest(MAssociationEnd modifiedEnd) {
		/* assocOfModified - association that the modified association end belongs to
		 * assocOfThis - association that this(fMassocEnd) association end belongs to */
		MAssociation assocOfThis, assocOfModified;
		assocOfThis = assocEnd().association();
		assocOfModified = modifiedEnd.association();

		return (null != Matcher.findMatchByHierarchy(assocOfModified.associationEnds(), assocOfThis.associationEnds()));
	}

    protected boolean subsetContains(int currentUpper ,MMultiplicity otherMult) {
    	for (Range r : otherMult.getRanges()) {
    		if (MMultiplicity.getBiggerNumber(currentUpper, r.getUpper()) == r.getUpper())
    			return true;
    	}
    	return false;
    }	
}
