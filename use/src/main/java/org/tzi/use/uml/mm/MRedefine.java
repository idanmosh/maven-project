package org.tzi.use.uml.mm;

import org.tzi.use.graph.DirectedEdgeBase;
import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.util.Matcher;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Meta model representation of redefine relation between association ends.
 * Instance of this class exists for each association end. 
 * 
 * @author vitalib
 *
 */

public class MRedefine extends MModifyAssociationEnd {

	/** set of association ends that where redefined by this association end, "parents" */
	private final Set<MAssociationEnd> fredefinedSet; 
	/** set of association ends that redefine this association end, "children"*/
	private final Set<MAssociationEnd> fredefiningSet;

	public MRedefine(MAssociationEnd ae) {
		super(ae);
		fredefinedSet = new TreeSet<MAssociationEnd>();
		fredefiningSet = new TreeSet<MAssociationEnd>();
	}

	
	/**
	 * Adds redefined (by this.assoEnd()) associationEnd and updates set of redefining associationEnds
	 * as well as the redefine associations graph.
	 * Should be executed only if {@link #validateRedefenition(MAssociationEnd)} didn't throw an exception.
	 * @param ae - redefined association end.
	 */
	public void addRedefinedAssocEnd(MAssociationEnd ae) {
		assocEnd().cls().model().redefineAssociationsGraph().
		addEdge(new DirectedEdgeBase(assocEnd().association(), ae.association()));
		ae.redefine().fredefiningSet.add(super.assocEnd());
		fredefinedSet.add(ae);
	}

	/**
	 * Getter for a redefined association ends, "parents"
	 * @return A set of redefined association ends.
	 */
	public Set<MAssociationEnd> getRedefined() {
		return fredefinedSet;
	}

	/**
	 * Getter for a redefining association ends, "children"
	 * @return A set of redefining association ends.
	 */
	public Set<MAssociationEnd> getRedefining() {
		return fredefiningSet;
	}
	
	/**
	 * Transitive closure of redefined association ends.
	 * @return new Set(MAssociationEnd)
	 */
	public Set<MAssociationEnd> getAllRdefined() {
		Set<MAssociationEnd> result = new HashSet<MAssociationEnd>();
		result.addAll(fredefinedSet);
		for (MAssociationEnd end : fredefinedSet) {
			result.addAll(end.redefine().getAllRdefined());
		}
		return result;
	}
	
	/**
	 * Transitive closure of redefining association ends.
	 * @return new Set(MAssociationEnd)
	 */
	public Set<MAssociationEnd> getAllRedefining() {
		Set<MAssociationEnd> result = new HashSet<MAssociationEnd>();
		result.addAll(fredefiningSet);
		for (MAssociationEnd end : fredefiningSet) {
			if (end.redefine().getAllRedefining() != null) {
				result.addAll(end.redefine().getAllRedefining());
			}
		}
		return result;
	}

	/**
	 * Creates a Set of Associations that where redefined by an association
	 * owning this association end. "parents"
	 * @return Set(MAssociation)
	 */
	public Set<MAssociation> getRedefinedAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fredefinedSet);
	}

	/**
	 * Creates a Set of Associations that redefine an association
	 * owning this association end. "children"
	 * @return Set(MAssociation)
	 */
	public Set<MAssociation> getRedefiningAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fredefiningSet);	
	}

	
	/**
	 * Calculates redefine strict range. <br>
	 * Redefine strict range - Range (MAX(lower), MIN(upper)) where 'lower' and 'upper' are sets that
	 * correspond to lower and upper bounds of direct or indirect parent associationEnds. 
	 * @return Range (MAX(lower), MIN(upper))
	 * @throws IllegalArgumentException if the resulting range is impossible.
	 */
	public Range getStrictRange() throws IllegalArgumentException {
		Set<MAssociationEnd> indirectParents = getIndirectParents();
		Range strictRange = this.assocEnd().multiplicity().getRange();
		for (MAssociationEnd indirectParentAE : indirectParents) {
			//intersect:
			try{ strictRange = MMultiplicity.getStrictRange(this.assocEnd().multiplicity().getRange(), 
					indirectParentAE.multiplicity().getRange());
			} catch (IllegalArgumentException e) { throw new IllegalArgumentException(
					"Resulting redefining stritct range is not possible " + e.getMessage());}
		}
		return strictRange;
	}


	/**
	 * Calculates redefine strict lower bound. <br>
	 * strict lower bound - maximal value of lower bounds of all indirect parent association ends. 
	 * @return MAX(lower)
	 */
	public int getStrictLowerBound() {
		int possibleLower;
		Set<MAssociationEnd> indirectParents = getIndirectParents();
		int strictLower = this.assocEnd().multiplicity().getRange().getLower();
		for (MAssociationEnd indirectParentAE : indirectParents) {
			//intersect
			possibleLower = indirectParentAE.multiplicity().getRange().getLower();
			strictLower = MMultiplicity.getSmallerNumber(strictLower, possibleLower);
		}
		return strictLower;
	}

	/**
	 * Calculates redefine strict upper bound. <br>
	 * strict upper bound - minimal value of upper bounds of all indirect parent association ends. 
	 * @return MIN(upper)
	 */
	public int getStrictUpperBound() {
		int possibleLower;
		Set<MAssociationEnd> indirectParents = getIndirectParents();
		int strictUpper = this.assocEnd().multiplicity().getRange().getUpper();
		for (MAssociationEnd indirectParentAE : indirectParents) {
			//intersect
			possibleLower = indirectParentAE.multiplicity().getRange().getUpper();
			strictUpper = MMultiplicity.getBiggerNumber(strictUpper, possibleLower);
		}
		return strictUpper;
	}

	/**
	 * Checks if this association end can redefine 'redefinedEnd'. If it can't exception is thrown.
	 * @param redefinedEnd MAssociationEnd  that this association end redefines
	 * @throws MInvalidModelException - if this association end can't redefine the given association end.
	 */
	public void validateRedefenition(MAssociationEnd redefinedEnd) throws MInvalidModelException {
		String errStr = "Association end '"+ redefinedEnd.name() + "' can't be redefined by '" 
		+ assocEnd().name()+"' because ";

		/* class that this association is connected to should inherit 
		 * or be of the same type as the class at 'redefinedEnd' */
		if (!thisSideInheretanceTest(redefinedEnd)) {
			throw new MInvalidModelException(errStr + "class '" 
					+ redefinedEnd.cls().name() 
					+ "' " + "is not a generalization of '" 
					+ assocEnd().cls().name()
					+ "'.");
		}

		/* if redefined association is reflexive then the redefining association 
		 * should also be reflexive */
/*		if (!reflexivityTest(redefinedEnd)) {
			throw new MInvalidModelException(errStr + "one of the associations '"
					+ redefinedEnd.association().name()
					+ "' '"
					+ assocEnd().association().name()
					+ "' is reflexive and the other is not.");															  
		}*/

		/* each class of the redefining association should have a superclass
		 * in the redefined association */
		if (!associationInheirtanceTest(redefinedEnd)) {
			throw new MInvalidModelException(errStr + "association '" 
					+ redefinedEnd.association().name() 
					+ "' cant't be fitted to association '" 
					+ assocEnd().association().name() 
					+ "'.");
		}

		/* at least one of the ends (not including the redefining end)
		 * should contain class that not connected to the redefined association
		 * (redefining association can't be connected to the sasme classes as the redefined association)*/
		if (!differentClassesConectionTest(redefinedEnd)) {
			throw new MInvalidModelException(errStr + "association '" 
					+ redefinedEnd.association().name() 
					+ "' and association '" 	
					+ assocEnd().association().name() 
					+ "' both connect same classes.");			
		}

		/* multiplicity of the redefining end should be contained in
		 * the multiplicity of the redefined end */
		if (!redefineMultiplicityTest(redefinedEnd)) {
			throw new MInvalidModelException(errStr + "multiplicity doesn't fit.");
		}
	}

	/**
	 * Tests if the redefined association contains at least one class that not 
	 * connected to the redefining class
	 * @param redefinedEnd - redefined association end
	 * @return true if conditions hold
	 */
	private boolean differentClassesConectionTest(MAssociationEnd redefinedEnd) {
		Set<MClass> redefinedAssocClasses = 
			new HashSet<MClass>(redefinedEnd.association().associatedClasses());
		Set<MClass> thisAssocClasses = 
			new HashSet<MClass>(assocEnd().association().associatedClasses());
		redefinedAssocClasses.remove(redefinedEnd.cls());
		thisAssocClasses.remove(assocEnd().cls());

		return !redefinedAssocClasses.containsAll(thisAssocClasses);
	}

	/**
	 * Tests if this.association().isReflexive() == redefined.assocition().isReflexive() 
	 * @param redefinedEnd redefined end
	 * @return true if conditions hold
	 */
	private boolean reflexivityTest(MAssociationEnd redefinedEnd) {
		boolean thisIsReflexive, redefinedIsReflexive;
		redefinedIsReflexive = redefinedEnd.association().isReflexive();
		thisIsReflexive = assocEnd().association().isReflexive();
		return (redefinedIsReflexive && thisIsReflexive) || (!redefinedIsReflexive && !thisIsReflexive);
	}


	/**
	 * Tests if multiplicity of the redefining end is contained in the multiplicity of the redefined end
	 * @param redefinedEnd
	 * @return true if conditions hold
	 */
	private boolean redefineMultiplicityTest(MAssociationEnd redefinedEnd) {
		return redefinedEnd.multiplicity().contains(assocEnd().multiplicity());
	}


	/**
	 * This function finds and returns all associationEnds that 
	 * @return
	 */
	private Set<MAssociationEnd> getIndirectParents() {

		Set<MAssociationEnd> indirectParents = new TreeSet<MAssociationEnd>();
		MAssociation directparentAssoc, indirectParentAssoc, possibleIndirectParentAssoc, thisAssoc;
		thisAssoc = assocEnd().association();

		for (MAssociationEnd directParentAE : fredefinedSet) {
			directparentAssoc = directParentAE.association();

			for (MAssociationEnd possibleIndirectParentAE : directParentAE.redefine().getRedefining()) {
				possibleIndirectParentAssoc = possibleIndirectParentAE.association();

				/*if class referenced by possible parent associationEnd is not a superClass of
				 * the class referenced by this associationEnd */
				if (!possibleIndirectParentAE.cls().isSuperClassOf(assocEnd().cls())) {
					continue;
				}
				
				/* if this association can't be matched to possible parent association */
				if (null == Matcher.findMatchByHierarchy(possibleIndirectParentAssoc.associationEnds(), 
						thisAssoc.associationEnds())) {
					continue;
				}

				/* if possible parent association connects same classes as direct parent association */
				if (directparentAssoc.associatedClasses().containsAll(possibleIndirectParentAssoc.associatedClasses())) {
					continue;
				}

				/* if possible parent association connects same classes as this association */
				if (thisAssoc.associatedClasses().containsAll(possibleIndirectParentAssoc.associatedClasses())) {
					continue;
				}
				
				indirectParents.add(possibleIndirectParentAE);
			}
		}
		return indirectParents;
	}
}
