package org.tzi.use.uml.mm;

import java.util.List;
import java.util.Set;

/**
 * Meta model representation of xor constraint between associations.
 * @author vitalib
 *
 */

public class MXor extends MModelElementImpl {
	
	MClass fSourceClass;
	List<MAssociationEnd> fEnds;
	
	public  MXor(String name ,MClass sourceClass, List<MAssociationEnd> ends) {
		this(name, sourceClass);
		assertXorValid(ends);
		fEnds = ends;
	}
	
	public MXor(String name ,MClass sourceClass) {
		super(name, "xor");
		fSourceClass = sourceClass;
	}
	
	public void addEnd(MAssociationEnd end) throws IllegalArgumentException  {
		try {
			fEnds.add(end);
			assertXorValid(fEnds);
			end.setPartOfXor(true);
		}
		catch (IllegalArgumentException e) {
			fEnds.remove(end);
			throw e;
		} 		
	}
	
	public MClass getSourceClass() {
		return fSourceClass;
	}
	
	public int numberOfEnds() {
		return fEnds.size();
	}
	
	public MAssociationEnd getEnd(int index) {
		return fEnds.get(index);
	}
	
	public List<MAssociationEnd> getEnds() {
		return fEnds;
	}
	
	
	public boolean isXor(MClass cls1, MClass cls2) {
		boolean cls1Found = false, cls2Found = false;
		Set<MClass> cls1Set = cls1.allChildren();
		cls1Set.addAll(cls1.allParents());
		cls1Set.add(cls1);
		Set<MClass> cls2Set = cls2.allChildren();
		cls2Set.addAll(cls2.allParents());
		cls2Set.add(cls2);
		
		if (cls1Set.containsAll(cls2Set)) {
			return false;
		}
		
		for (MAssociationEnd end : fEnds) {
			if (cls1Set.contains(end.cls())) {
				cls1Found = true;
			}
			if (cls2Set.contains(end.cls())) {
				cls2Found = true;
			}
		}
		
		return cls1Found && cls2Found;
	}
	
	public boolean isXor(MAssociation assoc1, MAssociation assoc2) {
		boolean foundMatchFor1 = false, foundMatchFor2 = false;
		List<MAssociationEnd> endsOf1, endsOf2;
		endsOf1 = assoc1.associationEnds();
		endsOf2 = assoc2.associationEnds();
		
		if (assoc1 == assoc2) return false;
		
		for (MAssociationEnd end : fEnds) {
			if (endsOf1.contains(end)) {
				foundMatchFor1 = true;
			}
			if (endsOf2.contains(end)) {
				foundMatchFor2 = true;
			}
		}
		return foundMatchFor1 && foundMatchFor2;
	}
	
	private void assertXorValid(List<MAssociationEnd> ends) {
		MAssociationEnd end1, end2;
		for (int i = 0; i < ends.size(); i++) {
			for (int j = i + 1 ; j < ends.size(); j ++) {
				end1 = ends.get(i);
				end2 = ends.get(j);
				if (end1 == end2) {
					throw new IllegalArgumentException("In xor: association end '" 
							+ end1.name() 
							+ "' appears more than once." );
				}
				if (end1.association() == end2.association()) {
					throw new IllegalArgumentException("In xor: association ends '" 
							+ end1.name() 
							+ "' and '" 
							+ end2.name() 
							+ "' belong to the same association.");
				}
			}
		}
	}

	@Override
	public void processWithVisitor(MMVisitor v) {
		// TODO Auto-generated method stub
		
	}

}
