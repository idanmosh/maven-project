/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: MAssociationClass.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.uml.mm;

import org.tzi.use.util.NotFoundException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * MAssociationClass instances represent associationclasses in a model.
 *
 * @version     $ProjectVersion: 0.393 $
 * @author <a href="mailto:hanna@tzi.de">Hanna Bauerdick</a>
 * @author <a href="mailto:gutsche@tzi.de">Fabian Gutsche</a>
 */
public interface MAssociationClass extends MClass, MAssociation, MNavigableElement {

    // inherited methods of MClass

    /**
     * Returns true if the class is marked abstract.
     */
    public boolean isAbstract();

    /**
     * Returns the name of this class with lowercase first letter.
     */
    public String nameAsRolename();

    /**
     * Returns the model owning this class.
     */
    public MModel model();

    /**
     * Sets the model owning this class. This method must be called by
     * MModel.addClass().
     *
     * @see MModel#addClass
     */
    public void setModel( MModel model );

    /**
     * Adds an attribute. The attribute must have a unique name within
     * the class and must not conflict with any attribute with the
     * same name defined in a superclass or subclass or with a
     * rolename of an association this class participates in.
     *
     * @exception MInvalidModelException the class already contains an
     *            attribute with the same name or a name clash
     *            occured.
     */
    public void addAttribute( MAttribute attr ) throws MInvalidModelException;

    /**
     * Returns the set of attributes defined for this class. Inherited
     * attributes are not included.
     *
     * @return List(MAttribute)
     */
    public List attributes();
    //public Set attributes();

    /**
     * Returns the set of all attributes (including inherited ones)
     * defined for this class.
     *
     * @return List(MAttribute)
     */
    public List allAttributes();
    //public Set allAttributes();

    /**
     * Returns the specified attribute. Attributes are also looked up
     * in superclasses if <code>searchInherited</code> is true.
     *
     * @return null if attribute does not exist.
     */
    public MAttribute attribute( String name, boolean searchInherited );


    /**
     * Adds an operation. The operation name must be unique among all
     * operations of this class. Operations in superclasses may be
     * redefined if the signature matches.
     *
     * @exception MInvalidModelException the operation is not unique
     *            or is not a valid redefinition.
     */
    public void addOperation( MOperation op ) throws MInvalidModelException;

    /**
     * Returns all operations defined for this class. Inherited
     * operations are not included.
     */
    public List operations();

   

    /**
     * Gets an operation by name. Operations are also looked up in
     * superclasses if <code>searchInherited</code> is true. This
     * method walks up the generalization hierarchy and selects the
     * first matching operation. Thus, if an operation is redefined,
     * this method returns the most specific one.
     *
     * @return null if operation does not exist.
     */
    public MOperation operation( String name, boolean searchInherited );


    /**
     * Returns the association end that can be reached by the
     * OCL expression <code>self.rolename</code>.
     *
     * @return null if no such association end exists.
     */
    public MNavigableElement navigableEnd( String rolename );

    /**
     * Returns a map of all association ends that can be reached from
     * this class by navigation.
     *
     * @return Map(String, MAssociationEnd)
     */
    Map navigableEnds();

    /**
     * Registers all association ends as navigable from this
     * class. This should be called by a MModel implementation when an
     * association is added to the model.
     *
     * @param associationEnds List(MAssociationEnd)
     * @see MModel#addAssociation
     */
    void registerNavigableEnds( List associationEnds );


    /**
     * Returns the set of all direct parent classes (without this
     * class).
     *
     * @return Set(MClass)
     */
    public Set parents();

    /**
     * Returns the set of all parent classes (without this
     * class). This is the transitive closure of the generalization
     * relation.
     *
     * @return Set(MClass)
     */
    public Set allParents();

    /**
     * Returns the set of all child classes (without this class). This
     * is the transitive closure of the generalization relation.
     *
     * @return Set(MClass)
     */
    public Set allChildren();

    /**
     * Returns the set of associations this class directly
     * participates in.
     *
     * @return Set(MAssociation).
     */
    public Set associations();

    /**
     * Returns the set of associations this class and all of its
     * parents participate in.
     *
     * @return Set(MAssociation).
     */
    public Set allAssociations();

    /**
     * Returns true if this class is a child of
     * <code>otherClass</code> or equal to it.
     */
    public boolean isSubClassOf( MClass otherClass );

    // inherited methods of MAssociation

    /**
     * Adds an association end.
     *
     * @exception MInvalidModelException trying to add another composition
     *            or aggregation end.
     */
    void addAssociationEnd( MAssociationEnd aend ) throws MInvalidModelException;

    /**
     * Returns the list of association ends.
     *
     * @return List(MAssociationEnd)
     */
    List associationEnds();

    /**
     * Returns the set of association ends attached to <code>cls</code>.
     *
     * @return Set(MAssociationEnd)
     */
    Set associationEndsAt( MClass cls );

    /**
     * Returns the set of classes participating in this association.
     *
     * @return Set(MClass).
     */
    Set associatedClasses();

    /**
     * Returns kind of association. This operation returns aggregate
     * or composition if one of the association ends is aggregate or
     * composition.
     */
    int aggregationKind();

    /**
     * Returns a list of association ends which can be reached by
     * navigation from the given class. Examples:
     *
     * <ul>
     * <li>For an association R(A,B), R.navigableEndsFrom(A)
     * results in (B).
     *
     * <li>For an association R(A,A), R.navigableEndsFrom(A) results
     * in (A,A).
     *
     * <li>For an association R(A,B,C), R.navigableEndsFrom(A) results
     * in (B,C).
     *
     * <li>For an association R(A,A,B), R.navigableEndsFrom(A) results
     * in (A,A,B).
     * </ul>
     *
     * This operation does not consider associations in which parents
     * of <code>cls</code> take part.
     *
     * @return List(MAssociationEnd)
     * @exception IllegalArgumentException cls is not part of this association.
     */
    List navigableEndsFrom( MClass cls );

    // Methods of MAssociation and MClass

    /**
     * Process this element with visitor. 
     */
    void processWithVisitor( MMVisitor v );

    /**
     * Given an association end name function finds and returns MAX(lower bounds)
     * where "lower bounds" is is a set of multiplicity constraints from this association end and 
     * parent association ends. The parenthood is by "suited".</br>
     * @param ae - MAssociationEnd name
     * @return integer >= 0
     * @throws NotFoundException 
     */
    public int getStrictLowerBound(String assocEndName) throws NotFoundException;

    /**
     * Given an association end name, function finds and returns MIN(upper bounds)
     * where "upper bounds" is is a set of multiplicity constraints from this association end and 
     * parent association ends. The inheritance is by "suited".</br>
     * @param ae - MAssociationEnd object
     * @return integer > 0 or -1 for infinity (star)
     * @throws NotFoundException
     */    
    public int getStrictUpperBound(String assocEndName) throws NotFoundException;

    
    /**
     * Given an association end's role name this function finds and returns Max(lower) and Min(upper) 
     * where lower and upper are multiplicity constraints of this class and his super classes.
     * @param assocEndRoleName
     * @return new Range object containing the strictest multiplicity constraints
     * @throws NotFoundException - thrown if assocEndRoleName is not found in this asscoiationClass
     */
//    Range getStrictRange(String assocEndRoleName) throws NotFoundException;
    
    /**
     * Given an association end this function finds and returns Max(lower) and Min(upper) 
     * where lower and upper are multiplicity constraints of this class and his super classes.
     * param ae - association end
     * @return new Range object containing the strictest multiplicity constraints
     * @throws NotFoundException - thrown if ae is not part of this asscoiationClass
     */
//    Range getStrictRange(MAssociationEnd ae) throws NotFoundException;

}
