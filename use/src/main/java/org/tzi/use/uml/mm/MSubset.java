package org.tzi.use.uml.mm;

import org.tzi.use.graph.DirectedEdgeBase;
import org.tzi.use.uml.mm.MMultiplicity.Range;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Meta model representation of subset relation between association ends.
 * Instance of this class exists for each association end. 
 * 
 * @author vitalib
 *
 */
public class MSubset extends MModifyAssociationEnd {
	
    /** pointers to association ends that this association end subsets, "parents")*/ 
    private final Set<MAssociationEnd> fsubsetedSet;
    /** pointers to association ends that subset this association end, "children" */
    private final Set<MAssociationEnd> fsubsettingSet;
    
    public MSubset(MAssociationEnd ae) {
		super(ae);
		fsubsetedSet = new TreeSet<MAssociationEnd>();
		fsubsettingSet = new TreeSet<MAssociationEnd>();
	}
    
	
	/**
     * Adds subseted (by this.assoEnd()) associationEnd and updates set of subseting associationEnds
     * Should be executed only if {@link #validateSubset(MAssociationEnd)} didn't throw an exception.
     * @param ae - redefined association end
     * @see #canSubset(MAssociationEnd)
     */
	public void addSubsettedAssocEnd(MAssociationEnd ae) {
		assocEnd().cls().model().subsetAssociationsGraph().
				addEdge(new DirectedEdgeBase(assocEnd().association(), ae.association()));
		//his subseting set contains me and my subseted set contains him
		this.fsubsetedSet.add(ae);
		ae.subset().fsubsettingSet.add(super.assocEnd());
		
	}
	
	/**
     * Getter for a subseted association ends 
     * @return A set of association end that this association end subsets
     */
	public Set<MAssociationEnd> getSubseted() {
		return fsubsetedSet;
	}
	
	/**
	 * Transitive closure of subseted association ends.
	 * @return new Set(MAssociationEnd)
	 */
	public Set<MAssociationEnd> getAllSubseted() {
		Set<MAssociationEnd> result = new HashSet<MAssociationEnd>();
		result.addAll(fsubsetedSet);
		for (MAssociationEnd end : fsubsetedSet) {
			if (end.subset().getSubseted() != null) {
				result.addAll(end.subset().getAllSubseted());
			}
		}
		return result;
	}
	
	/**
	 * Transitive closure of subseting association ends.
	 * @return new Set(MAssociationEnd)
	 */
	public Set<MAssociationEnd> getAllSubseting() {
		Set<MAssociationEnd> result = new HashSet<MAssociationEnd>();
		result.addAll(fsubsettingSet);
		for (MAssociationEnd end : fsubsettingSet) {
			if (end.subset().getSubseted() != null) {
				result.addAll(end.subset().getAllSubseted());
			}
		}
		return result;
	}

	
	/**
     * Getter for a subseting association ends
     * @return A set of subseting association ends.
     */
	public Set<MAssociationEnd> getSubsetting() {
		return fsubsettingSet;
	}
	
    /**
     * Creates a Set of Associations that where subseted by an association
     * owning this association end.
     * @return Set(MAssociation)
     */
	public Set<MAssociation> getSubsettedAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fsubsetedSet);
	}
	
    /**
     * Creates a Set of Associations that subsetting an association
     * owning this association end.
     * @return Set(MAssociation)
     */
	public Set<MAssociation> getSubsettingAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fsubsettingSet);
	}
	
    public void validateSubset(MAssociationEnd subsettedEnd) throws MInvalidModelException {
    	String errStr = "Association end "+ subsettedEnd.name() + " can't be subseted by " 
					+ assocEnd().name()+" because ";
    	
    	//the class of the subseting associationEnd inherits from the class of the subseted associationEnd
    	if (!thisSideInheretanceTest(subsettedEnd)) {
    		throw new MInvalidModelException(errStr + "class " 
    												+ assocEnd().cls().name() 
    												+ " doesn't inherit from/has the same type as " 
    												+ subsettedEnd.cls().name());
    	}
    	
    	if (!associationInheirtanceTest(subsettedEnd)) {
    		throw new MInvalidModelException(errStr + "classes that contain " 
    												+ assocEnd().name() 
    												+ " asscoiationEnd as a roleName" 
    												+ "don't inherit from classes that contain "
    												+ subsettedEnd.name() 
    												+ " as a roleName.");
    	}
    	
    	//multiplicity test: the upper bound of a parent is greater or equal to the upper bound of a child  	
    	if (!subsetMultiplicityTest(subsettedEnd)) {
    		throw new MInvalidModelException(errStr + " multiplicity of '" 
    												+ assocEnd().name() 
    												+ "' isn't contained in" 
    												+ " the muliplicity of '"
    												+ subsettedEnd.name()
    												+ "'.");
    	}

    	
    }
    
    private boolean subsetMultiplicityTest(MAssociationEnd subsettedEnd) {
    	boolean yes = true;
    	MMultiplicity otherMult = subsettedEnd.multiplicity();
    	for (Range r : assocEnd().multiplicity().getRanges()) {   		
    		yes &= subsetContains(r.getUpper(), otherMult);
    	}
    	return yes;
    }
    
    
    public void subsetNotSameTypeTest(MAssociationEnd subsettedEnd) throws MInvalidModelException {
    	String errStr = "Association end "+ subsettedEnd.name() + " can't be subseted by " 
		+ assocEnd().name()+" because ";
    
    	List<MAssociationEnd> opposite = subsettedEnd.getAllOtherAssociationEnds(); 
    	for (MAssociationEnd end: opposite){
    		if (end.cls().equals(subsettedEnd.cls()) )
    			throw new MInvalidModelException(errStr + "The association" +
    					subsettedEnd.association().name() +
	    				" is recursive. There is no way to derive the subsetting relations between" +
	    				"the association ends");
    	}
    	 
    	
    	
    }
}

