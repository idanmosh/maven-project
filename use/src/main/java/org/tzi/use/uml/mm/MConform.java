package org.tzi.use.uml.mm;

import org.tzi.use.parser.SemanticException;
import org.tzi.use.uml.mm.MMultiplicity.Range;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * For each two classes that connected by hierarchy "conform" connects
 * their association ends.
 * Deprecated use SubsetTransformator instead
 * @author vitalik
 *
 */
@Deprecated
public class MConform extends MModifyAssociationEnd {
	
	/** association ends that where conformed by this association end ("parents")  */
    private final Set<MAssociationEnd> fconformedSet; 
    /** association ends that conform to this association end ("children") */
    private final Set<MAssociationEnd> fconformingSet; 
    @Deprecated
    public MConform(MAssociationEnd ae) {
    	super(ae);
    	fconformedSet = new TreeSet<MAssociationEnd>();
    	fconformingSet = new TreeSet<MAssociationEnd>();
	}

    /**
     * Adds conformed associationEnd and updates set of conforming associationEnds on
     * the other side.
     * Should be executed only if {@link #validateConform(MAssociationEnd)} didn't throw an exception.
     * @param ae - conformed association end
     * @see #canConformTo(MAssociationEnd)
     */
    @Deprecated
    public void addConformedAssocEnd(MAssociationEnd ae) {
    	ae.conform().fconformingSet.add(super.assocEnd());
    	fconformedSet.add(ae);
    }
        
    /**
     * Getter for a conformed association ends ("parents")
     * @return A set of conformed association ends.
     */
    @Deprecated
    public Set<MAssociationEnd> getConformed() {
    	return fconformedSet;
    }
    
    /**
     * Getter for a conforming association ends ("children" of this association end)
     * @return A set of redefining association ends.
     */
    @Deprecated
    public Set<MAssociationEnd> getConforming() {
    	return fconformingSet;
    }
    
    /**
     * Creates a Set of Associations that where conformed to an association
     * owning this association end.
     * @return Set(MAssociation)
     */
    @Deprecated
	public Set<MAssociation> getConformedAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fconformedSet);
	}
	

	/**
     * Creates a Set of Associations that conforming an association
     * owning this association end.
     * @return Set(MAssociation) 
     */
    @Deprecated
	public Set<MAssociation> getConformingAssociations() {
		return super.getAssociationsFromSetOfAssociationEnds(this.fconformingSet);	
	}
	
	/**
	 * Transitive closure of conformed association ends. "parents"
	 * @return new Set(MAssociationEnd)
	 */
    @Deprecated
	public Set<MAssociationEnd> getAllConformed() {
		Set<MAssociationEnd> ret = new HashSet<MAssociationEnd>();
		for (MAssociationEnd ae : this.getConformed()) {
			if (ae.conform().getConformed() == null) {
				ret.add(ae);
			}
			else {
				ret.addAll(ae.conform().getAllConformed());
				ret.add(ae);
			}
		}
		return ret;
	}
	
	/**
	 * Calculates conform strict range. <br>
	 * Conform strict range - Range (MAX(lower), MIN(upper)) where 'lower' and 'upper' are sets that
	 * correspond to lower and upper bounds of all parent associationEnds. 
	 * @return Range (MAX(lower), MIN(upper))
	 * @throws IllegalArgumentException if the resulting range is impossible.
	 */
    @Deprecated
	public Range getStrictRange() throws IllegalArgumentException {
		Range ret_range;
		if (this.getConformed() == null) {
			return this.assocEnd().multiplicity().getRange();	
		}	
		try {
			ret_range = this.assocEnd().multiplicity().getRange();
			for (MAssociationEnd ae : this.getAllConformed()) {
				ret_range =  MMultiplicity.getStrictRange(ret_range, ae.multiplicity().getRange());
			}
		}
		catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Resulting conformed stritct range is not possible "+ e.getMessage());
		}
		
		return ret_range;
	}
	
    
    /**
     * Returns Highest lower bound of all conformed association ends including this one.
     */
    @Deprecated
	public int getStrictLowerBound() {
		int highest_lower = this.assocEnd().multiplicity().getRange().getLower();
		//if this association end wasn't conformed 
		if (this.getConformed() == null) {
			return highest_lower;
		}
		for(MAssociationEnd ae: this.getAllConformed())
			highest_lower = MMultiplicity.getBiggerNumber(highest_lower, ae.multiplicity().getRange().getLower());
		
		return highest_lower;
	}

    /**
     * Returns Lowest upper bound of all conformed association ends including this one. 
     */	
    @Deprecated
	public int getStrictUpperBound() {
		int lowest_upper = this.assocEnd().multiplicity().getRange().getUpper();
		//if this association end wasn't conformed 
		if (this.getConformed() == null) {
			return lowest_upper;
		}
		for(MAssociationEnd ae: this.getAllConformed())
			lowest_upper = MMultiplicity.getSmallerNumber(lowest_upper, ae.multiplicity().getRange().getUpper());
		 
		return lowest_upper;
	}
	
	/**
	 * Tests that this associationEnd conforms to conformd association end.
	 * @param conformedAssEnd association end that this association end should conform to
	 * @throws MInvalidModelException 
	 * @throws SemanticException if there is a problem to validate the conform operation
	 */
    @Deprecated
	public void validateConform(MAssociationEnd conformedAssEnd) throws MInvalidModelException {
		String errStr = "Association end '"+ conformedAssEnd.name() 
										   + "' can't conform to association end '" 
										   + assocEnd().name()
										   +"' because ";
		//tests that classes on this side are connected by Inheritance
		if (!thisSideInheretanceTest(conformedAssEnd)) {
			throw new MInvalidModelException(errStr + "class '" 
													+ conformedAssEnd.cls().name() 
													+ "' is not a generalization of '" 
													+ assocEnd().cls().name()
													+ "'.");
		}
		
		//tests that each end of this association has matching end in conformed association 
		if (!associationInheirtanceTest(conformedAssEnd)) {
			throw new MInvalidModelException(errStr + "association '" 
													+ conformedAssEnd.association().name() 
													+ "' cant't be fitted to association '" 
													+ assocEnd().association().name() 
													+ "'.");
		}
		
		//tests that multiplicity conditions hold
		if (!conformMultiplicityTest(conformedAssEnd)) {
			throw new MInvalidModelException(errStr + "multiplicity doesn't fit.");
		}
	}
	
	/**
	 * Tests that maximum of this association-end-multiplicity is contained in 
	 * conformed association-end-multiplicity.
	 * @param conformedEnd conformed association end.
	 * @return true if conditions hold and false otherwise.
	 */
    @Deprecated
    private boolean conformMultiplicityTest(MAssociationEnd subsettedEnd) {
    	boolean yes = true;
    	MMultiplicity otherMult = subsettedEnd.multiplicity();
    	for (Range r : assocEnd().multiplicity().getRanges()) {   		
    		yes &= subsetContains(r.getUpper(), otherMult);
    	}
    	return yes;
    }
}




