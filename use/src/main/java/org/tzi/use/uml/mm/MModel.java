/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: MModel.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.uml.mm;

import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.graph.DirectedGraphBase;
import org.tzi.use.uml.ocl.type.EnumType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A Model is a top-level package containing all other model elements.
 * 
 * @version $ProjectVersion: 0.393 $
 * @author Mark Richters
 */
public class MModel extends MModelElementImpl {
	private Map fEnumTypes; // (String enum -> EnumType)
	private Map fClasses; // (String classname -> MClass)
	private Map fAssociations; // (String assocName -> MAssociation)
	private DirectedGraph fGenGraph; // (MClass/MGeneralization)
	private DirectedGraph fRedefGraph;
	private DirectedGraph fSubsetGraph;
	private Map fClassInvariants; // (String name -> MClassInvariant)
	private Map fPrePostConditions; // (String name -> MPrePostCondition)
	private String fFilename; // name of .use file
	private Map<String,MGeneralizationSet> fGeneralizationSets;
	private Set<MXor> fXors;
	private Set<String> assocSet;

	protected MModel(String name) {
		super(name);
		fEnumTypes = new TreeMap();
		fClasses = new TreeMap();
		fAssociations = new TreeMap();
		fGenGraph = new DirectedGraphBase();
		fRedefGraph = new DirectedGraphBase();
		fSubsetGraph = new DirectedGraphBase();
		fXors = new HashSet<MXor>();
		fClassInvariants = new TreeMap();
		fPrePostConditions = new TreeMap();
		fFilename = "";

		fGeneralizationSets = new TreeMap<String,MGeneralizationSet>();
		//assoSet contain the names of the association classes to check consistency 
		assocSet = new HashSet<String>();
	}

	public void setFilename(String filename) {
		fFilename = filename;
	}

	/**
	 * Returns the filename of the specification from which this model was read.
	 * May be empty if model is not constructed from a file.
	 */
	public String filename() {
		return fFilename;
	}

	/**
	 * Adds a class. The class must have a unique name within the model.
	 * 
	 * @exception MInvalidModelException
	 *                model already contains a class with the same name.
	 */
	public void addClass(MClass cls) throws MInvalidModelException {
		if (fClasses.containsKey(cls.name()))
			throw new MInvalidModelException("Model already contains a class `"
					+ cls.name() + "'.");
		fClasses.put(cls.name(), cls);
		fGenGraph.add(cls);
		cls.setModel(this);
	}

	/**
	 * Returns the specified class.
	 * 
	 * @return null if class <code>name</name> does not exist.
	 */
	public MClass getClass(String name) {
		return (MClass) fClasses.get(name);
	}

	/**
	 * Returns the specified association class.
	 * 
	 * @return null if class <code>name</code> does not exist.
	 */
	public MAssociationClass getAssociationClass(String name) {
		MClass cls = (MClass) fClasses.get(name);
		if (cls instanceof MAssociationClass) {
			return (MAssociationClass) cls;
		} else {
			return null;
		}
	}

	/**
	 * Returns a collection containing all associationclasses in this model.
	 * 
	 * @return collection of MAssociationClass objects.
	 */
	public Collection getAssociationClassesOnly() {
		Collection result = new ArrayList();
		//System.out.println("Test 1");
		Iterator it = fClasses.values().iterator();
		//System.out.println("Test 2");

		while (it.hasNext()) {
			MClass elem = (MClass) it.next();
			if (elem instanceof MAssociationClass) {
				result.add(elem);
			}
		}
		return result;
	}

	/**
	 * Returns a collection containing all classes in this model.
	 * 
	 * @return collection of MClass objects.
	 */
	public Collection classes() {
		return fClasses.values();
	}

    /**
     * Returns a collection containing all classes in this model.
     *
     * @return collection of MClass objects.
     */
    public Collection classesOnly() {
        Collection result = new ArrayList();
        Iterator it = fClasses.values().iterator();
        while (it.hasNext()) {
            MClass elem = (MClass) it.next();
            if (!(elem instanceof MAssociationClass)) {
                result.add(elem);
            }
        }
        return result;
    }
	/**
	 * Adds an association. The association must have a unique name within the
	 * model.
	 * 
	 * @exception MInvalidModelException
	 *                model already contains an association with the same name.
	 */
	public void addAssociation(MAssociation assoc)
			throws MInvalidModelException {
		if (assoc.associationEnds().size() < 2)
			throw new IllegalArgumentException("Illformed association `"
					+ assoc.name() + "': number of associationEnds == "
					+ assoc.associationEnds().size());

		if (fAssociations.containsKey(assoc.name()))
			throw new MInvalidModelException(
					"Model already contains an association named `"
							+ assoc.name() + "'.");

		// check for role name conflicts: for each class the set of
		// navigable classes must have unique role names
		Iterator it = assoc.associatedClasses().iterator();
		while (it.hasNext()) {
			MClass cls = (MClass) it.next();
			Map aends = cls.navigableEnds();
			List newRolenames = new ArrayList();

			Iterator it2 = assoc.navigableEndsFrom(cls).iterator();
			while (it2.hasNext()) {
				String newRolename = ((MNavigableElement) it2.next())
						.nameAsRolename();
				newRolenames.add( newRolename );
				if (aends.containsKey(newRolename)) {
					throw new MInvalidModelException("Association end `"
							+ newRolename
							+ "' navigable from class `"
							+ cls.name()
							+ "' conflicts with same rolename in association `"
							+ ((MNavigableElement) aends.get(newRolename))
							.association().name() + "'.");
				}
			}

			// tests if the rolenames are already used in one of the subclasses
			Iterator it3 = cls.allChildren().iterator();
			while ( it3.hasNext() ) {
				MClass subCls = (MClass) it3.next();
				for ( int i = 0; i < newRolenames.size(); i++ ) {
					String newRolename = (String) newRolenames.get(i);
					if ( subCls.navigableEnds().containsKey( newRolename ) ) {
						throw new MInvalidModelException("Association end `"
								+ newRolename
								+ "' navigable from class `"
								+ subCls.name()
								+ "' conflicts with same rolename in association `"
								+ ((MNavigableElement) subCls.navigableEnds()
										.get( newRolename ))
										.association().name() + "'.");
					}
				}
			}
		}

		// for each class register the association and the
		// reachable association ends
		it = assoc.associationEnds().iterator();
		while (it.hasNext()) {
			MAssociationEnd aend = (MAssociationEnd) it.next();
			MClass cls = aend.cls();
			// cls.addAssociation(assoc);
			cls.registerNavigableEnds(assoc.navigableEndsFrom(cls));
		}
		assoc.setModel(this);
		fAssociations.put(assoc.name(), assoc);
	}

	/**
	 * Returns a collection containing all associations in this model.
	 * 
	 * @return collection of MAssociation objects.
	 */
	public Collection associations() {
        return fAssociations.values();
	}


    public Collection<MAssociation> associations(boolean k){
        if (k){

            Collection<MAssociation> res = new ArrayList<MAssociation>();
            Iterator<MAssociation> iter = fAssociations.values().iterator();
            while(iter.hasNext()){
                MAssociation ma = iter.next();
                if (ma.isDeleteAssoc() == false){
                    res.add(ma);
                }
            }
            return res;
        }else
            return this.associations();
    }
	/**
	 * Returns the specified association.
	 * 
	 * @return null if association does not exist.
	 */
	public MAssociation getAssociation(String name) {
		return (MAssociation) fAssociations.get(name);
	}

	/**
	 * Returns the set of all associations that exist between the specified
	 * classes (inherited ones are not included). The arity of the returned
	 * associations is equal to <code>classes.size()</code>.
	 * 
	 * @return Set(MAssociation)
	 */
	public Set getAssociationsBetweenClasses(Set classes) {
		Set res = new HashSet();

		// search associations
		Iterator assocIter = fAssociations.values().iterator();
		while (assocIter.hasNext()) {
			MAssociation assoc = (MAssociation) assocIter.next();
			if (assoc.associatedClasses().equals(classes))
				res.add(assoc);
		}
		return res;
	}

	/**
	 * Returns the set of all associations that exist between the specified
	 * classes (including inherited ones). The arity of the returned
	 * associations is equal to <code>classes.size()</code>.
	 * 
	 * @return Set(MAssociation)
	 */
	public Set getAllAssociationsBetweenClasses(Set classes) {
		// NOT IMPLEMENTED YET
		return getAssociationsBetweenClasses(classes);
	}



	/**
	 * Adds a generalization from <code>child</code> to <code>parent</code>
	 * class.
	 * 
	 * @exception MInvalidModelException
	 *                generalization is not well-formed, e.g., a cycle is
	 *                introduced into the generalization hierarchy.
	 */
	public void addGeneralization(MGeneralization gen)
			throws MInvalidModelException {
		// generalization is irreflexive
		if (gen.isReflexive())
			throw new MInvalidModelException("Class\\Association `" + gen.child()
					+ "' cannot be a superclass of itself.");

		// check for cycles that might be introduced by adding the new
		// generalization
		if (fGenGraph.existsPath(gen.parent(), gen.child()))
			throw new MInvalidModelException(
					"Detected cycle in  hierarchy. Class\\Association `"
							+ gen.parent().name()
							+ "' is already a subclass\\subassociation of `"
							+ gen.child().name() + "'.");

		// FIXME: check for any conflicts that might be introduced by
		// the : (1) attributes with same name, (2)
		// inherited associations??

		// silently ignore duplicates
		fGenGraph.addEdge(gen);
	}

	/**
	 * Returns the generalization graph of this model.
	 * 
	 * @return a DirectedGraph with MClass nodes and MGeneralization edges
	 */
	public DirectedGraph generalizationGraph() {
		return fGenGraph;
	}

	public DirectedGraph redefineAssociationsGraph() {
		return fRedefGraph;
	}

	public DirectedGraph subsetAssociationsGraph() {
		return fSubsetGraph;
	}

	/**
	 * Adds an enumeration type.
	 * 
	 * @exception MInvalidModelException
	 *                model already contains an element with same name.
	 */
	public void addEnumType(EnumType e) throws MInvalidModelException {
		if (fEnumTypes.containsKey(e.name()))
			throw new MInvalidModelException("Model already contains a type `"
					+ e.name() + "'.");
		fEnumTypes.put(e.name(), e);
	}

	/**
	 * Returns an enumeration type by name.
	 * 
	 * @return null if enumeration type does not exist.
	 */
	public EnumType enumType(String name) {
		return (EnumType) fEnumTypes.get(name);
	}

	/**
	 * Returns an enumeration type for a given literal.
	 * 
	 * @return null if enumeration type does not exist.
	 */
	public EnumType enumTypeForLiteral(String literal) {
		Iterator it = fEnumTypes.values().iterator();
		while (it.hasNext()) {
			EnumType t = (EnumType) it.next();
			if (t.contains(literal))
				return t;
		}
		return null;
	}

	/**
	 * Returns a set of all enumeration types.
	 */
	public Set enumTypes() {
		Set s = new HashSet();
		s.addAll(fEnumTypes.values());
		return s;
	}

	/**
	 * Adds a class invariant. The class + invariant name must have a unique
	 * name within the model.
	 * 
	 * @exception MInvalidModelException
	 *                model already contains an invariant with same name.
	 */
	public void addClassInvariant(MClassInvariant inv)
			throws MInvalidModelException {
		String name = inv.cls().name() + "::" + inv.name();
		if (fClassInvariants.containsKey(name))
			throw new MInvalidModelException(
					"Duplicate definition of invariant `" + inv.name()
					+ "' in class `" + inv.cls().name() + "'.");
		fClassInvariants.put(name, inv);
	}

	/**
	 * Returns a collection containing all class invariants.
	 * 
	 * @return collection of MClassInvariant objects.
	 */
	public Collection classInvariants() {
		return fClassInvariants.values();
	}

	/**
	 * Returns a collection containing all invariants for a given class.
	 * 
	 * @return collection of MClassInvariant objects.
	 */
	public Set classInvariants(MClass cls) {
		Set res = new HashSet();
		Iterator it = fClassInvariants.values().iterator();
		while (it.hasNext()) {
			MClassInvariant inv = (MClassInvariant) it.next();
			if (inv.cls().equals(cls))
				res.add(inv);
		}
		return res;
	}

	/**
	 * Returns a collection containing all invariants for a given class and its
	 * parents.
	 * 
	 * @return collection of MClassInvariant objects.
	 */
	public Set allClassInvariants(MClass cls) {
		Set res = new HashSet();
		Set parents = cls.allParents();
		parents.add(cls);
		Iterator it = fClassInvariants.values().iterator();
		while (it.hasNext()) {
			MClassInvariant inv = (MClassInvariant) it.next();
			if (parents.contains(inv.cls()))
				res.add(inv);
		}
		return res;
	}

	/**
	 * Returns the specified invariant. The name must be given as "class::inv".
	 * 
	 * @return null if invariant <code>name</name> does not exist.
	 */
	public MClassInvariant getClassInvariant(String name) {
		return (MClassInvariant) fClassInvariants.get(name);
	}

	/**
	 * Adds a pre-/postcondition.
	 */
	public void addPrePostCondition(MPrePostCondition ppc)
			throws MInvalidModelException {
		String name = ppc.cls().name() + "::" + ppc.operation().name()
				+ ppc.name();
		if (fPrePostConditions.containsKey(name))
			throw new MInvalidModelException(
					"Duplicate definition of pre-/postcondition `" + ppc.name()
					+ "' in class `" + ppc.cls().name() + "'.");
		fPrePostConditions.put(name, ppc);
		if (ppc.isPre())
			ppc.operation().addPreCondition(ppc);
		else
			ppc.operation().addPostCondition(ppc);
	}

	/**
	 * Returns a collection containing all pre-/postconditions.
	 * 
	 * @return collection of MPrePostCondition objects.
	 */
	public Collection prePostConditions() {
		return fPrePostConditions.values();
	}

	/**
	 * Returns a string with some statistics about the model: Number of classes,
	 * associations, invariants, and operations.
	 */
	public String getStats() {
		String stats = " (";
		int n = classes().size();
		stats += n + " class";
		if (n != 1)
			stats += "es";
		n = associations().size();
		stats += ", " + n + " association";
		if (n != 1)
			stats += "s";
		n = classInvariants().size();
		stats += ", " + n + " invariant";
		if (n != 1)
			stats += "s";

		n = 0;
		Iterator it = classes().iterator();
		while (it.hasNext()) {
			MClass cls = (MClass) it.next();
			n += cls.operations().size();
		}
		stats += ", " + n + " operation";
		if (n != 1)
			stats += "s";

		n = fPrePostConditions.size();
		stats += ", " + n + " pre-/postcondition";
		if (n != 1) {
			stats += "s";
		}

		return "Model " + name() + stats + ")";
	}

	/**
	 * Process this element with visitor.
	 */
	public void processWithVisitor(MMVisitor v) {
		v.visitModel(this);
	}

	public void addGeneralizationSet(MGeneralizationSet gs){

		fGeneralizationSets.put(gs.name(), gs);
		
	}

	public Collection< MGeneralizationSet> getGeneralizationSets(){
		return fGeneralizationSets.values();
	}

	public void setXorSet(Set<MXor> xors) {
		this.fXors = xors;
	}

	public Set<MXor> getXorSet() {
		return fXors;
	}

	public void addXor(MXor mXor) {
		fXors.add(mXor);	
	}

	/**
	 * Getter for all association ends in the model.
	 * @return new Set(MAssociationEnd)
	 */
	public Set<MAssociationEnd> asssociationEnds() {
		Set<MAssociationEnd> associationEnds = new HashSet<MAssociationEnd>();
		for (MAssociation a: (Collection<MAssociation>)associations()) {
			associationEnds.addAll(a.associationEnds());
		}
		return associationEnds;
	}

	private void genStrForSubAndRed(MAssociationEnd end, StringBuilder ret) {
		if (end.isQualified()) {
			ret.append(" qualifier attributes");
			for (MAttribute at : end.getQualifier().getAttributes()) {
				ret.append(" " + at.name() + ":" + at.type());
			}
		}
		if (!end.redefine().getRedefined().isEmpty()) {
			ret.append(" { redefines " );
			for (MAssociationEnd redEnd : end.redefine().getRedefined()) {
				ret.append(" " + redEnd.otherSideAssocEnd().cls().name() +
						"." + redEnd.nameAsRolename());
			}
			ret.append("}");
		}
		if (!end.subset().getSubseted().isEmpty()) {
			ret.append(" { subsets");
			for (MAssociationEnd subEnd : end.subset().getSubseted()) {
				ret.append(" " + subEnd.otherSideAssocEnd().cls().name() +
						"." + subEnd.nameAsRolename());
			}
			ret.append("}");
		}
	}

	private void genStrForassocHierarchy(MAssociation asc, StringBuilder ret) {
		if (asc.parents().isEmpty()) return;
		ret.append(" <");
		MAssociation assoc;
		for (Object o : asc.parents()) {
			assoc = (MAssociation)o;
			ret.append(" " + assoc.name() + ", ");
		}
		ret.delete(ret.length()-2, ret.length()-1);
	}

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("model " + this.name() + "\n\n");

		for (String name : (Set<String>)this.fEnumTypes.keySet()) {
			EnumType e = (EnumType) this.fEnumTypes.get(name);
			ret.append("enum " + name + "{\n  ");
			Iterator it = e.literals();
			while (it.hasNext()) {
				String s = (String) it.next();
				ret.append(s + ", ");
			}
			ret.delete(ret.length() - 2, ret.length());
			ret.append("\n}\n\n");
		}

		for (final Object obj : this.classes()) {
			final MClass cls = (MClass) obj;
			if (obj instanceof MAssociationClass) {
				continue;
			}
			StringBuilder attributes = new StringBuilder();

			for (Object a : cls.allAttributes()) {
				final MAttribute atr = (MAttribute) a;
				if (cls.attribute(atr.name(), false) != null)
					attributes.append(" " + atr.name() + " : "
							+ atr.type().shortName() + "\n");
			}

			if (cls.parents().isEmpty())
				ret.append("class " + cls.name() + "\nattributes\n"
						+ attributes.toString() + "end\n\n");
			else {
				ret.append("class " + cls.name() + " < ");
				for (Object p : cls.parents()) {
					MClass prnt = (MClass)p;
					ret.append(prnt.name() + ", ");
				}
				ret.replace(ret.length()-2, ret.length()-1,"\n");
				ret.append("attributes\n"
						+ attributes.toString() + "end\n\n");
			}
		}

		for (final Object obj : this.associations(true)) {
			final MAssociation asc = (MAssociation) obj;

			if (asc instanceof MAssociationClass) {
				MAssociationClass ascls = (MAssociationClass) asc;
				ret.append("associationclass " + asc.name());
				if (!ascls.parents().isEmpty()) {
					ret.append(" < ");
					for (Object p : ascls.parents()) {
						MClass prnt = (MClass)p;
						ret.append(prnt.name() + ", ");
					}
					ret.delete(ret.length()-2, ret.length());
				}

				ret.append("\nbetween\n");
				MAssociationEnd end = (MAssociationEnd) asc
						.associationEnds().get(0);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename()); 

				genStrForSubAndRed(end, ret);

				ret.append("\n");
				end = (MAssociationEnd) asc.associationEnds().get(1);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename());

				genStrForSubAndRed(end, ret);



				ret.append("\nend\n\n");
			}
			else if (asc.aggregationKind() == MAggregationKind.NONE) {
				ret.append("association " + asc.name()); 
				genStrForassocHierarchy(asc, ret);
				ret.append("\nbetween\n");
				MAssociationEnd end = (MAssociationEnd) asc
						.associationEnds().get(0);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename());

				genStrForSubAndRed(end, ret);

				ret.append("\n");
				end = (MAssociationEnd) asc.associationEnds().get(1);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename()); 

				genStrForSubAndRed(end, ret);

				ret.append("\nend\n\n");
			}
			else if (asc.aggregationKind() == MAggregationKind.AGGREGATION) {
				ret.append("aggregation " + asc.name());
				genStrForassocHierarchy(asc, ret);
				ret.append("\nbetween\n");
				MAssociationEnd end = (MAssociationEnd) asc
						.associationEnds().get(0);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename() );

				genStrForSubAndRed(end, ret);

				ret.append("\n");
				end = (MAssociationEnd) asc.associationEnds().get(1);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename());

				genStrForSubAndRed(end, ret);

				ret.append("\nend\n\n");
			}
			else if (asc.aggregationKind() == MAggregationKind.COMPOSITION) {
				ret.append("composition " + asc.name()); 
				genStrForassocHierarchy(asc, ret);
				ret.append("\nbetween\n");
				MAssociationEnd end = (MAssociationEnd) asc
						.associationEnds().get(0);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename()); 

				genStrForSubAndRed(end, ret);

				ret.append("\n");
				end = (MAssociationEnd) asc.associationEnds().get(1);
				ret.append(" " + end.cls().name() + "["
						+ end.multiplicity().toString() + "]");
				ret.append(" role " + end.nameAsRolename());

				genStrForSubAndRed(end, ret);

				ret.append("\nend\n\n");
			}
		}

		for (MXor xor : this.fXors) {
			ret.append("xor " + xor.name() + " " + xor.getSourceClass().name() + ", ");
			for (MAssociationEnd end : xor.getEnds()) {
				ret.append(end.nameAsRolename() + ", ");
			}
			ret.delete(ret.length() - 2, ret.length());
			ret.append(";");
		}

		for (MGeneralizationSet gs : this.getGeneralizationSets()) {
			ret.append(gs.toString() + "\n" );
		}
		return ret.toString();
	}
	
	
	public void updateAssocClsIntersection(MModel model)
	{
		try
		{
			Collection<MAssociationClass> assocs = model.getAssociationClassesOnly();
			Iterator<MAssociationClass> iter = assocs.iterator();
			while (iter.hasNext())
			{
				MAssociationClass assoc = iter.next();
				if (!assocSet.contains(assoc.name()))
				{
					assocSet.add(assoc.name());
					Intersect(assoc);
				}

			}
		}
		catch (Exception e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

	}
	private void Intersect(MAssociationClass assoc) throws Exception
	{
		
		Iterator<MClass> iter = assoc.allParents().iterator();
		while (iter.hasNext())
		{
			MClass cls = iter.next();

			if (!(cls instanceof MAssociationClass))
				throw new Exception("Association Class Hierarchy Error");

			MAssociationClass newAsoc = (MAssociationClass) cls;
			if (!assocSet.contains(newAsoc.name()))
			{
				assocSet.add(newAsoc.name());
				Intersect(newAsoc);
			}

			MAssociationEnd ae1 = (MAssociationEnd) newAsoc.associationEnds()
					.get(0);
			MAssociationEnd ae2 = (MAssociationEnd) newAsoc.associationEnds()
					.get(1);

			MAssociationEnd baseAe1 = (MAssociationEnd) assoc.associationEnds()
					.get(0);
			MAssociationEnd baseAe2 = (MAssociationEnd) assoc.associationEnds()
					.get(1);

			if (baseAe1.cls().isSubClassOf(ae1.cls())
					& baseAe2.cls().isSubClassOf(ae2.cls()))
			{
				baseAe1.multiplicity().addRange(
						Math.max(baseAe1.multiplicity().getRange().getLower(),
								ae1.multiplicity().getRange().getLower()),
						Math.min(baseAe1.multiplicity().getRange().getUpper(),
								ae1.multiplicity().getRange().getUpper()));

				baseAe2.multiplicity().addRange(
						Math.max(baseAe2.multiplicity().getRange().getLower(),
								ae2.multiplicity().getRange().getLower()),
						Math.min(baseAe2.multiplicity().getRange().getUpper(),
								ae2.multiplicity().getRange().getUpper()));
			}
			else if (baseAe2.cls().isSubClassOf(ae1.cls())
					& baseAe1.cls().isSubClassOf(ae2.cls()))
			{
				baseAe1.multiplicity().addRange(
						Math.max(baseAe1.multiplicity().getRange().getLower(),
								ae2.multiplicity().getRange().getLower()),
						Math.min(baseAe1.multiplicity().getRange().getUpper(),
								ae2.multiplicity().getRange().getUpper()));

				baseAe2.multiplicity().addRange(
						Math.max(baseAe2.multiplicity().getRange().getLower(),
								ae1.multiplicity().getRange().getLower()),
						Math.min(baseAe2.multiplicity().getRange().getUpper(),
								ae1.multiplicity().getRange().getUpper()));
			}
			else
			{
				throw new Exception("Association Class Hierarchy Error");
			}
		}

	}
	
	
	
	
	
}
