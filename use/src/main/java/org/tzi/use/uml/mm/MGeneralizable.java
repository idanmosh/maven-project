package org.tzi.use.uml.mm;

import java.util.Set;

/**
 * Interface for all generalizable types.
 * 
 * @author vitalib
 *
 */

//TODO: change this interface to be generic
public interface MGeneralizable extends MModelElement {
	/**
     * Returns true if the class is marked abstract.
     */
    public boolean isAbstract();
    
    
    /**
     * Returns the set of all direct parent classes (without this
     * class).
     *
     * @return Set(MClass)
     */
    public Set parents();
    
    /**
     * Returns the set of all parent classes (without this
     * class). This is the transitive closure of the generalization
     * relation.
     *
     * @return Set(MClass)
     */
    public Set allParents();

    /**
     * Returns the set of all child classes (without this class). This
     * is the transitive closure of the generalization relation.
     *
     * @return Set(MClass)
     */
    public Set allChildren();

    /**
     * Returns the set of all direct child classes (without this
     * class).
     *
     * @return Set(MClass) 
     */
    public Set children();
    
    public String name();
}
