package org.tzi.use.uml.mm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
/**
 * This class represents the constrained generalization set of the model.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov <br>
 * Created on 17/10/2008<br>
 * Changed by Vitaly Bichov on 10/10/2010<br> 
 */
public class MGeneralizationSet extends MModelElementImpl {

	private List<MClass> fSubClasses;     // list of all subclasses names in the GS
	private MClass fSuperClass;           //The super class 
	private String fType;                 //one of the 8 possible types: overlapping, complete, incomplete,disjoint et'
	private String fName;                 //the name of the GS
	
	/**
	 * Constructor for the GS.
	 * @param name is the name of the generalization set, must be present according to
	 * UML meta model. Default name is given  if none supplied in the specification file.
	 */
	public MGeneralizationSet(String name) {
		super(name);
		fName = name;
		fSubClasses = new ArrayList<MClass>();
		fType = "overlapping"; //default option
	}
	
	public MGeneralizationSet(String name ,String type) {
		this(name);
		fType = type;
		
	}
	public String getSuperClassName(){
		return fName;
	}
	
	
	public MGeneralizationSet(String name, String type, MClass superClass) {
		this(name, type);
		fSuperClass = superClass;
	}

	public String getType() {
		return fType;
	}

	public void setType(String gsType) {
	    fType = gsType;
	}

	public String getGSName() {
		return fName;
	}

	public void setGSName(String gsName) {
		fName = gsName;
	}

	public void addSubClass(MClass cls){
		fSubClasses.add(cls);
	}
	
	public void addSubClassSet(List<MClass> cls){
		fSubClasses = cls;
	}
	
	
	public void addSubClassSet(Set<MClass> gsSet){
		for (MClass mClass : gsSet) {
			this.addSubClass(mClass);
		}
		
	}
	public List<MClass> getSubClasses(){
		return fSubClasses;
	}
	
	public void setSuperClass(MClass name){
		fSuperClass = name;
	}
	
	public MClass getSuperClass(){
		return fSuperClass;
	}
	
	public String toString(){
		String subClasses = fSubClasses.toString().substring(1, fSubClasses.toString().length() - 1);
		
		return "gs GSname " +  fName + " type " + fType 
		+" super " + fSuperClass.name() + " subClasses " + subClasses;
	}
	
	public boolean isDisjoint(){
		return fType.equalsIgnoreCase("disjoint");
	}
	
	public boolean isComplete(){
		return fType.equalsIgnoreCase("complete");
	}
	
	public boolean isIncomplete(){
		return fType.equalsIgnoreCase("incomplete");
	}
	
	public boolean isOverlapping(){
		return fType.equalsIgnoreCase("overlapping");
	}
	
	public boolean isDisjointComplete(){
		return fType.equalsIgnoreCase("disjoint_complete");
	}
	
	public boolean isDisjointIncomplete(){
		return fType.equalsIgnoreCase("disjoint_incomplete");
	}
	
	public boolean isOverlappingComplete(){
		return fType.equalsIgnoreCase("overlapping_complete");
	}
	
	public boolean isOverlappingIncomplete(){
		return fType.equalsIgnoreCase("overlapping_incomplete");
	}
	
	
	@Override
	public void processWithVisitor(MMVisitor v) {
		//TODO Auto-generated method stub
		
	}

	public List<String> getSubClassesNames() {
		List<String> l = new ArrayList<String>(fSubClasses.size());
		for (MClass cls : fSubClasses) {
			l.add(cls.name());
		}
		return l;
	}

}
