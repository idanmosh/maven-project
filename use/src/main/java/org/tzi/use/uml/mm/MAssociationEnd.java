/*
 * USE - UML based specification environment
 * Copyright (C) 1999-2004 Mark Richters, University of Bremen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// $Id: MAssociationEnd.java 61 2008-04-11 11:52:15Z opti $

package org.tzi.use.uml.mm;

import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.type.TypeFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** 
 * An AssociationEnd stores information about the role a class plays
 * in an association.
 *
 * @version     $ProjectVersion: 0.393 $
 */
public final class MAssociationEnd extends MModelElementImpl 
    implements MNavigableElement{
    
    private MAssociation fAssociation; // Owner of this association end
    private MClass fClass;  // associated class
    private MMultiplicity fMultiplicity; // multiplicity spec
    private int fKind; // none, aggregation, or composition
    private boolean fIsOrdered; // use as Set or Sequence
    private boolean fIsNavigable = true; 
    private boolean fIsExplicitNavigable = false;
    private MQualifier fQualifier; //Optional, may be null
    
    private boolean isPartOfXor;

    private final MSubset fsubset;
    private final MRedefine fredefine;
    private final MConform fconform;

    
    /** 
     * Creates a new association end. 
     *
     * @param cls       the class to be connected.
     * @param rolename  role that the class plays in this association.
     * @param mult      multiplicity of end.
     * @param kind      MAggregationKind
     * @param isOrdered use as Set or Sequence
     */
    public MAssociationEnd(MClass cls, 
                           String rolename, 
                           MMultiplicity mult, 
                           int kind,
                           boolean isOrdered) {
        super(rolename);
        fClass = cls;
        fMultiplicity = mult;
        setAggregationKind(kind);
        fIsOrdered = isOrdered;
        fsubset = new MSubset(this);
        fredefine = new MRedefine(this);
        fconform = new MConform(this);
        setPartOfXor(false);
    }
    
    /** 
     * Creates a new association end. 
     *
     * @param cls       the class to be connected.
     * @param rolename  role that the class plays in this association.
     * @param mult      multiplicity of end.
     * @param kind      MAggregationKind
     * @param isOrdered use as Set or Sequence
     * @param isExplicitNavigable needs to be true if this end is explicit
     *                            navigable
     **/
    public MAssociationEnd(MClass cls, 
                           String rolename, 
                           MMultiplicity mult, 
                           int kind,
                           boolean isOrdered,
                           boolean isExplicitNavigable) {
        super(rolename);
        fClass = cls;
        fMultiplicity = mult;
        setAggregationKind(kind);
        fIsOrdered = isOrdered;
        fIsExplicitNavigable = isExplicitNavigable;
        fsubset = new MSubset(this);
        fredefine = new MRedefine(this);
        fconform = new MConform(this);
        setPartOfXor(false);
    }
    

    private void setAggregationKind(int kind) {
        if (kind != MAggregationKind.NONE 
            && kind != MAggregationKind.AGGREGATION
            && kind != MAggregationKind.COMPOSITION )
            throw new IllegalArgumentException("Invalid kind");
        fKind = kind;
    }

    /**
     * Sets the owner association. This operation should be called by
     * an implementation of MAssociation.addAssociationEnd.
     *
     * @see MAssociation#addAssociationEnd
     */
    public void setAssociation(MAssociation assoc) {
        fAssociation = assoc;
    }

    /**
     * Returns the owner association.
     */
    public MAssociation association() {
        return fAssociation;
    }

    /**
     * Returns the associated class.
     */
    public MClass cls() {
        return fClass;
    }

    /**
     * Returns the multiplicity of this association end.
     */
    public MMultiplicity multiplicity() {
        return fMultiplicity;
    }

    /**
     * Returns the aggregation kind of this association end.
     */
    public int aggregationKind() {
        return fKind;
    }

    /**
     * Returns true if this association end is ordered.
     */
    public boolean isOrdered() {
        return fIsOrdered;
    }

    public boolean isExplicitNavigable() {
        return fIsExplicitNavigable;
    }
    public void setNavigable( boolean isNavigable ) {
        fIsNavigable = isNavigable;
    }
    public boolean isNavigable() {
        return fIsNavigable;
    }
    
    /**
     * Process this element with visitor.
     */
    public void processWithVisitor(MMVisitor v) {
        v.visitAssociationEnd(this);
    }

    public int hashCode() { 
        return name().hashCode() + fAssociation.hashCode() + fClass.hashCode();
    }

    /**
     * Two association end are equal iff they connect the same class
     * in the same association with the same role name.  
     */
    public boolean equals(Object obj) { 
        if (obj == this )
            return true;
        if (obj instanceof MAssociationEnd ) {
            MAssociationEnd aend = (MAssociationEnd) obj;
            return name().equals(aend.name()) 
                && fAssociation.equals(aend.association())
                && fClass.equals(aend.cls());
        }
        return false;
    }

    //////////////////////////////////////////////////
    // IMPLEMENTATION OF MNavigableElement
    //////////////////////////////////////////////////

    public Type getType( MNavigableElement src ) {
        Type t = TypeFactory.mkObjectType( cls() );
        if ( src.equals( src.association() ) ) {
            return t;
        }
        if ( multiplicity().isCollection() ) {
            if ( isOrdered() )
                t = TypeFactory.mkSequence( t );
            else
                t = TypeFactory.mkSet( t );
        } else if ( association().associationEnds().size() > 2 ) {
            t = TypeFactory.mkSet(t);
        }
        return t;
    }

    public String nameAsRolename() {
        return name();
    }

    /**
     * @return List(MAssociationEnd)
     */
    public List<MAssociationEnd> getAllOtherAssociationEnds() {
        List<MAssociationEnd> nMinusOneClasses = new ArrayList();
        for (Iterator<MAssociationEnd> it = fAssociation.associationEnds().iterator(); it.hasNext();) {
            MAssociationEnd end = (MAssociationEnd) it.next();
            if (end != this) nMinusOneClasses.add(end);
        }
        return nMinusOneClasses;
    }
    
      
    public void setQualifier(MQualifier qualifier){
    	fQualifier = qualifier;
    }
    
    public MQualifier getQualifier(){
    	return fQualifier;
    }
    
    public boolean isQualified(){
    	return (fQualifier != null);
    }
   
    //////////////////////////////////////
    //////subset & redefine &conform /////
    //////////////////////////////////////
    /**
     * Getter to subset object.
     * If this association end wasn't subseted then <code>this.subset().getSubsetedSet()</code> will return
     * <code>null</code>. 
     * @return subset object.
     */    
    public MSubset subset() {
    	return fsubset;
    }

    /**
     * Getter to redefine object.
     * If this association end wasn't redefined then <code>this.redefine().getRedefinedSet()</code> will return
     * <code>null</code>. 
     * @return redefine object.
     */    
    public MRedefine redefine() {
    	return fredefine;
    }
  
    /**
     * Getter to conform object.
     * If this association end wasn't conformed then <code>this.conform().getConformedSet()</code> will return
     * <code>null</code>. 
     * @return conform object.
     */
    public MConform conform() {
    	return fconform;
    }
    
    /**
     * Returns the association end form the other side.
     * The main assumption is that the association binary.
     * @return MassociationEnd form the other side of the association.
     */
    public MAssociationEnd otherSideAssocEnd() {
    	if (association().associationEnds().get(0) == this) {
    		return (MAssociationEnd)association().associationEnds().get(1);
    	}
    	return (MAssociationEnd)association().associationEnds().get(0);
    }
    
    /**
     * Returns strict range composed form both "conform" and "redefine" strict ranges.
     * @return srict range of this association end.
     * 
 	 * @see  MMultiplicity.getStrictRange
     */
    public Range getStrictRange() throws IllegalArgumentException{
       	return MMultiplicity.getStrictRange(this.redefine().getStrictRange(), this.conform().getStrictRange());
    }

	public void setPartOfXor(boolean isPartOfXor) {
		this.isPartOfXor = isPartOfXor;
	}

	public boolean isPartOfXor() {
		return isPartOfXor;
	}




    
    

}