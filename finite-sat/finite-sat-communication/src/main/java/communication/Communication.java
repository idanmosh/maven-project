package communication;


import bgu.cmd.CMDArgs;
import bgu.detection.FiniteSatDetector;
import bgu.identification.FiniteSatIdentifier;
import bgu.propagation.DisjointConstraintPropogator;
import bgu.simplification.GraphConstructor;
import bgu.simplification.SimplificationHandler;
import bgu.translation.Translate;
import bgu.circleCheck.CircleChecker;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MGeneralizationSet;
import java.util.List;

/**
 * Created by user on 07/02/15.
 */


public class Communication implements communicateIntr{
    private static Communication communication;
    private MModel model=null;
    private SimplificationHandler sh = null;
    private Translate tr =null;
    private FiniteSatDetector detector = null;
    private FiniteSatIdentifier identifier = null;
    private CircleChecker checker = null;
    private DisjointConstraintPropogator propogateDisjoint = null;

    public static Communication getInstance(){
        if(communication == null)
            return new Communication();
        return communication;
    }
    private Communication(){    }

    public void setModel(MModel m){
        this.model = m;
    }

    public MModel getModel(){
        return this.model;
    }
    public String communicate(CMDArgs o, int msgNum){
        String msg = "";
        switch (o){
            case detect:
                msg = detect(msgNum);
                break;
            case identify:
                msg = identify(msgNum);
                break;
            case propogate:
                msg = propagate(msgNum);
                break;
            case circles:
                msg = circles(msgNum);
                break;
            case xorConform:
                break;
            case manSubset:
                break;
            case simplify:
                msg = simplify(msgNum);
                break;
            case translate:
                msg = translate(msgNum);
                break;
            case translateMinus:
                msg = translateMinus(msgNum);
        }
        return msg;
    }



    private String propagate(int msgNum) {
        String msg="";
        propogateDisjoint = DisjointConstraintPropogator.getNewInstance();
        this.model = propogateDisjoint.propogate(this.model);
        msg += propogateDisjoint.getDisjointGraph().toString() +"\n"+
                propogateDisjoint.getDisjointCliquesMap().toString();
        List<MGeneralizationSet> newAddedGS = propogateDisjoint.getUpdatedModelBuilder().getNewDisjointGSs();
        List<MGeneralizationSet> newupdatedGS = propogateDisjoint.getUpdatedModelBuilder().getUpdatedDisjointGSs();
        msg +="Done!\n\nThe new added GSs are: \n";
        if (newAddedGS.isEmpty()) msg+="There Is no added new GSs";
        else{
            for (MGeneralizationSet newGS : newAddedGS){
                msg+=newGS.toString();
            }
        }
        msg+="\nThe new updated GSs are: \n";
        if (newupdatedGS.isEmpty()) msg+="There Is no updated GSs";
        else{
            for (MGeneralizationSet newGS : newupdatedGS){
                msg+=newGS.toString();
            }
        }
        return msg;

    }

    private String circles(int msgNum) {
        String msg="";
        if(checker == null){
            checker = new CircleChecker(model);
        }
        if (msgNum == 0){
            checker.check();
        }
        else if (msgNum ==1){
            boolean hasDangerousCycle = checker.existCircle();
            if (hasDangerousCycle){
                msg += "\nWarning: Please be aware that the model can be " +
                        "not finitely satisfiable since it class hierarchy cycles with disjoint or complete constraints.";
            }

        }
        else if(msgNum == 2){
            boolean hasDangerousCycle = checker.existCircle();
            if(hasDangerousCycle){
                msg+="The model is verified as finitely satisfiable. There is no need for identification !\n" +
                       "Warning: Please be aware that the model can be not finitely satisfiable since it" +
                        "includes \n" + "class hierarchy cycles with disjoint or complete constraints.\n"+
                        "*Note*, the model includes elements that currently are not handled by the FiniteSatUSEs identification method\n";
            }
            else{
                msg+="The model is finitly satisfiable. There is no need for identification\n";
            }
        }
        return msg;
    }

    private String identify(int msgNum) {
        String msg="";
        identifier = new FiniteSatIdentifier();
        if (identifier.identify(model)){
            msg+="Identification complete\n";
        }
        else
           msg = "Identification Failed: The class diagram is not finitely satisfiabile\n"+
            "However, it includes elements that currently are not handled by the FiniteSatUSEs identification method!";
        return msg;
    }

    private String detect(int msgNum) {
        String msg="";
        detector = new FiniteSatDetector();
        boolean isFSat =  detector.isFinitelySatisfiable(model);
        msg = isFSat ? "true#": "false#";
        if (isFSat){
            if(msgNum == 0){
                msg += "The model is finitly satisfiable! \n";
                msg += circles(1);
            }
            else if(msgNum == 1){
                msg+=circles(2);
            }
            else if(msgNum == 2){
                circles(0);
            }
        }
        else{
            if (msgNum == 0){
                msg += "The model is not finitly satisfiable";
            }
            else if (msgNum == 1){
                msg += identify(msgNum);
            }
            else if (msgNum == 2){
                msg+="The model is not finitly satisfiable. There is no need for class hierarchy cycles identification";
                msg+="Do you still want to show the cycles (yes/no)";
            }
        }
        return msg;
    }

    private String translate(int msgNum) {
        String msg="";
        tr = Translate.getInstance();
        this.model = tr.translate(this.model);
        msg = "Translate complete\n";
        return msg;
    }

    private String translateMinus(int msgNum) {
        String msg="";
        tr = Translate.getInstance();
        this.model = tr.translateMinus1(this.model);
        msg = "Translate -1 complete\n";

        return msg;
    }

    private String simplify(int msgNum) {

        String msg="";
        switch (msgNum){
            case 0:
                if (this.sh == null){
                    GraphConstructor gc = new GraphConstructor(this.model);
                    this.sh = new SimplificationHandler(gc.buildGraph(),this.model);
                }
                msg = simplifyMsg();
                break;
            case 1://if client want to tightened the model
                try{
                    model= sh.tightened();
                    sh.getCurrentCircle();
                    msg = simplifyMsg();
                }catch (Exception e){
                    msg = "Invalid Model";
                }
                break;
        }
        return msg;
    }

    private String simplifyMsg() {
        String msg="";
        if(sh.hasDangerousCircle()){
            msg = "There is a redundancy problem in this model.\n";
            msg+=sh.getCurrentCircle().redundancyDescription();
            msg+="\nWould you like to tighten this model(y/n)?\n";
        }
        else{
            msg = "There is NOT a redundancy problem in this model.\n";
        }
        return msg;
    }


}
