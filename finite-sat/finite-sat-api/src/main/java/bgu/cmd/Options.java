package bgu.cmd;


public class Options {

		private static boolean xorModelIsOveriding;
		private static boolean autoConformIsOn;
	
	static {
		xorModelIsOveriding = false;
		autoConformIsOn = true;
	}
	
	public static boolean getXorModelIsOveriding() {
		return xorModelIsOveriding;
	}
	
	public static boolean getAutoConformIsOn() {
		return autoConformIsOn;
	}
	
	public static void setAutoConformIsOn(boolean autoConformIsOn) {
		Options.autoConformIsOn = autoConformIsOn;
	}
	
	public static void setXorModelIsOveriding(boolean xorModelIsOveriding) {
		Options.xorModelIsOveriding = xorModelIsOveriding;
	}

}