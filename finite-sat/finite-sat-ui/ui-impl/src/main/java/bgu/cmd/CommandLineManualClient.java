package bgu.cmd;

import bgu.util.*;
import org.tzi.use.uml.mm.MModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.LinkedHashMap;

public class CommandLineManualClient {


	/**
	 * Translates strings given as command line args to java structures.
	 * @param args String array of argumants that shuld contain commands and variables
	 * such as as source file name. 
	 * @return java representation of the input.
	 */
	private Map<CMDArgs, String> parse(String[] args) {
		Map<CMDArgs, String> parcedInput = new LinkedHashMap<CMDArgs, String>();
		for (int i = 0; i < args.length ; i++) {

		//"-s" means source, next argument is source file name
			if (args[i].equals("-f")){
				parcedInput.put(CMDArgs.src, args[++i]);
			}
			//"-save" means target, next argument is target file name
			else if (args[i].equals("-save")) {
				parcedInput.put(CMDArgs.tar, args[++i]);
			}
            else if (args[i].equals("-Tr")){
                parcedInput.put(CMDArgs.translate, args[i]);
            }
            else if (args[i].equals("-Sim")){
                parcedInput.put(CMDArgs.simplify, args[i]);
            }
            else if (args[i].equals("-Tr-1")){
                parcedInput.put(CMDArgs.translateMinus, args[i]);
            }
			else if (args[i].equals("-xorovveriding")) {
				parcedInput.put(CMDArgs.xorConform, "true");
			}
			else if (args[i].equals("-autoSubsetOff")) {
				parcedInput.put(CMDArgs.manSubset, "true");
			}
			//if not source and target or other flag and starts with "-" then this is a command
			else if (args[i].startsWith("-")) {
				String str = args[i];

				for (int j = 1 ; j < str.length(); j++) {
					if (str.charAt(j) == 'D') {

						parcedInput.put(CMDArgs.detect, "true");
					}
					else if (str.charAt(j) == 'I') {
						parcedInput.put(CMDArgs.identify, "true");
					}
					else if (str.charAt(j) == 'P') {
						parcedInput.put(CMDArgs.propogate, "true");
					}

					else if  (str.charAt(j)=='C'){
						parcedInput.put(CMDArgs.circles, args[1]);
					}
					else if  (str.charAt(j)=='S'){
						parcedInput.put(CMDArgs.show, args[1]);
					}
					else errorParse(i, args);

				}
			}
			else {
				errorParse(i, args);
			}
		}
		return parcedInput;
	}

	private void errorParse(int i, String[] args) {
		errorExit("Error: The " 
				+ i 
				+ " command line argment '" 
				+ args[i] 
				       + " is not recognized");

	}

	private void errorExit(String s) {
		System.err.println(s);
		System.exit(-1);
	}

	private void assertValid(Map<CMDArgs, String> parcedArgs) {
		if (!parcedArgs.containsKey(CMDArgs.src)) {
			
			errorExit("Error: on sorce file");
		}

		File srcFile = new File(parcedArgs.get(CMDArgs.src));
		if  (!srcFile.exists()) {
			errorExit("Error: file '" + parcedArgs.get(CMDArgs.src) + "' not found");
		}

		
		/*
		 * TODO: Consider removing the marked code. 
		 */
		
		
/*		if (parcedArgs.containsKey(CMDArgs.tar) &&
				!parcedArgs.containsKey(CMDArgs.propogate)) {
			errorExit("Error: " +
			"Target file should come only with propogation(-P) flag rasied.");
		}*/
	}

	public void runClient(String[] args) throws FileNotFoundException {
		Util u = Util.getUtil();

		Map<CMDArgs, String> parcedArgs = this.parse(args);
		assertValid(parcedArgs);
		MModel model = u.compile(parcedArgs.get(CMDArgs.src));
		if (model == null) {
			this.errorExit("Error: can't contunue due to compolation errors.");
		}
		
		
		Receiver receiver = new Receiver(model,parcedArgs);
		try  {
			receiver.run();
		}
		catch (Exception e){
			this.errorExit("Error: " + e.getMessage() + " can't contunue...");
		}
	}


	private static void EnterWelcomeMenu() {
		System.out.println("\n\n                Welcome to the FiniteSatUSE- 2011");
		System.out.println("===========================================================\n");

	}
	

	public static void main(String[] args) {

		EnterWelcomeMenu();		
		CommandLineManualClient c = new CommandLineManualClient();
		Scanner _in;
		
	
		_in = new Scanner(System.in);



		String filePath ;


		while (true){
			
			System.out.println("\nPlease enter a USE file path to examine or type end to exit:");
			filePath = _in.nextLine();
			
			if (filePath.equalsIgnoreCase("end")){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				System.out.println("                            Bye");
				System.out.println("           BGU Modeling Group Development Team 2011");
				System.out.println("              http://www.cs.bgu.ac.il/~modeling/");
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				System.exit(0); 
			}

			filePath = "-f ".concat(filePath); 


			System.out.println("\nPlease enter one of the following commands:");


			System.out.println(optionMenu);

			String commands = _in.nextLine();

			commands = filePath.concat(" ").concat(commands);

			//convert input to String[]
			args = commands.split(" ");
			


			try {
				c.runClient(args);
			} catch (FileNotFoundException e) {
				System.err.println(e);
				e.printStackTrace();
			}
		}
	}


	
	void PrintPrimaryMenu() {
		System.out.println("Please enter one of the following commands:");
		System.out.println("-1- Open [USE file path]");
		System.out.println("-2- Show [USE file path] ");
		System.out.println("-3- Exit\r\n");
	}




	private static final String optionMenu =

		"--------------------------------------------------------------------------\n" +
		"-P               Propagate the disjoint constraints within the class\n" +
		"                 hierarchy cycles .\n" +
		"-save <file name>Save the modified model (should be used only with -P \n" +
		"                 flag raised).\n" +
		"-S               Show the model. \n" +
		"-D               Detect finite satisfiability problems.\n" +
		"-I               Identify cause of finite satisfiability problems \n" +
		"                 (critical cycles).\n" +
		"-C               Identify class hierarchy cycles with disjoint or \n" +
		"                 complete constraints \n" +
        "-Sim             Simplification \n" +
        "-Tr              Translation\n"+
        "-Tr-1            Reverse Translation (Should be used only with -Tr)\n"+
		"-----------------------------------------------------------------------------";


}
