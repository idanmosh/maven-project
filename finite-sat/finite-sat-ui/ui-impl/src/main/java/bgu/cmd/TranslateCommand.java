package bgu.cmd;


import bgu.util.Util;
import communication.Communication;



/**
 * Created by user on 23/12/14.
 */
public class TranslateCommand extends CommandImpl implements  Command {

    @Override
    public void openCommunication() {

        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());
        Util util = Util.getUtil();
        String msg = comm.communicate(CMDArgs.translate ,0);

        util.print(msg);
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes
    }
}
