package bgu.cmd;

import bgu.circleCheck.CircleChecker;
import bgu.detection.FiniteSatDetector;
import bgu.util.Util;
import communication.Communication;

/**
 * This class should be called RecognizeCommand but Mira 
 * changed the names of the actions performed
 * @author vitalib 
 *
 */

public class DetectCommand extends CommandImpl implements Command {
	private boolean isFSat;

	public boolean isFSat() {
		return isFSat;
	}

	@Override
	public void openCommunication() {
		//long start = System.currentTimeMillis();
        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());
        Util util = Util.getUtil();
        String msg = comm.communicate(CMDArgs.detect ,0);
        String[] token = msg.split("#");
        util.print(token[1]);
        isFSat = Boolean.getBoolean(token[0]);
		//Util.getUtil().print("\nDetection execution time: "+(end-start)+" ms.");
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes
	}

}
