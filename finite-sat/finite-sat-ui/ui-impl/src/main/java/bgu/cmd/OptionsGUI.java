package bgu.cmd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class OptionsGUI extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4627712912162336217L;
	JButton xorModelButton;
    JButton autoSubsetButton;
    

    
    public OptionsGUI() {
    	
        optionsLayout customLayout = new optionsLayout();

        getContentPane().setFont(new Font("Helvetica", Font.PLAIN, 12));
        getContentPane().setLayout(customLayout);

        xorModelButton = new JButton(getTextForXorModelButton());
        getContentPane().add(xorModelButton);

        autoSubsetButton = new JButton(getTextForAutoConformButton());
        getContentPane().add(autoSubsetButton);
        
        ActionListener al = new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == autoSubsetButton) {
					Options.setAutoConformIsOn(!Options.getAutoConformIsOn());
					autoSubsetButton.setText(getTextForAutoConformButton());
				}
				if (e.getSource() == xorModelButton) {
					Options.setXorModelIsOveriding(!Options.getXorModelIsOveriding());
					xorModelButton.setText(getTextForXorModelButton());
				}
			}
		};
		
		autoSubsetButton.addActionListener(al);
		xorModelButton.addActionListener(al);

        setSize(getPreferredSize());

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    
    
	private String getTextForAutoConformButton() {
		return (Options.getAutoConformIsOn()) ?
				"Auto Conform is on" : "Auto Conform is off";
	}

	private String getTextForXorModelButton() {
		if (Options.getXorModelIsOveriding()) {
			return "Xor Model is Overiding";
		}
		return "Xor Model is Conforming"; 
	}


    public static void main(String args[]) {
        OptionsGUI window = new OptionsGUI();

        window.setTitle("options");
        window.pack();
        window.setVisible(true);
    }
}

class optionsLayout implements LayoutManager {

    public optionsLayout() {
    }

    public void addLayoutComponent(String name, Component comp) {
    }

    public void removeLayoutComponent(Component comp) {
    }

    public Dimension preferredLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);

        Insets insets = parent.getInsets();
        dim.width = 313 + insets.left + insets.right;
        dim.height = 127 + insets.top + insets.bottom;

        return dim;
    }

    public Dimension minimumLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);
        return dim;
    }

    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();

        Component c;
        c = parent.getComponent(0);
        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+8,288,40);}
        c = parent.getComponent(1);
        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+56,288,40);}
    }
}

