package bgu.cmd;

import org.tzi.use.uml.mm.MModel;

public abstract class CommandImpl implements Command {

	private static MModel model;

	protected static MModel getModel() {
		return model;
	}
	
	protected static void setModel(MModel model) {
		CommandImpl.model = model;
	}
}
