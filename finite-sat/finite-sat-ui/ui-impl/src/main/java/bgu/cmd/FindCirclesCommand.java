package bgu.cmd;

import bgu.circleCheck.*;
import bgu.util.*;
import communication.Communication;
import org.tzi.use.uml.mm.MModel;

import java.util.Scanner;

public class FindCirclesCommand extends CommandImpl implements Command {
	

	//MModel _model;
	//CircleChecker _checker;
	
	public FindCirclesCommand(){
		//_model = super.getModel();
		
		//_checker = new CircleChecker(_model);
	}

	@Override
	public void openCommunication() {
		long start = System.currentTimeMillis();
        String msg = "";
        Util util = Util.getUtil();
        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());
        msg = comm.communicate(CMDArgs.detect,2);
		if (msg.startsWith("The model is not")){
            util.print(msg);
            msg = util.read();
			if (msg.equalsIgnoreCase("yes")){
                comm.communicate(CMDArgs.circles, 0);
			}
		}
        else{
            util.print(msg);
        }
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes
		//long end = System.currentTimeMillis();
		//Util.getUtil().print("\nTotal identification time:" + (end-start)+" ms.");
	}
}
