package bgu.cmd;

import bgu.circleCheck.CircleChecker;
import bgu.identification.FiniteSatIdentifier;
import bgu.util.Util;
import communication.Communication;
//import bgu.reasoning.FiniteSatIdentifier_New;

/**
 * This class should be called DetectCommand but Mira 
 * changed the names of the actions performed
 * @author vitalib 
 *
 */
//
public class IdentifyCommand extends CommandImpl implements Command {


		
	@Override
	public void openCommunication() {
		long start = System.currentTimeMillis();
        String msg ="";
        Util util = Util.getUtil();
        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());

        msg = comm.communicate(CMDArgs.detect,1);
        String[] token = msg.split("#");
        util.print(token[1]);
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes

//		DetectCommand dc = new DetectCommand();
//		if (!dc.detect()) {
//			FiniteSatIdentifier identifier = new FiniteSatIdentifier();
//
//			if (identifier.identify(super.getModel()))
//				Util.getUtil().print("Identification Complete");
//			else
//				Util.getUtil().print("Identification Failed: The class diagram is not finitely satisfiabile\n"+
//						"However, it includes elements that currently are not handled by the FiniteSatUSEs identification method!");
//		}
//		else {
//			CircleChecker checkDangerousCycle = new CircleChecker(super.getModel());
//			boolean hasDangerousCycle = checkDangerousCycle.existCircle();
//			if (!hasDangerousCycle)
//				Util.getUtil().print("The model is finitly satisfiable. There is no need for identification");
//			else{
//				Util.getUtil().print("The model is verified as finitely satisfiable. There is no need for identification !\n" +
//						"Warning: Please be aware that the model can be not finitely satisfiable since it" +
//						"includes \n" + "class hierarchy cycles with disjoint or complete constraints.");
//
//
//				Util.getUtil().print("\n*Note*, the model includes elements that currently are not handled by the FiniteSatUSEs identification method");
//
//
//			}
//		}
//		long end = System.currentTimeMillis();
//		Util.getUtil().print("\nTotal identification time:" + (end-start)+" ms.");
	}
}
