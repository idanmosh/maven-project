package bgu.cmd;

import bgu.transformation.ModelTransformator;
import bgu.transformation.Transformator;
import bgu.util.Util;
import org.tzi.use.uml.mm.MModel;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map;

public class Receiver implements Runnable {
	
	private Map<CMDArgs, String> input;
	private Boolean detectionResult = false;
	public Receiver (MModel model,Map<CMDArgs, String> input) {
		CommandImpl.setModel(model);
        this.input = input;
	}
	
	public void identifyAction() {
		action(new IdentifyCommand());
	}
	
	public void propogateAction() {
		action(new PropogateCommand());
		
	}
    public void simplifyAction() {
        action(new SimplifyCommand());

    }
    public void translateAction() {
        action(new TranslateCommand());

    }
    public void translateMinusAction() {
        action(new TranslateMinusCommand());

    }
	public void propogateAction(PrintStream target) {
		action(new PropogateCommand());
		Util util = Util.getUtil();
		PrintStream tmp = util.getOut();
		util.setOut(target);
		util.print(CommandImpl.getModel().toString());
		util.getOut().flush();
		util.setOut(tmp);
	}

	public void circleAction() {
		action(new FindCirclesCommand());
	}

	public void detectAction() {
		DetectCommand detectorFS = new DetectCommand();
		action(detectorFS);
		detectionResult = detectorFS.isFSat();
	}
	
	private void action(Command cmd) {
		
		cmd.openCommunication();
		Util.getUtil().getOut().flush();
		
	}
	
	public void run() {
		long start = System.currentTimeMillis();
		Util util = Util.getUtil();
		boolean detectResult = false;
		
		if (input.containsKey(CMDArgs.manSubset)) {
			Options.setAutoConformIsOn(false);
		}
	
		
		if (input.containsKey(CMDArgs.xorConform)) {
			Options.setXorModelIsOveriding(true);
		}
		
		transformModel();

		//System.out.println(input.toString());
        for (Map.Entry<CMDArgs, String> entry : input.entrySet()) {
          //  System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

            if (entry.getKey().compareTo(CMDArgs.src) == 0)
                continue;
            if (entry.getKey().compareTo(CMDArgs.propogate) == 0){
                util.print("Propagation:");
                propogateAction();
                continue;
            }
            if (entry.getKey().compareTo(CMDArgs.show) == 0){
                util.print("The Model:");
                util.print(CommandImpl.getModel().toString());// getModel() always return the most up-to-date model
                continue;
            }
            if(entry.getKey().compareTo(CMDArgs.detect) == 0) {
                util.print("Detection:");
                detectAction();
                continue;
            }
                if(entry.getKey().compareTo(CMDArgs.identify) == 0) {
                     util.print("Identification:");
                    identifyAction();
                    continue;
                }
            if(entry.getKey().compareTo(CMDArgs.circles) == 0) {
                util.print("Class Hierarchy Cycles Identification:");
                circleAction();
                continue;
            }
            if(entry.getKey().compareTo(CMDArgs.simplify) == 0) {
                util.print("Simplification:");
                simplifyAction();
                continue;
            }
            if(entry.getKey().compareTo(CMDArgs.translate) == 0) {
                util.print("Translation:");
                translateAction();
                continue;
            }if(entry.getKey().compareTo(CMDArgs.translateMinus) == 0) {
                util.print("Translation-1:");
                translateMinusAction();
                continue;
            }
            else {
                if (entry.getKey().compareTo(CMDArgs.tar) == 0) {
                    try{
                        PrintStream stream = new PrintStream(input.get(CMDArgs.tar));
                        PrintStream tmp = util.getOut();
                        util.setOut(stream);
                        util.print(CommandImpl.getModel().toString());
                        util.getOut().flush();
                        util.setOut(tmp);
                        util.print("\nThe file has been saved");
                    }catch(FileNotFoundException e){

                    }
                }
            }
        }

		long end = System.currentTimeMillis();
		util.print("\n---------------------------------------------------------------------------------");
		util.print("Overall execution time: "+(end-start)+" ms.");
		util.print("---------------------------------------------------------------------------------");
		util.getOut().flush();
	}
		
	

	private void transformModel() {
		Transformator t = ModelTransformator.getInstance();
		CommandImpl.setModel(t.transform(CommandImpl.getModel()));
		
	}
}
