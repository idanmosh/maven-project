package bgu.cmd;

import bgu.util.Util;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.util.StringUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;


public class GUIClient extends JFrame {
	
	private class GUIClientLayout implements LayoutManager {

		public GUIClientLayout() {
		}

		public void addLayoutComponent(String name, Component comp) {
		}

		public void removeLayoutComponent(Component comp) {
		}

		public Dimension preferredLayoutSize(Container parent) {
			Dimension dim = new Dimension(0, 0);

			Insets insets = parent.getInsets();
			dim.width = 900 + insets.left + insets.right;
			dim.height = 387 + insets.top + insets.bottom;

			return dim;
		}
		

	    public Dimension minimumLayoutSize(Container parent) {
	        Dimension dim = new Dimension(0, 0);
	        return dim;
	    }

	    public void layoutContainer(Container parent) {
	        Insets insets = parent.getInsets();

	        Component c;
	        c = parent.getComponent(0);
	        if (c.isVisible()) {c.setBounds(insets.left+16,insets.top+136,700,216);}
	        c = parent.getComponent(1);
	        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+8,528,24);}
	        c = parent.getComponent(2);
	        if (c.isVisible()) {c.setBounds(insets.left+16,insets.top+96,80,24);}
	        c = parent.getComponent(3);
	        if (c.isVisible()) {c.setBounds(insets.left+100,insets.top+96,100,24);}
	        c = parent.getComponent(4);
	        if (c.isVisible()) {c.setBounds(insets.left+204,insets.top+96,160,24);}
	        c = parent.getComponent(5);
            if (c.isVisible()) {c.setBounds(insets.left+368,insets.top+96,120,24);}
            c = parent.getComponent(6);
            if (c.isVisible()) {c.setBounds(insets.left+492,insets.top+96,100,24);}
            c = parent.getComponent(7);
            if (c.isVisible()) {c.setBounds(insets.left+596,insets.top+96,100,24);}
            c = parent.getComponent(8);

	        if (c.isVisible()) {c.setBounds(insets.left+568,insets.top+8,104,24);}
	        c = parent.getComponent(9);
	        if (c.isVisible()) {c.setBounds(insets.left+750,insets.top+200,96,72);}
	        c = parent.getComponent(10);
	        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+40,528,24);}
	        c = parent.getComponent(11);
	        if (c.isVisible()) {c.setBounds(insets.left+568,insets.top+40,104,24);}
	        c = parent.getComponent(12);
	        if (c.isVisible()) {c.setBounds(insets.left+750,insets.top+320,96,24);}
	    }

	}

	
	private static final long serialVersionUID = 8589205513899065156L;
	JTextArea outputStreamBox;
	JScrollPane sp_outputStreamBox;
	JTextField srcFileTxtField;
	JCheckBox identificationCheckBox;
	JCheckBox classHierarchyCyclesCheckBox;
    JCheckBox simplifictionCeckBox;
    JCheckBox translationCheckBox;
	JCheckBox propogationCheckBox;
	JButton browseSrcButton;
	JButton runButton;
	JCheckBox detectionCheckBox;
	JTextField tarFileTxtField;
	JButton browseTarButton;
	JButton optsButton;
    String commands ="";

    boolean isoneOfTheCheckBoxesChecked= false;

	public GUIClient() {
		GUIClientLayout customLayout = new GUIClientLayout();

		getContentPane().setFont(new Font("Helvetica", Font.PLAIN, 12));
		getContentPane().setLayout(customLayout);

		outputStreamBox = new JTextArea("");
        outputStreamBox.setEditable(true);///////////////////////////////////////////////////////////
		sp_outputStreamBox = new JScrollPane(outputStreamBox);
		getContentPane().add(sp_outputStreamBox);

		srcFileTxtField = new JTextField("Source File");
		getContentPane().add(srcFileTxtField);

		detectionCheckBox = new JCheckBox("Detection");
        detectionCheckBox.setEnabled(false);
		getContentPane().add(detectionCheckBox);

		identificationCheckBox = new JCheckBox("Identification");
        identificationCheckBox.setEnabled(false);
		getContentPane().add(identificationCheckBox);

		classHierarchyCyclesCheckBox = new JCheckBox("Class Hierarchy Cycles");
        classHierarchyCyclesCheckBox.setEnabled(false);
		getContentPane().add(classHierarchyCyclesCheckBox);
		
        simplifictionCeckBox = new JCheckBox("Simplification");
        simplifictionCeckBox.setEnabled(false);
        getContentPane().add(simplifictionCeckBox);

        translationCheckBox = new JCheckBox("Translation");
        translationCheckBox.setEnabled(false);
        getContentPane().add(translationCheckBox);

        propogationCheckBox = new JCheckBox("Propogation");
        propogationCheckBox.setEnabled(false);
        getContentPane().add(propogationCheckBox);


		browseSrcButton = new JButton("Browse..");
		getContentPane().add(browseSrcButton);

		runButton = new JButton("Run");
        runButton.setEnabled(false);
		getContentPane().add(runButton);

		tarFileTxtField = new JTextField("Target File");
		getContentPane().add(tarFileTxtField);

		browseTarButton = new JButton("Browse..");
		getContentPane().add(browseTarButton);
		
        optsButton = new JButton("Options");
        getContentPane().add(optsButton);

        Util u = Util.getUtil();
        u.setOut(createStreamToBox());
        u.setIn(new JTextFieldInputStream(outputStreamBox));

		setButtonActionListener();
		setCheckboxActionListener();
        setFocusListener();

		setSize(getPreferredSize());

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	private void setCheckboxActionListener() {
		ActionListener al = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//Identification
                if (e.getSource() == identificationCheckBox && !identificationCheckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-I");
                    outputStreamBox.replaceRange("",start,start+3);
                    commands = commands.replace("-I ","");
                }
                if (e.getSource() == identificationCheckBox && identificationCheckBox.isSelected()) {
                    outputStreamBox.append("-I ");
                    commands = commands.concat("-I ");
                }
                //Detection
                if (e.getSource() == detectionCheckBox && !detectionCheckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-D");
                    outputStreamBox.replaceRange("",start,start+3);
                    commands = commands.replace("-D ","");
                }
                if (e.getSource() == detectionCheckBox && detectionCheckBox.isSelected()) {
                    outputStreamBox.append("-D ");
                    commands = commands.concat("-D ");
                }
                //Simplification
                if (e.getSource() == simplifictionCeckBox && !simplifictionCeckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-Sim");
                    outputStreamBox.replaceRange("",start,start+5);
                    commands = commands.replace("-Sim ","");
                }
                if (e.getSource() == simplifictionCeckBox && simplifictionCeckBox.isSelected()) {
                    outputStreamBox.append("-Sim ");
                    commands =commands.concat("-Sim ");
                }
                //ClassHierarchyCycles
                if (e.getSource() == classHierarchyCyclesCheckBox && !classHierarchyCyclesCheckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-C");
                    outputStreamBox.replaceRange("",start,start+3);
                    commands = commands.replace("-C ","");
                }
                if (e.getSource() == classHierarchyCyclesCheckBox && classHierarchyCyclesCheckBox.isSelected()) {
                    outputStreamBox.append("-C ");
                    commands = commands.concat("-C ");
                }
                //Translation
                if (e.getSource() == translationCheckBox && !translationCheckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-Tr");
                    outputStreamBox.replaceRange("",start,start+4);
                    commands = commands.replace("-Tr ","");
                }
                if (e.getSource() == translationCheckBox && translationCheckBox.isSelected()) {
                    outputStreamBox.append("-Tr ");
                    commands = commands.concat("-Tr ");
                }
                //Propagation
                if (e.getSource() == propogationCheckBox && !propogationCheckBox.isSelected()) {
                    int start = outputStreamBox.getText().indexOf("-P");
                    outputStreamBox.replaceRange("",start,start+3);
                    commands = commands.replace("-P ","");
                }
                if (e.getSource() == propogationCheckBox && propogationCheckBox.isSelected()) {
                    outputStreamBox.append("-P ");
                    commands = commands.concat("-P ");
                }
                outputStreamBox.setCaretPosition(outputStreamBox.getText().length());
                if(propogationCheckBox.isSelected() || translationCheckBox.isSelected() || classHierarchyCyclesCheckBox.isSelected() ||
                        simplifictionCeckBox.isSelected() || detectionCheckBox.isSelected() || identificationCheckBox.isSelected()){
                    isoneOfTheCheckBoxesChecked = true;
                }
                else{
                    isoneOfTheCheckBoxesChecked = false;
                }
			}
		};
		identificationCheckBox.addActionListener(al);
		detectionCheckBox.addActionListener(al);
		classHierarchyCyclesCheckBox.addActionListener(al);
        simplifictionCeckBox.addActionListener(al);
        translationCheckBox.addActionListener(al);
        propogationCheckBox.addActionListener(al);
	}

    private void clearCheckBoxes(){
        translationCheckBox.setSelected(false);
        commands = commands.replace("-Tr ","");
        identificationCheckBox.setSelected(false);
        commands = commands.replace("-I ","");
        detectionCheckBox.setSelected(false);
        commands = commands.replace("-D ","");
        simplifictionCeckBox.setSelected(false);
        commands = commands.replace("-Sim ","");
        propogationCheckBox.setSelected(false);
        commands = commands.replace("-P ","");
        classHierarchyCyclesCheckBox.setSelected(false);
        commands = commands.replace("-C ","");
        isoneOfTheCheckBoxesChecked = false;
    }

	private void setButtonActionListener() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		ActionListener al = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//Handle source button action.
				if (e.getSource() == browseSrcButton) {
					int returnVal = fc.showOpenDialog(GUIClient.this);		            
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = fc.getSelectedFile();
						//This is where a real application would open the file.
                        commands = commands.replace("-f ".concat(srcFileTxtField.getText()).concat(" "),"");
						srcFileTxtField.setText(file.getAbsolutePath());
						runButton.setEnabled(true);
                        translationCheckBox.setEnabled(true);
                        identificationCheckBox.setEnabled(true);
                        detectionCheckBox.setEnabled(true);
                        simplifictionCeckBox.setEnabled(true);
                        propogationCheckBox.setEnabled(true);
                        classHierarchyCyclesCheckBox.setEnabled(true);
                        commands = commands.concat("-f ".concat(srcFileTxtField.getText()).concat(" "));
					} 
					else {
                        commands = commands.replace("-f ".concat(srcFileTxtField.getText()).concat(" "),"");
						srcFileTxtField.setText("Source File");
						runButton.setEnabled(false);
                        translationCheckBox.setEnabled(false);
                        identificationCheckBox.setEnabled(false);
                        detectionCheckBox.setEnabled(false);
                        simplifictionCeckBox.setEnabled(false);
                        propogationCheckBox.setEnabled(false);
                        classHierarchyCyclesCheckBox.setEnabled(false);
					}

				}
				else if (e.getSource() == browseTarButton) {
					int returnVal = fc.showOpenDialog(GUIClient.this);		            
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = fc.getSelectedFile();
                        outputStreamBox.setCaretPosition(outputStreamBox.getText().length());
//                        commands = commands.replace("-save ".concat(tarFileTxtField.getText()).concat(" "),"");
						tarFileTxtField.setText(file.getAbsolutePath());
//                        commands = commands.concat("-save ".concat(tarFileTxtField.getText()).concat(" "));
					}
					else {
//                        commands = commands.replace("-save ".concat(tarFileTxtField.getText()).concat(" "),"");
                        tarFileTxtField.setText("Target File");
					}
					//Handle run button action.
				} else if (e.getSource() == runButton) {
                    if(isoneOfTheCheckBoxesChecked){
                        if (!tarFileTxtField.getText().equals("Target File")){
                            commands = commands.concat("-save ".concat(tarFileTxtField.getText()));
                        }
					    runClient();
                        commands = commands.replace("-save ".concat(tarFileTxtField.getText()),"");
                        clearCheckBoxes();
                    }
                    else{
                        outputStreamBox.append("Please choose Operation by checking one of the check box\n");
                        outputStreamBox.setCaretPosition(outputStreamBox.getText().length());
                    }
				} else if (e.getSource() == optsButton) {
					OptionsGUI.main(null);
				}
                outputStreamBox.setCaretPosition(outputStreamBox.getText().length());

			}
		};

		browseSrcButton.addActionListener(al);
		browseTarButton.addActionListener(al);
		runButton.addActionListener(al);
		optsButton.addActionListener(al);
	}

    private void setFocusListener(){
        FocusAdapter fa = new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                outputStreamBox.setCaretPosition(outputStreamBox.getText().length());
            }
        };
        outputStreamBox.addFocusListener(fa);
    }


	private void runClient() {
		Util u = Util.getUtil();

//        String filePath = "-f ".concat(srcFileTxtField.getText());
//        int start = outputStreamBox.getText().indexOf("-");
//        commands = outputStreamBox.getText().substring(start);
//        commands = filePath.concat(" ").concat(commands);
		Map<CMDArgs, String> parcedArgs = parse(commands.split(" "));

		MModel model = null;
		try {
			model = u.compile(parcedArgs.get(CMDArgs.src));

			if (model == null) {
				u.print("Error: can't contunue due to compolation errors.");
				return;
			}

			Receiver receiver = new Receiver(model,parcedArgs);
            Thread t = new Thread(receiver);
			t.start();
		} catch (FileNotFoundException e) {
			u.print(e.getMessage());
		}
	}

	private Map<CMDArgs, String> parse(String[] args) {
        Map<CMDArgs, String> parcedInput = new LinkedHashMap<CMDArgs, String>();
        for (int i = 0; i < args.length ; i++) {
            if (args[i].equals("-f")){
                parcedInput.put(CMDArgs.src, args[++i]);
            }
            else if (args[i].equals("-save")) {
                parcedInput.put(CMDArgs.tar, args[++i]);
            }
            else if (args[i].equals("-Tr")){
                parcedInput.put(CMDArgs.translate, args[i]);
            }
            else if (args[i].equals("-Sim")){
                parcedInput.put(CMDArgs.simplify, args[i]);
            }
            else if (args[i].equals("-Tr-1")){
                parcedInput.put(CMDArgs.translateMinus, args[i]);
            }
            //if not source and target or other flag and starts with "-" then this is a command
            else if (args[i].startsWith("-")) {
                String str = args[i];

                for (int j = 1 ; j < str.length(); j++) {
                    if (str.charAt(j) == 'D') {

                        parcedInput.put(CMDArgs.detect, "true");
                    }
                    else if (str.charAt(j) == 'I') {
                        parcedInput.put(CMDArgs.identify, "true");
                    }
                    else if (str.charAt(j) == 'P') {
                        parcedInput.put(CMDArgs.propogate, "true");
                    }

                    else if  (str.charAt(j)=='C'){
                        parcedInput.put(CMDArgs.circles, args[1]);
                    }
                }
            }
        }
        return parcedInput;
    }


	private void updateTextArea(final String text) {
        outputStreamBox.append(text);
//        String temp = outputStreamBox.getText();
//        outputStreamBox.setText(temp + " "+text);

        outputStreamBox.invalidate();
        outputStreamBox.validate();
        outputStreamBox.repaint();
        outputStreamBox.setCaretPosition(outputStreamBox.getText().length());
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				outputStreamBox.append(text);
//			}
//		});
//        try {
//            SwingUtilities.invokeAndWait(new Runnable() {
//                @Override
//                public void run() {
//                    outputStreamBox.append(text);
//                }
//            });
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
    }

	private PrintStream createStreamToBox() {  
		OutputStream out = new OutputStream() {  
			@Override  
			public void write(int b) throws IOException {  
				updateTextArea(String.valueOf((char) b));  
			}  

			@Override  
			public void write(byte[] b, int off, int len) throws IOException {  
				updateTextArea(new String(b, off, len));  
			}  

			@Override  
			public void write(byte[] b) throws IOException {  
				write(b, 0, b.length);  
			}  
		};  
		PrintStream ps = new PrintStream(out, true);
		return ps;
	}

    private class JTextFieldInputStream extends InputStream {
        byte[] contents;
        Boolean block = true;
        int pointer = 0;

        public JTextFieldInputStream(final JTextArea text) {

            text.addKeyListener(new KeyListener() {
                //@Override
                public void keyPressed(KeyEvent e) {
                    text.setCaretPosition(text.getText().length());
                }
               // @Override
                public void keyTyped(KeyEvent e) {
                }
              //  @Override
                public void keyReleased(KeyEvent e) {
                    if(e.getKeyChar()=='\n'){
                        String[] split = text.getText().split("[\\n\\r]");
                        contents = split[split.length -1].getBytes();
                       // pointer = 0;
                        synchronized (block){
                            block.notifyAll();
                        }

                    }
                }
            });
        }

        @Override
        public int read() throws IOException {
            try {
                synchronized (block){
                    while(block){
                        block.wait();
                        block = false;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (pointer >= contents.length){
                //init
                pointer=0;
                block=true;
                contents=null;
                return -1;
            }
            return contents[pointer++];
           // System.out.println("2" + Thread.currentThread().getName());
//            synchronized (block){
//                if(block)
//                    return 0;
//            }
//            if(contents != null && pointer >= contents.length){
//          //      System.out.println("3"+Thread.currentThread().getName());
//                return -1;
//            }
//            if (contents == null){
//                synchronized (block){
//                    block = true;
//                }
//                return 0;
//            }
//            return this.contents[pointer++];
        }


    }

    public static void main(String args[]) {
    GUIClient window = new GUIClient();

    window.setTitle("Finite Satisfability");
    window.pack();
    window.setVisible(true);
	}


}