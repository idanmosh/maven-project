package bgu.cmd;



import bgu.util.Util;
import communication.Communication;


/**
 * Created by user on 23/12/14.
 */
public class SimplifyCommand extends CommandImpl implements  Command {

    @Override
    public void openCommunication() {

        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());
        Util util = Util.getUtil();
        String msg = comm.communicate(CMDArgs.simplify ,0);
        while(!msg.startsWith("There is NOT")){
            util.print(msg);

            msg = util.read();
            if(msg.startsWith("y")){
                msg = comm.communicate(CMDArgs.simplify ,1);
            }
            else if(msg.equalsIgnoreCase("n")){
                break;
            }
        }
        if (!msg.equals("n"))
            util.print(msg);
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes
    }



/*
    public void saveModel(MModel m, Util u) throws IOException {
        u.print("Insert path to save the model");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        u.print("Insert name");
        BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
        String input1 = reader1.readLine();

        String newUse = m.toString();
        //System.out.println(newUse);
        PrintWriter writer;
        if (input1.endsWith(".use"))
            writer = new PrintWriter(input+"/"+input1, "UTF-8");
        else
            writer = new PrintWriter(input+"/"+input1+".use", "UTF-8");
        writer.println(newUse);
        writer.close();
    }
    */
}
