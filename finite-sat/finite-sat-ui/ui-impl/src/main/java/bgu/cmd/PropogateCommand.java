package bgu.cmd;

import bgu.propagation.DisjointConstraintPropogator;
import bgu.util.Util;
import communication.Communication;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.List;


public class PropogateCommand extends CommandImpl implements Command {

	private DisjointConstraintPropogator propogateDisjoint;


	public MModel propogate() {
        Util util = Util.getUtil();
		propogateDisjoint = DisjointConstraintPropogator.getNewInstance();
		MModel newModel =  propogateDisjoint.propogate(super.getModel());
        util.print(propogateDisjoint.getDisjointGraph().toString());
        util.print(propogateDisjoint.getDisjointCliquesMap().toString());
		//propogateDisjoint.getUpdatedModelBuilder();
		return newModel;
	}

	@Override
	public void openCommunication() {
		Util util = Util.getUtil();
        Communication comm = Communication.getInstance();
        comm.setModel(CommandImpl.getModel());
		//long start = System.currentTimeMillis();
        String msg = comm.communicate(CMDArgs.propogate ,0);
        util.print(msg);
        CommandImpl.setModel(comm.getModel());//to save the changes from the current command so that the next command will get the changes
        /*
		MModel newModel = propogate();
		CommandImpl.setModel(newModel);//this command id useles because of super.getModl()


		List<MGeneralizationSet> newAddedGS = propogateDisjoint.getUpdatedModelBuilder().getNewDisjointGSs();
		List<MGeneralizationSet> newupdatedGS = propogateDisjoint.getUpdatedModelBuilder().getUpdatedDisjointGSs();

		util.print("Done!\n\nThe new added GSs are: ");
		if (newAddedGS.isEmpty()) util.print("There Is no added new GSs");
		else{
			for (MGeneralizationSet newGS : newAddedGS){
				util.print(newGS.toString());
			}
		}


		util.print("\nThe new updated GSs are: ");
		if (newupdatedGS.isEmpty()) util.print("There Is no updated GSs");
		else{
			for (MGeneralizationSet newGS : newupdatedGS){
				util.print(newGS.toString());
			}
		}

        */
		//long end = System.currentTimeMillis();
		//util.print("\nPropagation execution time: "+(end-start)+" ms.");


		//util.print(newModel.toString());

	}

}
