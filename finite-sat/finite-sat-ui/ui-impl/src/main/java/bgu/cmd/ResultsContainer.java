package bgu.cmd;

import java.util.HashMap;
import java.util.Map;

public class ResultsContainer {

	Map<String, String> results;
	
	private static ResultsContainer container;
	
	
	public static ResultsContainer getResultsContainer() {
		if (container == null) container = new ResultsContainer();
		return container;
	}
	
	
	
	private ResultsContainer() {
		results = new HashMap<String, String>(3);
	}
	
	public void addResult(String command, String result) {
		results.put(command, result);
	}
	
	public String getResult(String command) {
		return results.get(command);
	}
	
	public void reset() {
		container = new ResultsContainer();
	}
}
