package bgu.cmd;

import bgu.util.Util;
import org.tzi.use.uml.mm.MModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class CommandLineClient {

	/**
	 * Translates strings given as command line args to java structures.
	 * @param args String array of argumants that shuld contain commands and variables
	 * such as as source file name. 
	 * @return java representation of the input.
	 */
	private Map<CMDArgs, String> parse(String[] args) {
		Map<CMDArgs, String> parcedInput = new HashMap<CMDArgs, String>();
		for (int i = 0; i < args.length ; i++) {
			
			//"-s" means source, next argument is source file name
			if (args[i].equals("-s")){
				parcedInput.put(CMDArgs.src, args[++i]);
			}
			//"-t" means target, next argument is target file name
			else if (args[i].equals("-t")) {
				parcedInput.put(CMDArgs.tar, args[++i]);
			}
			else if (args[i].equals("-xorovveriding")) {
				parcedInput.put(CMDArgs.xorConform, "true");
			}
			else if (args[i].equals("-autoSubsetOff")) {
				parcedInput.put(CMDArgs.manSubset, "true");
			}
			//if not source and target or other flag and starts with "-" then this is a command
			else if (args[i].startsWith("-")) {
				String str = args[i];
				
				for (int j = 1 ; j < str.length(); j++) {
					if (str.charAt(j) == 'D') {
						
						parcedInput.put(CMDArgs.detect, "true");
					}
					else if (str.charAt(j) == 'I') {
						parcedInput.put(CMDArgs.identify, "true");
					}
					else if (str.charAt(j) == 'P') {
						parcedInput.put(CMDArgs.propogate, "true");
					}
					
					if (str.charAt(j)=='C'){
						parcedInput.put(CMDArgs.circles, "true");
					}						
				}
			}
			else {
				errorParse(i, args);
			}
		}
		return parcedInput;
	}

	private void errorParse(int i, String[] args) {
		errorExit("Error: The " 
				+ i 
				+ " command line argment '" 
				+ args[i] 
				+ " is not recognized");

	}

	private void errorExit(String s) {
		System.err.println(s);
		System.exit(-1);
	}

	private void assertValid(Map<CMDArgs, String> parcedArgs) {
		if (!parcedArgs.containsKey(CMDArgs.src)) {
			errorExit("Error: on sorce file");
		}
		
		File srcFile = new File(parcedArgs.get(CMDArgs.src));
		if  (!srcFile.exists()) {
			errorExit("Error: file '" + parcedArgs.get(CMDArgs.src) + "' not found");
		}

		if (parcedArgs.containsKey(CMDArgs.tar) &&
				!parcedArgs.containsKey(CMDArgs.propogate)) {
			errorExit("Error: " +
					"Target file should come only with propogation(-P) flag rasied.");
		}
	}

	public void runClient(String[] args) throws FileNotFoundException {
		Util u = Util.getUtil();
		
		Map<CMDArgs, String> parcedArgs = this.parse(args);
		assertValid(parcedArgs);
		MModel model = u.compile(parcedArgs.get(CMDArgs.src));
		if (model == null) {
			this.errorExit("Error: can't contunue due to compolation errors.");
		}

		Receiver receiver = new Receiver(model,parcedArgs);
		try  {
			receiver.run();
		}
		 catch (Exception e){
			 this.errorExit("Error: " + e.getMessage() + " can't contunue...");
		 }
	}




	public static void main(String[] args) {
		CommandLineClient c = new CommandLineClient();
		if (args.length < 3) {
			
			Util.getUtil().print(usage);
			c.errorExit("");
		}
		try {
			
			c.runClient(args);
		} catch (FileNotFoundException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	
	private static final String usage =
		"-s <file name>         Specifies the name of the source file\n" +
		"-t <file name>         Specifies the name of the file in which\n" +
		"                       modified model will be saved. Creates a new\n" +
		"                       file or truncates exisisting one.\n" +
		"                       (should be used only with -P flag raised.)\n" +
		"-C                     detects circles\n" +
		"-P                     propagation of disjoint constraint enabled.\n" +
		"-D                     detection of problematic cycles enabled\n" +
		"                       this option implicitly raises the -D flag.\n" +
		"-I                     recognition of finite satisfability.\n";

}
