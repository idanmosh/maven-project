package bgu.translate;

import bgu.translation.Translate;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

/**
 * This is a skeleton test case that explains the usage of the different annotations
 * allowing u to write a clean test without needing to open any files and compile use models
 * the custom test runner ModelCustomTestRunner does it all for u,
 * it requires u to tell it what USE specs u wish to test, where u want them compiled, and that's it !
 * it will compile the spec for u and inject it into your test class
 *
 * The @RunWith annotation is a standard JUNIT annotation telling JUNIT to use a different test runner
 * in our case it's a test runner which compiles USE models
 */

@RunWith(ModelCustomTestRunner.class)
public class TranslationTest extends TestParent {
    /*
        This annotation tells the test runner to put the compiled use spec on a specific field
        that we can later reference in our tests
        here model would contain a compiled use model
     */
    @InjectCompiledModel
    private MModel model;

    /*
        This annotation tells the test runner to load our test configuration file (specified below under paramFiles)
        Here under this string field, every time the test will run with a corresponding spec, it will also load it's
        configuration here.
     */
    @InjectSpecParamObject
    private String param;

    /*
        The @Test annotation is a regular JUnit test case
        The @CompileModels annotation allows u to specify a string array of USE specs to be compiled
        the number of iteration this specific test case performs depends on the amount of USE specs,
        each time the test is loaded a different USE spec is compiled in the field under @InjectCompiledModel

        the paramFiles is a mandatory, but if it's specified it has to contain exactly the same amount of configuration
        as use files, the test will load the use spec and param file in conjunction.
     */
    @Test
    @CompileModels(useFiles = {"testUseSpecs/trans_test1.use"},
            paramFiles = {"testUseSpecs/translate_test1.use.params"})
    public void someTest() {

        int numClassBefore= model.classes().size();

        int from = 0;
        int until = param.indexOf("\n");
        String sub = param.substring(from,until);
        int expectedClassBefore = Integer.parseInt(sub);

        Assert.assertEquals(expectedClassBefore,numClassBefore);

        int numAssociasionBefore = model.associations().size();
        from = until + 1;
        until = param.indexOf("\n", from);
        sub = param.substring(from,until);
        int expectedAssociationBefore = Integer.parseInt(sub);

        Assert.assertEquals(expectedAssociationBefore,numAssociasionBefore);

        Translate tr = Translate.getInstance();
        MModel newModel = tr.translate(model);

        int numClassAfter = newModel.classesOnly().size();

        from = until+1;
        until = param.indexOf("\n",from);
        sub = param.substring(from,until);
        int expectedClassAfter = Integer.parseInt(sub);


        Assert.assertEquals(expectedClassAfter,numClassAfter);

        int numAssociasioAfter = newModel.associations(true).size();
        from = until + 1;
        until = param.indexOf("#", from);
        sub = param.substring(from,until);
        int expectedAssociationAfter = Integer.parseInt(sub);

        Assert.assertEquals(expectedAssociationAfter,numAssociasioAfter);





    }
}



