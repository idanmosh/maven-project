package bgu;

import bgu.circleCheck.GraphBuilderImpl;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.runner.ModelCustomTestRunner;
import org.jgrapht.Graph;
import org.jgrapht.util.VertexPair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(ModelCustomTestRunner.class)
public class GraphBuilderTest extends TestParent {
    @InjectCompiledModel
	private MModel _model;

	private Graph<MClass, VertexPair<MClass>> _graph;  //object under test
	private GraphBuilderImpl _graphBuilder;


	@Test
	@CompileModels(useFiles = {"testUseSpecs/disjointcycle_1.use"})
    public void testGetGraph() {
        _graphBuilder = new GraphBuilderImpl(_model);
        _graph = _graphBuilder.getGraph();  // undirected graph

		//checks vertices	
		assertEquals(_model.classes().size(), _graph.vertexSet().size());
		
		for (Object o : _model.classes())
			if(o instanceof MClass)
				assertTrue(_graph.containsVertex((MClass)o));
		
		//checks edges
		DirectedGraph directedGraph = _model.generalizationGraph();	
		Iterator<DirectedEdge> edges_iter = directedGraph.edgeIterator();
		
		while(edges_iter.hasNext()){
			DirectedEdge edge = edges_iter.next();

			MClass source = (MClass)edge.source(); 
			MClass target = (MClass)edge.target();

			assertTrue(_graph.containsEdge(source, target));


		}
	}

}
