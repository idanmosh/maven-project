package bgu.identification;

import bgu.detection.FiniteSatDetector;
import bgu.propagation.DisjointConstraintPropogator;
import bgu.identification.FiniteSatIdentifier;
import bgu.propagation.DisjointIncompleteConstraintPropogator;
import bgu.tests.TestParent;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;

public class DetectionIdentificationPropagationTest extends TestParent {
	
	static String graph_fig_13_1 = "testUseSpecs/graph_fig_13_1.use";
    static String test1 = "testUseSpecs/gen_set_1.use";
    static String test4 = "testUseSpecs/gen_set_2.use";
	

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {

        MModel model = null;



        String fileName = test4;
        URL resource = findResource(fileName);
        Reader r = new BufferedReader(new FileReader(resource.getFile()));
        model = USECompiler.compileSpecification(r,
                fileName, new PrintWriter(System.err),
                new ModelFactory());
        
        if (model == null) {
        	System.out.println("Compilation errors");
        	return;
        }
        
       
        
        System.out.println(model);
        
        DisjointConstraintPropogator dcp = DisjointConstraintPropogator.getNewInstance();
        model = dcp.propogate(model);
        
        System.out.println(model);
        
//        DisjointIncompleteConstraintPropogator dicp =
//        	DisjointIncompleteConstraintPropogator.getNewInstance();
//        model = dicp.propogate(model);
//
//        System.out.println(model);
        
               
		
        FiniteSatDetector fr = new FiniteSatDetector();
        
        boolean isFinitSat = fr.isFinitelySatisfiable(model);
        
        System.out.println("\n----------\n");
        
        System.out.println("Is finitly satisfiable: " + isFinitSat);
        
        System.out.println("\n----------\n");
        
        if (!isFinitSat) {
			FiniteSatIdentifier detector_New = new FiniteSatIdentifier();
			boolean f =detector_New.identify(model);
            System.out.println("identify: "+f);
			System.out.println(detector_New.get_result());
        	return;
        }
        
        System.out.println(fr.get_constraints_string());

        System.out.println("\n----------");
        
        System.out.println(fr.get__fullSolution());
        
	}
	

}
