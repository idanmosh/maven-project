package bgu.identification;

import bgu.identification.CriticalCircle;
import bgu.identification.FiniteSatIdentifier;
import bgu.tests.TestParent;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;
import java.util.Vector;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

@Ignore
public class FiniteSatIdentifierTest extends TestParent {
	
	private String fileName = "testUseSpecs/DangerousTest.use";
	private FiniteSatIdentifier _out;
	private MModel _model;
	private Vector<CriticalCircle> _circles = 
		new Vector<CriticalCircle>();

	@Before
	public void setUp() throws Exception {


        URL resource = findResource(fileName);
		try{
			Reader r = new BufferedReader(new FileReader(resource.getFile()));
			_model = USECompiler.compileSpecification(r,
					fileName, new PrintWriter(System.err),
					new ModelFactory());
			
			_out = new FiniteSatIdentifier();
			
		}catch(FileNotFoundException e){
			System.out.println("Use test file isn't in place: "+e.getMessage());
		}
	}

	@Test
	public void testIdentify() {
		String firstCircle =
			"A1 -> A3 -> assoc_r -> A1\n\n";
		String secondCircle =
			"A1 -> A2 -> A3 -> assoc_r -> A1\n\n";
		
		assertTrue(_out.identify(_model));
		_circles = _out.getCircles();
		
		CriticalCircle c1 = _circles.elementAt(0);
		CriticalCircle c2 = _circles.elementAt(1);
		
		//first circle is valid
		assertArrayEquals(firstCircle.toCharArray(), 
				c1.cycleName().toCharArray());
		
		//first circle is valid
		assertArrayEquals(secondCircle.toCharArray(), 
				c2.cycleName().toCharArray());
	}
}
