package bgu;

import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

/**
 * This is a skeleton test case that explains the usage of the different annotations
 * allowing u to write a clean test without needing to open any files and compile use models
 * the custom test runner ModelCustomTestRunner does it all for u,
 * it requires u to tell it what USE specs u wish to test, where u want them compiled, and that's it !
 * it will compile the spec for u and inject it into your test class
 *
 * The @RunWith annotation is a standard JUNIT annotation telling JUNIT to use a different test runner
 * in our case it's a test runner which compiles USE models
 */

@RunWith(ModelCustomTestRunner.class)
public class TestSkeleton extends TestParent {
    /*
        This annotation tells the test runner to put the compiled use spec on a specific field
        that we can later reference in our tests
        here model would contain a compiled use model
     */
    @InjectCompiledModel
    private MModel model;

    /*
        This annotation tells the test runner to load our test configuration file (specified below under paramFiles)
        Here under this string field, every time the test will run with a corresponding spec, it will also load it's
        configuration here.
     */
    @InjectSpecParamObject
    private String param;

    /*
        The @Test annotation is a regular JUnit test case
        The @CompileModels annotation allows u to specify a string array of USE specs to be compiled
        the number of iteration this specific test case performs depends on the amount of USE specs,
        each time the test is loaded a different USE spec is compiled in the field under @InjectCompiledModel

        the paramFiles is a mandatory, but if it's specified it has to contain exactly the same amount of configuration
        as use files, the test will load the use spec and param file in conjunction.
     */
    @Test
    @CompileModels(useFiles = {"testUseSpecs/circle.use", "testUseSpecs/DangerousTest.use" /* ...... */},
            paramFiles = {"testUseSpecs/circle.use.params", "testUseSpecs/DangerousTest.use.params" /* ...... */})
    public void someTest() {
        // When the code here runs the model field will contain the compiled specification of each of the use files
        // specified under @CompileModels
        // Try running this test and see the outputs

        // notice the model is different every time
        System.out.println("Test has started with a compiled model under " + model.hashCode());

        // notice the params are different every time
        System.out.println("With params "+param);

        // u can use standard JUNIT assertions to test your code
        Assert.assertNotNull(model);
    }
}



