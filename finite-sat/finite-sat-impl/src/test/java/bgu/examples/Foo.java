package bgu.examples;

import java.util.List;


public class Foo {
    private List<String> names;
    private Bar bar;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Bar getBar() {
        return bar;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Foo foo = (Foo) o;

        if (bar != null ? !bar.equals(foo.bar) : foo.bar != null) return false;
        if (names != null ? !names.equals(foo.names) : foo.names != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = names != null ? names.hashCode() : 0;
        result = 31 * result + (bar != null ? bar.hashCode() : 0);
        return result;
    }
}
