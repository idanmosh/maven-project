package bgu.examples;

import bgu.circleCheck.CircleChecker;
import bgu.circleStructure.Circle;
import bgu.circleStructure.CircleImpl;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This is a skeleton test case that explains the usage of the different annotations
 * allowing u to write a clean test without needing to open any files and compile use models
 * the custom test runner ModelCustomTestRunner does it all for u,
 * it requires u to tell it what USE specs u wish to test, where u want them compiled, and that's it !
 * it will compile the spec for u and inject it into your test class
 * <p/>
 * The @RunWith annotation is a standard JUNIT annotation telling JUNIT to use a different test runner
 * in our case it's a test runner which compiles USE models
 */

@RunWith(ModelCustomTestRunner.class)
public class JsonExampleTest extends TestParent {
    @InjectCompiledModel
    private MModel model;

    @InjectSpecParamObject
    private String param;

    private ObjectMapper objectMapper = new ObjectMapper();


    @Test
    @CompileModels(useFiles = {"testUseSpecs/disjointcycle_2.use"})
    public void jsonExampleTest() throws IOException {
        Foo foo = createFoo();
        String fooJson = objectMapper.writeValueAsString(foo);
        System.out.println("converted foo to json : "+fooJson);

        System.out.println("created new instance of foo for it's json string");
        Foo newFoo = objectMapper.readValue(fooJson, Foo.class);

        System.out.println("making sure both instances are the same");
        Assert.assertEquals(foo, newFoo);
    }

    private Foo createFoo() {
        Foo foo = new Foo();

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add("hello");
        stringArrayList.add("world");
        foo.setNames(stringArrayList);

        Bar bar = new Bar();
        bar.setValue(true);
        foo.setBar(bar);

        return foo;
    }
}



