package bgu.simplifyTest;


import bgu.simplification.GraphConstructor;
import bgu.simplification.MyEdge;

import org.jgrapht.graph.DirectedWeightedMultigraph;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

import java.util.Iterator;
import java.util.Set;

/**
 * This is a skeleton test case that explains the usage of the different annotations
 * allowing u to write a clean test without needing to open any files and compile use models
 * the custom test runner ModelCustomTestRunner does it all for u,
 * it requires u to tell it what USE specs u wish to test, where u want them compiled, and that's it !
 * it will compile the spec for u and inject it into your test class
 * <p/>
 * The @RunWith annotation is a standard JUNIT annotation telling JUNIT to use a different test runner
 * in our case it's a test runner which compiles USE models
 */

@RunWith(ModelCustomTestRunner.class)
public class GraphConstructorTest extends TestParent {
    /*
        This annotation tells the test runner to put the compiled use spec on a specific field
        that we can later reference in our tests
        here model would contain a compiled use model
     */
    @InjectCompiledModel
    private MModel model;

    /*
        This annotation tells the test runner to load our test configuration file (specified below under paramFiles)
        Here under this string field, every time the test will run with a corresponding spec, it will also load it's
        configuration here.
     */
    @InjectSpecParamObject
    private String param;

    /*
        The @Test annotation is a regular JUnit test case
        The @CompileModels annotation allows u to specify a string array of USE specs to be compiled
        the number of iteration this specific test case performs depends on the amount of USE specs,
        each time the test is loaded a different USE spec is compiled in the field under @InjectCompiledModel

        the paramFiles is a mandatory, but if it's specified it has to contain exactly the same amount of configuration
        as use files, the test will load the use spec and param file in conjunction.
     */
    @Test
    @CompileModels(useFiles = { "testUseSpecs/simplify1.use"},
            paramFiles = {"testUseSpecs/simplify1.use.params"})
    public void someTest() {
        GraphConstructor constructor = new GraphConstructor(model);
        DirectedWeightedMultigraph<String, MyEdge> graph= constructor.buildGraph();
        System.out.println(graph.toString());
        int expectedVertex= model.classes().size();
        int Vertex= graph.vertexSet().size();

        int edges= graph.edgeSet().size();

        int expectedEdges = model.associations().size();
        expectedEdges = expectedEdges * 2;
        Set Edges= graph.edgeSet();
        Iterator iter = Edges.iterator();
        while(iter.hasNext()) {
            MyEdge e= (MyEdge) iter.next();
            if (e.getAssociation() == null){
                expectedEdges ++ ;
            }
           //String s = e.getAssociation() + " " + graph.getEdgeSource(e) + " " + graph.getEdgeTarget(e)+ " ";
            //System.out.print(s);
            //System.out.println(graph.getEdgeWeight(e));
        }


        Assert.assertEquals(expectedVertex, Vertex);
        Assert.assertEquals(expectedEdges, edges);
    }
}






