package bgu.simplifyTest;

import bgu.simplification.Circle;
import bgu.simplification.CDsimplification;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;

import java.io.IOException;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 09/09/13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
@RunWith(ModelCustomTestRunner.class)
public class CDsimplificationTest extends TestParent {
    /*
        This annotation tells the test runner to put the compiled use spec on a specific field
        that we can later reference in our tests
        here model would contain a compiled use model
     */
    @InjectCompiledModel
    private MModel model;

    /*
        This annotation tells the test runner to load our test configuration file (specified below under paramFiles)
        Here under this string field, every time the test will run with a corresponding spec, it will also load it's
        configuration here.
     */
    @InjectSpecParamObject
    private String param;

    /*
        The @Test annotation is a regular JUnit test case
        The @CompileModels annotation allows u to specify a string array of USE specs to be compiled
        the number of iteration this specific test case performs depends on the amount of USE specs,
        each time the test is loaded a different USE spec is compiled in the field under @InjectCompiledModel

        the paramFiles is a mandatory, but if it's specified it has to contain exactly the same amount of configuration
        as use files, the test will load the use spec and param file in conjunction.
     */
    @Test
    @CompileModels(useFiles = { "testUseSpecs/simplify1.use"},
            paramFiles = {"testUseSpecs/simplify1.use.params"})
    public void someTest() throws IOException, MInvalidModelException {

        System.out.println("        Before: \n");
        CDsimplification sim = new CDsimplification(model);
        Vector<Circle> circles = sim.getAllCircles();
        for(int i = 0; i< circles.size(); i++){
            System.out.println("The circle is: ");
            System.out.println(circles.get(i).toString());
            sim.getAllCorrections(circles.get(i));
            System.out.println("The corrections need to be done are: ");
            for (int j = 0; j<circles.get(i).getCorrections().size(); j++){
                System.out.println(circles.get(i).getCorrections().get(j).toString());

            }
            System.out.println("\n ");
        }
        Assert.assertTrue(circles.size() >= 0);
        System.out.println("\n \n");
        MModel m = sim.removeAllCircles(circles);
        //sim.saveModel(m);
       // System.out.println(sim.getModel().toString());
        System.out.println("        After: \n");
        CDsimplification sim1 = new CDsimplification(m);
        Vector<Circle> circles1 = sim1.getAllCircles();

        for(int i = 0; i< circles1.size(); i++){
            System.out.println(circles1.get(i).toString());
            sim1.getAllCorrections(circles1.get(i));
            System.out.println("The corrections need to be done are: ");
            for (int j = 0; j<circles1.get(i).getCorrections().size(); j++){
                System.out.println(circles1.get(i).getCorrections().get(j).toString());

            }
            System.out.println("\n ");
        }
        System.out.println(circles1.size());
        Assert.assertTrue(circles1.size() == 0);



    }

}














