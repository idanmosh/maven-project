package bgu.simplifyTest;

import bgu.simplification.Circle;
import bgu.simplification.GraphConstructor;
import bgu.simplification.MyEdge;
import bgu.simplification.SimplificationHandler;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.annotations.InjectSpecParamObject;
import bgu.tests.runner.ModelCustomTestRunner;
import junit.framework.Assert;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;

import java.io.IOException;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 09/09/13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
@RunWith(ModelCustomTestRunner.class)
public class SimplificationHandlerTest extends TestParent {
    /*
        This annotation tells the test runner to put the compiled use spec on a specific field
        that we can later reference in our tests
        here model would contain a compiled use model
     */
    @InjectCompiledModel
    private MModel model;

    /*
        This annotation tells the test runner to load our test configuration file (specified below under paramFiles)
        Here under this string field, every time the test will run with a corresponding spec, it will also load it's
        configuration here.
     */
    @InjectSpecParamObject
    private String param;

    /*
        The @Test annotation is a regular JUnit test case
        The @CompileModels annotation allows u to specify a string array of USE specs to be compiled
        the number of iteration this specific test case performs depends on the amount of USE specs,
        each time the test is loaded a different USE spec is compiled in the field under @InjectCompiledModel

        the paramFiles is a mandatory, but if it's specified it has to contain exactly the same amount of configuration
        as use files, the test will load the use spec and param file in conjunction.
     */
    @Test
    @CompileModels(useFiles = { "testUseSpecs/simplify1.use"},
            paramFiles = {"testUseSpecs/simplify1.use.params"})
    public void someTest() throws IOException, MInvalidModelException {


        GraphConstructor constructor = new GraphConstructor(model);
        DirectedWeightedMultigraph<String, MyEdge> graph= constructor.buildGraph();


        SimplificationHandler sim = new SimplificationHandler(graph, model);

        int numOfCircles = sim.getFoundCircles().size();
        //System.out.println(numOfCircles);
        // get the number of circles
        int temp = param.indexOf("\n");
        String sub = param.substring(0,temp);
        int expNumCircles = 0;
        int mul = 1;
        for(int i =0; i<sub.length();i++){
            expNumCircles += (sub.charAt(i)-48)*mul;
             mul = mul*10;
        }



        Assert.assertEquals(expNumCircles, numOfCircles);
        Vector<String> circles = new Vector<String>();

        //test the circles
        for(int i = 0 ; i<numOfCircles; i++){
            Circle circle = sim.getFoundCircles().elementAt(i);
            String c = new String(circle.toString());
            System.out.println(circle.toString());
            circles.add(c);
        }

        //holds the expected circles
        Vector<String> expectedCircles = new Vector<String>();

        for(int i = 1 ; i<expNumCircles; i++){
            int index = param.indexOf("#"+i);
            int index1 = param.indexOf("#"+(i+1));
            sub = param.substring(index+2,index1-1);
            expectedCircles.add(sub);

        }
        //the last circle
        int index = param.indexOf("#"+expNumCircles);
        int index1 = param.indexOf("End");
        sub = param.substring(index+2,index1);
        expectedCircles.add(sub);

        for(int i =0 ;i<circles.size(); i++){
            String c = circles.get(i);
            boolean flag = false;
            for(int j= 0; j<expectedCircles.size() && !flag ; j++){
                String s = expectedCircles.get(j);
                if(sim.theSameCircle(c,s)){
                    flag = true;
                }
            }
            Assert.assertTrue(flag);
        }



        }

    }














