package bgu.compositionAggregationCorrectness;

/**
 * Created by user on 26/07/14.
 */
import bgu.tests.TestParent;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import bgu.compositionAggregationCorrectness.ACcycle;
import bgu.compositionAggregationCorrectness.CompositionAggregationCorrectnessIdentification;
import bgu.compositionAggregationCorrectness.LabledEdge;
import bgu.detection.FiniteSatDetector;
import bgu.identification.FiniteSatIdentifier;
import bgu.propagation.DisjointConstraintPropogator;
import bgu.propagation.DisjointIncompleteConstraintPropogator;

public class CompositionAggregationCorrectnessTest extends TestParent{


    public static void testDetection(){
        MModel model = null;
        CompositionAggregationCorrectnessIdentification detector;
        boolean detectionResult;
        String expectedResultFileName;
        String expectedResult;

        String fileName="testUseSpecs/Cycles.txt";
        URL resource = findResource(fileName);
        try{
            FileReader fr = new FileReader(resource.getFile());
            BufferedReader br = new BufferedReader(fr);
            String strLine;
            int i=1;
            while ((strLine = br.readLine()) != null){
                fileName=strLine.split("@")[0];
                resource = findResource(fileName);
                Reader r = new BufferedReader(new FileReader(resource.getFile()));
                model = USECompiler.compileSpecification(r,fileName, new PrintWriter(System.err),new ModelFactory());

                detector= new CompositionAggregationCorrectnessIdentification(model);
                detector.setPrintAllFindings_ON();
                detector.setPrinting_OFF();
                detector.identifyCompositionAggregationCycle();// prints
                Vector<ACcycle<LabledEdge>> circls = detector.getCircles();

                expectedResultFileName=strLine.split("@")[1];
                expectedResult = fileToString(expectedResultFileName);
                System.out.println(fileName);

                String checkResult = check(expectedResultFileName, circls, detector);
                if(checkResult.equals("passed")){
                    System.out.println(checkResult);
                } else {
                    System.out.println("failed"+"\nexpected result:\n" + expectedResult + "\n\nyour result:\n\n"+"Matching cycles found:"+checkResult);
                    for(ACcycle c : circls){
                        System.out.println(detector.printCycleForAlgo2(c));
                    }
                }
                System.out.println("********************************************************************");
                i++;
            }
            br.close();
        }

        catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }

    }


    public static String fileToString(String filename){
        URL resource = findResource(filename);
        String result = "";
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(resource.getFile()));
            String strLine;
            while ((strLine = br.readLine()) != null){
                result = result+"\n" + strLine;
            }
            br.close();

        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
        return result;
    }

    public static String check(String filename, Vector<ACcycle<LabledEdge>> circls, CompositionAggregationCorrectnessIdentification detector){
        URL resource = findResource(filename);
        String result = "";
        BufferedReader br;
        int found = 0;
        try {
            br = new BufferedReader(new FileReader(resource.getFile()));
            int numberOfCycles = Integer.parseInt(br.readLine().split(":")[1]);

            String strLine;
            while ((strLine = br.readLine()) != null){
                strLine = strLine.replaceAll(" ", "");
                for(ACcycle c : circls){
                    if (0==detector.printCycleForAlgo2(c).replaceAll(" ", "").compareTo(strLine))
                        found++;
                }
            }

            if(found==numberOfCycles){
                result = "passed";
            } else {
                result = Integer.toString(found);
            }
            br.close();

        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    public static void main(String[] args){
        testDetection();

    }
}
