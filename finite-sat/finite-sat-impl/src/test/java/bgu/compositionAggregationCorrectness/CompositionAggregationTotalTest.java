package bgu.compositionAggregationCorrectness;

/**
 * Created by user on 26/07/14.
 */
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import bgu.compositionAggregationCorrectness.ACcycle;
import bgu.compositionAggregationCorrectness.CompositionAggregationCorrectnessIdentification;
import bgu.compositionAggregationCorrectness.LabledEdge;
import bgu.detection.FiniteSatDetector;
import bgu.identification.FiniteSatIdentifier;
import bgu.propagation.DisjointConstraintPropogator;
import bgu.propagation.DisjointIncompleteConstraintPropogator;


public class CompositionAggregationTotalTest {


    public static void main(String[] args){
        System.out.println("");
        System.out.println("###########################################################################");
        System.out.println("------------ Composition Agrregation Cycles Tests (Algorithm 2) -----------");
        System.out.println("###########################################################################");
        CompositionAggregationCorrectnessTest.testDetection();

        System.out.println("");
        System.out.println("###########################################################################");
        System.out.println("------- Multi Composition Finite Satisfiability Tests (Algorithm 3) -------");
        System.out.println("###########################################################################");
        MultiCompositionNoFiniteSatCorrectness.testDetection();

        System.out.println("");
        System.out.println("###########################################################################");
        System.out.println("----------- Multi Composition Inconsistency Tests (Algorithm 4) -----------");
        System.out.println("###########################################################################");
        MultiCompositionInconsistencyCorrectness.testDetection();
    }
}



