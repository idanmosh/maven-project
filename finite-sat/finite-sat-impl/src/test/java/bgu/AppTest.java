package bgu;

import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.runner.ModelCustomTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

@RunWith(ModelCustomTestRunner.class)
public class AppTest extends TestParent {

    @InjectCompiledModel
    private MModel mModel;



    @Test
    @CompileModels(useFiles = {"testUseSpecs/gen_set_1.use", "testUseSpecs/gen_set_2.use"})
    public void testSlow() {
        System.out.println("testSlow");

    }

    @Test
    public void testSlower() {
        System.out.println("slower");
    }

}