package bgu.circleCheck;

import bgu.circleCheck.CheckerImpl;
import bgu.circleCheck.DangerousCircle;
import bgu.circleCheck.GraphBuilderImpl;
import bgu.tests.TestParent;
import junit.framework.TestSuite;
import org.jgrapht.Graph;
import org.jgrapht.util.VertexPair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertArrayEquals;

@Ignore
public class CheckerTest extends TestParent {

	
	private String fileName = "testUseSpecs/disjointcycle_1.use";
	private Graph<MClass, VertexPair<MClass>> _graph;
	private GraphBuilderImpl _graphBuilder;
	Collection<MGeneralizationSet> _gst;
	private CheckerImpl _out;  //object under test
	private MModel _model;

	@Before
	public void setUp() throws Exception {
        URL resource = findResource(fileName);

        try{
			Reader r = new BufferedReader(new FileReader(resource.getFile()));
			_model = USECompiler.compileSpecification(r,
					fileName, new PrintWriter(System.err),
					new ModelFactory());

			_graphBuilder = new GraphBuilderImpl(_model);
			_graph = _graphBuilder.getGraph();  // undirected graph
            _gst = _model.getGeneralizationSets();  //all generalizations
			_out = new CheckerImpl(_graph, _gst);  //checks circles
			
			
			
			
		}catch(FileNotFoundException e){
			System.out.println("Use test file isn't in place: "+e.getMessage());
		}
	}

	@Test
	public void testCheck() throws IOException{
		String firstCircle = "Circle Alert! A, B, C, A";
		String secondCircle = "Circle Alert! A, B, D, C, A";
		DangerousCircle toTest = null;
		TestSuite suite= new TestSuite("ProjectTrack");
		

		//asserts that the circles that were found are valid
		_out.check(false);
		
		Iterator<DangerousCircle> circIter = _out.getCircles().iterator();				
	
		if(circIter.hasNext())
			toTest = circIter.next();

		//first circle is valid
		assertArrayEquals(firstCircle.toCharArray(), 
				toTest.toString().toCharArray());
		
		if(circIter.hasNext())
			toTest = circIter.next();
		
		//second circle is valid
		assertArrayEquals(secondCircle.toCharArray(),
				toTest.toString().toCharArray());

	}
}
