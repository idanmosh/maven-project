package bgu.circleCheck;

import bgu.circleCheck.CircleChecker;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.runner.ModelCustomTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.uml.mm.MModel;

import static org.junit.Assert.assertTrue;

@RunWith(ModelCustomTestRunner.class)
public class CircleCheckerTest extends TestParent {

    @InjectCompiledModel
	private MModel _model;

    private CircleChecker _out;  //object under test



    @Test
    @CompileModels(useFiles = {"testUseSpecs/disjointcycle_2.use"})
    public void testExistCircle() {
        _out = new CircleChecker(_model);

        //check for circle existence
        assertTrue(_out.existCircle());
    }

}
