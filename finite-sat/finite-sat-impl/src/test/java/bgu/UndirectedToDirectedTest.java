package bgu;

import bgu.circleCheck.GraphBuilderImpl;
import bgu.circleCheck.UndirectedToDirectedImpl;
import bgu.tests.TestParent;
import bgu.tests.annotations.CompileModels;
import bgu.tests.annotations.InjectCompiledModel;
import bgu.tests.runner.ModelCustomTestRunner;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.util.VertexPair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(ModelCustomTestRunner.class)
public class UndirectedToDirectedTest extends TestParent {

	private String fileName = "testUseSpecs/disjointcycle_1.use";
	private UndirectedToDirectedImpl _converter;
	private Graph<MClass, VertexPair<MClass>> _graph;
	private DirectedGraph<MClass, VertexPair<MClass>> _out;
	private GraphBuilderImpl _graphBuilder;

    @InjectCompiledModel
	private MModel _model;
	

	@Test
    @CompileModels(useFiles = {"testUseSpecs/disjointcycle_1.use"})
	public void testGetGraph() {
        _graphBuilder = new GraphBuilderImpl(_model);
        _graph = _graphBuilder.getGraph();  // undirected graph
        _converter = new UndirectedToDirectedImpl(_graph);
        _out = _converter.getGraph();
		
		//checks vertices	
		assertEquals(_model.classes().size(), _out.vertexSet().size());
		
		for (Object o : _model.classes())
			if(o instanceof MClass)
				assertTrue(_out.containsVertex((MClass)o));
		
		//checks edges	
		Iterator<DirectedEdge> edges_iter = 
			_model.generalizationGraph().edgeIterator();
		
		while(edges_iter.hasNext()){
			DirectedEdge edge = edges_iter.next();

			MClass source = (MClass)edge.source(); 
			MClass target = (MClass)edge.target();

			assertTrue(_out.containsEdge(source, target));
			assertTrue(_out.containsEdge(target, source));
		}
	}
}
