package bgu.circleCheck;
import bgu.circleStructure.Circle;
import bgu.circleStructure.CircleImpl;
import org.tzi.use.uml.mm.MClass;

/**
 * The Class DangerousCircle.
 */
public class DangerousCircle extends CircleImpl<MClass> implements Circle<MClass> {
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String s = "Class hierarchy cycle Alert! ";

		for(MClass v : super.getCircle())
			s = s + v.name()+", ";

		s = s.substring(0, s.length()-2);

		return s;
	}
}
