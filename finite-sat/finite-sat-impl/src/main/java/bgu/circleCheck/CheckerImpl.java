package bgu.circleCheck;

import bgu.util.Util;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * The Class CheckerImpl.
 */
public class CheckerImpl implements Checker{

	/** The _graph. */
	private DirectedGraph<MClass, VertexPair<MClass>> _graph;

	/** The _gst. */
	private Collection<MGeneralizationSet> _gst;

	/** The _converter. */
	private UndirectedToDirected _converter;

	/** The _paths. */
	private KShortestPaths<MClass, VertexPair<MClass>> _paths;
	
	/** The _circles. */
	private boolean _circle = false;
	
	/** The _print all circles. */
	private boolean _printAll = false;
	
	/** The _circles. */
	private Vector<DangerousCircle> _circles;
	
	/** The _exit. */
	private boolean _exit = false;
	
	private Util util;

	/**
	 * Instantiates a new checker implementation.
	 *
	 * @param graph the graph
	 * @param gst the generalization set collection
	 */
	public CheckerImpl(Graph<MClass, VertexPair<MClass>> graph, 
			Collection<MGeneralizationSet> gst){
		_gst = gst;
		_converter = new UndirectedToDirectedImpl(graph);
		_graph = _converter.getGraph();
		_circles = new Vector<DangerousCircle>();
		util = Util.getUtil();
	}

	/* (non-Javadoc)
	 * @see bgu.circleCheck.Checker#check()
	 */
	public boolean check(boolean mode){
		
		Vector<MClass> children = new Vector<MClass>();  //going to hold all children 
		//of a potential super class
		Iterator<MGeneralizationSet> gs_iter = _gst.iterator();
		while(gs_iter.hasNext() && !_exit){
			MGeneralizationSet mg = gs_iter.next();
			
			if(mg.isDisjointComplete() || mg.isDisjoint() ||
					mg.isDisjointIncomplete()){
				for(MClass v : mg.getSubClasses()){
					children.add(v);
				}

				for(int i = 0; i<children.size()-1 && !_exit; i++)
					for(int j = i+1; j<children.size() && !_exit; j++){
						if(this.inCircle(children.elementAt(i), 
								children.elementAt(j), mg.getSuperClass(), mode) && mode)
							return true;
					}

				children.clear();
			}
		}

		if(!_circle && !mode)  //no circles
			util.print("There is no circles alert");
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see bgu.circleCheck.Checker#getCircles()
	 */
	public Vector<DangerousCircle> getCircles(){
		return _circles;
	}

	/**
	 * In circle.
	 *
	 * @param source the source
	 * @param target the target
	 * @param superDis the super vertex of a disjoint
	 * @param mode the mode
	 * @return true, if successful
	 */
	private boolean inCircle(MClass source, MClass target, MClass superDis, boolean mode){
		boolean validPath = true;
		
		_paths = new KShortestPaths<MClass, VertexPair<MClass>>(_graph,
				source, _graph.edgeSet().size());

		Iterator<GraphPath<MClass, VertexPair<MClass>>> paths_iter = 
			_paths.getPaths(target).iterator();
		//for(GraphPath<MClass, VertexPair<MClass>> p : _paths.getPaths(target)){
		while(paths_iter.hasNext() && !_exit){
			GraphPath<MClass, VertexPair<MClass>> p = paths_iter.next();
			List<VertexPair<MClass>> lst = p.getEdgeList();		
			List<MClass> pathVertices = this.getPathVertices(lst);

			//checks weather the path goes through super of disjoint set or not
			for(MClass v : pathVertices)
				if(v.name().equals(superDis.name()))
					validPath = false;  

			//printing valid path 
			if(validPath){
				if(mode)  //in case only the existence of one circle at least
					return true;
				else
					this.validPath(pathVertices, superDis);		
			}

			validPath = true;
		}
		
		return false;
	}

	/**
	 * Valid path.
	 *
	 * @param pathVertices the path vertices
	 * @param superDis the super vertex of a disjoint
	 */
	private void validPath(List<MClass> pathVertices, MClass superDis){
		
		String currLine = "";
		InputStreamReader reader = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(reader); 
		
		this.circleFound(superDis, pathVertices);	
		
		_circle = true;  //at least one circle exists

		if(!_printAll){
			
			util.print("Continue looking for more circles? (yes/no/all)");
			try{
				currLine = in.readLine();

				if(currLine.equals("no"))  //in case of "no", system terminates
					_exit = true;          //any other input and algorithm will 
										   //continue looking for more circles
				if(currLine.equals("all")){
					_printAll = true;
				}
				
			}catch(IOException e){
				util.print("Incorrect input: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Circle found.
	 * Creates a new circle.
	 * Prints a new circle.
	 *
	 * @param superDis the super vertex of a disjoint
	 * @param pathVertices the path vertices
	 */
	private void circleFound(MClass superDis, List<MClass> pathVertices){
		DangerousCircle circ = new DangerousCircle();		


		circ.addVertex(superDis);
		for(MClass v : pathVertices)
			circ.addVertex(v);

		circ.addVertex(superDis);
		_circles.add(circ);

		//prints newly discovered circle
		util.print(circ.toString());
	}

	/**
	 * Gets the path vertices.
	 *
	 * @param lst the list of edges in path
	 * @return the path vertices
	 */
	private List<MClass> getPathVertices(List<VertexPair<MClass>> lst){
		List<MClass> path = new ArrayList<MClass>();

		for(VertexPair<MClass> e : lst){
			MClass v1 = e.getFirst();
			MClass v2 = e.getSecond();

			if(!path.contains(v1))
				path.add(v1);

			if(!path.contains(v2))
				path.add(v2);			
		}

		return path;
	}
	
	/**
	 * Prints the circle.
	 *
	 * @param circ the circle
	 * @return the string
	 */
/*	public String circleName(Circle<MClass> circle){
		String s = "Circle Alert! ";

		for(MClass v : circ.getCircle())
			s = s + v.name()+", ";

		s = s.substring(0, s.length()-2);

		return s;
	}*/
}
