package bgu.circleCheck;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Iterator;

/**
 * The Class GraphBuilderImpl.
 */
public class GraphBuilderImpl implements GraphBuilder{

	/** The _model. */
	private MModel _model; 

	/**
	 * Instantiates a new graph builder implementation.
	 *
	 * @param model the model
	 */
	public GraphBuilderImpl(MModel model){
		_model = model;
	}

	/* (non-Javadoc)
	 * @see bgu.circleCheck.GraphBuilder#getGraph()
	 */
	public Graph<MClass, VertexPair<MClass>> getGraph(){

		EdgeFactory<MClass, VertexPair<MClass>> ef = 
			new EdgeFactory<MClass, VertexPair<MClass>>() {
			@Override
			public VertexPair<MClass> createEdge(MClass sourceVertex,
					MClass targetVertex) {
				return new VertexPair<MClass>(sourceVertex, targetVertex);
			}};

			Graph<MClass, VertexPair<MClass>> graph = 
				new SimpleGraph<MClass, VertexPair<MClass>>(ef); 
			DirectedGraph directedGraph = _model.generalizationGraph();	
			
			Iterator<DirectedEdge> edges_iter = directedGraph.edgeIterator();
			
			//adding vertices
			for (Object o : _model.classes()) {
				if (o instanceof MClass) {
					MClass cls = (MClass)o;
					graph.addVertex(cls);
				}
			}

			//adding edges
			while(edges_iter.hasNext()){
				DirectedEdge edge = edges_iter.next();

				MClass source = (MClass)edge.source(); 
				MClass target = (MClass)edge.target();

				graph.addEdge(source, target);
			}

			return graph;
	}
}

