/*
 * This is a circle notifier. Circles are paths 
 * through MClasses hierarchies that includes super MClass of 
 * some generalization set which is disjoint, disjoint complete,
 * disjoint incomplete and two sons (MClasses) of a super 
 * MClass.
 * The notification is about all circles.
 * The notifier will print all circles.
 * 
 * Author MichaelB
 * 
 */

package bgu.circleCheck;

import org.jgrapht.Graph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;

/**
 * The Class CircleChecker.
 */
public class CircleChecker {
	
	/** The _graph builder. */
	GraphBuilderImpl _graphBuilder;
	
	/** The _graph. */
	Graph<MClass, VertexPair<MClass>> _graph;
	
	/** The _gst. */
	Collection<MGeneralizationSet> _gst;

	/**
	 * Instantiates a new circle checker.
	 *
	 * @param model the model
	 * //@param fileName the file name
	 */
	public CircleChecker(MModel model){

		_graphBuilder = new GraphBuilderImpl(model);
		_graph = _graphBuilder.getGraph();  // undirected graph
		_gst = model.getGeneralizationSets();  //all generalizations
	}
	
	
	/**
	 * Check.
	 * Finds all circles if needed
	 */
	public void check(){

		CheckerImpl checker = new CheckerImpl(_graph, _gst);  //checks circles
		checker.check(false);		
	}
	
	/**
	 * Exist circle.
	 * Check only the existence of at least one circle
	 * @return true, if successful
	 */
	public boolean existCircle(){
		CheckerImpl checker = new CheckerImpl(_graph, _gst);  //checks circles
		return checker.check(true);	
	}
}

