package bgu.circleCheck;

import org.jgrapht.Graph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;

/**
 * The Interface GraphBuilder.
 */
public interface GraphBuilder {
	
	/**
	 * Gets the graph. Undirected graph is being build from a model.
	 *
	 * @return the graph
	 */
	public Graph<MClass, VertexPair<MClass>> getGraph();

}
