package bgu.circleCheck;

import org.jgrapht.DirectedGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;

/**
 * The Interface UndirectedToDirected.
 * Directed graph is being build from an undirected graph.
 */
public interface UndirectedToDirected {
	
	/**
	 * Gets the graph.
	 *
	 * @return the graph
	 */
	public DirectedGraph<MClass, VertexPair<MClass>> getGraph();

}
