package bgu.circleCheck;


/**
 * The Interface Checker.
 */
public interface Checker {

    /**
     * Check. Perform a circle check that includes generalization
     * set which is disjoint, disjoint complete,
     * disjoint incomplete set of vertices.
     */
    public boolean check(boolean mode);

}
