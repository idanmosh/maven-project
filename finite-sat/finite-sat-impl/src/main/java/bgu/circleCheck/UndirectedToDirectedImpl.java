package bgu.circleCheck;

import org.jgrapht.DirectedGraph;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;

/**
 * The Class UndirectedToDirectedImpl.
 */
public class UndirectedToDirectedImpl implements UndirectedToDirected{

	/** The _graph. */
	private Graph<MClass, VertexPair<MClass>> _graph;

	/**
	 * Instantiates a new undirected to directed implementation.
	 *
	 * @param graph the graph
	 */
	public UndirectedToDirectedImpl(Graph<MClass, VertexPair<MClass>> graph){
		_graph = graph;
	}

	/* (non-Javadoc)
	 * @see bgu.circleCheck.UndirectedToDirected#getGraph()
	 */
	public DirectedGraph<MClass, VertexPair<MClass>> getGraph(){

		EdgeFactory<MClass, VertexPair<MClass>> ef = 
			new EdgeFactory<MClass, VertexPair<MClass>>() {
			@Override
			public VertexPair<MClass> createEdge(MClass sourceVertex,
					MClass targetVertex) {
				return new VertexPair<MClass>(sourceVertex, targetVertex);
			}};
			
			//a new directed graph
			DirectedGraph<MClass, VertexPair<MClass>> _directedGraph = 
				new SimpleDirectedGraph<MClass, VertexPair<MClass>>(ef);

			//vertices of a directed graph
			for(MClass v : _graph.vertexSet())
				_directedGraph.addVertex(v);

			//edges of a directed graph
			for(VertexPair<MClass> e : _graph.edgeSet()){
				_directedGraph.addEdge(e.getFirst(), e.getSecond());
				_directedGraph.addEdge(e.getSecond(), e.getFirst());
			}

			return _directedGraph;
	}

}
