package bgu.transformation;

import bgu.util.DeepCopyMModel;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.util.Matcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SubsetTransformator extends DeepCopyMModel implements Transformator {

	private SubsetTransformator() {
	}
	
	public static Transformator getInstanse() {
		return new SubsetTransformator();
	}
	
	@Override
	public MModel transform(MModel model){
		MModel newModel = factory.createModel(model.name());
		super.setNewModel(newModel);
		super.setOldModel(model);
		try {
			
			copyEnums();
			copyClasses();
			copyAssociations();
			copyGeneralizations();
			copyGEneralizationSets();
			copyXors();

			addSubsetting();
				
		} catch (MInvalidModelException e) {
			
			System.err.println("Error: " + e.getMessage() + " can't continue...");
			System.exit(-1);
		}
		
		return super.tar_model;

	}

	private void addSubsetting() throws MInvalidModelException {
		Collection<MAssociation> coll = (Collection<MAssociation>)tar_model.associations();
		for (MAssociation assoc : coll) {
			if (!assoc.parents().isEmpty()) {
				updateAutoSubset(assoc, assoc.parents());			
			}
		}
	}

	/**
	 * Adds to the model conforms that are implicit (not defined explicitly by {conforms to ..})
	 * @param assoc association that should be conformed
	 * @param parents  collection of parent associations.
	 * @throws MInvalidModelException if 'assoc' can't conform to one of it's parents
	 */
	private void updateAutoSubset(MAssociation assoc,
			Set<MAssociation> parents) throws MInvalidModelException {

		MAssociationEnd sup, sub;
		for (MAssociation parentAssoc : parents) {
			//find all not subsetted ends from 'parentAssoc'
			
			List<MAssociationEnd> notSubsetted = getNotsubsettedAssocEnds(assoc, parentAssoc);
			//if all assocEnd subsetted then no need to autoSubset
			if (notSubsetted.isEmpty()) continue; 
			//find all not subsetting ends from 'assoc'
			List<MAssociationEnd> notSubsetting = getNotSubsettingAssocEnds(assoc, parentAssoc);
			//match both lists by hierarchy
			notSubsetting = (List<MAssociationEnd>) Matcher.findMatchByHierarchy(notSubsetted, notSubsetting);
			
			
			
			Iterator<MAssociationEnd> supIt, subIt;
			supIt = notSubsetted.iterator();
			subIt = notSubsetting.iterator();
			while (supIt.hasNext() && subIt.hasNext()) {
				sup = supIt.next();
				sub = subIt.next();
			
				
		/*		if (!sub.subset().subsetNotSameTypeTest(sup)){
					
					System.out.println("Test 1");
		    		throw new MInvalidModelException(" The association" +
		    				sup.association().name() +
		    				"is recursive. There is no way to derive the subsetting relations between" +
		    				"the association ends"); 
				}*/
				sub.subset().subsetNotSameTypeTest(sup);
				sub.subset().validateSubset(sup);
				
				sub.subset().addSubsettedAssocEnd(sup);
			}
		}
	}
				
	
	
	
	private List<MAssociationEnd> getNotSubsettingAssocEnds(MAssociation assoc,
			MAssociation parentAssoc) {
		List<MAssociationEnd> result = new ArrayList<MAssociationEnd>(assoc.associationEnds());
		result.removeAll(getSubsettingEnds(assoc.associationEnds()));
		return result;
	}


	private List<MAssociationEnd> getNotsubsettedAssocEnds(MAssociation assoc,
			MAssociation parentAssoc) {
		List<MAssociationEnd> result = new ArrayList<MAssociationEnd>(parentAssoc.associationEnds());
		result.removeAll(getSubsettedEnds(assoc.associationEnds()));
		return result;
	}

	private List<MAssociationEnd> getSubsettedEnds(List<MAssociationEnd> associationEnds) {
		List<MAssociationEnd> result = new ArrayList<MAssociationEnd>();
		for (MAssociationEnd assocEnd : associationEnds) {
			result.addAll(assocEnd.conform().getConformed());
		}
		return result;
	}
	
	private List<MAssociationEnd> getSubsettingEnds(List<MAssociationEnd> associationEnds) {	
		List<MAssociationEnd> result = new ArrayList<MAssociationEnd>();
		for (MAssociationEnd assocEnd : associationEnds) {
			result.addAll(assocEnd.conform().getConforming());
		}
		return result;
	}

	
	

	@Override
	public MModel createNewModel() {
		return null;
	}



}
