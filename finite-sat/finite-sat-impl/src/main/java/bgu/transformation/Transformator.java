package bgu.transformation;

import org.tzi.use.uml.mm.MModel;

public interface Transformator {
	
	public MModel transform(MModel model) ;
}
