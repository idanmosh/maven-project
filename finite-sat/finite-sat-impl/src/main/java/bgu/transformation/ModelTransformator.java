package bgu.transformation;

import bgu.cmd.Options;
import org.tzi.use.uml.mm.MModel;

//TODO vitalyb use DeepCopyMModel instead

public class ModelTransformator implements Transformator {

	private static ModelTransformator mt = null;
	
	private ModelTransformator() {
		
	}

	public static ModelTransformator getInstance() {
		if (mt == null) {
			mt = new ModelTransformator();
		}
		return mt;
	}
	
	
	@Override
	public MModel transform(MModel model) {
		
		if (Options.getAutoConformIsOn()) {
			
			
			Transformator subsetTrunsformator = SubsetTransformator.getInstanse();
			model = subsetTrunsformator.transform(model);
		}
		
		//System.out.println(model);
		
		if (Options.getXorModelIsOveriding()) {
			Transformator xorTransformator = XorTransformator.getInstance();
			model =  xorTransformator.transform(model);
		}
		
		return model;
	}


}
