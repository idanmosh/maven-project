package bgu.transformation;

import bgu.util.DeepCopyMModel;
import org.tzi.use.uml.mm.MAggregationKind;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralization;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MXor;
import org.tzi.use.uml.mm.ModelFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class XorTransformator extends DeepCopyMModel implements Transformator {
	
	private static XorTransformator xortm = null;

	
	private XorTransformator() {
		super();
	}


	public static XorTransformator getInstance() {
		if (xortm == null) {
			xortm = new XorTransformator();
		}		
		return xortm;
	}

	
	
	@Override
	public MModel transform(MModel model) {
		
		if (model.getXorSet().isEmpty()) {
			return model;
		}
		
		
		MModel newModel = factory.createModel(model.name());
		super.setNewModel(newModel);
		super.setOldModel(model);
		try {
			copyEnums();
			copyClasses();
			copyAssociations();
			copyGeneralizations();
			copyGEneralizationSets();


			createXors(newModel, model);
				
		} catch (MInvalidModelException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		
		return newModel;
	}


	@Deprecated
	private void transformXors(MModel newModel, MModel model) throws MInvalidModelException {
		MGeneralizationSet gs;
		MGeneralization gen;
		MClass cls;
		MAssociation assoc;
		MAssociationEnd assocEnd;
		MMultiplicity mult;
		for (MXor xor : model.getXorSet()) {
			gs = factory.createGeneralizationSet("gs_for_xor_'" + xor.name() +"'");
			gs.setType("disjoint");
			gs.setSuperClass(newModel.getClass(xor.getSourceClass().name()));
			
			for (MAssociationEnd end : xor.getEnds()) {
				cls = factory.createClass("chain_cls_for_xor_" 
						+ xor.name() 
						+ "_and cls:_" 
						+ end.cls().name() , false);
				
				newModel.addGeneralization(factory.createGeneralization(cls, gs.getSuperClass()));
				
				assoc = factory.createAssociation("assoc_for_xor_" 
						+ xor.name() 
						+ "_and cls_" 
						+ end.cls().name());
				
				mult = factory.createMultiplicity();
				mult.addRange(end.otherSideAssocEnd().multiplicity().getRange().getLower(), 
						end.otherSideAssocEnd().multiplicity().getRange().getUpper());
				
				assocEnd = factory.createAssociationEnd(cls, 
						end.otherSideAssocEnd().nameAsRolename() + "_for_xor_" + xor.name() , 
						mult,
						MAggregationKind.NONE, 
						false);
				
				assoc.addAssociationEnd(assocEnd);
				
				mult = factory.createMultiplicity();
				mult.addRange(end.multiplicity().getRange().getLower(), 
						end.multiplicity().getRange().getUpper());
			
				assocEnd = factory.createAssociationEnd(cls, 
						end.otherSideAssocEnd().nameAsRolename() , 
						mult,
						MAggregationKind.NONE, 
						false);
				
				assoc.addAssociationEnd(assocEnd);
				
				newModel.addClass(cls);
				newModel.addAssociation(assoc);
			}
			
			newModel.addGeneralizationSet(gs);
			
		}
		
	}

	private void createXors(MModel newModel, MModel model) throws MInvalidModelException {
		for (MXor xor : model.getXorSet()) {
			tranformXor(xor, newModel);
		}
	}
	
	private void tranformXor(MXor xor, MModel model) throws MInvalidModelException {
		MClass cls;
		MGeneralization gen;
		MAssociation association;
		MAssociationEnd newAssocEnd;
		MMultiplicity multiplicity;
		String className, assocName;

		ModelFactory factory = new ModelFactory();
		MGeneralizationSet gs  = new MGeneralizationSet("gs_for_"+ xor.name());
		gs.setType("disjoint");
		gs.setSuperClass(model.getClass(xor.getSourceClass().name()));
		
		for (MAssociationEnd end: xor.getEnds()) {
			className = "chain_cls_for_xor_" 
				+ xor.name() 
				+ "_and_cls_" 
				+ end.cls().name();
			cls = factory.createClass(className, false);		

			model.addClass(cls);				
			gs.addSubClass(cls);
			
			gen = factory.createGeneralization(cls, model.getClass(xor.getSourceClass().name()));
			model.addGeneralization(gen);
			
			//create assoctiation
			assocName = "assoc_for_xor_" 
			+ xor.name() 
			+ "_and cls_" 
			+ end.cls().name();
			association = factory.createAssociation(assocName);
			
			//create associationEnd for the xored end
			multiplicity = end.multiplicity().clone();
			
			newAssocEnd = factory.createAssociationEnd(model.getClass(end.cls().name()),
					end.name() + "_for_" + xor.name(), multiplicity, MAggregationKind.NONE, false);
			
			association.addAssociationEnd(newAssocEnd);
			
			//create associationEnd for the other end
			multiplicity = end.otherSideAssocEnd().multiplicity().clone();
			
			newAssocEnd = factory.createAssociationEnd(cls, 
					end.otherSideAssocEnd().name() + "_for_" + xor.name(), 
					multiplicity, MAggregationKind.NONE, false);
			
			association.addAssociationEnd(newAssocEnd);
			
			model.addAssociation(association);
			model.generalizationGraph().add(association);
		}
		
		model.addGeneralizationSet(gs);
	}
	
	public void Associations(MModel newModel, MModel model) throws MInvalidModelException {
		MAssociation newAssoc;
		MAssociationEnd end;
		for(MAssociation assoc : (Collection<MAssociation>)model.associations()) {
			
			//associations that containd in xors are excluded, 
			//they will be added later as part of the gs that represents the xor
			if (containedInXor(model, assoc)) {
				continue;
			}
			
			if (assoc instanceof MAssociationClass) {
				newAssoc = newModel.getAssociationClass(((MAssociationClass)assoc).name());
			}
			//assoc is an instance of Massociation
			else {
				newAssoc = factory.createAssociation(assoc.name());
			}
			ListIterator<MAssociationEnd> it = assoc.associationEnds().listIterator();
			while (it.hasNext()) {
				end = it.next();
				newAssoc.addAssociationEnd(
						factory.createAssociationEnd(
								newModel.getClass(end.cls().name()), 
								end.nameAsRolename(), 
								end.multiplicity().clone(), 
								end.aggregationKind(), 
								end.isOrdered()));
			}
			
			newModel.addAssociation(newAssoc);
			newModel.generalizationGraph().add(newAssoc);
		}
		
	}
	
	private boolean containedInXor(MModel model, MAssociation assoc) {
		Iterator<MAssociationEnd> it = assoc.associationEnds().iterator();
		
		while (it.hasNext()) {
			if (it.next().isPartOfXor()) {
				return true;
			}
		}
		return false;
	}


	@Override
	public MModel createNewModel() {
		
		return null;
	}
}
