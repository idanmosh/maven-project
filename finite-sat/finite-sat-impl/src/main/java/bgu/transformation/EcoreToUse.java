package bgu.transformation;

import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClassImpl;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.ModelFactory;
//import antlr.collections.List;



public class EcoreToUse {
	ModelFactory usemodeinstance;
	MModel modelEx;

	public EcoreToUse() {


		usemodeinstance = new ModelFactory();
		modelEx = usemodeinstance.createModel("Test");
		try {
			MClassImpl c1 = (MClassImpl) usemodeinstance.createClass("A", true);
			MClassImpl c2 = (MClassImpl) usemodeinstance.createClass("B", true);
			MClassImpl c3 = (MClassImpl) usemodeinstance.createClass("C", true);
			MClassImpl c4 = (MClassImpl) usemodeinstance.createClass("D", true);
			modelEx.addClass(c1);
			modelEx.addClass(c2);
			modelEx.addClass(c3);
			modelEx.addClass(c4);



			MMultiplicity m1 = new MMultiplicity(1, -1);
			MMultiplicity m2 = new MMultiplicity(1, 1);
			MMultiplicity m3 = new MMultiplicity(2, 2);
			MMultiplicity m4 = new MMultiplicity(1, 1);


			MAssociationEnd assend1 = usemodeinstance.createAssociationEnd(c1,"r1",m1,0,false);
			
			
			//MAssociationEnd assend1 = new MAssociationEnd(c1, "r1", m1, 0, false);
			MAssociationEnd assend2 = new MAssociationEnd(c2, "r2", m2, 0, false);

			MAssociation as1 = usemodeinstance.createAssociation("r");
			as1.addAssociationEnd(assend1);
			as1.addAssociationEnd(assend2);

			modelEx.addAssociation(as1);


			MAssociationEnd assend3 = usemodeinstance.createAssociationEnd(c1, "r3", m3, 0, false); 

			//new MAssociationEnd(c1, "r3", m3, 0, false);
			MAssociationEnd assend4 = new MAssociationEnd(c2, "r4", m4, 0, false);

			MAssociation as2 = usemodeinstance.createAssociation("q");
			as2.addAssociationEnd(assend3);
			as2.addAssociationEnd(assend4);

			modelEx.addAssociation(as2);
		}
		 
		catch (MInvalidModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}






	}
	public ModelFactory getModelFactory(){
		return usemodeinstance;
	}
	public MModel getMModel(){
		return modelEx;

	}

}
