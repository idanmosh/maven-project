package bgu.propagation;

import bgu.propagation.graph.DisjointGraphFactory;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This class implements 'Disjoint Algorithm' as described in 
 * 'Finite Satisfiability of UML Class Diagrams with Constrained Class Hierarchy' page 19
 * @author vitalib
 *
 */
public class DisjointConstraintPropogator extends ConstraintPropogator implements Propogator {

	
 	public DisjointConstraintPropogator () {
 		
 		updatedModelBuilder = new DisjointConstraintModelBuilder();
 	}
 	
 	public static DisjointConstraintPropogator getNewInstance() {
 		return new DisjointConstraintPropogator();
 	}
 	
	
	@Override
	public MModel propogate(MModel model) {
			
		createGraphs(model);
		
		createCliques(model);
			
		MModel mmodel = updateModel(model, disjointCliquesMap);
		
		
		
		return mmodel;
	}

	private void createCliques(MModel model) {
		TopCliquesFactory disjointCliquesFactory = 
			new TopCliquesFactory(disjointGraph);		
		
		disjointCliquesMap = 
			disjointCliquesFactory.generateTopCliques(model);
	}
	
	
	/**
	 * Recreate the model with the propagated constraints 
	 * @param model - use MModel
	 * @param cliquesMap - collection of cliques form dis_graph
	 * @return recreated MModel.
	 */
	private MModel updateModel(MModel model,
			Map<MClass, Collection<Set<MClass>>> cliquesMap) {
		if (cliquesMap.isEmpty()) return model;
		updatedModelBuilder = 	new DisjointConstraintModelBuilder(model, cliquesMap);
		return updatedModelBuilder.createNewModel();
	}


	private void createGraphs(MModel model) {
		DisjointGraphFactory disjointGraphFactory = new DisjointGraphFactory();
	
		disjointGraph = disjointGraphFactory.buildGraph(model);
		
		DisjointIncompleteConstraintPropogator.setDisjointGraph(disjointGraph);
	}

    public SimpleGraph<MClass, VertexPair<MClass>> getDisjointGraph(){
        return disjointGraph;
    }
    public Map<MClass, Collection<Set<MClass>>> getDisjointCliquesMap(){
        return disjointCliquesMap;
    }
}
