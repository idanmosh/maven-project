package bgu.propagation;

import org.tzi.use.uml.mm.MModel;

public interface Propogator {

	public MModel propogate(MModel model);
}
