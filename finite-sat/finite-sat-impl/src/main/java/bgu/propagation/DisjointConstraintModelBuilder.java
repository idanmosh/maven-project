package bgu.propagation;

import bgu.util.DeepCopyMModel;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class builds a new MModel instance with the propagated 'disjoint' constraint.
 * @author vitalib
 *
 */

public class DisjointConstraintModelBuilder extends DeepCopyMModel implements ModelBuilder {

	private final Map<MClass, Collection<Set<MClass>>> cliquesMap;
	private List<MGeneralizationSet> addedGSS;
	private List<MGeneralizationSet> updatedGSS;

	public DisjointConstraintModelBuilder(){
		cliquesMap = null;
		addedGSS = new ArrayList<MGeneralizationSet>();
		updatedGSS = new ArrayList<MGeneralizationSet>();
	}
	
	

	public DisjointConstraintModelBuilder(MModel model, Map<MClass, Collection<Set<MClass>>> cliquesMap) {
		super(model);
		this.cliquesMap = cliquesMap;
		addedGSS = new ArrayList<MGeneralizationSet>();
		updatedGSS = new ArrayList<MGeneralizationSet>();
	}

	/* (non-Javadoc)
	 * @see bgu.propagation.ModelBuilder#getNewDisjointGSs()
	 */
	@Override
	public List<MGeneralizationSet> getNewDisjointGSs(){


		return addedGSS;
	}
	

	/* (non-Javadoc)
	 * @see bgu.propagation.ModelBuilder#getUpdatedDisjointGSs()
	 */
	@Override
	public List<MGeneralizationSet> getUpdatedDisjointGSs(){

		return updatedGSS;
	}


	/* (non-Javadoc)
	 * @see bgu.propagation.ModelBuilder#createNewModel()
	 */
	@Override
	public MModel createNewModel() {
		try {

			super.copyEnums();
			super.copyClasses();
			super.copyAssociations();
			super.copyGeneralizations();
			super.copyXors();		
			this.addGenSets();



			return super.getNewModel();
		}catch(MInvalidModelException e) {
			System.err.println(e);
		}


		//should never be reached
		return null;
	}

	private void addGenSets() {
		MGeneralizationSet newGs;

		for (MGeneralizationSet gs : src_model.getGeneralizationSets()) {
			newGs = factory.createGeneralizationSet(gs.getGSName());
			newGs.setSuperClass(tar_model.getClass(gs.getSuperClass().name()));
			for (MClass cls : gs.getSubClasses()) {
				newGs.addSubClass(tar_model.getClass(cls.name()));
			}
			newGs.setType(gs.getType());


			tar_model.addGeneralizationSet(newGs);				
		}

		addDisjointGensets();

		removeRedunduntGensets();
	}

	private void removeRedunduntGensets() {

		Collection<MGeneralizationSet> gsSet = this.tar_model.getGeneralizationSets();
		Iterator<MGeneralizationSet> gsIt1 = gsSet.iterator();
		Iterator<MGeneralizationSet> gsIt2 = gsSet.iterator();
		MGeneralizationSet gs1, gs2, toRemove;

		while(gsIt1.hasNext()) {

			gs1 = gsIt1.next();
			//System.out.println("\nGS 1 " + gs1.toString());
			gsIt2 = gsSet.iterator();
			while (gsIt2.hasNext()) {
				gs2 = gsIt2.next();
				//System.out.println("\nGS 2 " + gs2.toString() +"\n");
				if (gs2 != gs1) {
					//FIXME: I Removed the calling to the function validTyes!
					if (hasSameClasses(gs1, gs2)) {
						toRemove = getWeaker(gs1, gs2);
						//System.out.println("Remove : " + toRemove.toString());
						gsSet.remove(toRemove);
						gsIt1 = gsSet.iterator();
						gsIt2 = gsSet.iterator();
						break;
					}
				}
			}
		}

	}

	private boolean validTyes(MGeneralizationSet gs1, MGeneralizationSet gs2) {
		boolean forGs1 = gs1.isDisjoint() || gs1.isDisjointComplete() || gs1.isDisjointIncomplete() || gs1.isOverlappingIncomplete() || gs1.isIncomplete();
		boolean forGs2 = gs2.isDisjoint() || gs2.isDisjointComplete() || gs2.isDisjointIncomplete() || gs1.isOverlappingIncomplete() || gs2.isIncomplete();
		return forGs1 && forGs2;
	}

	private MGeneralizationSet getWeaker(MGeneralizationSet gs1,
			MGeneralizationSet gs2) {
		if (gs1.isOverlappingIncomplete() || gs1.isIncomplete() || gs1.isOverlapping()) return gs1;
		if (gs2.isOverlappingIncomplete() || gs2.isIncomplete() || gs2.isOverlapping()) return gs2;
		if (gs1.isDisjoint() && gs2.isDisjointComplete()) return gs1;
		if (gs1.isDisjoint() && gs2.isDisjointIncomplete()) return gs1;
		if (gs2.isDisjoint() && gs1.isDisjointComplete()) return gs2;
		if (gs2.isDisjoint() && gs1.isDisjointIncomplete()) return gs2;
		if (gs1.isDisjointIncomplete() && gs2.isDisjointComplete()) return gs1;
		if (gs2.isDisjointIncomplete() && gs2.isDisjointComplete()) return gs2;
		return gs1;
	}

	private boolean hasSameClasses(MGeneralizationSet gs1,
			MGeneralizationSet gs2) {
		if(gs1.getSuperClass() != gs2.getSuperClass()) return false;
		if(!gs1.getSubClasses().containsAll(gs2.getSubClasses()) || 
				!gs2.getSubClasses().containsAll(gs1.getSubClasses())) 
			return false;
		return true;
	}
	
	
	private boolean checkSuperClassCoverage(MGeneralizationSet gs){
		MClass superclass = gs.getSuperClass();
		//System.out.println("New_GS " + gs.toString());
		List<MClass> subclesses = gs.getSubClasses();
		boolean isSuperClassCoverage =false;

		Set<MClass> allDescendants = superclass.allChildren();


/*		if (superclass.name().equals("A")){
			System.out.println("A:--");
			for (MClass cls: allDescendants){
				System.out.print(cls.name() +",");

			}

		}*/
		
		
		//System.out.println();

		//Set<MClass> subclassDescendants;

		for (MClass cls: allDescendants){

			//subclassDescendants = cls.allChildren();

			//System.out.println("Child:" + cls.name());
			if (cls.allChildren().containsAll(subclesses)){
			//	System.out.println("There is covererage by " + cls.name());
				return true;
			}
			


		}

		return false;

	}

	@SuppressWarnings("unchecked")
	private void addDisjointGensets() {
		MGeneralizationSet gs;
		int i = 1;
		
		
		boolean updatedGS =false;
//		boolean newGS = false;
		boolean added = false;

		Collection<MGeneralizationSet> gsSet = tar_model.getGeneralizationSets();


		

		for (MClass cls : this.cliquesMap.keySet()) {
			for ( Set<MClass> set : this.cliquesMap.get(cls)) {
				gs = factory.createGeneralizationSet("tempname_" + i);
				gs.addSubClassSet(set);
				gs.setSuperClass(tar_model.getClass(cls.name()));

				for(MGeneralizationSet existGS: gsSet){


					if ( hasSameClasses(gs, existGS)){
						if (existGS.isComplete() || existGS.isOverlappingComplete()){
							gs.setGSName("gs_disjoint_" + i++);
							gs.setType("disjoint_complete");
							updatedGS = true;
							
							added =true;
						}
						else 
							if (existGS.isIncomplete()|| existGS.isOverlappingIncomplete()){

								gs.setGSName("gs_disjoint_incomplete_" + i++);
								gs.setType("disjoint_incomplete");

								updatedGS = true;								
								added =true;
								
							}
							else 
								if (existGS.isDisjoint() || existGS.isDisjointIncomplete()||existGS.isDisjointComplete()){
									added = true;
									
								}
						
									
						
					}
				}


				if (!added){

					//The GS do not existed in the Model.
					gs.setGSName("gs_disjoint_" + i++);	
					gs.setType("disjoint");
					added = true;
					//System.out.println("GS AFTER ADDING IS Test4: " + gs.toString());
				}


				//Add only non existed GS
				if (added && !checkSuperClassCoverage(gs) && !gs.getType().equals("overlapping")){
					
					tar_model.addGeneralizationSet(gs);
					//System.out.println("GS AFTER ADDING IS Test5: " + gs.toString());
					if (updatedGS)
						updatedGSS.add(gs);
					else
						addedGSS.add(gs);
				}

				added = false;
				updatedGS =false;
				
			}
			added = false;
			updatedGS =false;
	

		}
	}
}
