package bgu.propagation;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class TopCliquesFactory {

	SimpleGraph<MClass, VertexPair<MClass>> graph;

	public TopCliquesFactory(SimpleGraph<MClass, VertexPair<MClass>> graph) {
		this.graph = graph;
	}


	public Map<MClass, Collection<Set<MClass>>> generateTopCliques(MModel model) {

		CliquesFactory cliquesGenerator = 
			new CliquesFactory(graph);

		Map<MClass, Collection<Set<MClass>>> cliquesMap = 
			cliquesGenerator.generateCliques(model);

		for (MClass cls : cliquesMap.keySet()) {

			Collection<Set<MClass>> cliques = cliquesMap.get(cls);

			Iterator<Set<MClass>> it1 = cliques.iterator();

			while (it1.hasNext()) {
				Set<MClass> clique1 = it1.next();
				Iterator<Set<MClass>> it2 = cliques.iterator();
				while (it2.hasNext()) {
					Set<MClass> clique2 = it2.next();
					if (clique1 != clique2) {
						if (firstContainsSecond(clique1, clique2)) {
							cliques.remove(clique2);
							it1 = cliques.iterator();
							it2 = cliques.iterator();
						}
					}

				}
			} //end while

			//not sure next line is needed 
			cliquesMap.put(cls, cliques);
		}

		return cliquesMap;
	}


	/**
	 * Given 2 sets this function reruns true iff first set of classes 'contains' the second set. <br>
	 * 'contains' relationship is defined by inheritance 
	 * @param clique1 first set
	 * @param clique2 second set
	 * @return true iff clique1 contains ciique2
	 */
	private boolean firstContainsSecond(Set<MClass> clique1, Set<MClass> clique2) { 
		for (MClass cls : clique2) {
			if (!setContainsClass(clique1, cls)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * returns true iff set 'contains' class(cls) <br>
	 * 'contains' relationship is defined by inheritance 
	 * @param clique1 set of MClass instances that should be disjoint
	 * @param cls MClass instance in question
	 * @return true if clique1 contins class that is a super class of 'cls'
	 */
	private boolean setContainsClass(Set<MClass> clique1, MClass cls) {
		for (MClass mClass : clique1) {
			if (mClass.isSuperClassOf(cls)) return true;
		}
		return false;
	}
}
