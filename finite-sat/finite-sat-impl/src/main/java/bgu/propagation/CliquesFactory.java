package bgu.propagation;

import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.MaskFunctor;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.UndirectedMaskSubgraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CliquesFactory {

	
	SimpleGraph<MClass, VertexPair<MClass>> graph;
	
	public CliquesFactory(SimpleGraph<MClass, VertexPair<MClass>> graph) {
		this.graph = graph;
	}

	public Map<MClass ,Collection<Set<MClass>>> generateCliques(MModel model) {
		
		Map<MClass ,Collection<Set<MClass>>> cliquesMap = 
			new HashMap<MClass, Collection<Set<MClass>>>();
				
		
		//for each class in model
		for (Object o : model.classes()) {
			final MClass cls = (MClass) o;
			//filter out all classes that are not children of 'cls'
			MaskFunctor<MClass, VertexPair<MClass>> mask = new MaskFunctor<MClass, VertexPair<MClass>>() {
				@Override
				public boolean isVertexMasked(MClass vertex) {
					return !cls.allChildren().contains(vertex);
				}
				@Override
				public boolean isEdgeMasked(VertexPair<MClass> edge) {
					return isVertexMasked(edge.getFirst()) || isVertexMasked(edge.getSecond());
				}
			};
			//create subgraph that contains only children of 'cls'
			UndirectedMaskSubgraph<MClass, VertexPair<MClass>> subGraph = 
				new UndirectedMaskSubgraph<MClass, VertexPair<MClass>>(graph, mask);
			
			BronKerboschCliqueFinder<MClass, VertexPair<MClass>> cliqueFinder = 
				new BronKerboschCliqueFinder<MClass, VertexPair<MClass>>(subGraph);
			
			Collection<Set<MClass>> cliques = cliqueFinder.getAllMaximalCliques();
			
			cliques = removeCliquesOfOne(cliques);
			
			if (!cliques.isEmpty()) {
				cliquesMap.put(cls, cliques);
			}
		}

			return cliquesMap;

	}
		
		/**
		 * This function removes sets of size 1 from collection of disjoint cliques
		 * @param cliques - collection containing sets of MClass instances that should be disjoint
		 * @return same collection but without  sets of size 1
		 */
		private Collection<Set<MClass>> removeCliquesOfOne(Collection<Set<MClass>> cliques) {
			ArrayList<Set<MClass>> ret = new ArrayList<Set<MClass>>(cliques.size());
			for (Set<MClass> clq : cliques) {
				if (clq.size() > 1) ret.add(clq);
			}
			return ret;
			
		}

}
