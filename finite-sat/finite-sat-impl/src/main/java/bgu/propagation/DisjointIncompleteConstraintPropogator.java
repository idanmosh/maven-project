package bgu.propagation;

import bgu.propagation.graph.DisjointIncompleteGraphFactory;
import bgu.propagation.graph.IncompleteGraphFactory;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This class implements 'Disjoint Algorithm' as described in 
 * 'Finite Satisfiability of UML Class Diagrams with Constrained Class Hierarchy' page 19
 * @author vitalib
 *
 */
public class DisjointIncompleteConstraintPropogator extends ConstraintPropogator implements Propogator {

	private static SimpleGraph<MClass, VertexPair<MClass>> disjointGraph;
	
 	private DisjointIncompleteConstraintPropogator () {
 		
 		updatedModelBuilder = new DisjointIncompleteModelBuilder();
 	}
 	
 	public static void setDisjointGraph(SimpleGraph<MClass, VertexPair<MClass>> disjointGraph) {
 		DisjointIncompleteConstraintPropogator.disjointGraph = 
 			disjointGraph;
 	}
 	
 	public static DisjointIncompleteConstraintPropogator getNewInstance() {
 		return new DisjointIncompleteConstraintPropogator();
 	}
 	
	
	@Override
	public MModel propogate(MModel model) {
			
		createGraphs(model);
		
		createCliques(model);
			
		MModel mmodel = updateModel(model, disIncCliquesMap);
		
		
		
		return mmodel;
	}

	private void createCliques(MModel model) {
		
		TopCliquesFactory disIncCliquesFactory =
			new TopCliquesFactory(disIncGraph);
		
		disIncCliquesMap =
			disIncCliquesFactory.generateTopCliques(model);
	}
	
	
	/**
	 * Recreate the model with the propagated constraints 
	 * @param model - use MModel
	 * @param cliquesMap - collection of cliques form dis_graph
	 * @return recreated MModel.
	 */
	private MModel updateModel(MModel model,
			Map<MClass, Collection<Set<MClass>>> cliquesMap) {
		if (cliquesMap.isEmpty()) return model;
		updatedModelBuilder = 	new DisjointIncompleteModelBuilder(model, cliquesMap);
		return updatedModelBuilder.createNewModel();
	}


	private void createGraphs(MModel model) {
		
		IncompleteGraphFactory incompleteGraphFactory = 
			new IncompleteGraphFactory();
		
		
		incompleteGraph = incompleteGraphFactory.buildGraph(model);

		DisjointIncompleteGraphFactory disIncGraphFactory =
			new DisjointIncompleteGraphFactory(disjointGraph, incompleteGraph);
	
		
		disIncGraph = disIncGraphFactory.buildGraph(model);
	}

}
