package bgu.propagation;

import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.List;

public interface ModelBuilder {
	
	

	public abstract List<MGeneralizationSet> getNewDisjointGSs();

	public abstract List<MGeneralizationSet> getUpdatedDisjointGSs();

	public abstract MModel createNewModel();

}