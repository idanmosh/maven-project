package bgu.propagation;

import bgu.propagation.graph.GSIncompleteType;
import bgu.propagation.graph.GSType;
import bgu.util.DeepCopyMModel;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DisjointIncompleteModelBuilder extends DeepCopyMModel implements ModelBuilder{

	private final Map<MClass, Collection<Set<MClass>>> cliquesMap;
	private List<MGeneralizationSet> addedGSS;
	private List<MGeneralizationSet> updatedGSS;

	public DisjointIncompleteModelBuilder(){
		cliquesMap = null;
		addedGSS = new ArrayList<MGeneralizationSet>();
		updatedGSS = new ArrayList<MGeneralizationSet>();
	}



	public DisjointIncompleteModelBuilder(MModel model, Map<MClass, Collection<Set<MClass>>> cliquesMap) {
		super(model);
		this.cliquesMap = cliquesMap;
		addedGSS = new ArrayList<MGeneralizationSet>();
		updatedGSS = new ArrayList<MGeneralizationSet>();
	}

	public List<MGeneralizationSet> getNewDisjointGSs(){


		return addedGSS;
	}


	public List<MGeneralizationSet> getUpdatedDisjointGSs(){

		return updatedGSS;
	}


	public MModel createNewModel() {
		try {

			super.copyEnums();
			super.copyClasses();
			super.copyAssociations();
			super.copyGeneralizations();
			super.copyXors();		
			this.addGenSets();



			return super.getNewModel();
		}catch(MInvalidModelException e) {
			System.err.println(e);
		}


		//should never be reached
		return null;
	}

	private void addGenSets() {
		MGeneralizationSet newGs;

		for (MGeneralizationSet gs : src_model.getGeneralizationSets()) {
			newGs = factory.createGeneralizationSet(gs.getGSName());
			newGs.setSuperClass(tar_model.getClass(gs.getSuperClass().name()));
			for (MClass cls : gs.getSubClasses()) {
				newGs.addSubClass(tar_model.getClass(cls.name()));
			}
			newGs.setType(gs.getType());


			tar_model.addGeneralizationSet(newGs);				
		}

		addDisjointIncompleteGensets();

		removeRedunduntGensets();
	}

	private void removeRedunduntGensets() {

		Collection<MGeneralizationSet> gsSet = this.tar_model.getGeneralizationSets();
		Iterator<MGeneralizationSet> gsIt1 = gsSet.iterator();
		Iterator<MGeneralizationSet> gsIt2 = gsSet.iterator();
		MGeneralizationSet gs1, gs2, toRemove;

		while(gsIt1.hasNext()) {

			gs1 = gsIt1.next();
			//System.out.println("\nGS 1 " + gs1.toString());
			gsIt2 = gsSet.iterator();
			while (gsIt2.hasNext()) {
				gs2 = gsIt2.next();
				//System.out.println("\nGS 2 " + gs2.toString() +"\n");
				if (gs2 != gs1) {
					//FIXME: I Removed the calling to the function validTyes!
					if (hasSameClasses(gs1, gs2) ) {
						toRemove = tryToCombine(gs1, gs2);
						if (toRemove == null) continue;
						gsSet.remove(toRemove);
						gsIt1 = gsSet.iterator();
						gsIt2 = gsSet.iterator();
						break;
					}
				}
			}
		}

	}




	/**
	 * receives 2 GSs and trys to combine them to 1
	 * @return GS that should be removed or null.
	 * If null is returned then the GSs can't be combined.(i.e. gs1=disjoint and gs2=overlapping)
	 */
	private MGeneralizationSet tryToCombine(MGeneralizationSet gs1,
			MGeneralizationSet gs2) {
		if (gs1.getType().equalsIgnoreCase(gs2.getType())) return gs1;
		if ((gs1.isComplete() && gs2.isDisjoint()) ||
				(gs2.isComplete() && gs1.isDisjoint())) {
			gs2.setType("disjoint_complete");
			return gs1;
		}
		if (gs1.isComplete() && gs2.isDisjointComplete()) return gs1;
		if (gs2.isComplete() && gs1.isDisjointComplete()) return gs2;
		if (gs1.isDisjoint() && gs2.isDisjointComplete()) return gs1;
		if (gs2.isDisjoint() && gs1.isDisjointComplete()) return gs2;
		if ((gs1.isComplete() && gs2.isOverlapping()) ||
				(gs2.isOverlapping() && gs1.isComplete())) {
			gs2.setType("overlapping_complete");
			return gs1;
		}
		if (gs1.isComplete() && gs2.isOverlappingComplete()) return gs1;
		if (gs2.isComplete() && gs1.isOverlappingComplete()) return gs2;
		if (gs1.isOverlapping() && gs2.isOverlappingComplete()) return gs1;
		if (gs2.isOverlapping() && gs1.isOverlappingComplete()) return gs2;

		if ((gs1.isIncomplete() && gs2.isDisjoint()) ||
				(gs2.isIncomplete() && gs1.isDisjoint())) {
			gs2.setType("disjoint_incomplete");
			return gs1;
		}
		if (gs1.isIncomplete() && gs2.isDisjointIncomplete()) return gs1;
		if (gs2.isIncomplete() && gs1.isDisjointIncomplete()) return gs2;
		if (gs1.isDisjoint() && gs2.isDisjointIncomplete()) return gs1;
		if (gs2.isDisjoint() && gs1.isDisjointIncomplete()) return gs2;

		if ((gs1.isIncomplete() && gs2.isOverlapping()) ||
				(gs1.isIncomplete() && gs2.isOverlapping())) {
			gs2.setType("overlapping_incomplete");
			return gs1;
		}
		if (gs1.isIncomplete() && gs2.isOverlappingIncomplete()) return gs1;
		if (gs2.isIncomplete() && gs1.isOverlappingIncomplete()) return gs2;
		if (gs1.isOverlapping() && gs2.isOverlappingIncomplete()) return gs1;
		if (gs2.isOverlapping() && gs1.isOverlappingIncomplete()) return gs2;


		return null;
	}



	private boolean validTyes(MGeneralizationSet gs1, MGeneralizationSet gs2) {
		boolean forGs1 = gs1.isDisjoint() || gs1.isDisjointComplete() || gs1.isDisjointIncomplete() || gs1.isOverlappingIncomplete() || gs1.isIncomplete();
		boolean forGs2 = gs2.isDisjoint() || gs2.isDisjointComplete() || gs2.isDisjointIncomplete() || gs1.isOverlappingIncomplete() || gs2.isIncomplete();
		return forGs1 && forGs2;
	}

	private MGeneralizationSet getWeaker(MGeneralizationSet gs1,
			MGeneralizationSet gs2) {
		if (gs1.isOverlappingIncomplete() || gs1.isIncomplete() || gs1.isOverlapping()) return gs1;
		if (gs2.isOverlappingIncomplete() || gs2.isIncomplete() || gs2.isOverlapping()) return gs2;
		if (gs1.isDisjoint() && gs2.isDisjointComplete()) return gs1;
		if (gs1.isDisjoint() && gs2.isDisjointIncomplete()) return gs1;
		if (gs2.isDisjoint() && gs1.isDisjointComplete()) return gs2;
		if (gs2.isDisjoint() && gs1.isDisjointIncomplete()) return gs2;
		if (gs1.isDisjointIncomplete() && gs2.isDisjointComplete()) return gs1;
		if (gs2.isDisjointIncomplete() && gs2.isDisjointComplete()) return gs2;
		return gs1;
	}

	private boolean hasSameClasses(MGeneralizationSet gs1,
			MGeneralizationSet gs2) {
		if(gs1.getSuperClass() != gs2.getSuperClass()) return false;
		if(!gs1.getSubClasses().containsAll(gs2.getSubClasses()) || 
				!gs2.getSubClasses().containsAll(gs1.getSubClasses())) 
			return false;
		return true;
	}


	private boolean checkSuperClassCoverage(MGeneralizationSet gs){
		MClass superclass = gs.getSuperClass();
		//System.out.println("New_GS " + gs.toString());
		List<MClass> subclesses = gs.getSubClasses();
		boolean isSuperClassCoverage =false;

		Set<MClass> allDescendants = superclass.allChildren();


		/*		if (superclass.name().equals("A")){
			System.out.println("A:--");
			for (MClass cls: allDescendants){
				System.out.print(cls.name() +",");

			}

		}*/


		//System.out.println();

		//Set<MClass> subclassDescendants;

		for (MClass cls: allDescendants){

			//subclassDescendants = cls.allChildren();

			//System.out.println("Child:" + cls.name());
			//if one of the children contains 'subclusses' of GS
			if (cls.allChildren().containsAll(subclesses)){
				//	System.out.println("There is covererage by " + cls.name());
				return true;
			}



		}

		return false;

	}

	@SuppressWarnings("unchecked")
	private void addDisjointIncompleteGensets() {
		MGeneralizationSet gs;
		int i = 1;


		boolean updatedGS =false;
		//		boolean newGS = false;
		boolean added = false;

		Collection<MGeneralizationSet> gsSet = tar_model.getGeneralizationSets();


		Set<MClass> relevantClasses = new HashSet<MClass>();

		GSType gsType = new GSIncompleteType();
		for (MGeneralizationSet gs1 : src_model.getGeneralizationSets()) {
			if (gsType.isValidType(gs1)) {
				relevantClasses.add(gs1.getSuperClass());
			}
		}

		//the relevant classes are superclasses of gs that imposes 'incomplete'constraint
		for (MClass cls : relevantClasses) {
			for ( Set<MClass> set : this.cliquesMap.get(cls)) {
				gs = factory.createGeneralizationSet("gs_disjoint_incomplete" + i++);
				gs.setType("disjoint_incomplete");
				for (MClass subCls : set) {
					gs.addSubClass(tar_model.getClass(subCls.name()));    
				}
				gs.setSuperClass(tar_model.getClass(cls.name()));

				// merging(or)combination of 2 GS's that consist of the same calsses is done in "this.tryToCombine"
				tar_model.addGeneralizationSet(gs);
			}
		}


	}
}
