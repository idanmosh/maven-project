package bgu.propagation.graph;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;

public class DisjointIncompleteGraphFactory extends EmptyGraphFactory implements GraphGenerator {

	private SimpleGraph<MClass, VertexPair<MClass>> disjointGraph;
	private SimpleGraph<MClass, VertexPair<MClass>> incompleteGraph;




	public DisjointIncompleteGraphFactory(
			SimpleGraph<MClass, VertexPair<MClass>> disjointGraph,
			SimpleGraph<MClass, VertexPair<MClass>> incompleteGraph) {
		this.disjointGraph = disjointGraph;
		this.incompleteGraph = incompleteGraph;
	}



	@Override
	public SimpleGraph<MClass, VertexPair<MClass>> buildGraph(MModel model) {
		super.buildGraph(model);
		
		IncompleteGraphFactory incompleteGraphFactory =
			new IncompleteGraphFactory();
		incompleteGraph = incompleteGraphFactory.buildGraph(model);


		addEdges(model);

		return this.graph;
	}



	private void addEdges(MModel model) {
		for (MClass cls1 : (Collection<MClass>)model.classes()) {
			for (MClass cls2 : (Collection<MClass>)model.classes()) {
				if (cls1 == cls2) break;
				if (disjointGraph.containsEdge(cls1, cls2) && 
						incompleteGraph.containsEdge(cls1, cls2)) {
					graph.addEdge(cls1, cls2);
				}
			}
		}

	}

}
