package bgu.propagation.graph;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class GraphFactory extends EmptyGraphFactory implements GraphGenerator {

	
	protected abstract boolean isValidType(MGeneralizationSet gs);
	

	@Override
	public SimpleGraph<MClass, VertexPair<MClass>> buildGraph(MModel model) {
		super.buildGraph(model);

		addEdges(model);
		
		return this.graph;
	}

	
	private void addEdges(MModel model) {
		for (MGeneralizationSet gs : model.getGeneralizationSets()) {
			//make sure that the GS is a correct constraint
			if (!isValidType(gs) ) continue;
			ArrayList<Set<MClass>> disjointPartitions = 
				new ArrayList<Set<MClass>>(gs.getSubClasses().size());
			//create family for each subClass in GS
			for (MClass cls : gs.getSubClasses()) {
				Set<MClass> family = new HashSet<MClass>();
				family.addAll(cls.allChildren());
				family.add(cls);
				disjointPartitions.add(family);
			}
			
			//add edges between each 2 classes in the families 
			for (Set<MClass> family1 : disjointPartitions) {
				for (Set<MClass> family2 : disjointPartitions) {
					if (family1 != family2) {
						addEdgesBetweenTwoFamilies(family1, family2);
					}
				}
			}
		}
		
	}

	private void addEdgesBetweenTwoFamilies(Set<MClass> family1,
			Set<MClass> family2) {
		for (MClass cls1 : family1) {
			for (MClass cls2 : family2) {
				if (!cls1.isSubClassOf(cls2) && !cls2.isSubClassOf(cls1)) {
					this.graph.addEdge(cls1, cls2);
				}
			}
		}
		
	}

}
