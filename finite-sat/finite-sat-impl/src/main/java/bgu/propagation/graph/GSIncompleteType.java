package bgu.propagation.graph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public class GSIncompleteType implements GSType {

	@Override
	public boolean isValidType(MGeneralizationSet gs) {
		return gs.isDisjointIncomplete() || gs.isIncomplete() || gs.isOverlappingIncomplete();
	}

}
