package bgu.propagation.graph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public interface GSType {
	
	boolean isValidType(MGeneralizationSet gs);
}
