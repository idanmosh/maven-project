package bgu.propagation.graph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public class IncompleteGraphFactory extends GraphFactory implements GraphGenerator {

	private static GSType gsType = new GSIncompleteType();
	

	@Override
	protected boolean isValidType(MGeneralizationSet gs) {
		return gsType.isValidType(gs);
	}
	
	

	
	

}
