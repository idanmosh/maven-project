package bgu.propagation;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class ConstraintPropogator {

	protected ModelBuilder updatedModelBuilder;
	
	protected SimpleGraph<MClass, VertexPair<MClass>> disjointGraph, incompleteGraph, disIncGraph;
	
	protected Map<MClass, Collection<Set<MClass>>> disjointCliquesMap, disIncCliquesMap;
	
 	public ModelBuilder getUpdatedModelBuilder() {
		return updatedModelBuilder;
	}

	long start;

}
