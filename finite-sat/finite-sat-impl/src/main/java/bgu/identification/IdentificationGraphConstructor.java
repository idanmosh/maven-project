package bgu.identification;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MMultiplicity.Range;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


// TODO: Auto-generated Javadoc
/**
 * Class Responsible for the construction of detection graph of the reason for
 * non finite satsifiability. Ben-Gurion University of the Negev, Department of
 * Computer Science
 * 
 * @author Rami Prilutsky Created on 08/12/2010
 */
public class IdentificationGraphConstructor
{

	/** The m_detection_graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> m_detection_multigraph;

	/** The assoc set. */
	private Set<String> assocSet;

	/**
	 * Instantiates a new detection graph constructor.
	 */
	public IdentificationGraphConstructor()
	{
		m_detection_multigraph = new DirectedWeightedMultigraph <String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		assocSet = new HashSet<String>();
	}

	/**
	 * Builds the detection graph.
	 * 
	 * @param model the model
	 */
	public void buildDetectionGraph(MModel model)
	{
		//System.out.println("test1");
		//FIXME: Remove from here: This method should be called automatically after compiling the model
		model.updateAssocClsIntersection(model);
		createAssocClsNodes(model);
		// System.out.println("test2");
		createAssocNodes(model);
		// System.out.println("test3");
		createClassHierarchyNodes(model);
		// System.out.println("test4");

	}

	

	private void updateAssocClsIntersection(MModel model)
	{
		try
		{
			Collection<MAssociationClass> assocs = model.getAssociationClassesOnly();
			Iterator<MAssociationClass> iter = assocs.iterator();
			while (iter.hasNext())
			{
				MAssociationClass assoc = iter.next();
				if (!assocSet.contains(assoc.name()))
				{
					assocSet.add(assoc.name());
					Intersect(assoc);
				}

			}
		}
		catch (Exception e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

	}

	
	/**
	 * Intersect.
	 * 
	 * @param assoc the assoc
	 * 
	 * @throws Exception the exception
	 **/
	private void Intersect(MAssociationClass assoc) throws Exception
	{
		
		Iterator<MClass> iter = assoc.allParents().iterator();
		while (iter.hasNext())
		{
			MClass cls = iter.next();

			if (!(cls instanceof MAssociationClass))
				throw new Exception("Association Class Hierarchy Error");

			MAssociationClass newAsoc = (MAssociationClass) cls;
			if (!assocSet.contains(newAsoc.name()))
			{
				assocSet.add(newAsoc.name());
				Intersect(newAsoc);
			}

			MAssociationEnd ae1 = (MAssociationEnd) newAsoc.associationEnds()
					.get(0);
			MAssociationEnd ae2 = (MAssociationEnd) newAsoc.associationEnds()
					.get(1);

			MAssociationEnd baseAe1 = (MAssociationEnd) assoc.associationEnds()
					.get(0);
			MAssociationEnd baseAe2 = (MAssociationEnd) assoc.associationEnds()
					.get(1);

			if (baseAe1.cls().isSubClassOf(ae1.cls())
					& baseAe2.cls().isSubClassOf(ae2.cls()))
			{
				baseAe1.multiplicity().addRange(
						Math.max(baseAe1.multiplicity().getRange().getLower(),
								ae1.multiplicity().getRange().getLower()),
						Math.min(baseAe1.multiplicity().getRange().getUpper(),
								ae1.multiplicity().getRange().getUpper()));

				baseAe2.multiplicity().addRange(
						Math.max(baseAe2.multiplicity().getRange().getLower(),
								ae2.multiplicity().getRange().getLower()),
						Math.min(baseAe2.multiplicity().getRange().getUpper(),
								ae2.multiplicity().getRange().getUpper()));
			}
			else if (baseAe2.cls().isSubClassOf(ae1.cls())
					& baseAe1.cls().isSubClassOf(ae2.cls()))
			{
				baseAe1.multiplicity().addRange(
						Math.max(baseAe1.multiplicity().getRange().getLower(),
								ae2.multiplicity().getRange().getLower()),
						Math.min(baseAe1.multiplicity().getRange().getUpper(),
								ae2.multiplicity().getRange().getUpper()));

				baseAe2.multiplicity().addRange(
						Math.max(baseAe2.multiplicity().getRange().getLower(),
								ae1.multiplicity().getRange().getLower()),
						Math.min(baseAe2.multiplicity().getRange().getUpper(),
								ae1.multiplicity().getRange().getUpper()));
			}
			else
			{
				throw new Exception("Association Class Hierarchy Error");
			}
		}

	}

	/**
	 * Gets the detection graph.
	 * 
	 * @return the detection graph
	 */


	public DirectedWeightedMultigraph<String, DefaultWeightedEdge> getDetectionGraph()
	{
		return m_detection_multigraph;
	}
	
	/**
	 * Creates the assoc cls nodes.
	 * 
	 * @param model the model
	 */
	private void createAssocClsNodes(MModel model)
	{
		Collection<MAssociationClass> assocs = model
				.getAssociationClassesOnly();
		Iterator<MAssociationClass> iter = assocs.iterator();
		while (iter.hasNext())
		{
			MAssociationClass assoc = iter.next();
			MAssociationEnd ae1 = (MAssociationEnd) assoc.associationEnds()
					.get(0);
			MAssociationEnd ae2 = (MAssociationEnd) assoc.associationEnds()
					.get(1);
			//if (ae1.cls() != ae2.cls())
				addAssocClassToGraph(assoc);
		}

	}

	/**
	 * Adds the assoc class to graph.
	 * 
	 * @param assoc the assoc
	 */
	private void addAssocClassToGraph(MAssociationClass assoc)
	{


		m_detection_multigraph.removeVertex("assoc_" + assoc.name());

		MAssociationEnd ae1 = (MAssociationEnd) assoc.associationEnds().get(0);
		MAssociationEnd ae2 = (MAssociationEnd) assoc.associationEnds().get(1);
		Range range1 = ae1.multiplicity().getRange();
		Range range2 = ae2.multiplicity().getRange();
		MClass c1 = ae1.cls();
		MClass c2 = ae2.cls();
		
		//FIXME: change the name of the mothod

		String assoc_vertex = getVertexByName(assoc.cls().name());
		String c1_vertex = getVertexByName(c1.name());
		String c2_vertex = getVertexByName(c2.name());

		if (range2.getUpper() == MMultiplicity.MANY)
			addEdge(c1_vertex, assoc_vertex, Double.POSITIVE_INFINITY);
		else
			addEdge(c1_vertex, assoc_vertex, range2.getUpper());
		addEdge(assoc_vertex, c1_vertex, 1.0 / range2.getLower());
		if (range2.getUpper() == MMultiplicity.MANY)
			addEdge(c2_vertex, assoc_vertex, Double.POSITIVE_INFINITY);
		else
			addEdge(c2_vertex, assoc_vertex, range1.getUpper());
		addEdge(assoc_vertex, c2_vertex, 1.0 / range1.getLower());

	}

	/**
	 * Creates the gs nodes.
	 * 
	 * @param model the model
	 * 
	 * 
	 */
	
	
	private void createClassHierarchyNodes(MModel model)
	{
		// System.out.println("test1");
		org.tzi.use.graph.DirectedGraph directedGraph =  model.generalizationGraph();
		
		Iterator<DirectedEdge> edges_iter =  directedGraph.edgeIterator();
		
		while(edges_iter.hasNext()){
				DirectedEdge edge = edges_iter.next();

				MClass superClass = (MClass)edge.source(); 
				MClass subClass = (MClass)edge.target();

				String sub_vertex = getVertexByName(superClass.name());
				String super_vertex = getVertexByName(subClass.name());
				addEdge(sub_vertex, super_vertex, Double.POSITIVE_INFINITY);
				addEdge(super_vertex, sub_vertex, 1);
			}
			
	}	
		
/*	private void createGSNodes(MModel model)
	{
		// System.out.println("test1");
		Collection<MGeneralizationSet> gss = model.getGeneralizationSets();
		// System.out.println("test2");
		Iterator<MGeneralizationSet> iter = gss.iterator();
		// System.out.println("test3");
		while (iter.hasNext())
		{ // System.out.println("test1");
			MGeneralizationSet gs = iter.next();
			// System.out.println("test2");
			for (String subClass : gs.getSubClassesNames())
			{
				// System.out.println("test21");
				addGSToGraph(gs.getSuperClass().name(), subClass);
				// System.out.println("test22");
			}
			// System.out.println("test3");
		}
		// System.out.println("test4");
	}*/

	/**
	 * Adds the gs to graph.
	 * 
	 * @param superClass the super class
	 * @param subClass the sub class
	 */
	private void addGSToGraph(String superClass, String subClass)
	{

		// System.out.println("test1");
		String super_vertex = getVertexByName(superClass);
		// System.out.println("test2");
		String sub_vertex = getVertexByName(subClass);
		// System.out.println("test3");
		String gs_vertex = getVertexByName("GS_" + superClass + "_" + subClass);

		// System.out.println("test4");
		addEdge(super_vertex, gs_vertex, 1);
		// System.out.println("test5");
		addEdge(gs_vertex, super_vertex, Double.POSITIVE_INFINITY);
		// System.out.println("test6");
		addEdge(sub_vertex, gs_vertex, 1);
		// System.out.println("test7");
		addEdge(gs_vertex, sub_vertex, 1);
		// System.out.println("test8");
	}

	/**
	 * Creates the assoc nodes.
	 * 
	 * @param model the model
	 */
	private void createAssocNodes(MModel model)
	{
		Collection<MAssociation> associations = model.associations();
		Iterator<MAssociation> iter = associations.iterator();
		while (iter.hasNext())
		{
			MAssociation assoc = iter.next();
			// Rami handle assoc to itself
			MAssociationEnd ae1 = (MAssociationEnd) assoc.associationEnds()
					.get(0);
			MAssociationEnd ae2 = (MAssociationEnd) assoc.associationEnds()
					.get(1);
			//if (ae1.cls() != ae2.cls())
			
			addAssociationToGraph(assoc);
		}
	}

	/**
	 * Adds the association to graph.
	 * 
	 * @param assoc the assoc
	 */
	private void addAssociationToGraph(MAssociation assoc)
	{
		if (m_detection_multigraph.containsVertex(assoc.name()))
			return;
		
		MAssociationEnd ae1 = (MAssociationEnd) assoc.associationEnds().get(0);
		MAssociationEnd ae2 = (MAssociationEnd) assoc.associationEnds().get(1);
		Range range1 = ae1.multiplicity().getRange();
		Range range2 = ae2.multiplicity().getRange();
		MClass c1 = ae1.cls();
		MClass c2 = ae2.cls();
		String c1_vertex = getVertexByName(c1.name());
		String c2_vertex = getVertexByName(c2.name());
		String assoc_vertex = getVertexByName("assoc_" + assoc.name());
		if (range2.getUpper() != MMultiplicity.MANY)
			addEdge(c1_vertex, assoc_vertex, range2.getUpper());
		else
			addEdge(c1_vertex, assoc_vertex, Double.POSITIVE_INFINITY);
		
		addEdge(assoc_vertex, c1_vertex, 1.0 / range2.getLower());
		
		if (range1.getUpper() != MMultiplicity.MANY)
			addEdge(c2_vertex, assoc_vertex, range1.getUpper());
		else
			addEdge(c2_vertex, assoc_vertex, Double.POSITIVE_INFINITY);
		
		addEdge(assoc_vertex, c2_vertex, 1.0 / range1.getLower());
	}

	/**
	 * Adds the edge.
	 * 
	 * @param fromVertex the from vertex
	 * @param toVertex the to vertex
	 * @param weight the weight
	 */
	private void addEdge(String fromVertex, String toVertex, double weight)
	{
		
		//if (weight != MMultiplicity.MANY && weight != 0)
		//{
		  
		
			m_detection_multigraph.setEdgeWeight(m_detection_multigraph.addEdge(fromVertex,
					toVertex), weight);
			
			//DefaultWeightedEdge edge2 = m_detection_multigraph.addEdge(arg0, arg1);
		//}
		

	}

	/**
	 * Gets the vertex by name.
	 * 
	 * @param name the name
	 * 
	 * @return the vertex by name
	 */
	private String getVertexByName(String name)
	{
		if (!m_detection_multigraph.containsVertex(name))
			m_detection_multigraph.addVertex(name);

		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return m_detection_multigraph.toString();
	}

}
