/*
 * 
 */
package bgu.identification;

import bgu.circleStructure.CircleImpl;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity.Range;
import bgu.circleStructure.Circle;
import java.util.Iterator;

/**
 * The Class CriticalCircle.
 */
public class CriticalCircle extends CircleImpl<DefaultWeightedEdge>
        implements Circle<DefaultWeightedEdge> {

	/** The _weight. */
	private double _weight;

	/** The _model. */
	private MModel _model;

	/** The _graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> _graph;

	/**
	 * Instantiates a new critical circle.
	 *
	 * @param graph the graph
	 */
	public CriticalCircle(DirectedWeightedMultigraph<String, DefaultWeightedEdge>
			graph, MModel model){

		_graph = graph;
		_model = model;
	}

	/**
	 * Sets the weight.
	 *
	 * @param weight the new weight
	 */
	public void setWeight(double weight){
		_weight = weight;
	}

	/**
	 * Gets the weight.
	 *
	 * @return the weight
	 */
	public double getWeight(){
		return _weight;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String s = "\nCritical cycle alert!\n";

		s = s + this.cycleName();
		s = s + this.constraints();
		s = s + this.hierarchies();
		s = s + this.cycleRepresentation();

		return s;
	}

	/**
	 * Cycle name, by vertices
	 * 
	 * @return the string
	 */
	public String cycleName(){
		String s = "";

		for(DefaultWeightedEdge e : super.getCircle())
			s = s + _graph.getEdgeSource(e) + " -> ";

		s = s + _graph.getEdgeSource(super.getCircle().get(0)) + "\n\n";

		return s;
	}

	/**
	 * Cycle representation, by edges with weights
	 *
	 * @return the string
	 */
	private String cycleRepresentation(){
		String s = "\nGraph repesentaion of the cycle:\n";

		for(DefaultWeightedEdge e : super.getCircle())
			s = s + e.toString() + "[weight=" + _graph.getEdgeWeight(e)
			+ "]" + ", ";

		s = s.substring(0, s.length()-2);
		s = s + "\nCycle weight: " + _weight + "\n";

		return s;
	}

	/**
	 * Constraints.
	 *
	 * @return the string
	 */
	private String constraints(){
		String s = "The involved constraints in the cycle are:\n";

		//cycle should not start with association
		while(_graph.getEdgeSource(super.getCircle().get(0)).startsWith("assoc")){
			DefaultWeightedEdge edgeToMove = super.getCircle().remove(0);
			super.getCircle().add(edgeToMove);
		}

		//association constraints
		s += this.assocConstraints();

		//association classes constraints
		s += this.assocClassConstraints();

		return s;
	}

	/**
	 * Association constraints.
	 *
	 * @return the string
	 */
	private String assocConstraints(){
		String s = "";
		String v = "";
		double w;

		Iterator<DefaultWeightedEdge> e_iter = super.getCircle().iterator();

		while(e_iter.hasNext()){
			DefaultWeightedEdge e = e_iter.next();

			if(_graph.getEdgeTarget(e).startsWith("assoc")){
				s = s + "Association " + _graph.getEdgeTarget(e).substring(6, 7) + "\n"; 
				v += _graph.getEdgeSource(e);
				w = _graph.getEdgeWeight(e);
				e = e_iter.next();
				s = s + "Minimum multiplicity constraint " + 
				1.0/_graph.getEdgeWeight(e) +
				" next to " + v + 
				" and maximum multiplicity constraint " +
				w + " next to " + 
				_graph.getEdgeTarget(e) + "\n";
				v = "";
			}
		}

		return s;
	}

	/**
	 * Association class constraints.
	 *
	 * @return the string
	 */
	private String assocClassConstraints(){
		String s = "";
		
		Iterator<DefaultWeightedEdge> e_iter2 = super.getCircle().iterator();

		while(e_iter2.hasNext()){
			DefaultWeightedEdge e = e_iter2.next();

			//looking for class -> assoc_class pattern
			if(!_graph.getEdgeSource(e).startsWith("assoc") &&
					_graph.getEdgeTarget(e).startsWith("assoc_class")){
				String aClsName = _graph.getEdgeTarget(e);
				aClsName = aClsName.substring(12,aClsName.length());
				MAssociationClass cls = _model.getAssociationClass(aClsName);
				MAssociationEnd ae1 = (MAssociationEnd) cls.associationEnds().get(0);
				MAssociationEnd ae2 = (MAssociationEnd) cls.associationEnds().get(1);
				String outOfGraph;
				double w2;
				if(ae1.cls().name().equals(_graph.getEdgeSource(e))){
					outOfGraph = ae2.cls().name();
					Range r = ae1.getStrictRange();
					w2 = r.getLower();
				}
				else{
					outOfGraph = ae1.cls().name();
					Range r = ae2.getStrictRange();
					w2 = r.getLower();
				}

				if(w2 == -1 )
					w2 = Double.POSITIVE_INFINITY;

				s = s + "Association " + aClsName + "\n" + 
				"Minimum multiplicity constraint " + 
				w2 + " next to " + _graph.getEdgeSource(e) +
				" and maximum multiplicity constraint " +
				_graph.getEdgeWeight(e) + " near to " + outOfGraph + "\n";
			}
			//looking for assoc_class -> class pattern
			else if(_graph.getEdgeSource(e).startsWith("assoc_class") &&
					!_graph.getEdgeTarget(e).startsWith("assoc")){
				String aClsName = _graph.getEdgeSource(e);
				aClsName = aClsName.substring(12,aClsName.length());
				MAssociationClass cls = _model.getAssociationClass(aClsName);
				MAssociationEnd ae1 = (MAssociationEnd) cls.associationEnds().get(0);
				MAssociationEnd ae2 = (MAssociationEnd) cls.associationEnds().get(1);
				String outOfGraph;
				double w2;
				if(ae1.cls().name().equals(_graph.getEdgeTarget(e))){
					outOfGraph = ae2.cls().name();
					Range r = ae2.getStrictRange();
					w2 = r.getUpper();
				}
				else{
					outOfGraph = ae1.cls().name();
					Range r = ae1.getStrictRange();
					w2 = r.getUpper();
				}

				if(w2 == -1 )
					w2 = Double.POSITIVE_INFINITY;

				s = s + "Association " + aClsName + "\n" + 
				"Minimum multiplicity constraint " + 
				1.0/_graph.getEdgeWeight(e) + " next to " +
				outOfGraph + " and maximum multiplicity constraint " +
				w2 + " near to " + _graph.getEdgeTarget(e) + "\n";
			}
		}

		return s;
	}

	/**
	 * Hierarchies.
	 *
	 * @return the string
	 */
	private String hierarchies(){
		String s = "";

		for(DefaultWeightedEdge e : super.getCircle()){
			String v1 = _graph.getEdgeSource(e);
			String v2 = _graph.getEdgeTarget(e);

			//two classes
			if(!v1.startsWith("assoc") && !v2.startsWith("assoc"))
				s = s + "\nClass Hierarchies:\n" + "Super: " + 
				v1 + ",  Sub: " + v2 + "\n";
			//two association classes
			else if(v1.startsWith("assoc_class") && v2.startsWith("assoc_class"))
				s = s + "\nClass Hierarchies:\n" + "Super: " + 
				v1 + ",  Sub: " + v2 + "\n";
			//two association classes 
			else if(v1.startsWith("assoc") && v2.startsWith("assoc") && 
					v1.length() < 10 && v2.length() < 10)
				s = s + "\nClass Hierarchies:\n" + "Super: " + 
				v1 + ",  Sub: " + v2 + "\n";		
		}

		return s;
	}
}
