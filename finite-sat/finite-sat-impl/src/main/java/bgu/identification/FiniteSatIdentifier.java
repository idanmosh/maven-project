/*
 * 
 */
package bgu.identification;

import bgu.circleStructure.Circle;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedSubgraph;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.uml.mm.MModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;


/**
 * Class responsible for detecting critical cycles in a finite satisfiability detection graph.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * 
 * @author Victor Makarenkov & Rami Prilutsky & MichaelB
 * Created on 20/10/2008
 */
public class FiniteSatIdentifier {


	/** The _result. */
	private String _result = "";

	/** The _graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> _graph;

	/** The _filtered graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> _filteredGraph = 
		new DirectedWeightedMultigraph <String, DefaultWeightedEdge>(DefaultWeightedEdge.class);

	/** The _circles. */
	private Vector<CriticalCircle> _circles = 
		new Vector<CriticalCircle>();
	
	/** The _print all. */
	private boolean _printAll = false;
	
	/** The _exit . */
	private boolean _exit = false;
	
	/** The _Graph Corrector . */
	private GraphCorrector gc;
	
	/** The _model . */
	private MModel _model;

	/**
	 * Gets the _result.
	 * 
	 * @return the _result
	 */
	public String get_result()
	{
		return _result;
	}

	/**
	 * Method to detect the cause for not finite satisfiability.
	 * This is done by discovering critical cycles in the graph 
	 * built out of the UML class diagram.
	 * 
	 * @param model is the representation of UML class diagram.
	 * 
	 * @return true, if detect
	 */
	public boolean identify(MModel model){
		DetectionGraphConstructor constructor = new DetectionGraphConstructor();

		constructor.buildDetectionGraph(model);
		gc = new GraphCorrector(model, constructor.getDetectionGraph());		
		_graph = gc.getGraph();	
		_model = model;
		return this.findCycles();
	}
	
	/**
	 * Gets the circles, that were found so far.
	 *
	 * @return the circles
	 */
	public Vector<CriticalCircle> getCircles(){
		return _circles;
	}

	/**
	 * Identify existence.
	 *
	 * @param model the model
	 * @return true, if successful
	 */
	public boolean identifyExistence(MModel model){
		boolean res = true;
		//System.out.println("test1");
		DetectionGraphConstructor constructor = new DetectionGraphConstructor();
		//System.out.println("test12");
		constructor.buildDetectionGraph(model);
		//System.out.println("test13");
		_graph = constructor.getDetectionGraph();    
		//System.out.println("test2");
		Set<String> vertices = _graph.vertexSet();
		//System.out.println("test3");
		Iterator<String> iter = vertices.iterator();
		while (iter.hasNext()){
			String vertex = iter.next();
			res = res & checkVertex(vertex,_graph);
		}
		//System.out.println("test4");
		return res;
	}

	/**
	 * Find cycles.
	 *
	 * @return true, if successful
	 */
	private boolean findCycles(){
		boolean found = false;
		double weight;

		//filtering of infinite edges
		this.infinityCleaning();

		//List of Strongly Connected Components which contains
		//at least one edge of weight lower than 1
		List<DirectedSubgraph<String, DefaultWeightedEdge>> scc = 
			this.sccCleaning();
		
		Iterator<DirectedSubgraph<String, DefaultWeightedEdge>> scc_iter = 
			scc.iterator();

		//looking for circles in SCC's.
		//for each SCC
		while(scc_iter.hasNext() && !_exit){			
			DirectedSubgraph<String, DefaultWeightedEdge> g = scc_iter.next();
			
			//for each edge
			Iterator<DefaultWeightedEdge> edge_iter = g.edgeSet().iterator();
			while(edge_iter.hasNext() && !_exit){
				DefaultWeightedEdge e = edge_iter.next(); 

				KShortestPaths<String, DefaultWeightedEdge> ksp= 
					new KShortestPaths<String, 
						DefaultWeightedEdge>(g, g.getEdgeTarget(e), g.edgeSet().size());

				List<GraphPath<String, DefaultWeightedEdge>> lst = 
					ksp.getPaths(g.getEdgeSource(e));
				
				
				//for each path
				Iterator<GraphPath<String, DefaultWeightedEdge>> path_edges_it =
					lst.iterator();
				while(path_edges_it.hasNext() && !_exit){
					GraphPath<String, DefaultWeightedEdge> p = path_edges_it.next();
					
					weight = this.validWeight(p,e); 
					if(weight < 1 && !this.discoveredBefore(p,e)){
						found = true;
						this.addCircle(p,e, weight);			
					}
				}
			}
		}

		return found;
	}

	/**
	 * Infinity cleaning.
	 * Removes all infinite edges
	 */
	private void infinityCleaning(){

		//vertices of a new graph
		for(String s : _graph.vertexSet())
			_filteredGraph.addVertex(s);

		//edges of a new filtered graph
		for(DefaultWeightedEdge e : _graph.edgeSet()){
			if(!Double.isInfinite(_graph.getEdgeWeight(e))){
				DefaultWeightedEdge newEdge = 
					_filteredGraph.addEdge(_graph.getEdgeSource(e), 
							_graph.getEdgeTarget(e));
				_filteredGraph.setEdgeWeight(newEdge, _graph.getEdgeWeight(e));
			}
		}
	}

	/**
	 * SCC cleaning.
	 * Finds strongly connected components while each component
	 * contains at least one edge with weight less than 1
	 * @return the list
	 */
	private List<DirectedSubgraph<String, DefaultWeightedEdge>> sccCleaning(){
		boolean contains = false;

		//filtered components, each components will contain at least 
		//one edge with weight less than 1
		List<DirectedSubgraph<String, DefaultWeightedEdge>> scc = 
			new ArrayList<DirectedSubgraph<String,DefaultWeightedEdge>>();

		//strongly connected components
		StrongConnectivityInspector<String, DefaultWeightedEdge> sccToFilter = 
			new StrongConnectivityInspector<String, 
				DefaultWeightedEdge>(_filteredGraph);		

		//iterator over graph components
		Iterator<DirectedSubgraph<String, DefaultWeightedEdge>> iter = 
			sccToFilter.stronglyConnectedSubgraphs().iterator();

		while(iter.hasNext()){
			DirectedSubgraph<String, DefaultWeightedEdge> g = iter.next();

			for(DefaultWeightedEdge e : g.edgeSet())
				if(g.getEdgeWeight(e) < 1){
					contains = true;
					break;
				}
			
			if(contains){
				scc.add(g);
				contains = false;
			}
		}
		
		return scc;
	}

	/**
	 * Valid weight.
	 * Checks weather the multiplication of weights in
	 * circle is valid, less than 1, or not.
	 *
	 * @param p the p
	 * @param e the e
	 * @return cycle weight
	 */
	private double validWeight(GraphPath<String, DefaultWeightedEdge> p,
			DefaultWeightedEdge e){

		double result = _graph.getEdgeWeight(e);

		for(DefaultWeightedEdge edge : p.getEdgeList())
			result *= _graph.getEdgeWeight(edge);

		return result;
	}

	/**
	 * Discovered before.
	 * Checks if a newly discovered circle has already
	 * been discovered before
	 *
	 * @param p the p
	 * @param e the e
	 * @return true, if successful
	 */
	private boolean discoveredBefore(GraphPath<String, DefaultWeightedEdge> p,
			DefaultWeightedEdge e){
		boolean contains;

		for(Circle<DefaultWeightedEdge> circ : _circles){
			contains = true;

			for(DefaultWeightedEdge edge : p.getEdgeList())
				if(!circ.getCircle().contains(edge)){
					contains = false;
					break;
				}

			if(contains && circ.getCircle().contains(e) && 
					p.getEdgeList().size()+1 == circ.getCircle().size())
				return true;
		}

		return false;
	}

	/**
	 * Adds the circle.
	 *
	 * @param p the p
	 * @param e the e
	 */
	private void addCircle(GraphPath<String, DefaultWeightedEdge> p, 
			DefaultWeightedEdge e, double weight){
		CriticalCircle circ = new CriticalCircle(_graph, _model);

		circ.addVertex(e);
		for(DefaultWeightedEdge edge : p.getEdgeList())
			circ.addVertex(edge);
		
		circ.setWeight(weight);
		_circles.add(circ);
		
		System.out.println(circ.toString());  //printing
		this.execution();  //interactive mode
	}
	
	/**
	 * Execution.
	 * Provides interactive mode.
	 */
	private void execution(){
		String currLine = "";
		InputStreamReader reader = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(reader); 
		
		if(!_printAll){

			System.out.println("Continue looking for more cycles? (yes/no/all)");			
			try{
				currLine = in.readLine();

				if(currLine.equals("no"))   //in case of "no", system terminates
					_exit = true;           //any other input and algorithm will 
				if(currLine.equals("all"))  //continue looking for more circles
					_printAll = true;

			}catch(IOException exc){
				System.out.println("Incorrect input: " + exc.getMessage());
			}
		}
	}

	/**
	 * Method for checking for critical cycles with a participation of specific vertex.
	 * 
	 * @param vertex is the vertex being investigated
	 * @param graph is the detection graph (built according to Lenzerini and Nobili method)
	 * 
	 * @return true, if check vertex
	 */
	public boolean checkVertex(String vertex, DirectedWeightedMultigraph<String, DefaultWeightedEdge> graph) {
		
		boolean res = true;
		
		Set<DefaultWeightedEdge> edges = graph.incomingEdgesOf(vertex);
		Iterator<DefaultWeightedEdge> iter = edges.iterator();
		while (iter.hasNext()){
			DefaultWeightedEdge edge = iter.next();
			if (graph.getEdgeWeight(edge) < 1){
				 DijkstraProduct d = new DijkstraProduct(graph,vertex);
			      d.findShortestPaths();
			     TreeMap<String, Double> distances =d.getDistances();
			     
			     double vertex_distance = distances.get(graph.getEdgeSource(edge));
			     if (vertex_distance * graph.getEdgeWeight(edge) < 1){
			    	 res = false;
			    	 String from = graph.getEdgeSource(edge);
			    	 String to = graph.getEdgeTarget(edge);
			    	 
			    	 _result += "Critical Cycle Detected, Cost:  " +  graph.getEdgeWeight(edge)* vertex_distance + "\n";
			    	 _result += "Cycle: " + vertex + " ->";
			    	 TreeMap<String, DistanceVertex> paths = d.getPaths();
			    	
			    	 while (! (from == null || from.equals(vertex))){
			    		 _result += from + " ->";
			    		 DistanceVertex from_distance = paths.get(from);
			    		 from = from_distance.getParent();
			    	 }
			    	 _result += vertex + "\n";


			      	 _result += "Suggestion: change max multiplicty at "+ 
			    	 (from.startsWith("assoc_") ? 
			    	 from.replaceFirst("assoc_", "") : 
			    		 from.replaceAll("[0-9]+assoc_", ""))
			    	 + 
			    	 "->" + 

			    	 (to.startsWith("assoc_") ? 
			    			 to.replaceFirst("assoc_", "") : 
			    				 to.replaceAll("[0-9]+assoc_", ""))
			    	 
			    	 + 
			    	 " association to " + 1.0 / graph.getEdgeWeight(edge) + "\n";
			    	 
			     }
			}
		}
		return res;
	}
	
}
