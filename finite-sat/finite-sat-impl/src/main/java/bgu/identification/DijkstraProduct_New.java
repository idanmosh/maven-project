package bgu.identification;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedPseudograph;

import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;


// TODO: Auto-generated Javadoc
/**
 * Implementation of Dijkstra'a single source shortest paths algorithm. The
 * weight of the paths is the product of the weights of the edges along this
 * path. Ben-Gurion University of the Negev, Department of Computer Science.
 * 
 * @author Victor Makarenkov Created on 20/10/2008
 */
public class DijkstraProduct_New {

	/** The m_distances. */
	private TreeMap<String, Double> m_distances;
	
	/** The m_queue. */
	private PriorityQueue<DistanceVertex> m_queue;
	
	/** The m_paths. */
	private TreeMap<String, DistanceVertex> m_paths;
	
	/** The m_source. */
	private String m_source;

	DirectedPseudograph<String, DefaultWeightedEdge>   _graph;
	/**
	 * The constructor. All preparations for Dijsktra algorithm are made here.
	 * This includes initialization of the initial distances and a priority
	 * queue.
	 * 
	 * @param graph is the graph to perform algorithm on.
	 * @param source is the vertex, that is the single source shortest paths
	 */
	public DijkstraProduct_New(DirectedPseudograph<String, DefaultWeightedEdge>  graph, String source) {
		_graph = graph;
		m_source = source;
		m_distances = new TreeMap<String, Double>();
		m_queue = new PriorityQueue<DistanceVertex>(graph.vertexSet().size(),
				new DistanceVertexComparator());
		m_paths = new TreeMap<String, DistanceVertex>();
		Set<String> vertices = graph.vertexSet();
		Iterator<String> iter = vertices.iterator();
		DistanceVertex source_distance = new DistanceVertex(source, 1.0);
		source_distance.setParent(source);
		m_distances.put((String) source, new Double(1.0));
		m_queue.add(source_distance);
		while (iter.hasNext()) {
			String vertex = iter.next();
			if (!vertex.equals(source)) {
				DistanceVertex distance_vertex = new DistanceVertex(vertex,
						Double.MAX_VALUE);
				m_queue.add(distance_vertex);
				m_distances.put(((String) vertex), distance_vertex
						.getDistance());
			}
		}
	}

	/**
	 * Method for invoking the algorithm. Iterating over vertices from a
	 * priority queue, by the shortest distance and applying the greedy method.
	 */
	public void findShortestPaths() {
		while (!m_queue.isEmpty()) {
			DistanceVertex d_u = m_queue.poll();
			m_paths.put((String) d_u.getVertex(),d_u);
			String u = d_u.getVertex();
			Set<DefaultWeightedEdge> out_edges = _graph.outgoingEdgesOf(u);
			Iterator<DefaultWeightedEdge> iter = out_edges.iterator();
			while (iter.hasNext()) {
				DefaultWeightedEdge edge = iter.next();
				String z = _graph.getEdgeTarget(edge);
					
				double z_distance = ((Double) m_distances.get(z))
						.doubleValue();
				DistanceVertex d_z = new DistanceVertex(z, z_distance);
				if (m_queue.contains(d_z)) {
					doRelaxation(d_u, d_z, edge);
				}
			}
		}
	}

	/**
	 * Relaxation procedure of the distance - an update.
	 * 
	 * @param d_u  is the current node investigated.
	 * @param d_z is the node to which the relaxation is done
	 * @param edge is the edge <u,z>
	 */
	private void doRelaxation(DistanceVertex d_u, DistanceVertex d_z, DefaultWeightedEdge edge) {
		// System.out.println("before making relaxation " + d_z.getDistance());
		double edge_weight = _graph.getEdgeWeight(edge);
		if (d_u.getDistance() * edge_weight < d_z.getDistance()) {
			m_distances.put((String) d_z.getVertex(), d_u
					.getDistance()
					* edge_weight);
			m_queue.remove(d_z);
			d_z.setDistance(d_u.getDistance() * edge_weight);
			d_z.setParent(d_u.getVertex());
			m_queue.add(d_z);
			// System.out.println("after making relaxation " +
			// d_z.getDistance());
		}
	}

	/**
	 * Gets the distances.
	 * 
	 * @return the distances
	 */
	public TreeMap<String, Double> getDistances() {
		return m_distances;
	}

	/**
	 * Gets the paths.
	 * 
	 * @return the paths
	 */
	public TreeMap<String, DistanceVertex> getPaths() {
		return m_paths;
	}
}
