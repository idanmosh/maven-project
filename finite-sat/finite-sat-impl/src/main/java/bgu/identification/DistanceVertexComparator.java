package bgu.identification;

import java.util.Comparator;
// TODO: Auto-generated Javadoc

/**
 * A comparator for DistanceVertex objects. Needed to store the PriorityQueue
 * in the Dijkstra algorithm.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * 
 * @author Victor Makarenkov
 * Created on 21/10/2008
 */
public class DistanceVertexComparator implements Comparator<DistanceVertex>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(DistanceVertex o1, DistanceVertex o2) {
		if (o1.getDistance() < o2.getDistance()) return -1;
		if (o1.getDistance() > o2.getDistance()) return 1;
		return 0;
	}

}
