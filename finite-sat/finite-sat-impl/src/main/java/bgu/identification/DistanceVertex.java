package bgu.identification;

// TODO: Auto-generated Javadoc

/**
 * Ben-Gurion University of the Negev, Department of Computer Science.
 * 
 * @author Victor Makarenkov & Rami Prilutsky
 * Created on 20/10/2008
 */
public class DistanceVertex {

	/** The m_vertex. */
	private String m_vertex;
	
	/** The m_distance. */
	private double m_distance;
	
	/** The m_parent. */
	private String m_parent;
	
	/**
	 * Instantiates a new distance vertex.
	 * 
	 * @param vertex the vertex
	 * @param distance the distance
	 */
	public DistanceVertex(String vertex, double distance){
		m_vertex = vertex;
		m_distance = distance;
	}
	
	/**
	 * Gets the vertex.
	 * 
	 * @return the vertex
	 */
	public String getVertex() {
		return m_vertex;
	}
	
	/**
	 * Sets the vvertex.
	 * 
	 * @param vertex the new vvertex
	 */
	public void setVvertex(String vertex) {
		this.m_vertex = vertex;
	}
	
	/**
	 * Gets the distance.
	 * 
	 * @return the distance
	 */
	public double getDistance() {
		return m_distance;
	}
	
	/**
	 * Sets the distance.
	 * 
	 * @param distance the new distance
	 */
	public void setDistance(double distance) {
		this.m_distance = distance;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other){
		try{
			DistanceVertex v = (DistanceVertex) other;
			return (v.getDistance() == m_distance && v.getVertex().equals(m_vertex));
		}
		catch(ClassCastException e){
			return false;
		}
	}
	
	/**
	 * Sets the parent.
	 * 
	 * @param vertexParent the new parent
	 */
	public void setParent(String vertexParent){
		m_parent = vertexParent;
	}
	
	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	public String getParent(){
		return m_parent;
	}
}
