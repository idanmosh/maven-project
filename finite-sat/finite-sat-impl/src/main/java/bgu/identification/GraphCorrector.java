/*
 * This class takes a directed weighted multigraph and makes a correction,
 * each vertex which is association class gets prefix "assoc_class_".
 * Michael.B
 */
package bgu.identification;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;


/**
 * The Class GraphCorrector.
 */
public class GraphCorrector {
	
	/** The _model. */
	private MModel _model;
	
	/** The _new graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> _newGraph;
	
	/** The _old graph. */
	private DirectedWeightedMultigraph<String, DefaultWeightedEdge> _oldGraph;
	
	/**
	 * Instantiates a new graph corrector.
	 *
	 * @param model the model
	 * @param graph the graph
	 */
	public GraphCorrector(MModel model, 
			DirectedWeightedMultigraph<String, DefaultWeightedEdge> graph){
		
		_model = model;
		_oldGraph = graph;
		_newGraph = new DirectedWeightedMultigraph<String, 
						DefaultWeightedEdge>(DefaultWeightedEdge.class);		
	}
	
	/**
	 * Gets the correvted graph.
	 *
	 * @return the graph
	 */
	public DirectedWeightedMultigraph<String, DefaultWeightedEdge> getGraph(){
		
		//Vertices
		for(String v : _oldGraph.vertexSet()){
			if(this.contains(v))
				_newGraph.addVertex("assoc_class_" + v);
			else
				_newGraph.addVertex(v);
		}
		
		//Edges
		for(DefaultWeightedEdge e : _oldGraph.edgeSet()){
			String source = _oldGraph.getEdgeSource(e);
			String target = _oldGraph.getEdgeTarget(e);
			boolean s = this.contains(source);
			boolean t = this.contains(target);
			
			if(!s && !t){
				DefaultWeightedEdge newEdge = _newGraph.addEdge(source, target);
				_newGraph.setEdgeWeight(newEdge, _oldGraph.getEdgeWeight(e));
			}
			else if(s && !t){
				DefaultWeightedEdge newEdge = _newGraph.addEdge("assoc_class_"+source, target);
				_newGraph.setEdgeWeight(newEdge, _oldGraph.getEdgeWeight(e));
			}
			else if(!s && t){
				DefaultWeightedEdge newEdge = _newGraph.addEdge(source, "assoc_class_"+target);
				_newGraph.setEdgeWeight(newEdge, _oldGraph.getEdgeWeight(e));
			}
			else if(s && t){
				DefaultWeightedEdge newEdge = _newGraph.addEdge("assoc_class_"+source,
												"assoc_class_"+target);
				_newGraph.setEdgeWeight(newEdge, _oldGraph.getEdgeWeight(e));
			}
			
		}

		return _newGraph;
	}
	
	/**
	 * Contains. Checks weather the vertex is association class
	 *
	 * @param v the v
	 * @return true, if successful
	 */
	private boolean contains(String v){
		Collection<MAssociationClass> assocs = 
			_model.getAssociationClassesOnly();
		
		for(MAssociationClass assoc : assocs)
			if(v.equals(assoc.name()))
				return true;
		
		return false;
	}

}
