package bgu.simplification;

import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.uml.mm.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 06/09/13
 * Time: 17:33
 * To change this template use File | Settings | File Templates.
 */
public class SimplificationHandler {

    /** the graph*/
    private DirectedWeightedMultigraph<String, MyEdge> graph;
    /**the model*/
    private MModel model;
    /*all the circle we already found*/
    private Vector<Circle> foundCircles;
    /*the first circle*/
    private Circle currentCircle;

    private int current;




    /** the constructor*/
    public SimplificationHandler(DirectedWeightedMultigraph<String, MyEdge> graph,MModel m){
        this.graph= graph;
        model = m;
        foundCircles = new Vector<Circle>();
        currentCircle = new Circle();
        current =0;
        findAllDangerousCircles();

    }
    /**methods*/
    /**
     *
     * @return   a new Model, with tighten circle
     * @throws IOException
     * @throws MInvalidModelException
     */
    public MModel testSimplify() throws IOException, MInvalidModelException {
        //check if there is a circle
        while(true){
        if(hasDangerousCircle()){
            System.out.println("There is a redundancy problem in the model.");
            System.out.println(currentCircle.redundancyDescription());
            System.out.println("Would you like to tighten the model?");
            System.out.println("Press Y for 'yes'");
            System.out.println("Press N for 'no'");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String input = reader.readLine();
            if(input.equals("N")){
                foundCircles.add(currentCircle);
                continue;
            }
            if(input.equals("Y")){
                foundCircles.add(currentCircle);
                MModel newModel = this.tightenThisModel(currentCircle, model);
                //update the model
                model = newModel;
                }
            }
        else{
            System.out.println("There is no redundancy problem in the model.");
            break;
            }
        }
        return model;
    }

    public MModel tightened() throws MInvalidModelException{
        foundCircles.add(currentCircle);
        MModel newModel = this.tightenThisModel(currentCircle, model);
        //update the model
        model = newModel;
        return newModel;
    }

    public MModel getModel(){
        return model;
    }

    public Vector<Circle> getFoundCircles(){
        return foundCircles;
    }

    public Circle getCurrentCircle() {
        Circle c = null;
        if (hasDangerousCircle()){
            c = foundCircles.get(current);
            current++;
        }
        return c;
    }

    public void addToFoundCircles(Circle c){
        if (!chekIfFound(c) && !c.isEmpty())
            foundCircles.add(new Circle(c));
    }

    public boolean hasDangerousCircle() {
        if(current<foundCircles.size()){
            return true;
        }
        return false;

//        currentCircle = null;
//        findDangerousCircle();//garntee to return dangerous circle
//        if  (currentCircle != null){
//           return true;
//        }
//        return false;
    }

//    private void findDangerousCircle() {
//        Set<String> vertex= graph.vertexSet();
//        Iterator<String> iter = vertex.iterator();
//        double weight=0;
//        Circle circle= new Circle();
//        Vector<String> arr= new Vector<String>();
//        Vector<MyEdge> _edges = new Vector<MyEdge>();
//        //go over all the vertex
//        while (iter.hasNext())
//        {
//            String c = iter.next();
//            arr.removeAllElements(); //empty the vertex array
//            _edges.removeAllElements(); //empty the edges array
//            arr.add(c);
//            if(circle==null){
//                circle = new Circle();
//            }
//            else{
//                circle.clear();
//            }
//            circleFromC(c, arr, 1, _edges, circle);  //find circle from c , current weight:
////            if(circle != null){
////                if(circle.isDangerous()) {
////                    if(!this.chekIfFound(circle)){
////                        return circle;  //if not found circle, return the circle
////                    }
////                }
////            }
//        }
//    }

    /**
     *
     * @param c - a circle
     * @return  true if"f this is a new circle
     */
    private  boolean chekIfFound(Circle c){
        boolean ans = false;
        for(int i=0; i<foundCircles.size()&& !ans;i++){
            if(foundCircles.get(i).equals(c)){
                ans = true;
            }
        }
        return ans;
    }

    private void circleFromC(String c, Vector<String> arr, double weight, Vector<MyEdge> _edges, Circle circle){
        // Set<MyEdge> edges= graph.edgesOf(c);//all edges of the vertex
        Set<MyEdge> edges = graph.outgoingEdgesOf(arr.lastElement());
        Iterator<MyEdge> iter = edges.iterator();
        //go over all the neighbors
        while (iter.hasNext())
        {
            MyEdge edge= iter.next();

            if(graph.getEdgeTarget(edge).equals(c)){
                if(arr.size() == 2){
                    if(edge.getAssociation()!= null &&_edges.get(0).getAssociation()!= null && edge.getAssociation().name().equals(_edges.get(0).getAssociation().name())){
                        continue;
                    }
                    else if(edge.getAssociation() == null && _edges.get(0).getAssociation() == null){
                        continue;
                    }
                    else{
                        double temp =  (weight*graph.getEdgeWeight(edge));
                        if(Math.abs(temp -1)< 0.001){
                            weight= temp;
                            arr.add(graph.getEdgeTarget(edge));
                            _edges.add(edge);
                            circle = new Circle();
                            circle.addAllVertex(arr);
                            circle.addAllEdges(_edges);
                            circle.setWeight(weight);
                            currentCircle = circle;
                            if(circle.isDangerous()) {
                                addToFoundCircles(circle);
                            }
                            arr.removeElementAt(arr.size() - 1);
                            _edges.remove(edge);
                            weight= weight/graph.getEdgeWeight(edge);
                        }
                    }
                }
                else{
                    double temp =  (weight*graph.getEdgeWeight(edge));
                    if(Math.abs(temp -1)< 0.001){
                        weight= temp;
                        arr.add(graph.getEdgeTarget(edge));
                        _edges.add(edge);
                        circle = new Circle();
                        circle.addAllVertex(arr);
                        circle.addAllEdges(_edges);
                        circle.setWeight(weight);
                        currentCircle = circle;
                        if(circle.isDangerous()) {
                            addToFoundCircles(circle);
                        }
                        arr.removeElementAt(arr.size() - 1);
                        _edges.remove(edge);
                        weight= weight/graph.getEdgeWeight(edge);
                    }
                }
            }
            else{
                if(!arr.contains(graph.getEdgeTarget(edge))) {
                    arr.insertElementAt(graph.getEdgeTarget(edge), arr.size());
                    _edges.add(edge);
                    weight= weight*graph.getEdgeWeight(edge);
                    circleFromC(c, arr, weight, _edges, circle);  //deep copy
                    arr.removeElementAt(arr.size() - 1);
                    _edges.remove(edge);
                    weight= weight/graph.getEdgeWeight(edge);
//                    if (circle!=null){
//                        return circle;
//                    }
                    //else{ //the circle is null, need to remove all the non relevant items from arr and _edges

                    //}
                }
            }

        }
    }

    

    public void clearFoundCircles(){
        foundCircles.removeAllElements();
    }

    /**
     * this methods search for all the circles in the model
     */
    private void findAllDangerousCircles(){
        Set<String> vertex= graph.vertexSet();
        Iterator<String> iter = vertex.iterator();
        double weight=0;
        Circle circle= new Circle();
        Vector<String> arr= new Vector<String>();
        Vector<MyEdge> _edges = new Vector<MyEdge>();
        //go over all the vertex
        while (iter.hasNext())
        {
            String c = iter.next();
            arr.removeAllElements(); //empty the vertex array
            _edges.removeAllElements(); //empty the edges array
            arr.add(c);
            if(circle==null){
                circle = new Circle();
            }
            else{
                circle.clear();
            }
            circleFromC(c, arr, 1, _edges, circle);  //find circle from c , current weight:1
//            if(circle != null){
//                if(circle.isDangerous()) {
//                    addToFoundCircles(circle);
//                }
//            }
        }

    }

    /**
     *
     * @param c  a given circle
     * @param m   a given model
     * @return     this model such that all its associations related to the circle edges are tighten
     * @throws MInvalidModelException
     */
    public MModel tightenThisModel(Circle c, MModel m) throws MInvalidModelException{

        Collection<MAssociation> associations = m.associations();
        Iterator<MAssociation> iter2 = associations.iterator();
        List<MyEdge> edges = c.getEdges();

        while (iter2.hasNext())
        {
            MAssociation association = iter2.next();

            for(int i = 0 ; i < edges.size() ; i++){
                MyEdge edge = edges.get(i);
                MAssociation ass = edge.getAssociation();
                if(ass == null)
                    continue;
                //found association to tight
                if (ass.name().equals(association.name())){
                    String source = graph.getEdgeSource(edge);
                    String target = graph.getEdgeTarget(edge);
                    //check if this association is already tight
                    if(!edge.isTight()){
                        List<MAssociationEnd> list =  association.associationEnds();
                        MAssociationEnd s1= list.get(0);//index 0 is the source
                        MAssociationEnd s2= list.get(1);// index 1 is the target
                        if(s1.cls().nameAsRolename().equals(source) && s2.cls().nameAsRolename().equals(target)){

                            //set the max to the max

                            int upper = s2.multiplicity().getRange().getUpper() ;
                            MMultiplicity.Range r = s2.multiplicity().getRange();

                            s2.multiplicity().addRange(upper,upper);
                            s2.multiplicity().getRanges().remove(r);

                            MMultiplicity.Range range = s2.multiplicity().getRange();
                            Correction correction = new Correction(ass, s2, range, true, false);
                            c.addCorrection(correction);
                            //set the min to the min

                            int lower =  s1.multiplicity().getRange().getLower();
                            MMultiplicity.Range r1 = s1.multiplicity().getRange();

                            s1.multiplicity().addRange(lower,lower);
                            s1.multiplicity().getRanges().remove(r1);

                            MMultiplicity.Range range1 = s1.multiplicity().getRange();
                            Correction correction1 = new Correction(ass, s1, range1, false, true);
                            c.addCorrection(correction1);
                        }
                        else{    //s2 is the source and s1 is the target
                            //set the max to the max
                            int upper = s1.multiplicity().getRange().getUpper() ;

                            MMultiplicity.Range r = s1.multiplicity().getRange();

                            s1.multiplicity().addRange(upper,upper);
                            s1.multiplicity().getRanges().remove(r);

                            MMultiplicity.Range range1 = s1.multiplicity().getRange();
                            Correction correction1 = new Correction(ass, s1, range1, true, false);
                            c.addCorrection(correction1);

                            //set the min to the min
                            int lower =  s2.multiplicity().getRange().getLower();
                            MMultiplicity.Range r1 = s2.multiplicity().getRange();

                            s2.multiplicity().addRange(lower,lower);
                            s2.multiplicity().getRanges().remove(r1);

                            MMultiplicity.Range range = s2.multiplicity().getRange();
                            Correction correction = new Correction(ass, s2, range, false, true);
                            c.addCorrection(correction);

                        }
                    }
                }
            }

        }

        return  m;

    }

    /**
     * getter for the circles vector
     * @return
     */
//    public Vector<Circle> getAllDangerousCircles(){
//        //findAllDangerousCircles();
//        return foundCircles;
//    }


    public void getCorrections(Circle c){
        List<MyEdge> edges = c.getEdges();
        for(int i = 0 ; i < edges.size() ; i++){
              MyEdge edge = edges.get(i);
              MAssociation ass = edge.getAssociation();
              String source = graph.getEdgeSource(edge);
              String target = graph.getEdgeTarget(edge);
              //check if this association is already tight
               if(!edge.isTight()){
                    List<MAssociationEnd> list =  edge.getAssociation().associationEnds();
                    MAssociationEnd s1= list.get(0);//source in use
                    MAssociationEnd s2= list.get(1);//target in use
                    if(s1.cls().nameAsRolename().equals(source) && s2.cls().nameAsRolename().equals(target)){

                            //set the max to the max

                            int upper = s2.multiplicity().getRange().getUpper() ;

                            MMultiplicity range = new MMultiplicity();
                            range.addRange(upper,upper);
                            Correction correction = new Correction(ass, s2, range.getRange(), true, false);
                            c.addCorrection(correction);
                            //set the min to the min

                            int lower =  s1.multiplicity().getRange().getLower();
                             MMultiplicity range1 = new MMultiplicity();
                             range1.addRange(lower,lower);
                            Correction correction1 = new Correction(ass, s1, range1.getRange(), false, true);
                            c.addCorrection(correction1);
                        }
                        else{    //s2 is the source and s1 is the target
                            //set the max to the max
                            int upper = s1.multiplicity().getRange().getUpper() ;

                             MMultiplicity range1 = new MMultiplicity();

                             range1.addRange(upper,upper);

                            Correction correction1 = new Correction(ass, s1, range1.getRange(), true, false);
                            c.addCorrection(correction1);

                            //set the min to the min
                            int lower =  s2.multiplicity().getRange().getLower();
                            MMultiplicity range = new MMultiplicity();
                            range.addRange(lower,lower);
                            Correction correction = new Correction(ass, s2, range.getRange(), false, true);
                            c.addCorrection(correction);

                        }
                    }


                }
            }


    /*public Vector<Circle> getCircles(){
        return foundCircles;
    }*/

    /**
     *
     * @param c
     * @param s
     * @return   true if the two strings represent the same circle
     */
    public boolean theSameCircle(String c, String s) {
        boolean ans = true;
        for(int i = 0 ; i<c.length() && ans; i++ ){
            char a = c.charAt(i);
            if(s.indexOf(a) == -1)
                ans = false;
        }
        return ans;
    }


}
