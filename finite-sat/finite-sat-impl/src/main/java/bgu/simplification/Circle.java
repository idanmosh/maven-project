package bgu.simplification;

import bgu.circleStructure.CircleImpl;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.tzi.use.uml.mm.MClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 06/09/13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class Circle   {

    /** The _vertices. */
    private List<String> vertex;
    private List<MyEdge> edges ;
    private List<Correction> corrections;
    /** the circle wight*/
    private double weight;


    public Circle(){
        vertex=new ArrayList<String>();
        edges = new ArrayList<MyEdge>();
        corrections = new ArrayList<Correction>();
        weight=0;
    }

    /**
     * copy constructor
     * @param other
     */
    public Circle(Circle other){
        vertex=new ArrayList<String>();
        for (int i =0; i< other.getVertex().size(); i++) {
            vertex.add(other.getVertex().get(i));
        }
        edges = new ArrayList<MyEdge>();
        for (int i =0; i< other.getEdges().size(); i++) {
            edges.add(other.getEdges().get(i));
        }

        corrections = new ArrayList<Correction>();
        for (int i =0; i< other.getCorrections().size(); i++) {
            corrections.add(other.getCorrections().get(i));
        }
        weight=other.getWeight();
    }

    public double getWeight(){
        return weight;
    }

    public void setWeight(double _weight){
        weight= _weight;
    }

    public void addVertex(String c) {
        vertex.add(c);
    }
    public  void addEdge(MyEdge e){
        edges.add(e);
    }
    public List<MyEdge> getEdges(){
        return edges;
    }

    public void addAllVertex(Vector<String> arr) {
        vertex.addAll(arr);
    }
    public String toString(){
        String ans ="<";
        for(int i=0;i<vertex.size();i++){
            ans=ans+" "+vertex.get(i) ;
        }
        ans=ans+" >";
        /*for(int i=0;i<edges.size();i++){
            ans=ans+" "+ edges.get(i).toString();
        }*/
        return ans;
    }

    /**this method checks if the circle is not only with one association, and the range is tight*/
    public boolean isDangerous() {
        int tight=0;
       /** if(edges.size()==2){
            if(edges.get(0).getAssociation().equals(edges.get(1).getAssociation())){
                if(edges.get(0).isTight()){
                    ans = false;
                }
            }
        } */
        for(int i =0; i<edges.size(); i++) {
            if(!(edges.get(i).isTight())){
                tight++;
            }
        }
        return tight != 0;
    }

    public void addAllEdges(Vector<MyEdge> _edges) {
        edges.addAll(_edges);
    }

    /**
     *
     * @param c  Circle other
     * @return    true if the two object represent the same circle
     * two circles will be the same if they both have the same edges.
     */
    public boolean equals(Circle c){
        boolean ans = true;
        if(c.edges.size()!=edges.size()){
            ans = false;
        }
        else{
            for(int i=0; i<edges.size()&&ans;i++){
                if(!c.edges.contains(edges.get(i))){
                    ans = false;
                }
                if(!edges.contains(c.edges.get(i))){
                    ans = false;
                }
            }
        }
        return ans;
    }

    public void clear() {
        weight =0;
        vertex=new ArrayList<String>();
        edges = new ArrayList<MyEdge>();
        corrections = new ArrayList<Correction>();
    }

    public boolean isEmpty() {
        return (vertex.size()==0)&&(edges.size()==0);
    }

    public List<String> getVertex() {
        return vertex;
    }

    public void addCorrection(Correction c){
        corrections.add(c);
    }

    public List<Correction> getCorrections(){
        return corrections;
    }


    public String redundancyDescription() {
        String ans = "Redundancy problem with associations: \n";
        for(int i =0; i<edges.size(); i++){
            if(edges.get(i).getAssociation()!= null){
                ans = ans +" "+edges.get(i).getAssociation();
            }
        }
        ans = ans+" \n and classes: \n";
        for(int i =0; i<vertex.size()-1; i++){
                ans = ans+ " " +vertex.get(i);
            }
        ans = ans +"\n";
        return ans;
    }
}
