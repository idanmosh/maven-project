package bgu.simplification;

import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 22/08/13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
public class CDsimplification {

    //the given model
    private MModel model;
    //the graph
    private DirectedWeightedMultigraph<String, MyEdge> graph;

    private GraphConstructor constructor;

    private SimplificationHandler simplify;

    /**
     * a constructor for the class
     * @param model   - receive the model from the user
     */
    public CDsimplification(MModel model) {
        this.model = model;
        constructor=new GraphConstructor(model);
        graph =constructor.buildGraph();
        simplify=new SimplificationHandler(graph, model);

    }



    /** find a circle in the graph in weight 1, and simplify the graph*/
    public MModel simplify() throws IOException, MInvalidModelException {
        MModel m = simplify.testSimplify();
        System.out.println("Would you like to save the new model?");
        System.out.println("Press Y for 'yes'");
        System.out.println("Press N for 'no'");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        if(input.equals("Y")){
           saveModel(m);
         }
        return m;
    }

    /** find a circle in the graph in weight 1, and simplify the graph*/
    public MModel simplifyAll() throws IOException, MInvalidModelException {
        MModel m = removeAllCircles();
        System.out.println("Would you like to save the new model?");
        System.out.println("Press Y for 'yes'");
        System.out.println("Press N for 'no'");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        if(input.equals("Y")){
            saveModel(m);
        }
        return m;
    }

    public void saveModel(MModel m) throws IOException {
        System.out.println("Insert path to save the model");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        System.out.println("Insert name");
        BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
        String input1 = reader1.readLine();

        String newUse = m.toString();
        //System.out.println(newUse);
        PrintWriter writer = new PrintWriter(input+"/"+input1+".use", "UTF-8");
        writer.println(newUse);
        writer.close();


    }

    public Vector<Circle> getAllCircles(){
        Vector <Circle> circles = simplify.getFoundCircles();
        return  circles;
    }

//    public Circle getCurrentCircle(){
//        return simplify.getCurrentCircle();
//    }

    public MModel removeAllCircles() throws MInvalidModelException {
       Vector <Circle> circles = getAllCircles();

        for(int i = 0; i<circles.size(); i++){
            model = simplify.tightenThisModel(circles.get(i), model);
        }
        return model;
    }

    public MModel removeAllCircles(Vector<Circle> circles) throws MInvalidModelException {
        for(int i = 0; i<circles.size(); i++){
            model = simplify.tightenThisModel(circles.get(i), model);
        }
        return model;
    }

    public void getAllCorrections(Circle circle) {
        simplify.getCorrections(circle);
    }

    public MModel getModel() {
        return this.model;
    }

    public DirectedWeightedMultigraph<String, MyEdge> getGraph() {
        return this.graph;
    }

}
