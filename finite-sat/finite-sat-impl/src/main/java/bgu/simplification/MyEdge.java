package bgu.simplification;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 28/09/13
 * Time: 14:47
 * This class represent Default weighted edge with corresponding association
 */
public class MyEdge extends DefaultWeightedEdge{

    /**the association the edge relate to*/
    private MAssociation association;


    /**constructor*/
    public MyEdge(MAssociation ass){
        super();
        association = ass;
    }
    /**setter for the association*/
    public void setAssociation(MAssociation ass){
        association = ass;
    }
    /**getter for the association*/
    public MAssociation getAssociation(){
        return association;
    }


    /**return true if the association is already tight*/
    public boolean isTight(){
        boolean ans;
        if(association == null){
            return true;
        }
        List<MAssociationEnd> list =  association.associationEnds();
        MAssociationEnd s1= list.get(0);
        MAssociationEnd s2= list.get(1);
        ans = (s1.multiplicity().getRange().getLower()==s1.multiplicity().getRange().getUpper())&&(s2.multiplicity().getRange().getLower()==s2.multiplicity().getRange().getUpper());
        return ans;

    }
    public String toString(){
        if (association!= null){
            List<MAssociationEnd> list =  association.associationEnds();
            MAssociationEnd s1= list.get(0);
            MAssociationEnd s2= list.get(1);
            String s = association.name() + " "+ s1.nameAsRolename() + " " + s2.nameAsRolename()+" ";
            return  s;
        }
        return "hirrachy";
    }
}
