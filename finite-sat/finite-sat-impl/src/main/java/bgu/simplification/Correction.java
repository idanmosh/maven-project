package bgu.simplification;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 28/05/14
 * Time: 10:55
 * To change this template use File | Settings | File Templates.
 */

import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MMultiplicity;

/**
 * This class represent a single correction need to be done to the model
 */
public class Correction {

    private MAssociation association;
    private MAssociationEnd end;
    private MMultiplicity.Range range;
    private boolean changedUpper;
    private boolean changedLower;

    public Correction(MAssociation ass, MAssociationEnd _source, MMultiplicity.Range _sourceRange, boolean upper, boolean lower){
        association = ass;
        end =_source;
        range = _sourceRange;
        changedLower = lower;
        changedUpper = upper;
    }

    public String toString(){
        return "The correction in Association: "+association.name()+" is: in class: "+end.nameAsRolename()+
                " the range should be: [ "+range.getLower()+" , "+range.getUpper()+" ]";

    }

    public  MAssociation getAssociation(){
        return association;
    }

    public MAssociationEnd getAssociationEnd(){
        return end;
    }

   public MMultiplicity.Range getRange(){
       return range;
   }

    public boolean isChangedUpper(){
        return changedUpper;
    }

    public boolean isChangedLower(){
        return  changedLower;
    }
}
