package bgu.simplification;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.gui.views.diagrams.Multiplicity;
import org.tzi.use.uml.mm.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: brit
 * Date: 22/08/13
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */
public class GraphConstructor {
    //the user model
    private MModel model;

    /**
     *
     * @param m  - MMmodel, given by the user
     */
    public GraphConstructor(MModel m){
        model= m;
    }

    /**
     * the main method wich responsible for building the directed weighted multiGraph
     */
    public  DirectedWeightedMultigraph<String, MyEdge> buildGraph() {

        DirectedWeightedMultigraph<String, MyEdge> graph= new  DirectedWeightedMultigraph<String,MyEdge>(MyEdge.class);
        //adding the vartex
        addVertex(graph);
        //adding the edges
        addHierarchyEdges(graph);
        addEdges(graph);

        return graph;
    }

    /**
     *  add all the classes in the model as vertex
     * @param graph - directed weighted multi graph
     */
    private void addVertex( DirectedWeightedMultigraph<String, MyEdge> graph){
        Collection <MClass> classes = model.classes();
        Iterator<MClass> iter = classes.iterator();
        //go over all the classes  and add them to the new model
        while (iter.hasNext())
        {
            MClass mClass = iter.next();
            String name = mClass.nameAsRolename();
            graph.addVertex(name);
        }
    }

    /**
     * add all the edges between the parents and children, edges in weight==1
     * @param graph - directed weighted multi graph
     */
    /***Problem to fix********************************************************************
     * subClassOf, superClassOf*/
    private void addHierarchyEdges(DirectedWeightedMultigraph<String, MyEdge> graph){
        Collection<MClass> classes = model.classes();
        Iterator<MClass> iter = classes.iterator();
        //go over all the classes
        while (iter.hasNext())
        {
            MClass c = iter.next();
            //for each class, go over all the possible children
            Collection<MClass> children = model.classes();
            Iterator<MClass> iter1 = children.iterator();

            // go over all the possible children
            while(iter1.hasNext()){
                MClass c1 = iter1.next();
                //System.out.println("the child name "+c1.nameAsRolename()+" the father "+c.nameAsRolename());
                if(c.isSuperClassOf(c1)){
                    //System.out.println(c1.nameAsRolename()+" is subClassOf "+c.nameAsRolename());
                    if(!(c.nameAsRolename().equals(c1.nameAsRolename()))){
                        //no association corresponding to this edge
                        MyEdge edge=new MyEdge(null);
                        //edge from parent to child, the weight is 1
                        // System.out.println("adding hierarchy edge");
                        graph.addEdge(c1.nameAsRolename(), c.nameAsRolename(),edge);
                        graph.setEdgeWeight(edge,1);
                    }
                }
            }
        }
    }

    /**
     *  add all the associations edges
     * @param graph - directed weighted multi graph
     */
    private void addEdges(DirectedWeightedMultigraph<String, MyEdge> graph){
		Collection<MAssociation> associations = model.associations();
        Iterator<MAssociation> iter1 = associations.iterator();
        while(iter1.hasNext()){
            MAssociation association = iter1.next();
            if (association.isDeleteAssoc())
                continue;
            else{
                List<MAssociationEnd>  list = association.associationEnds();

                //the two classes in the association
                MAssociationEnd target= list.get(1); //target in use file
                MAssociationEnd source= list.get(0);//source in use file
                if((target.multiplicity().getRange().getUpper()!= target.multiplicity().MANY)|| (source.multiplicity().getRange().getLower()!= 0) ){
                    MyEdge edge1 = new MyEdge(association);
                    double weight= calcWeight(source, target);

                    graph.addEdge(source.cls().nameAsRolename(), target.cls().nameAsRolename(), edge1);
                    graph.setEdgeWeight(edge1, weight);
                }
                if((source.multiplicity().getRange().getUpper()!=source.multiplicity().MANY)&& (target.multiplicity().getRange().getLower()!=0)){
                    MyEdge edge2 = new MyEdge(association);
                    double weight2= calcWeight(target, source);
                    graph.addEdge(target.cls().nameAsRolename(), source.cls().nameAsRolename(),edge2);
                    graph.setEdgeWeight(edge2,weight2);

                }
            }
        }
    }
	
	

    private double calcWeight( MAssociationEnd source,MAssociationEnd target){
        double max= target.multiplicity().getRange().getUpper();
        double min= source.multiplicity().getRange().getLower();
        double temp= (1/min);
        double ans= max*temp;
        return ans;
    }
}
