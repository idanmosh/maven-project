package bgu.compositionAggregationCorrectness;

import org.jgrapht.graph.DefaultEdge;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;

public class LabledEdge extends DefaultEdge {

    /**
     *
     */
    private static final long serialVersionUID = 1478531830694987403L;

    //~ Instance fields --------------------------------------------------------
    private MClass fSource;
    private MClass fTarget;
    private String label = null;
    private MAssociationEnd fp;
    private MAssociationEnd fq;
    //~ Methods ----------------------------------------------------------------

    /**
     * Retrieves the label String of this edge. This is protected, for use by
     * subclasses only (e.g. for implementing toString).
     *
     * @return label of this edge
     */
    protected String getLabel()
    {
        return label;
    }

    protected void setLabel(String str){
        label = str;

    }

    /**
     * Returns the source node of this edge.
     */
    public MClass source() {
        return (MClass) getSource();
    }

    /**
     * Returns the target node of this edge.
     */
    public MClass target() {
        return (MClass) getTarget();
    }

    public MAssociationEnd p() {
        return fp;
    }
    public MAssociationEnd q() {
        return fq;
    }


    public void setP(MAssociationEnd p) {
        fp=p;
    }
    public void setQ(MAssociationEnd q) {
        fq=q;
    }

}
