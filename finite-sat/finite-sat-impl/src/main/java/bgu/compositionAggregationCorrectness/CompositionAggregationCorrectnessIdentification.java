/*
 * 
 */
package bgu.compositionAggregationCorrectness;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.DirectedSubgraph;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MAggregationKind;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;
import bgu.circleStructure.Circle;
import bgu.identification.CriticalCircle;
import bgu.compositionAggregationCorrectness.disjontGraph.DisjointGraphFactory;
import bgu.compositionAggregationCorrectness.disjontGraph.EmptyGraphFactory;


/**
 * Class responsible for identifying Composition, Aggregation finite satisfiability correctness.
 */
public class CompositionAggregationCorrectnessIdentification {

    /** The _graph. */
    private DirectedGraph<MClass,LabledEdge> _graph;

    /** The _circles. */
    private Vector<ACcycle<LabledEdge>> _circles = new Vector<ACcycle<LabledEdge>>();

    /** Multi composition problems */
    private Vector<MultiCompositionProblem> _comps = new Vector<MultiCompositionProblem>();

    /** The _print all. */
    private boolean _printAll = false;

    /** The _exit . */
    private boolean _exit = false;

    /** The _model . */
    private MModel _model;

    /** for testing */
    private boolean _print = true;

    /**Constructor
     * Class with methods to identify the cause for not finite satisfiability.
     * This is done by discovering cycles in the graph
     * built out of the UML class diagram.
     *
     * @param model is the representation of UML class diagram.
     */
    public CompositionAggregationCorrectnessIdentification(MModel model){
        AggregateCompositionGraphConstructor constructor = new AggregateCompositionGraphConstructor();
        constructor.buildDetectionGraph(model);
        _graph = constructor.getGraph();
        _model = model;
    }
    /**
     * checks for a circle in the graph.
     *
     * @return true if was found
     */
    public boolean identifyCompositionAggregationCycle(){
        return this.findCycles();
    }

    /**
     * checks for multi composition in the graph
     *
     * @return true if was found
     */
    public boolean identifyMultiCompositionFniteSatisfiability(){
        return this.multiComposition(0);
    }

    /**
     * checks for multi composition in the graph
     *
     * @return true if was found
     */
    public boolean identifyMultiCompositionConsistency(){
        return this.multiComposition(1);
    }

    /**
     * checks for multi composition in the graph
     *
     * @return true if was found
     */
    public boolean identifyMultiCompositionAny(){
        return this.multiComposition(2);
    }

    /**
     * Gets the circles, that were found so far. (Algorithm 2 problems)
     *
     * @return the circles
     */
    public Vector<ACcycle<LabledEdge>> getCircles(){
        return _circles;
    }

    /**
     * Gets the problems that were found so far. (Algorithm 3&4 problems)
     *
     * @return the multi-composition problems
     */
    public Vector<MultiCompositionProblem> getProbs(){
        return _comps;
    }

    /**
     * Find cycles.
     *
     * @return true, if successful
     */
    private boolean findCycles(){
        boolean found = false;

        StrongConnectivityInspector<MClass, LabledEdge> scc =
                new StrongConnectivityInspector<MClass, LabledEdge>(_graph);

        List<DirectedSubgraph<MClass, LabledEdge>> scc_list = scc.stronglyConnectedSubgraphs();

        Iterator<DirectedSubgraph<MClass, LabledEdge>> scc_iter =
                scc_list.iterator();

        //looking for circles in SCC's.
        //for each SCC
        while(scc_iter.hasNext() && !_exit){
            DirectedSubgraph<MClass, LabledEdge> g = scc_iter.next();

            //for each edge
            Iterator<LabledEdge> edge_iter = g.edgeSet().iterator();
            while(edge_iter.hasNext() && !_exit){
                LabledEdge e = edge_iter.next();

                KShortestPaths<MClass, LabledEdge> ksp=
                        new KShortestPaths<MClass,LabledEdge>(g, g.getEdgeTarget(e), g.edgeSet().size());

                List<GraphPath<MClass, LabledEdge>> lst = ksp.getPaths(g.getEdgeSource(e));

                //for each path
                Iterator<GraphPath<MClass, LabledEdge>> path_edges_it =
                        lst.iterator();
                while(path_edges_it.hasNext() && !_exit){
                    GraphPath<MClass, LabledEdge> p = path_edges_it.next();

                    if(validCycle(p,e) && !this.discoveredBefore(p,e)){
                        found = true;
                        this.addCycle(p,e);
                    }
                }
            }
        }
        return found;
    }

    /**
     * Valid Cycle.
     * Checks whether the cycle consisted of one type of edges.
     * + or -
     *
     * @param p the p
     * @param e the e
     * @return true if cycle + or -. false if mixed.
     */
    private boolean validCycle(GraphPath<MClass, LabledEdge> p, LabledEdge e){
        char type = 0;
        for(LabledEdge edge : p.getEdgeList()){
            if(edge.getLabel()!=null){
                type = edge.getLabel().charAt(1);
                break;
            }
        }
        for(LabledEdge edge : p.getEdgeList()){
            if(edge.getLabel()!=null && edge.getLabel().charAt(1)!=type)
                return false;
        }
        if(type!=0 && e.getLabel()!=null && e.getLabel().charAt(1)!=type)
            return false;
        return true;
    }

    /**
     * Discovered before.
     * Checks if a newly discovered circle has already
     * been discovered before
     *
     * @param p the p
     * @param e the e
     * @return true, if successful
     */
    private boolean discoveredBefore(GraphPath<MClass, LabledEdge> p, LabledEdge e){
        boolean contains;

        for(Circle<LabledEdge> circ : _circles){
            contains = true;

            for(LabledEdge edge : p.getEdgeList())
                if(!circ.getCircle().contains(edge)){
                    contains = false;
                    break;
                }

            if(contains && circ.getCircle().contains(e) && p.getEdgeList().size()+1 == circ.getCircle().size())
                return true;
        }

        return false;
    }


    /**
     * Adds the circle.
     *
     * @param p the p
     * @param e the e
     */
    private void addCycle(GraphPath<MClass, LabledEdge> p, LabledEdge e){
        ACcycle<LabledEdge> circ = new ACcycle<LabledEdge>();

        circ.addVertex(e);
        for(LabledEdge edge : p.getEdgeList())
            circ.addVertex(edge);

        ACcycle<LabledEdge> c = sortLexicaly(circ);
        _circles.add(c);

        if (_print)
            System.out.println(printCycleForAlgo2(c));  //printing
        this.execution();  //interactive mode
    }

    /**turning the cycle to start from lowest order source class name.
     *
     */
    private ACcycle<LabledEdge> sortLexicaly(ACcycle<LabledEdge> c){
        LabledEdge first = c.getCircle().get(0);
        String firstName = c.getCircle().get(0).source().name();
        int index = 0;

        for(LabledEdge e: c.getCircle()){
            if(e.source().name().compareTo(firstName) < 0){
                first = e;
                firstName = e.source().name();
            }
        }
        index = c.getCircle().indexOf(first);
        int length = c.getCircle().size();
        int i = index;

        ACcycle<LabledEdge> circ = new ACcycle<LabledEdge>();
        do{
            circ.addVertex(c.getCircle().get(i));
            i = (i+1)%length;

        }while(i!=(index+length)%length);

        return circ;
    }

    /**
     * Execution.
     * Provides interactive mode.
     */
    private void execution(){
        String currLine = "";
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(reader);

        if(!_printAll){

            System.out.println("Continue looking for more cycles? (yes/no/all)");
            try{
                currLine = in.readLine();

                if(currLine.equals("no"))   //in case of "no", system terminates
                    _exit = true;           //any other input and algorithm will
                if(currLine.equals("all"))  //continue looking for more circles
                    _printAll = true;

            }catch(IOException exc){
                System.out.println("Incorrect input: " + exc.getMessage());
            }
        }
    }

    /** print the cycle as needed for Algorithm 2
     *
     */
    public String printCycleForAlgo2 (ACcycle circ){
        String result = new String("");
        String label;
        String[] labelParts;
        String source_p, p, p_1, q, target_p, minMul_p, minMul_p_1;

        for(LabledEdge e : (List<LabledEdge>)circ.getCircle()){
            label = e.getLabel();
            if(label!=null){
                labelParts = label.split(",");
                labelParts[0] = labelParts[0].substring(1);
                labelParts[2] = labelParts[2].substring(0, labelParts[2].length()-1);

                source_p = e.p().otherSideAssocEnd().cls().name();
                p = e.p().name();
                p_1 = e.p().otherSideAssocEnd().name();
                q = e.q().name();
                target_p = e.p().cls().name();
                minMul_p = Integer.toString(e.p().multiplicity().getRange().getLower());
                minMul_p_1 = Integer.toString(e.p().otherSideAssocEnd().multiplicity().getRange().getLower());
                if(labelParts[0].equals("+")){

                    if(labelParts[1].equals(labelParts[2])){

                        result = result + new String("<"+source_p+","+p+","+target_p+","+minMul_p+">");
                    } else {
                        result = result + new String("<"+source_p+","+p+" substes* "+q+","+target_p+","+minMul_p+">");
                    }
                } else if(labelParts[0].equals("-")){
                    if(labelParts[1].equals(labelParts[2])){

                        result = result + new String("<"+source_p+","+p+","+target_p+",<"+p_1+","+minMul_p_1+">>");
                    } else {
                        result = result + new String("<"+source_p+","+p+" substes* "+q+","+target_p+",<"+p_1+","+minMul_p_1+">>");
                    }
                }
            } else result = result + "<H>";
            result = result+", ";
        }
        result = result.substring(0, result.length()-2);
        return result;
    }



    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //:::::::::::::::::::::::::::::::::: ALGO 3 & 4 ::::::::::::::::::::::::::::::::::::::::

    /**
     //* @param model
     * @param kind - 0 for no-finite, 1 for inconsistency, 2 for both.
     * @return true if problems were found.
     */
    private boolean multiComposition(int kind){
        boolean found = false;
        //DirectedGraphLabled CD = new DirectedGraphLabled();
        DisjointGraphFactory disjointFactory = new DisjointGraphFactory();
        SimpleGraph<MClass, VertexPair<MClass>> disjointGraph = disjointFactory.buildGraph(_model);

        AggregateCompositionGraphConstructor constructor = new AggregateCompositionGraphConstructor();
        LabledDirectedMultigraph<MClass, LabledEdge> graph = constructor.getGraph();
        constructor.initialize(_model);
        Set<MAssociationEnd> assoc = _model.asssociationEnds();
        for(MAssociationEnd end : assoc){
            if(end.multiplicity().getRange().getLower()>0){
                MClass src_p = end.otherSideAssocEnd().cls();
                MClass trgt_p = end.cls();
                LabledEdge e = graph.addEdge(src_p, trgt_p);
                e.setP(end);
                e.setQ(end.otherSideAssocEnd());
            }
        }
        constructor.step3(_model.classes());
        Set<MAssociationEnd> ends = _model.asssociationEnds();
        Iterator<MAssociationEnd> it1 = ends.iterator();
        Iterator<MAssociationEnd> it2 = ends.iterator();
        int criterions = 0;
        int i=-1, j=-1;
        while(it1.hasNext()){
            MAssociationEnd p_end = it1.next();
            i++;
            while(it2.hasNext()){
                MAssociationEnd q_end = it2.next();
                j++;
                MClass p_src=p_end.otherSideAssocEnd().cls();
                MClass q_src=q_end.otherSideAssocEnd().cls();
                MClass p_trgt=p_end.cls();
                MClass q_trgt=q_end.cls();
                if(p_src.equals(q_src))
                    criterions++;
                if(criterions==1 && !compositionRelated(p_trgt,q_trgt,_model))
                    criterions++;
                //Set	p_parents = p_trgt.allParents();
                //Set	q_parents = q_trgt.allParents();
                //boolean sameParentsboolean = p_parents.retainAll(q_parents);
                //Set sameParents = p_parents;
                if(criterions==2 && disjointGraph.containsEdge(p_trgt, q_trgt)) //checking if necessarily disjoint
                    criterions++;
                Set<MAssociationEnd> pCompPropEnds = constructor.AggrCompSubset(p_end,1);
                Set<MAssociationEnd> qCompPropEnds = constructor.AggrCompSubset(q_end,1);
                if(criterions==3 && !qCompPropEnds.isEmpty() && !pCompPropEnds.isEmpty()){
                    criterions++;
                }
                if(criterions==4){
                    if(kind==0 || kind==2){
                        KShortestPaths<MClass, LabledEdge> ksp=
                                new KShortestPaths<MClass,LabledEdge>(graph, p_trgt, graph.edgeSet().size());

                        List<GraphPath<MClass, LabledEdge>> paths = ksp.getPaths(q_trgt);
                        if(paths!=null)
                            for(GraphPath<MClass, LabledEdge> p : paths){
                                List<LabledEdge> edgelst = p.getEdgeList();
                                LabledEdge e1 = graph.getProperyEdge(p_end);
                                LabledEdge e2 = graph.getProperyEdge(q_end.otherSideAssocEnd());
                                if(e2!=null && e1!=null){
                                    edgelst.add(e2);
                                    edgelst.add(e1);
                                    MultiCompositionProblem prob = new MultiCompositionProblem(0);
                                    prob.addCycle(edgelst);
                                    prob.add_p_property(p_end, pCompPropEnds.iterator().next());
                                    prob.add_q_property(q_end, qCompPropEnds.iterator().next());
                                    _comps.add(prob);
                                    found = true;
                                    if(_print)
                                        System.out.println(prob.toString());  //printing
                                    this.execution();  //interactive mode
                                }
                            }
                    }
                    if((kind==1 || kind==2) && j>i && p_end.multiplicity().getRange().getLower()==1 && q_end.multiplicity().getRange().getLower()==1){
                        MultiCompositionProblem prob = new MultiCompositionProblem(1);
                        prob.add_p_property(p_end, pCompPropEnds.iterator().next());
                        prob.add_q_property(q_end, qCompPropEnds.iterator().next());
                        _comps.add(prob);
                        found = true;
                        if(_print)
                            System.out.println(prob.toString());  //printing
                        this.execution();  //interactive mode
                    }
                }
                criterions=0;
            }
            j=-1;
            it2 = ends.iterator();
        }
        i=-1;
        return found;
    }


    private boolean compositionRelated(MClass a, MClass b, MModel model){
        if(a.equals(b))
            return true;
        AggregateCompositionGraphConstructor constructor = new AggregateCompositionGraphConstructor();
        LabledDirectedMultigraph<MClass, LabledEdge> CD = constructor.getGraph(); // contains only H and Composition

        constructor.initialize(model);
        for(MAssociationEnd end : model.asssociationEnds()){
            if(end.aggregationKind()==MAggregationKind.COMPOSITION){
                MClass src_p = end.otherSideAssocEnd().cls();
                MClass trgt_p = end.cls();
                CD.addEdge(src_p, trgt_p);
            }
        }
        constructor.step3(model.classes());
        KShortestPaths<MClass, LabledEdge> ksp1= new KShortestPaths<MClass,LabledEdge>(CD, a, CD.edgeSet().size());
        KShortestPaths<MClass, LabledEdge> ksp2= new KShortestPaths<MClass,LabledEdge>(CD, b, CD.edgeSet().size());

        return( ksp1.getPaths(b)!=null || ksp2.getPaths(a)!=null);
    }

    /**
     * calling this function will result in printing all findings without asking later on Identification.
     * used mainly for testing purposes
     */
    public void setPrintAllFindings_ON(){
        _printAll=true;
    }

    /**
     * calling this function will return printing to default.
     * used mainly for testing purposes
     */
    public void setPrintAllFindings_OFF(){
        _printAll=false;
    }

    /**
     * calling this function will result in finding problems without printing them to <Code>System.out</code>.
     * used mainly for testing purposes
     */
    public void setPrinting_OFF(){
        _print=false;
    }

    /**
     * calling this function will return printing.
     * used mainly for testing purposes
     */
    public void setPrinting_ON(){
        _print=true;
    }



    //:::::::::::::::::::::::::::::::::: END ALGO 3 ::::::::::::::::::::::::::::::::::::::::
    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

}
