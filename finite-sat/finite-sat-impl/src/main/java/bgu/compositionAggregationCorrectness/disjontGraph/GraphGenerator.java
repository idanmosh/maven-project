package bgu.compositionAggregationCorrectness.disjontGraph;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.util.VertexPair;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

public interface GraphGenerator {
	
	public SimpleGraph<MClass, VertexPair<MClass>> buildGraph(MModel model);
}
