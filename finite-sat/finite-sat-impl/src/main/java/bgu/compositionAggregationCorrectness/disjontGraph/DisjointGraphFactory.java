package bgu.compositionAggregationCorrectness.disjontGraph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public class DisjointGraphFactory extends GraphFactory implements GraphGenerator {

	private static GSType gsType = new GSDisjointType();
	
	@Override
	protected boolean isValidType(MGeneralizationSet gs) {
		return gsType.isValidType(gs);
	}


}
