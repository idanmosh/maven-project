package bgu.compositionAggregationCorrectness.disjontGraph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public interface GSType {
	
	boolean isValidType(MGeneralizationSet gs);
}
