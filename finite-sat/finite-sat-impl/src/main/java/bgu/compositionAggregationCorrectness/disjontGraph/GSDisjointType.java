package bgu.compositionAggregationCorrectness.disjontGraph;

import org.tzi.use.uml.mm.MGeneralizationSet;

public class GSDisjointType implements GSType {

	@Override
	public boolean isValidType(MGeneralizationSet gs) {
		return gs.isDisjoint() || gs.isDisjointIncomplete() || gs.isDisjointComplete();
	}

}
