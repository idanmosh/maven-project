package bgu.compositionAggregationCorrectness;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DirectedMultigraph;
import org.tzi.use.uml.mm.MAggregationKind;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MClass;


/**
 *A class that build the graph by running algorithm 1.
 */
public class AggregateCompositionGraphConstructor
{
    private LabledDirectedMultigraph<MClass,LabledEdge> CD;
//	private DirectedGraphLabled CDplus;
//	private DirectedGraphLabled CDminus;
    /**
     * Instantiates a new detection graph constructor.
     */
    public AggregateCompositionGraphConstructor()
    {
        CD = new LabledDirectedMultigraph<MClass,LabledEdge>(LabledEdge.class);

    }

    /**
     * Builds the detection graph.
     *
     * @param model the model
     */
    public void buildDetectionGraph(MModel model)
    {
        // System.out.println("test1");
        initialize(model);
        step2(model.asssociationEnds());
        step3(model.classes());

    }
    /**
     * Initialize the nodes of the graph, node for each class.
     *
     * @param model the uml model we get.
     */
    public void initialize(MModel model){
        Iterator<MClass> it = model.classes().iterator();
        while(it.hasNext()){
            MClass cls = it.next();
            //Node n = new Node(cls);
            CD.addVertex(cls);
            //CDplus.add(n);
            //CDminus.add(n);
        }
    }
    /**
     * For each association-end add edges as expected.
     *
     * @param assocEnds the association-end.
     */
    public void step2(Set<MAssociationEnd> assocEnds){
        MAssociationEnd p_property;
        Iterator<MAssociationEnd> it = assocEnds.iterator();
        while (it.hasNext()){
            p_property = it.next();
            Set<MAssociationEnd> subset = AggrCompSubset(p_property, 2);
            if(!subset.isEmpty()){
                step2a(p_property, subset);
                step2b(p_property, subset);
            }
        }
    }
    /**
     * adds an plus edge.
     *
     * @param p_end the association-end.
     * @param subset the subset.
     */
    public void step2a(MAssociationEnd p_end, Set<MAssociationEnd> subset){
        if(!p_end.multiplicity().contains(0)){
            Iterator<MAssociationEnd> it = subset.iterator();
            while(it.hasNext()){
                MAssociationEnd q_end = it.next();
                //Node src_p = CD.findNode(p_end.otherSideAssocEnd().cls());
                //Node trgt_p = CD.findNode(p_end.cls());
                MClass src_p = p_end.otherSideAssocEnd().cls();
                MClass trgt_p = p_end.cls();
                //LabledEdge e = new LabledEdge(src_p, trgt_p);
                String label = new String("<+,"+p_end.name()+","+q_end.name()+">");
                LabledEdge e = CD.addEdge(src_p, trgt_p);
                CD.addLabel(e, label);
                e.setP(p_end);
                e.setQ(q_end);
                //e.addLabel('+', p_end, q_end);
                //CD.addEdge(e);
                //CDplus.addEdge(e);
            }
        }
    }
    /**
     * adds a minus edge.
     *
     * @param p_end
     * @param subset
     */
    public void step2b(MAssociationEnd p_end, Set<MAssociationEnd> subset){
        MAssociationEnd opposite_p_end = p_end.otherSideAssocEnd();
        if(!opposite_p_end.multiplicity().contains(0)){
            Iterator<MAssociationEnd> it = subset.iterator();
            while(it.hasNext()){
                MAssociationEnd q_end = it.next();
                //Node src_p = CD.findNode(p_end.otherSideAssocEnd().cls());
                //Node trgt_p = CD.findNode(p_end.cls());
                MClass src_p = p_end.otherSideAssocEnd().cls();
                MClass trgt_p = p_end.cls();
                //DirectedEdgeLabled e = new DirectedEdgeLabled(trgt_p, src_p);
                String label = new String("<-,"+p_end.name()+","+q_end.name()+">");
                LabledEdge e = CD.addEdge(src_p, trgt_p);
                CD.addLabel(e, label);
                e.setP(p_end);
                e.setQ(q_end);
                //e.addLabel('-', p_end, q_end);
                //CD.addEdge(e);
                //CDminus.addEdge(e);
            }
        }
    }
    /**
     * add a edge between classes if they are hierarchy.
     *
     * @param classes
     */
    public void step3(Collection<MClass> classes){
        Iterator<MClass> it = classes.iterator();
        while(it.hasNext()){
            MClass cls = it.next();
            Set<MClass> cls_parent = cls.parents();
            Iterator<MClass> it2 = cls_parent.iterator();
            while(it2.hasNext()){
                MClass cls2 = it2.next();
                //DirectedEdgeLabled e = new DirectedEdgeLabled(cls, cls2);
                CD.addEdge(cls, cls2);

            }
        }
    }
    /**
     * adds all the transitive associations that happen because subsets.
     *
     * @param end the association end.
     * @param kind the kind of the connection. 0 for aggregation 1 for composition 2 for both.
     * @return If end subsets* q for an aggregate/composite property q return <code>Set</code>,
     * otherwise false.
     */
    public Set<MAssociationEnd> AggrCompSubset(MAssociationEnd end, int kind){
        Set<MAssociationEnd> subset = new HashSet<MAssociationEnd>();
        if (((kind==0 || kind==2) && end.aggregationKind() == MAggregationKind.AGGREGATION) ||
                ((kind==1 || kind==2) && end.aggregationKind() == MAggregationKind.COMPOSITION))
            subset.add(end);
        Set<MAssociationEnd> p_subsets = null;
        if(end.subset()!=null){
            p_subsets= end.subset().getAllSubseted();
        }
        Iterator<MAssociationEnd> it = p_subsets.iterator();
        MAssociationEnd q_property;
        while(it.hasNext()){
            q_property = it.next();
            if (((kind==0 || kind==2) && q_property.aggregationKind() == MAggregationKind.AGGREGATION) ||
                    ((kind==1 || kind==2) && q_property.aggregationKind() == MAggregationKind.COMPOSITION))
                subset.add(q_property);
        }

        return subset;
    }

    public LabledDirectedMultigraph<MClass,LabledEdge> getGraph(){
        return CD;
    }

}

