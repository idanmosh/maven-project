package bgu.compositionAggregationCorrectness;

import org.jgrapht.graph.DirectedMultigraph;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;
/**
 *Creates a Labeled multi graph. 
 *
 *
 */
public class LabledDirectedMultigraph<V, E> extends DirectedMultigraph<V, E>{

    /**
     *
     */
    private static final long serialVersionUID = 8968979937929711490L;

    private String label = null;

    public LabledDirectedMultigraph(Class<? extends E> edgeClass) {
        super(edgeClass);
    }
    /**adds label to Edge <code>e</code>
     *
     * @param e is the edge
     * @param str is the label
     */
    public void addLabel(LabledEdge e, String str){
        //label = str;
        e.setLabel(str);
    }
    /**
     * return the edge that belongs to the property p.
     *
     * @param p is the property.
     * @return the edge that belongs to the property.
     */
    public LabledEdge getProperyEdge(MAssociationEnd p){
        MClass cls = p.otherSideAssocEnd().cls();
        for(E e : this.outgoingEdgesOf((V) cls)){

            if(((LabledEdge) e).p()!=null && ((LabledEdge) e).p().equals(p)){
                return (LabledEdge) e;
            }
        }
        return null;
    }

}
