package bgu.compositionAggregationCorrectness;

import java.util.ArrayList;
import java.util.List;

import bgu.circleStructure.Circle;
/**
 *
 * A class that store all the circles in that was found.
 *
 * @param <T> the circles.
 */
public class ACcycle<T> implements Circle<T> {
    /** + or - */
    private char kind;

    /** The _vertices. */
    private List<T> _vertices;
    /**
     * A constructor of the circles list.
     *
     */
    public ACcycle(){
        _vertices = new ArrayList<T>();
    }
    /**
     * Adds vertex to the circle.
     */
    public void addVertex(T v) {
        _vertices.add(v);
    }
    /**
     * return the circle.
     *
     * @return the circle.
     */
    public List<T> getCircle() {
        return _vertices;
    }
    /**
     * represent the circle in string.
     * @return the represent of the circle in string.
     */
    public String toString(){
        return _vertices.toString();

    }
}
