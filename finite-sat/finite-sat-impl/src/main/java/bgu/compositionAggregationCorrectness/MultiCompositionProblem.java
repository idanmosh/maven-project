package bgu.compositionAggregationCorrectness;

import java.util.List;

import org.jgrapht.GraphPath;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MClass;

public class MultiCompositionProblem {

    private MAssociationEnd p, pt;
    private MAssociationEnd q, qt;
    private ACcycle<LabledEdge> circ;
    private int kind;
    /**
     *
     * @param kind - 0 for no-finite satisfiability, 1 for inconsistency.
     */
    public MultiCompositionProblem(int kind){
        this.kind=kind;
    }


    public void add_p_property(MAssociationEnd p, MAssociationEnd pt){
        this.p=p;
        this.pt=pt;
    }
    public void add_q_property(MAssociationEnd q, MAssociationEnd qt){
        this.q=q;
        this.qt=qt;
    }

    /**
     * Adds the circle.
     *
     * @param p the path
     * @param e the edge
     */
    public void addCycle(List<LabledEdge>l){
        circ = new ACcycle<LabledEdge>();

        for(LabledEdge edge : l)
            circ.addVertex(edge);

        //System.out.println(printCycleForAlgo2(circ));  //printing
        //this.execution();  //interactive mode
    }

    public String toString(){
        String str = null;
        switch (kind){
            case 0:
                str = "no-finite satisfiability in: "+p.name()+" subsets* "+pt.name()+", "+q.name()+" subsets* "+qt.name()+", ";
                for(LabledEdge e : circ.getCircle()){
                    String source_p = e.p().otherSideAssocEnd().cls().name();
                    String p = e.p().name();
                    String target_p = e.p().cls().name();
                    String minMul_p = Integer.toString(e.p().multiplicity().getRange().getLower());
                    str = str + "<"+source_p+","+p+","+target_p+","+minMul_p+">";

                }
                str = str+"\n";
                break;
            case 1:
                str = "inconsistency in: "+p.name()+" subsets* "+pt.name()+", "+q.name()+" subsets* "+qt.name();
                str = str+"\n";
                break;
            default:
                str = "kind is invalid";
                break;
        }
        return str;
    }
}
