
package bgu.circleStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class CircleImpl.
 */
public class CircleImpl<T> implements Circle<T> {
	
	/** The _vertices. */
	private List<T> _vertices;
	
	/**
	 * Instantiates a new circle implementation.
	 */
	public CircleImpl(){
		_vertices = new ArrayList<T>();
	}
	
	/* (non-Javadoc)
	 * @see bgu.circleStructure.Circle#addVertex(org.tzi.use.uml.mm.MClass)
	 */
	public void addVertex(T v){
		_vertices.add(v);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public List<T> getCircle(){
		return _vertices;
	}
}
