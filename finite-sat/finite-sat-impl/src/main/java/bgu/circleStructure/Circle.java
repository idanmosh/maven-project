package bgu.circleStructure;

import java.util.List;

/**
 * The Interface Circle.
 *
 * @param <T> the generic type
 */
public interface Circle<T> {


    /**
     * Adds the vertex.
     *
     * @param v the v
     */
    public void addVertex(T v);

    /**
     * Gets the circle.
     *
     * @return the circle
     */
    public List<T> getCircle();

}
