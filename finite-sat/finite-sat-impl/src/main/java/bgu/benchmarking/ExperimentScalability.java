package bgu.benchmarking;

import bgu.detection.FiniteSatDetector;
import org.tzi.use.config.Options;
import org.tzi.use.main.Session;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MMPrintVisitor;
import org.tzi.use.uml.mm.MMVisitor;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Class for running experiment of measuring the running time of algorithm. 
 */
public class ExperimentScalability {

	/*
	 * Multiplicity range is for association ends, from zero to the specified in
	 * the constructor number.
	 */
	private int m_multiplicity_range;
	
	public ExperimentScalability(){
		m_multiplicity_range = 100;
	}
	
	/**
	 * Method performing the experiment. 
	 * @param classesNumber is the number of classes to be  generated in each model
	 * @param associationsNumber is the number of associations to be randomly generated in each model among the classes
	 * @param repetitions is the number of times to run the experiment of generating and running the algorithm. 
	 * @return the average running time of the algorithm.
	 */
	public double runExperiment(int classesNumber, int associationsNumber, int repetitions){
		
		long totalTime=0;
		double avg=1;
		
		for (int i=0;i<repetitions;i++){
			totalTime += solve(classesNumber,associationsNumber);
		}
		avg = totalTime/repetitions;
		return avg;
	}
	
private long solve (int classesNumber, int associationsNumber) {
	     Stopwatch stoper = new Stopwatch();
		ModelGenerator generator = new ModelGenerator(classesNumber,associationsNumber,m_multiplicity_range);
		generator.generateClassDiagram("c:\\exp.use");
		Session session = new Session();
        MModel model = null;
        MSystem system = null;
        String fileName = "c:\\exp.use";
        
        // compile spec if filename given as argument
        if (true) {
            Reader r = null;
            try {
                Log.verbose("compiling specification...");
                r = new BufferedReader(new FileReader(fileName));
                model = USECompiler.compileSpecification(r,
                        fileName, new PrintWriter(System.err),
                        new ModelFactory());
            } catch (FileNotFoundException e) {
                Log.error("File `" + fileName + "' not found.");
                System.exit(1);
            } finally {
                if (r != null)
                    try {
                        r.close();
                    } catch (IOException ex) {
                        // ignored
                    }
            }

            // compile errors?
            if (model == null) {
                System.exit(1);
            }

            if (Options.compileOnly) {
                Log.verbose("no errors.");
                if (Options.compileAndPrint) {
                    MMVisitor v = new MMPrintVisitor(new PrintWriter(
                            System.out, true));
                    model.processWithVisitor(v);
                }
                System.exit(0);
            }

            // print some info about model
            Log.verbose(model.getStats());

            // create system
            system = new MSystem(model);
            session.setSystem(system);
        } else {
	  model = new ModelFactory().createModel("empty model");
	  system = new MSystem(model);
	  Log.verbose("using empty model.");
	  session.setSystem(system);
	} 
  //      System.out.println(model.getStats());
   //     printModel(model);
        FiniteSatDetector recognizer = new FiniteSatDetector();

   //     FiniteSatDetector detector = new FiniteSatDetector();
 //       detector.getCause(model);
 //       System.out.println("Starting recognition...");
       stoper.start();
       recognizer.isFinitelySatisfiable(model);
       stoper.stop();
       return stoper.elapsedTimeMills();
		
	}
}
