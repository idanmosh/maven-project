package bgu.benchmarking;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 12/4/2009
 * Class for automatic generation of large class diagrams, including constraints.
 * The class uses multiple class diagram metrics to describe the model.
 * The idea is to test the resulting diagram for finite satisfiability problems.
 */
public class ModelGenerator {

	//Private data members. Each member stands for some class diagram metric
	
	private int m_number_of_classes;                //NCM - number of classes in a model
	private int m_number_of_associations;       //NASM - number of associations in a model
	private int m_depth_of_inheritance_tree;    //DIT - depth of inheritance tree
	private double m_inheritance_fraction;       // Fraction of all classes that have same DIT
	private int m_number_GS;                            //NGS - number of Generalization-Set constraints
	private int m_mult_range;                            //MULR - multiplicity constraints range
	private int m_number_of_super_classes;     //NSCC - Number of super classes for a class
	private double m_number_of_super_classes_fraction; //Fraction of the total classes number having NSCC property
	private int m_number_of_sub_classes;        //Number of sub classes for class
	
	/**
	 * An empty constructor.
	 * The model generator must be configured via getters and setters of the metrics.
	 */
	public ModelGenerator(){}
	
	/**
	 * Constructor with partial initiation values
	 * @param classesNumber NCM - number of classes in a model
	 * @param associationsNumber NASM - number of associations in a model
	 * @param multRange MULR - multiplicity constraints range
	 */
	public ModelGenerator(int classesNumber, int associationsNumber, int multRange){
		
		m_number_of_classes = classesNumber;
		m_number_of_associations = associationsNumber;
		m_mult_range = multRange;
		m_depth_of_inheritance_tree = 2;
		m_inheritance_fraction = 0.1;
		
	}
	
	/**
	 * Constructor.
	 * @param classesNumber NCM - number of classes in a model
	 * @param associationsNumber NASM - number of associations in a model
	 * @param multRange MULR - multiplicity constraints range
	 * @param depthOfInheritanceTree DIT - depth of inheritance tree
	 * @param classesFractionForInheritance  Fraction of all classes that have same DIT
	 */
	public ModelGenerator(int classesNumber, 
											 int associationsNumber, 
			                                 int multRange,
			                                 int depthOfInheritanceTree,
			                                 double classesFractionForInheritance){
		
		m_number_of_classes = classesNumber;
		m_number_of_associations = associationsNumber;
		m_mult_range = multRange;
		m_depth_of_inheritance_tree = depthOfInheritanceTree;
		m_inheritance_fraction = classesFractionForInheritance;
		
	}
	
	/**
	 * Method for generating large UML model in USE format.
	 * Takes into account the data members relevant to the model
	 * @param filename is the output file where the symbolic representation of the model is written
	 */
	public void generateClassDiagram(String filename){
		  
    BufferedWriter bufferedWriter = null;
        
        try {
            
            bufferedWriter = new BufferedWriter(new FileWriter(filename));  //Construct the BufferedWriter object
            bufferedWriter.write("model bench");  //Start writing to the output stream
            bufferedWriter.newLine();
      
            generateClasses(bufferedWriter);
            generateAssociations(bufferedWriter);
            
            
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedWriter
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

	}

	private void generateAssociations(BufferedWriter bufferedWriter)
			throws IOException {
		//Write associations into file
		for (int j=0; j<= m_number_of_associations; j++){
			 bufferedWriter.write("association association"+j+" between");
		     bufferedWriter.newLine();	
		     bufferedWriter.write(getRandomClassName() + getMultiplicityRange() + " role role" + j + "a");
		     bufferedWriter.newLine();	
		     bufferedWriter.write(getRandomClassName() + getMultiplicityRange() +" role role" + j + "b");
		     bufferedWriter.newLine();	
		     bufferedWriter.write("end");
		     bufferedWriter.newLine();	
		}
	}

	private void generateClasses(BufferedWriter bufferedWriter)
			throws IOException {
		//Write classes into file
		int single_classes = 0;
		for (int i = 0;  i < m_number_of_classes - m_number_of_classes*m_number_of_super_classes_fraction ; i++){
		    bufferedWriter.write("class class"+i);
		    bufferedWriter.newLine();
		    bufferedWriter.write("end");
		    bufferedWriter.newLine();
		    single_classes=i;
		}
		
		if (m_number_of_super_classes > 0)
			generateHierarchy(bufferedWriter, single_classes);
		
	}

	private void generateHierarchy(BufferedWriter bufferedWriter,
			int single_classes) throws IOException {
		int class_counter = 0;
		for (int j=single_classes + 1; j<= m_number_of_classes; j++){
			
			   bufferedWriter.write("class class"+ j + " < ");
			   for (int k=0; k<m_number_of_super_classes;k++){
				   if (k>0)  {
					   bufferedWriter.write(" ,");
				   }
				   bufferedWriter.write("class" + class_counter+ " ");
				   if (class_counter < single_classes){
					   class_counter ++ ;
				   }
				   else{
					   class_counter = 0;
				   }
			   }
			    bufferedWriter.newLine();
			    bufferedWriter.write("end");
			    bufferedWriter.newLine();
		}
	}
	
	/**
	 * To obtain a class name inside a model
	 * @return a name of class inside the model
	 */
	private String getRandomClassName(){
		
		Random r = new Random();
		int class_number = r.nextInt(m_number_of_classes);
		return "class"+class_number;
		
	}
	
	private String getMultiplicityRange(){
		
		String answer = "";
		int min = getRandomMultiplicity();
		int max = getRandomMultiplicity();
		while (max < min){
			max = getRandomMultiplicity();
		}
		answer ="[" + min + ".." + max + "]";
		return answer;
	}
	
	private int getRandomMultiplicity(){
		
		Random r = new Random();
		return r.nextInt(m_mult_range) + 1;
		
	}

	public int getNumberOfClasses() {
		return m_number_of_classes;
	}

	public void setNumberOfClasses(int numberOfClasses) {
		m_number_of_classes = numberOfClasses;
	}

	public int getNumberOfAssociations() {
		return m_number_of_associations;
	}

	public void setNumberOfAssociations(int numberOfAssociations) {
		m_number_of_associations = numberOfAssociations;
	}

	public int getDepthOfInherotanceTree() {
		return m_depth_of_inheritance_tree;
	}

	public void setDepthOfInheritanceTree(int depthOfInheritanceTree) {
		m_depth_of_inheritance_tree = depthOfInheritanceTree;
	}

	public double getInheritanceFraction() {
		return m_inheritance_fraction;
	}

	public void setInheritanceFraction(double inheritanceFraction) {
		m_inheritance_fraction = inheritanceFraction;
	}

	public int getNumberGS() {
		return m_number_GS;
	}

	public void setNumberGS(int numberOfGS) {
		m_number_GS = numberOfGS;
	}

	public int getMaxMultiplicityValue() {
		return m_mult_range;
	}

	public void setMultiplicityRange(int multiplicityMaxValue) {
		m_mult_range = multiplicityMaxValue;
	}

	public int getNumberOfSuperClasses() {
		return m_number_of_super_classes;
	}

	public void setNumberOfSuperClasses(int numberOfSuperClasses) {
		m_number_of_super_classes = numberOfSuperClasses;
	}

	public double getNumberOfSuperClassesFraction() {
		return m_number_of_super_classes_fraction;
	}

	public void setNumberOfSuperClassesFraction(double numberOfSuperClassesFraction) {
		if (numberOfSuperClassesFraction < 1 && numberOfSuperClassesFraction > 0)
		m_number_of_super_classes_fraction = numberOfSuperClassesFraction;
		
	}

	public int getNumberOfSubClasses() {
		return m_number_of_sub_classes;
	}

	public void setNumberOfSubClasses(int numberOfSubClasses) {
		m_number_of_sub_classes = numberOfSubClasses;
	}
	
	
}
