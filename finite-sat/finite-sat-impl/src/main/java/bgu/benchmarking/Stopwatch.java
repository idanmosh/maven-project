package bgu.benchmarking;
/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on: 13/4/2009
 * Class for measurement of scalability for FiniteSat algorithm.
 */
public class Stopwatch {

	private long m_start;
	private long m_stop;
	
	public void start(){
		m_start = System.currentTimeMillis();
	}
	
	public void stop(){
		m_stop = System.currentTimeMillis();
	}
	
	public long elapsedTimeMills(){
		return m_stop - m_start;
	}
	
	public String toString(){
		return "Elapsed time in millis: " + elapsedTimeMills();
	}
}
