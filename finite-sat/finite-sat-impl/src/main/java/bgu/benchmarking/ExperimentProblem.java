package bgu.benchmarking;

import bgu.detection.FiniteSatDetector;
import org.tzi.use.config.Options;
import org.tzi.use.main.Session;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MMPrintVisitor;
import org.tzi.use.uml.mm.MMVisitor;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

public class ExperimentProblem {

	private int m_multiplicity_range;
	
	public ExperimentProblem() {
		m_multiplicity_range = 100;
	}
	
	public double[] runExperiment(int classesNumber, int repetitions){
		
		 int associations_number;
		double percent=1;
		double[] results = new double[classesNumber];
		
		for (associations_number=0; associations_number < classesNumber;associations_number++){
			double counter=0;
			for (int i=0;i<repetitions;i++){
				if (problemExists(classesNumber,associations_number))
					counter++;
			}
			percent = counter/repetitions;
			results[associations_number] = percent;
		}
		
		return results;
	}

	private boolean problemExists(int classesNumber, int associationsNumber) {
		 
		String fileName = "c:\\exp1.use";
		ModelGenerator generator = new ModelGenerator(classesNumber,associationsNumber,m_multiplicity_range);
		generator.generateClassDiagram(fileName);
		Session session = new Session();
        MModel model = null;
        MSystem system = null;
      
        // compile spec if filename given as argument
        if (true) {
            Reader r = null;
            try {
                Log.verbose("compiling specification...");
                r = new BufferedReader(new FileReader(fileName));
                model = USECompiler.compileSpecification(r,
                        fileName, new PrintWriter(System.err),
                        new ModelFactory());
            } catch (FileNotFoundException e) {
                Log.error("File `" + fileName + "' not found.");
                System.exit(1);
            } finally {
                if (r != null)
                    try {
                        r.close();
                    } catch (IOException ex) {
                        // ignored
                    }
            }

            // compile errors?
            if (model == null) {
                System.exit(1);
            }

            if (Options.compileOnly) {
                Log.verbose("no errors.");
                if (Options.compileAndPrint) {
                    MMVisitor v = new MMPrintVisitor(new PrintWriter(
                            System.out, true));
                    model.processWithVisitor(v);
                }
                System.exit(0);
            }

            Log.verbose(model.getStats());

            system = new MSystem(model);
            session.setSystem(system);
        } else {
	  model = new ModelFactory().createModel("empty model");
	  system = new MSystem(model);
	  Log.verbose("using empty model.");
	  session.setSystem(system);
	} 

        FiniteSatDetector recognizer = new FiniteSatDetector();

        return !recognizer.isFinitelySatisfiable(model);
		
	}

}
