package bgu.benchmarking;

import java.util.Arrays;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 12/4/2009
 * This class is used for running generator object, and creating different models
 * via symbolic representations. 
 */
public class BenchmarkMain {

	public static void main(String[] args){

	//	ModelGenerator gen = new ModelGenerator(100, 30, 10);
	//	gen.setNumberOfSuperClasses(2);
	//	gen.setNumberOfSuperClassesFraction(0.1);
	//	gen.generateClassDiagram("c:\\bench_inheritance.use");
	
	  runExistanceExperiment(100);
   //   runExperimentProblem(100);
	//	runScalabilityExperiment();
	}

	public static void runExperimentProblem(int repetitions){
		System.out.println("Starting problem exists? experience...");
		ExperimentProblem exp = new ExperimentProblem();
		double[] ans = exp.runExperiment(100, repetitions);
		System.out.println("The problem occurs for 100 classes"+ Arrays.toString(ans));
		ans = exp.runExperiment(200, repetitions);
		System.out.println("The problem occurs for 200 classes "+ Arrays.toString(ans));
		ans = exp.runExperiment(300, repetitions);
		System.out.println("The problem occurs for 300 classes"+ Arrays.toString(ans));
	}
	
	public static void runScalabilityExperiment(){
		System.out.println("Starting scalability experience...");
		ExperimentScalability exp = new ExperimentScalability();
    
	//	System.out.println("Average Time: " + exp.runExperiment(50, 10,100));
	//	System.out.println("Average Time: " + exp.runExperiment(50, 25,100));
	//	System.out.println("Average Time: " + exp.runExperiment(50, 50,100));
	/*	System.out.println("Average Time: " + exp.runExperiment(100, 50,100));
		System.out.println("Average Time: " + exp.runExperiment(100, 100,100));
		System.out.println("Average Time: " + exp.runExperiment(200, 100,100));
		System.out.println("Average Time: " + exp.runExperiment(200, 200,100));
		System.out.println("Average Time: " + exp.runExperiment(500, 100,100));
		System.out.println("Average Time: " + exp.runExperiment(500, 250,100));
		System.out.println("Average Time: " + exp.runExperiment(500, 500,100));
		System.out.println("Average Time: " + exp.runExperiment(1000, 100,100));
		System.out.println("Average Time: " + exp.runExperiment(1000, 500,100));
		System.out.println("Average Time: " + exp.runExperiment(1000, 1000,100));*/
	}

	public static void runExistanceExperiment(int repetitionsNumber){
		System.out.println("Starting existance experience...");
		ExperimentExistance exp = new ExperimentExistance();
		System.out.println("Percent: "+exp.runExperiment(100, 300,repetitionsNumber));
	    System.out.println("Percent: "+exp.runExperiment(50, 25,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(50, 50,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(100, 50,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(100, 100,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(200, 100,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(200, 200,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(500, 100,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(500, 250,repetitionsNumber));
		System.out.println("Percent: "+exp.runExperiment(500, 500,repetitionsNumber));
	}


}
