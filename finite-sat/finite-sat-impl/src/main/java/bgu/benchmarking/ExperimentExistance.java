package bgu.benchmarking;

import bgu.detection.FiniteSatDetector;
import org.tzi.use.config.Options;
import org.tzi.use.main.Session;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MMPrintVisitor;
import org.tzi.use.uml.mm.MMVisitor;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * This class is for creating large number of huge class diagrams, and showing the
 * existence of FiniteSat problem in such diagrams.
 */
public class ExperimentExistance {

	/**
	 * Is the range in which the lower and upper bounds of the multiplicity is chosen.
	 */
	private int m_multiplicity_range;
	
	public ExperimentExistance(){
		m_multiplicity_range = 100;
	}
	
	/**
	 * Method for running the experience. The goal is to show the existence of the problem. 
	 * It is shown by performing multiple repetitions on large amount of classes and associations.
	 * @param classesNumber the number of classes to be automatically generated in the model
	 * @param associationsNumber the number of associations among classes to be automatically generated in the model
	 * @param repetitions the number of repetitions to run the experiment
	 * @return the fraction of models where FiniteSat problem occured
	 */
	public double runExperiment(int classesNumber, int associationsNumber, int repetitions){
		
		double percent=1;
		double counter=0;
		
		for (int i=0;i<repetitions;i++){
			if (problemExists(classesNumber,associationsNumber))
			    counter++;
		}
		percent = counter/repetitions;
		return percent;
	}

	private boolean problemExists(int classesNumber, int associationsNumber) {
		 
		String fileName = "c:\\exp1.use";
		ModelGenerator generator = new ModelGenerator(classesNumber,associationsNumber,m_multiplicity_range);
		generator.generateClassDiagram(fileName);
		Session session = new Session();
        MModel model = null;
        MSystem system = null;
       
        
        // compile spec if filename given as argument
        if (true) {
            Reader r = null;
            try {
                Log.verbose("compiling specification...");
                r = new BufferedReader(new FileReader(fileName));
                model = USECompiler.compileSpecification(r,
                        fileName, new PrintWriter(System.err),
                        new ModelFactory());
            } catch (FileNotFoundException e) {
                Log.error("File `" + fileName + "' not found.");
                System.exit(1);
            } finally {
                if (r != null)
                    try {
                        r.close();
                    } catch (IOException ex) {
                        // ignored
                    }
            }

            // compile errors?
            if (model == null) {
                System.exit(1);
            }

            if (Options.compileOnly) {
                Log.verbose("no errors.");
                if (Options.compileAndPrint) {
                    MMVisitor v = new MMPrintVisitor(new PrintWriter(
                            System.out, true));
                    model.processWithVisitor(v);
                }
                System.exit(0);
            }

            // print some info about model
            Log.verbose(model.getStats());

            // create system
            system = new MSystem(model);
            session.setSystem(system);
        } else {
	  model = new ModelFactory().createModel("empty model");
	  system = new MSystem(model);
	  Log.verbose("using empty model.");
	  session.setSystem(system);
	} 
  //      System.out.println(model.getStats());
   //     printModel(model);
        FiniteSatDetector recognizer = new FiniteSatDetector();

   //     FiniteSatDetector detector = new FiniteSatDetector();
 //       detector.getCause(model);
 //       System.out.println("Starting recognition...");
        return !recognizer.isFinitelySatisfiable(model);
		
	}
}
