package bgu.benchmarking;

import org.tzi.use.uml.mm.MClass;

import java.util.Comparator;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * This class is for comparing two classes in a model. It is used to sort classes according to their 
 * associations number. The idea is to determine classes, which are the best candidates to ve involved
 * in Finite Satisfiability problem.
 */
public class ClassComparator implements Comparator<MClass>{

	@Override
	public int compare(MClass class1, MClass class2) {
		if (class1.associations().size() < class2.associations().size()) return 1;
		if (class1.associations().size() > class2.associations().size()) return  -1;
		return 0;
	}

	
}
