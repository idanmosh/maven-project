package bgu.detection;

import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;

import java.util.HashSet;
import java.util.Set;


public class RedefineInequalitiesCreator implements IInequalitiesCreator {

	@Override
	public void addInequalities(MModel model, Problem problem) {
		Set<MAssociationEnd> redefiningEnds = getAllRedefiningEnds(model);
		for (MAssociationEnd end: redefiningEnds) {
			try {
				addRedefiningEndToProblem(end, problem);
			} catch (DuplicateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void addRedefiningEndToProblem(MAssociationEnd redefiningEnd, Problem problem) throws DuplicateException, NotFoundException {

		VariableI rdfndAssocVar,rdfningAssocVar,rdfningOtherSideClassVar,rdfndOtherSideClassVar;
		ConstraintI constraint;
		int lower, upper;

		String redefiningEndName, redefinedEndName, redefinigAssocName, redefinedAssocName;

		redefiningEndName = redefiningEnd.name();
		redefinigAssocName = redefiningEnd.association().name();

		rdfningAssocVar = problem.getVariable("association var " +redefinigAssocName);
		rdfningOtherSideClassVar = problem.getVariable("class var "+
				redefiningEnd.otherSideAssocEnd().cls().name());

		for (MAssociationEnd redefined : redefiningEnd.redefine().getAllRdefined()) {
			redefinedEndName = redefined.name();
			redefinedAssocName = redefined.association().name();
			
		


			rdfndAssocVar = problem.getVariable("association var " +redefinedAssocName);
			rdfndOtherSideClassVar = problem.getVariable("class var "+
					redefined.otherSideAssocEnd().cls().name());

			lower = redefined.multiplicity().getRange().getLower();
			upper = redefined.multiplicity().getRange().getUpper();


			constraint = problem.
			newConstraint("assoc end "
					+ redefiningEndName 
					+ " from association "
					+ redefinigAssocName
					+ " redefines "
					+ redefinedEndName
					+ " from association "
					+ redefinedAssocName);

			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.GREATER);
			problem.setCoefficientAt(constraint.getName(), rdfndAssocVar.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), rdfningAssocVar.getName(), -1);
			problem.setCoefficientAt(constraint.getName(), rdfndOtherSideClassVar.getName(), lower*(-1));
			problem.setCoefficientAt(constraint.getName(), rdfningOtherSideClassVar.getName(), lower);

			constraint = problem.newConstraint("assoc end_2"
					+ redefiningEndName 
					+ " from association "
					+ redefinigAssocName
					+ " redefines "
					+ redefinedEndName
					+ " from association "
					+ redefinedAssocName);


			constraint.setType(ConstraintI.LESS);
			problem.setCoefficientAt(constraint.getName(), rdfndAssocVar.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), rdfningAssocVar.getName(), -1);
			problem.setCoefficientAt(constraint.getName(), rdfndOtherSideClassVar.getName(), upper*(-1));
			problem.setCoefficientAt(constraint.getName(), rdfningOtherSideClassVar.getName(), upper);



















		}
	}

	private Set<MAssociationEnd> getAllRedefiningEnds(MModel model) {
		Set<MAssociationEnd> redefiningEnds = new HashSet<MAssociationEnd>(); 
		for (MAssociationEnd end : model.asssociationEnds()) {
			if (end.redefine().getRedefined() != null) {
				redefiningEnds.add(end);			
			}
		}
		return redefiningEnds;
	}

}
