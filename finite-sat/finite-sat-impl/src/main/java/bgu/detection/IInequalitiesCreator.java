package bgu.detection;

import drasys.or.mp.Problem;
import org.tzi.use.uml.mm.MModel;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 16/10/2008
 */
public interface IInequalitiesCreator {

	/**
	 * A method for extending the problem with linear inequalities, according to the
	 * specific implementation.
	 * @param model is the UML model that contains all data
	 * @param problem is the linear inequalities problem to extended
	 */
	public void addInequalities(MModel model, Problem problem);
	
}
