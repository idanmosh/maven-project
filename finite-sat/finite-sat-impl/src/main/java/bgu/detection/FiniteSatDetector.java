package bgu.detection;

import bgu.cmd.Options;
import drasys.or.matrix.MatrixI;
import drasys.or.matrix.VectorI;
import drasys.or.mp.ConstraintI;
import drasys.or.mp.ConvergenceException;
import drasys.or.mp.InfeasibleException;
import drasys.or.mp.InvalidException;
import drasys.or.mp.NoSolutionException;
import drasys.or.mp.Problem;
import drasys.or.mp.ScaleException;
import drasys.or.mp.UnboundedException;
import drasys.or.mp.lp.DenseSimplex;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
/**
 * This is an implementation of FiniteSat algorithm for recognizing finite satisfiability
 * problems in class diagrams. A model representing the UML class diagram is provided, and
 * a reasoning procedure with linear programming techniques is applied to it.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 13/10/2008
 */
public class FiniteSatDetector {
	
	//inequalities constructors container.
	private List<IInequalitiesCreator> inequalities_creators;
	private String _result;
	private String _fullSolution;
	private String _constraints;
	/**
	 * Constructor. Initializes the inequalities constructors container.
	 */
	public FiniteSatDetector(){
		inequalities_creators = new ArrayList<IInequalitiesCreator>();
		set_result("");
	}
	/**
	 * Method for recognition of finite satisfiability problem within a class diagram. 
	 * @param model is the UML model containing the description of a class diagram,
	 * by UML meta model. In the method, an appropriate linear programming problem 
	 * is created to represent the constraints in the class diagram.
	 * @return if the model is satisfiable or not. 
	 */
	public boolean isFinitelySatisfiable(MModel model){
	/*	if (Options.Variables.xorModel == Options.Variables.XOR_MODEL_OVERIDING) {
			Transformator tm = ModelTransformator.getInstance();
			model = tm.transform(model);
		}*/
		Problem problem = createProblem(model);
		
		
	
	

		return detect(problem);
	}

	/**
	 * Method for creating the appropriate linear programming problem.
	 * Later on, the simplex algorithm will run on that problem in order to decide
	 * if the system of linear inequalities has a solution. 
	 * @param model the UML model representing the class diagram
	 * @return the problem
	 */
	private Problem createProblem(MModel model) {
		//First investigate the size of the LP matrix, by number of variables in the problem
		Collection<MClass> classes = model.classes();
		Collection <MAssociation> associations = model.associations();
		int classes_number = classes.size();
		int associations_number = associations.size();
		//create the problem with a size close to real one
		Problem problem = new Problem(classes_number + associations_number,
				classes_number + associations_number);
		createInequalities(model, problem);
		return problem;
	}
	
	/**
	 * Method for creating the inequalities of the Model. First all classes that perform the inequalities
	 * (aka. Factory classes) creation are instantiated, 
	 * and afterwards all inequalities are created by invoking the addInequalities method in all classes
	 * @param model is the UML class diagram model
	 * @param problem is the linear programming program
	 */
	private void createInequalities(MModel model, Problem problem) {
		//populate the container with all inequalities creators objects
		inequalities_creators.add(new ClassInequalitiesCreator());
		inequalities_creators.add(new AssociationInequalitiesCreator());
		inequalities_creators.add(new GeneralizationInequalityCreator());
		inequalities_creators.add(new GSConstraintInequalitiesCreator());
		inequalities_creators.add(new SubsetInequalitiesCreator());
		inequalities_creators.add(new RedefineInequalitiesCreator());
		if (!Options.getXorModelIsOveriding()) {
			inequalities_creators.add(new XorInequalitiesCreator());
		}
		
		//create the inequalities
		for (int i=0;i<inequalities_creators.size();i++){
			inequalities_creators.get(i).addInequalities(model, problem);
		}
	}
	/**
	 * Method for solving the linear inequalities problem.
	 * @param problem appropriate linear programming problem 
	 * @return if the optimum solution could be achieved.
	 */
	private boolean detect(Problem problem) {
		//First create an object for solving the problem
		//Simplex algorithm is used
		DenseSimplex simplex = new DenseSimplex();
		try {
			//try solving the problem by running simplex
			simplex.setProblem(problem);
			double optimum = simplex.solve();
			set_solution_String(problem, simplex.getSolution());
			set_constraints_string(problem);
			set_result("Solved! Solution Vector: " + simplex.getSolution().toString() + 
			"\nSatisifiable -Number of Instances " + optimum + "\n");
			//if solution achieved that means linear inequalities system has a solution
			return true;
		} catch (InvalidException e) {
			set_result("The simplex object could not be created - not satisfiable");
			return false;
		} catch (NoSolutionException e) {
			set_result("Not Finitely Satisifiable - NoSolutionException");
			return false;
		} catch (UnboundedException e) {
			//If the solution is unbounded - the class diagram is still satisfiable - report it
			System.out.println(" Finitely Satisifiable -UnboundedException");
			return true;
		} catch (InfeasibleException e) {
			set_result("Not Finitely Satisifiable - InfeasibleException");
			return false;
		} catch (ConvergenceException e) {
			set_result("Not Finitely Satisifiable - ConvergenceException");
			return false;
		} catch (ScaleException e) {
			set_result("Not Finitely Satisifiable - ScaleException");
			return false;
		}

	}
	private void set_constraints_string(Problem problem) {
		ConstraintI c;
		StringBuilder sb = new StringBuilder();
		Enumeration<ConstraintI> en =  problem.constraints();
		while (en.hasMoreElements()) {
			c = en.nextElement();
			sb.append(c + "\n");
		}
		MatrixI m = problem.getCoefficientMatrix();
		for (int i = 0; i < m.sizeOfRows(); i++)
			for (int j = 0; j < m.sizeOfColumns(); j++) {
				if (0 == j ) sb.append("\n");
				sb.append(m.elementAt(i, j) + " ");
			}
		
		_constraints = sb.toString();
	}
	
	public String get_constraints_string() {
		return _constraints;
	}
	
	private void set_solution_String(Problem problem, VectorI solution) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < solution.size() ; i++) {
			sb.append(problem.getVariable(i).getName() + " = "+ solution.elementAt(i) + "\n");
		}
		_fullSolution = sb.toString();
	}
	/**
	 * @param _result the _result to set
	 */
	private void set_result(String _result)
	{
		this._result = _result;
	}
	/**
	 * @return the _result
	 */
	public String get_result()
	{
		return _result;
	}
	
	public String get__fullSolution() {
		return _fullSolution;
	}


}