package bgu.detection;

import drasys.or.mp.Constraint;
import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.uml.ocl.type.EnumType;
import org.tzi.use.uml.ocl.type.Type;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
/**
 * Class responsible for creating linear inequalities to reflect associations
 * both regular binary and qualified  in UML class diagram. 
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 16/10/2008
 */
public class AssociationInequalitiesCreator implements IInequalitiesCreator{

	@Override
	public void addInequalities(MModel model, Problem problem) {

		Collection <MAssociation> associations = model.associations();
		addAssociationsToProblem(associations, problem); // add associations constraints
	}

	/**
	 * Private method for adding multiplicity constraints - inequalities, for the associations
	 * in the class diagram. In this process, multiplicities at the association ends are determined,
	 * and appropriate inequality is added to the whole LP problem.
	 * @param associations is the collection of all associations in the class diagram
	 * @param problem is the linear programming problem that contains all needed data and extended
	 * with all the inequalities for the associations
	 * @throws DuplicateException
	 */
	private void addAssociationsToProblem(Collection<MAssociation> associations, Problem problem) {
		Iterator<MAssociation>  iter= associations.iterator();
		while (iter.hasNext()){ //iterate over all associations
			MAssociation association = iter.next(); 
			
			
			/*
			 * 
			 */
			try {
				VariableI var = problem.newVariable("association var " +association.name());
				if (association instanceof MAssociationClass) {
					addAssociationClassConstraint(association, var, problem);
				}
				//create a new variable for each association
				List<MAssociationEnd> ends = association.associationEnds();
				//get each one of the two association ends, and create appropriate constraints
				//binary associations only assumed
				MAssociationEnd end1 = ends.get(0);
				MAssociationEnd end2 = ends.get(1);
				
						
				
				if (end1.isQualified() || end2.isQualified()){ //Apply different constraint in case of qualified association
					addQualifiedConstraint(problem,var,end1,end2);
				}
				else{
					addMultiplicityConstraint(problem,var,end1,end2);
				}
			} catch (DuplicateException e) {
				System.out.println("Unable adding association - Duplicate: "+association.name());
				e.printStackTrace();
			} 
		}
	}

	//Vars of the association and the class of an association class are equal.
	private void addAssociationClassConstraint(MAssociation association,
		VariableI assocVar, Problem problem) throws DuplicateException {
		VariableI classVar =  problem.getVariable("class var "+association.name());
		ConstraintI constraint = problem.newConstraint("AssociationClass constraint "+association.name());	
		constraint.setRightHandSide(0);
		constraint.setType(Constraint.EQUAL);					
		problem.setCoefficientAt(constraint.getRowIndex(), assocVar.getColumnIndex(), 1);
		problem.setCoefficientAt(constraint.getRowIndex(), classVar.getColumnIndex(), -1);
	}

	/**
	 * Method for adding a qualified multiplicity constraint
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param associationEnd1 first end of the association
	 * @param associationEnd2 second end of the association
	 * @throws DuplicateException
	 */
	private void addQualifiedConstraint(Problem problem, VariableI var,
			MAssociationEnd end1, MAssociationEnd end2)  {
	
		if (end1.isQualified()) addQualifiedMultiplicityConstraint(problem, var, end1, end2);
		if (end2.isQualified()) addQualifiedMultiplicityConstraint(problem, var, end2, end1);
	}

	/**
	 * The method differs from the single multiplicity constraint method by the fact the size of the
	 * qualifier domain must be found in order to create the inequalities. The assumption is that only one association
	 * end out of two is qualified and that the domain of its attributes is enumeration thus finite. If some attribute is
	 * of some other kind, for example Integer, it is ignored.
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param qualifiedEnd is the the association end that owns the qualifier
	 * @param associatedEnd is the opposite to qualifier association end
	 */
	private void addQualifiedMultiplicityConstraint(Problem problem, VariableI var,
			MAssociationEnd qualifiedEnd,MAssociationEnd associatedEnd)  {
		//pass through the attributes and multiply domain sizes
		List<MAttribute> attributes = qualifiedEnd.getQualifier().getAttributes();
		
		int qualified_domain_size = 1;
		for (int i=0; i < attributes.size(); i++){
			Type attribute_type = attributes.get(i).type();
			
			if (attribute_type.isEnum()){
				qualified_domain_size=qualified_domain_size * (((EnumType) attribute_type).getEnumerationSize());
				
			}
		}
		//Add regular inequality for qualified range
//old code:
//		Range qualified_range = qualifiedEnd.multiplicity().getRange();
//new code (vitalyb)
		Range qualified_range = qualifiedEnd.getStrictRange();

		addUpperMultiplicityConstraint(problem, var,qualified_range, associatedEnd.cls().name(),"1");
		addLowerMultiplicityConstraint(problem, var, qualified_range, associatedEnd.cls().name(),"1");
		//Add special inequality for qualified range
		MMultiplicity associated_multiplicity = new MMultiplicity();

//old code:
/*		associated_multiplicity.addRange
		(associatedEnd.multiplicity().getRange().getLower() * qualified_domain_size,
				associatedEnd.multiplicity().getRange().getUpper() * qualified_domain_size);
				
 */
//new code (vitalyb)
		associated_multiplicity.addRange
		(associatedEnd.getStrictRange().getLower() * qualified_domain_size,
				associatedEnd.getStrictRange().getUpper() * qualified_domain_size);
				

		
		Range associated_range = associated_multiplicity.getRange();
		addUpperMultiplicityConstraint(problem, var,associated_range,qualifiedEnd.cls().name(),"2");
		addLowerMultiplicityConstraint(problem, var, associated_range, qualifiedEnd.cls().name(),"2");
	}
	/**
	 * Method for adding a specific inequality for a specific association. Binary associations assumed.
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param associationEnd1 first end of the association
	 * @param associationEnd2 second end of the association
	 */
	void addMultiplicityConstraint(Problem problem, VariableI var,
			MAssociationEnd associationEnd1,MAssociationEnd associationEnd2)  {
		//extract the multiplicities numbers for inequalities creation
		MMultiplicity mul1 = associationEnd1.multiplicity();
		MMultiplicity mul2 = associationEnd2.multiplicity();
		Range range1, range2;
//old code:
/*		range1 = mul1.getRange();
		range2 = mul2.getRange();
*/
//new code:
		//TODO: Check if strictRange consider also qalifier.
		range1 = associationEnd1.getStrictRange();
		range2 = associationEnd2.getStrictRange();
		//Add inequalities for both ends , first upper and then lower bounds
		addUpperMultiplicityConstraint(problem, var, range1,associationEnd2.cls().name(),"1");
		addUpperMultiplicityConstraint(problem, var, range2,associationEnd1.cls().name(),"2");
		addLowerMultiplicityConstraint(problem, var, range1,associationEnd2.cls().name(),"1");
		addLowerMultiplicityConstraint(problem, var, range2,associationEnd1.cls().name(),"2");
	}

	/**
	 * The method adds constraints for the lower bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the lower bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the lower range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	private void addLowerMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName, String time) {
		/** When a new constraint created a name is concatenated with string length in order
		to prevent throwing of duplicate exception , but still put association name in constraint name
		as an identifier.**/
		try{
			ConstraintI constraint = problem.newConstraint(var.getName() + " Lower " + time );
			constraint.setRightHandSide(0);
			constraint.setType(Constraint.GREATER);
			problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), "class var "+oppositeClassName, range.getLower() * (-1));
		}catch(NotFoundException e) {
			e.printStackTrace();
		}
		catch(DuplicateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method adds constraints for the upper bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the upper bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the upper range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	public void addUpperMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName,String time)  {
		//Only if upper bound is bounded a new constraint created
		if (range.getUpper() != MMultiplicity.MANY){
			/** When a new constraint created a name is concatenated with string length in order
			to prevent throwing of duplicate exception , but still put association name in constraint name
			as an identifier.**/
			try {
				ConstraintI constraint = problem.newConstraint(var.getName() + " Higher " +time);
				constraint.setRightHandSide(0);
				constraint.setType(Constraint.LESS);
				problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
				problem.setCoefficientAt(constraint.getName(), "class var "+oppositeClassName, range.getUpper() * (-1));
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch(DuplicateException e) {
				e.printStackTrace();
			}
		}
	}

}

