package bgu.detection;

import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
/**
 * Class responsible for creating linear inequalities to reflect constrained
 * generalization sets (GS) of class hierarchy in UML class diagram.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 16/10/2008
 */
public class GSConstraintInequalitiesCreator implements IInequalitiesCreator{

	
	@Override
	public void addInequalities(MModel model, Problem problem) {
		Collection<MGeneralizationSet> gs_collection = model.getGeneralizationSets();
		addConstrainedGeneralizationSetsToProblem(gs_collection,problem);
	}

	/**
	 * Method for extending the LP problem with inequalities for constrained generalization sets
	 * @param gs_collection is the collection of constrained generalization sets
	 * @param problem is the LP problem to be extended
	 */
	private void addConstrainedGeneralizationSetsToProblem(
			Collection<MGeneralizationSet> gs_collection, Problem problem) {
		Iterator<MGeneralizationSet> iter = gs_collection.iterator();
		int constraint_number=0;
		while (iter.hasNext()){
			MGeneralizationSet gs = iter.next();
			addSingleGSToProblem(gs,problem,constraint_number);
			constraint_number++;
		}
	}

	/**
	 * Adds a single generalization set to the problem by constructing appopriate inequality
	 * @param gs is the generalization set
	 * @param problem problem is the LP problem to be extended
	 * @param constraintNumber 
	 */
	private void addSingleGSToProblem(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		
		//System.out.println(gs.getType());
		
		if (gs.isComplete()) addComplete(gs,problem,constraintNumber);
		if (gs.isDisjoint()) addDisjoint(gs,problem,constraintNumber);
		if (gs.isDisjointComplete()) addDisjointComplete(gs,problem,constraintNumber);
		if (gs.isDisjointIncomplete()) addDisjointIncomplete(gs,problem,constraintNumber);
		if (gs.isIncomplete()) addIncomplete(gs,problem,constraintNumber);
		if (gs.isOverlapping()) addOverlapping(gs,problem,constraintNumber);
		if (gs.isOverlappingComplete()) addOverlappingComplete(gs,problem,constraintNumber);
		if (gs.isOverlappingIncomplete()) addOverlappingIncomplete(gs,problem,constraintNumber);
	}

	private void addOverlappingIncomplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			
			List<String> subs = gs.getSubClassesNames();
			String superVariable = "class var " + gs.getSuperClass().name();
			
			for (int i=0; i < subs.size(); i++){
				ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber + "  sub " + i);

				constraint.setRightHandSide(1);
				constraint.setType(ConstraintI.GREATER);
				problem.setCoefficientAt(constraint.getName(), superVariable, 1);
				problem.setCoefficientAt(constraint.getName(), "class var "+ subs.get(i), -1);
			}
			
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
	}

	private void addOverlappingComplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber);
			
			
			//System.out.println("Test1: overlapping_complete");
			constraint.setRightHandSide(-1);
			constraint.setType(ConstraintI.LESS);
			problem.setCoefficientAt(constraint.getName(), "class var " + gs.getSuperClass().name(), 1);
			List<String> subs = gs.getSubClassesNames();
			for (int i = 0; i < subs.size(); i++){
				problem.setCoefficientAt(constraint.getName(), "class var " + subs.get(i), -1);
			}
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
		
	}

	private void addOverlapping(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		// No need in inequality
		
	}

	private void addIncomplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		addOverlappingIncomplete(gs, problem, constraintNumber);
		
	}

	private void addDisjointIncomplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber);
			
			

			constraint.setRightHandSide(1);
			constraint.setType(ConstraintI.GREATER);
			problem.setCoefficientAt(constraint.getName(), "class var " + gs.getSuperClass().name(), 1);
			List<String> subs = gs.getSubClassesNames();
			for (int i = 0; i < subs.size(); i++){
				problem.setCoefficientAt(constraint.getName(), "class var " + subs.get(i), -1);
			}
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
		
	}

	private void addDisjointComplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber);
			
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.EQUAL);
			problem.setCoefficientAt(constraint.getName(), "class var " + gs.getSuperClass().name(), 1);
			List<String> subs = gs.getSubClassesNames();
			for (int i = 0; i < subs.size(); i++){
				problem.setCoefficientAt(constraint.getName(), "class var " + subs.get(i), -1);
			}
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}		
	}

	private void addDisjoint(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			
			
			ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber);
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.GREATER);
			problem.setCoefficientAt(constraint.getName(), "class var " + gs.getSuperClass().name(), 1);
			List<String> subs = gs.getSubClassesNames();
			for (int i = 0; i < subs.size(); i++){
				problem.setCoefficientAt(constraint.getName(), "class var " + subs.get(i), -1);
			}
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
	}

	private void addComplete(MGeneralizationSet gs, Problem problem, int constraintNumber) {
		try {
			
			ConstraintI constraint = problem.newConstraint(gs.getGSName() + constraintNumber);
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.LESS);
			problem.setCoefficientAt(constraint.getName(), "class var " + gs.getSuperClass().name(), 1);
			List<String> subs = gs.getSubClassesNames();
			for (int i = 0; i < subs.size(); i++){
				problem.setCoefficientAt(constraint.getName(), "class var " + subs.get(i), -1);
			}
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

}
