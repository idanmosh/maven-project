package bgu.detection;

import bgu.benchmarking.ClassComparator;
import drasys.or.mp.Constraint;
import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.uml.ocl.type.EnumType;
import org.tzi.use.uml.ocl.type.Type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 18/4/2009
 * 
 * Class for creating adding inequalities to a part of a supplied class diagram.
 * Specifically a natural logarithm of of classes number are considered in the inequalities creation
 * For convenience, the associations relevant to the selected classes only are added as well
 * to the linear programming problem.
 */
public class ParameterizedClassInequalitiesCreator 
						extends ClassInequalitiesCreator 
						implements IInequalitiesCreator{

	protected ArrayList<MAssociation> m_assocs ;
	
	/**
	 * A Constructor. The list of relevant associations is initialized.
	 */
	public ParameterizedClassInequalitiesCreator(){
		m_assocs = new ArrayList<MAssociation>();
	}
	
	public ArrayList<MAssociation> getRelevantAssociations(){
		return m_assocs;
	}
	
	@Override
	public void addInequalities(MModel model, Problem problem) {
		
		Collection<MClass> classes = model.classes();
		addLogClassesToProblem(classes, problem);
		addAssociationsToProblem(m_assocs, problem);
		
	}


	/**
	 * Create a variables and constraints for classes in the diagram
	 * @param classes is the collection of all classes in class diagram
	 * @param problem the linear programming problem
	 * @throws DuplicateException
	 */
	protected void addLogClassesToProblem(Collection<MClass> classes, Problem problem)  {
		
		PriorityQueue<MClass> classes_queue = new PriorityQueue<MClass>(classes.size(),new ClassComparator());
		classes_queue.addAll(classes);
		
		double classes_number = Math.log(classes.size());
		
		int effective_class_counter = 0;
		while (!classes_queue.isEmpty()){   //iterate over Log classes classes
			MClass c = classes_queue.poll();
			addClassInequality(problem, c);
			effective_class_counter ++;
			if (effective_class_counter <= classes_number){
				Set<MAssociation> class_assocs = c.associations();
				addAssociationsToSet(class_assocs);
			}
			
		}
	}

/**
 * The method for adding the association of particular class to the associations set,
 * out of which the linear inequalities should be constructed.
 * @param classAssocs is the Set of associations for particular class
 */
	protected void addAssociationsToSet(Set<MAssociation> classAssocs) {
		
		Iterator<MAssociation> iter = classAssocs.iterator();
		while (iter.hasNext()){
			MAssociation assoc = iter.next();
			if (!m_assocs.contains(assoc)){
				m_assocs.add(assoc);
			}
		}
		
	}

	/**
	 * Method for adding a specific class to the inequalities system.
	 * @param problem is the linear programming problem instance
	 * @param c is the class to be processed
	 */
	protected void  addClassInequality(Problem problem, MClass c) {
		
		VariableI var;
		try {
			var = problem.newVariable("class var "+c.name());
			ConstraintI class_constraint = problem.newConstraint("class constraint "+c.name());		
			//set minimum size to 1 to ensure non empty instance			 
			class_constraint.setRightHandSide(1);
			class_constraint.setType(Constraint.GREATER);			
			problem.setCoefficientAt(class_constraint.getName(), var.getName(), 1);
			var.setObjectiveCoefficient(1);  //objective function to estimate instances number
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		catch (DuplicateException e1) {
			// TODO Auto-generated catch block
		//	e1.printStackTrace();
		} //create a new variable for every class
	
	}
	

	/**
	 * Private method for adding multiplicity constraints - inequalities, for the associations
	 * in the class diagram. In this process, multiplicities at the association ends are determined,
	 * and appropriate inequality is added to the whole LP problem.
	 * @param associations is the collection of all associations in the class diagram
	 * @param problem is the linear programming problem that contains all needed data and extended
	 * with all the inequalities for the associations
	 * @throws DuplicateException
	 */
	protected void addAssociationsToProblem(Collection<MAssociation> associations, Problem problem) {
		
		Iterator<MAssociation>  iter= associations.iterator();
		while (iter.hasNext()){ //iterate over all associations
			MAssociation association = iter.next();
			try {
				VariableI var = problem.newVariable("association var " + association.name());
				//create a new variable for each association
				List<MAssociationEnd> ends = association.associationEnds();
				//get each one of the two association ends, and create appropriate constraints
				//binary associations only assumed
				MAssociationEnd end1 = ends.get(0);
				MAssociationEnd end2 = ends.get(1);
				if (end1.isQualified() || end2.isQualified()){ //Apply different constraint in case of qualified association
					addQualifiedConstraint(problem,var,end1,end2);
				}
				else{
					addMultiplicityConstraint(problem,var,end1,end2);
				}
			} catch (DuplicateException e) {
			System.out.println("Unable adding association - Duplicate: "+association.name());
			e.printStackTrace();
			} 
		}
	}

	/**
	 * Method for adding a qualified multiplicity constraint
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param associationEnd1 first end of the association
	 * @param associationEnd2 second end of the association
	 * @throws DuplicateException
	 */
	protected void addQualifiedConstraint(Problem problem, VariableI var,
			MAssociationEnd end1, MAssociationEnd end2)  {
		
		if (end1.isQualified()) addQualifiedMultiplicityConstraint(problem, var, end1, end2);
		if (end2.isQualified()) addQualifiedMultiplicityConstraint(problem, var, end2, end1);
	}

	/**
	 * The method differs from the single multiplicity constraint method by the fact the size of the
	 * qualifier domain must be found in order to create the inequalities. The assumption is that only one association
	 * end out of two is qualified and that the domain of its attributes is enumeration thus finite. If some attribute is
	 * of some other kind, for example Integer, it is ignored.
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param qualifiedEnd is the the association end that owns the qualifier
	 * @param associatedEnd is the opposite to qualifier association end
	 */
	protected void addQualifiedMultiplicityConstraint(Problem problem, VariableI var,
			MAssociationEnd qualifiedEnd,MAssociationEnd associatedEnd)  {
		//pass through the attributes and multiply domain sizes
		List<MAttribute> attributes = qualifiedEnd.getQualifier().getAttributes();
		int qualified_domain_size = 1;
		for (int i=0; i < attributes.size(); i++){
			Type attribute_type = attributes.get(i).type();
			if (attribute_type.isEnum()){
				qualified_domain_size=qualified_domain_size * (((EnumType) attribute_type).getEnumerationSize());
			}
		}
		//Add regular inequality for qualified range
		Range qualified_range = qualifiedEnd.multiplicity().getRange();
		addUpperMultiplicityConstraint(problem, var,qualified_range, associatedEnd.cls().name(),"1");
		addLowerMultiplicityConstraint(problem, var, qualified_range, associatedEnd.cls().name(),"1");
		//Add special inequality for qualified range
		MMultiplicity associated_multiplicity = new MMultiplicity();
		associated_multiplicity.addRange
		(associatedEnd.multiplicity().getRange().getLower() * qualified_domain_size,
				associatedEnd.multiplicity().getRange().getUpper() * qualified_domain_size);
		Range associated_range = associated_multiplicity.getRange();
		addUpperMultiplicityConstraint(problem, var,associated_range,qualifiedEnd.cls().name(),"2");
		addLowerMultiplicityConstraint(problem, var, associated_range, qualifiedEnd.cls().name(),"2");
	}
	/**
	 * Method for adding a specific inequality for a specific association. Binary associations assumed.
	 * @param problem  problem is the linear programming problem that contains all needed data and extended
	 * @param var is the variable for concrete association, already created into the problem
	 * @param associationEnd1 first end of the association
	 * @param associationEnd2 second end of the association
	 */
	protected void addMultiplicityConstraint(Problem problem, VariableI var,
			MAssociationEnd associationEnd1,MAssociationEnd associationEnd2)  {
		//extract the multiplicities numbers for inequalities creation
		MMultiplicity mul1 = associationEnd1.multiplicity();
		MMultiplicity mul2 = associationEnd2.multiplicity();
		Range range1 = mul1.getRange();
		Range range2= mul2.getRange();
		//Add inequalities for both ends , first upper and then lower bounds
		addUpperMultiplicityConstraint(problem, var, range1,associationEnd2.cls().name(),"1");
		addUpperMultiplicityConstraint(problem, var, range2,associationEnd1.cls().name(),"2");
		addLowerMultiplicityConstraint(problem, var, range1,associationEnd2.cls().name(),"1");
		addLowerMultiplicityConstraint(problem, var, range2,associationEnd1.cls().name(),"2");
	}

	/**
	 * The method adds constraints for the lower bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the lower bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the lower range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	protected void addLowerMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName, String time) {
		/** When a new constraint created a name is concatenated with string length in order
		to prevent throwing of duplicate exception , but still put association name in constraint name
		as an identifier.**/
		try{
			ConstraintI constraint = problem.newConstraint(var.getName() + " Lower " + time );
			constraint.setRightHandSide(0);
			constraint.setType(Constraint.GREATER);
			problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), "class var "+oppositeClassName, range.getLower() * (-1));
		}catch(NotFoundException e) {
		e.printStackTrace();
		}
		catch(DuplicateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method adds constraints for the upper bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the upper bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the upper range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	protected void addUpperMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName,String time)  {
		//Only if upper bound is bounded a new constraint created
		if (range.getUpper() != MMultiplicity.MANY){
			/** When a new constraint created a name is concatenated with string length in order
			to prevent throwing of duplicate exception , but still put association name in constraint name
			as an identifier.**/
			try {
				ConstraintI constraint = problem.newConstraint(var.getName() + " Higher " +time);
				constraint.setRightHandSide(0);
				constraint.setType(Constraint.LESS);
				problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
				problem.setCoefficientAt(constraint.getName(), "class var "+oppositeClassName, range.getUpper() * (-1));
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch(DuplicateException e) {
				e.printStackTrace();
			}
		}
	}


}
