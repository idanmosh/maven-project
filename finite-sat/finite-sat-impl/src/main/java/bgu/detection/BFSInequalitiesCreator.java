package bgu.detection;

import bgu.benchmarking.ClassComparator;
import drasys.or.mp.Problem;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 21/6/2009
 * 
 * Class for creating adding inequalities to a part of a supplied class diagram.
 * Specifically a natural logarithm of of classes number are considered in the inequalities creation.
 * The classes are not selected randomly. They are found in breadth first search (BFS) instead.
 * ParameterizedClassInequalitiesCreator is subclassed in order to save all the functionality
 * but the selection of log classes.
 * For convenience, the associations relevant to the selected classes only are added as well
 * to the linear programming problem.
 */

public class BFSInequalitiesCreator extends ParameterizedClassInequalitiesCreator implements IInequalitiesCreator{

	private HashSet<MAssociation> m_explored_assocs;
	private HashSet<MClass> m_explored_classes;
	
	public BFSInequalitiesCreator(){
		super();
		m_explored_assocs = new HashSet<MAssociation>();
		m_explored_classes = new HashSet<MClass>();
	}
	
	@Override
	public void addInequalities(MModel model, Problem problem) {
		Collection<MClass> classes = model.classes();
		addBFSLogClassesToProblem(classes, problem);
		addAssociationsToProblem(m_explored_assocs, problem);
		
	}
	
	/**
	 * Method performing the creation of inequalities in BFS order traversed class diagram, starting
	 * with the most associated class.
	 * @param classes is the collection of classes in the class diagram
	 * @param problem is the LP problem to be extended with inequalities
	 */
	protected void addBFSLogClassesToProblem(Collection<MClass> classes, Problem problem)  {
		
		PriorityQueue<MClass> classes_queue = new PriorityQueue<MClass>(classes.size(),new ClassComparator());
		classes_queue.addAll(classes);
		double classes_number = Math.log(classes.size());
		traverseBFS(problem, classes_queue, classes_number);
		
	}

	private void traverseBFS(Problem problem, PriorityQueue<MClass> classesQueue, double classesNumber) {
		
		int effective_class_counter = 0;
		ArrayList<MClass> current_level_classes = new ArrayList<MClass>();
		
		MClass most_associated_class = classesQueue.poll();
		current_level_classes.add(most_associated_class);
		addClassInequality(problem, most_associated_class);
				
		while (!current_level_classes.isEmpty() && effective_class_counter<=classesNumber){
			ArrayList<MClass> next_level_classes = new ArrayList<MClass>();
			for (MClass current_class : current_level_classes){
			
				for (MAssociation current_assoc : (Set<MAssociation>)current_class.allAssociations()){
					if (!m_explored_assocs.contains(current_assoc)){
						m_explored_assocs.add(current_assoc);
						for (MClass next_class : (Set<MClass>)current_assoc.associatedClasses()){
							if (!m_explored_classes.contains(next_class)){
								m_explored_classes.add(next_class);
								next_level_classes.add(next_class);
								Set<MAssociation> class_assocs = next_class.associations();
								addAssociationsToSet(class_assocs);
								effective_class_counter ++;
								addClassInequality(problem, next_class);
							}
						}
					}
				}
			}
			current_level_classes = next_level_classes; // moving to the next level in breadth search
		}
		
	}


}
