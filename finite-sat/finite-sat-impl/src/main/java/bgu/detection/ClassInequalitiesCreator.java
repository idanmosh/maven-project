package bgu.detection;

import drasys.or.mp.Constraint;
import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;

import java.util.Collection;
import java.util.Iterator;
/**
 * Class responsible for creating linear inequalities to reflect classes  in UML class diagram.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 16/10/2008
 */

public class ClassInequalitiesCreator implements IInequalitiesCreator{

	@Override
	public void addInequalities(MModel model, Problem problem) {
		Collection<MClass> classes = model.classes();
		addClassesToProblem(classes, problem);
	}


	/**
	 * Create a variables and constraints for classes in the diagram
	 * @param classes is the collection of all classes in class diagram
	 * @param problem the linear programming problem
	 * @throws DuplicateException
	 */
	private void addClassesToProblem(Collection<MClass> classes, Problem problem)  {
		Iterator<MClass> iter = classes.iterator();
		while (iter.hasNext()){                                        //iterate over all classes
			MClass c = iter.next();
			
			
			VariableI var;
			try {
				var = problem.newVariable("class var "+c.name());
				ConstraintI class_constraint = problem.newConstraint("class constraint "+c.name());	
				//set minimum size to 1 to ensure non empty instance			 
				class_constraint.setRightHandSide(1);
				class_constraint.setType(Constraint.GREATER);			
				problem.setCoefficientAt(class_constraint.getName(), var.getName(), 1);
				var.setObjectiveCoefficient(1);  //objective function to estimate instances number
			} catch (NotFoundException e) {
				e.printStackTrace();
			}
			catch (DuplicateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} //create a new variable for every class
		}
	}

}
