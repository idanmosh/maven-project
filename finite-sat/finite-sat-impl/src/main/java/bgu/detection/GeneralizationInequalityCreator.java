package bgu.detection;

import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import org.tzi.use.graph.DirectedEdge;
import org.tzi.use.graph.DirectedGraph;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizable;
import org.tzi.use.uml.mm.MModel;

import java.util.Iterator;

/**
 * Class responsible for creating linear inequalities to reflect class hierarchy  in UML class diagram.
 * Ben-Gurion University of the Negev, Department of Computer Science
 * @author Victor Makarenkov
 * Created on 16/10/2008
 */
public class GeneralizationInequalityCreator implements IInequalitiesCreator {

	@Override
	public void addInequalities(MModel model, Problem problem) {
	//iterate over all the generalizations and create appropriate inequalities
		DirectedGraph graph = model.generalizationGraph();
		Iterator<DirectedEdge> edges_iter = graph.edgeIterator();
		while(edges_iter.hasNext()){
			DirectedEdge edge = edges_iter.next();     //get the next edge in the hierarchy graph
			
			 
			MGeneralizable source = (MGeneralizable) edge.source(); 
			MGeneralizable target = (MGeneralizable) edge.target(); 
			if (source instanceof MClass) {
				addHierarcgyInequality((MClass)target,(MClass)source,problem);
			}
			
			else if (source instanceof MAssociation) {
				addHierarcgyInequality((MAssociation)target, (MAssociation)source, problem);
			}
		}
	}

	/**
	 * Method creating a new inequality for a pair of classes standing in inheritance relation
	 * @param superClass is the super class 
	 * @param derivedClass is the derived class
	 * @param problem is the linear programming problem to be extended
	 */
	private void addHierarcgyInequality(MClass superClass, MClass derivedClass, Problem problem) {
		try {
			
			ConstraintI constraint = 
				problem.newConstraint(superClass.name() + " is super to " + derivedClass.name());

			problem.setCoefficientAt(constraint.getName(), "class var "+derivedClass.name(), 1);
			problem.setCoefficientAt(constraint.getName(), "class var "+superClass.name(), -1);
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.LESS);
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void addHierarcgyInequality(MAssociation superAssoc, MAssociation derivedAssoc, Problem problem) {
		try {
			ConstraintI constraint = 
				problem.newConstraint(superAssoc.name() + " is super to " + derivedAssoc.name());

			problem.setCoefficientAt(constraint.getName(), "association var "+derivedAssoc.name(), 1);
			problem.setCoefficientAt(constraint.getName(), "association var "+superAssoc.name(), -1);
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.LESS);
		} catch (DuplicateException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
