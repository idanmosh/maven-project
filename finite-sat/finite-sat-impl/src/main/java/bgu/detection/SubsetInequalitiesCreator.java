package bgu.detection;

import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;

import java.util.HashSet;
import java.util.Set;

public class SubsetInequalitiesCreator implements IInequalitiesCreator {

	@Override
	public void addInequalities(MModel model, Problem problem) {
		Set<MAssociationEnd> subsetingEnds = getAllSubsetingEnds(model);
		for (MAssociationEnd end: subsetingEnds) {
			try {
				addSubsetingEndToProblem(end, problem);
			} catch (DuplicateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void addSubsetingEndToProblem(MAssociationEnd subsetingEnd, Problem problem) throws DuplicateException, NotFoundException {
		
		VariableI sbstdAssocVar,sbstingAssocVar,sbstingOtherSideClassVar,sbstdOtherSideClassVar;
		ConstraintI constraint;
		int lower;
		
		String subsetingEndName, subsetedEndName, subsetingAssocName, subsetedAssocName;
		
		subsetingEndName = subsetingEnd.name();
		subsetingAssocName = subsetingEnd.association().name();
		
		sbstingAssocVar = problem.getVariable("association var " +subsetingAssocName);
		sbstingOtherSideClassVar = problem.getVariable("class var "+
				subsetingEnd.otherSideAssocEnd().cls().name());
		
		for (MAssociationEnd subsetedEnd : subsetingEnd.subset().getAllSubseted()) {
			subsetedEndName = subsetedEnd.name();
			subsetedAssocName = subsetedEnd.association().name();
			
			
			sbstdAssocVar = problem.getVariable("association var " +subsetedAssocName);
			sbstdOtherSideClassVar = problem.getVariable("class var "+
					subsetedEnd.otherSideAssocEnd().cls().name());
			
			lower = subsetedEnd.multiplicity().getRange().getLower();
			
			
			constraint = problem.
			newConstraint("assoc end "
					+ subsetingEndName 
					+ " from association "
					+ subsetingAssocName
					+ " subsets "
					+ subsetedEndName
					+ " from association "
					+ subsetedAssocName);
			constraint.setRightHandSide(0);
			constraint.setType(ConstraintI.GREATER);
			problem.setCoefficientAt(constraint.getName(), sbstdAssocVar.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), sbstingAssocVar.getName(), -1);
			problem.setCoefficientAt(constraint.getName(), sbstdOtherSideClassVar.getName(), lower*(-1));
			problem.setCoefficientAt(constraint.getName(), sbstingOtherSideClassVar.getName(), lower);	
			
			  
			
			
		}
	}

	/**
	 * Get all ends that subset some other end.
	 * @param model
	 * @return result
	 */
	private Set<MAssociationEnd> getAllSubsetingEnds(MModel model) {
		Set<MAssociationEnd> subsetingEnds = new HashSet<MAssociationEnd>(); 
		for (MAssociationEnd end : model.asssociationEnds()) {
				subsetingEnds.addAll(end.subset().getSubsetting());			
		}
		return subsetingEnds;
	}

}
