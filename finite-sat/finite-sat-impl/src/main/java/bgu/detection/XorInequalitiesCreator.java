package bgu.detection;

import drasys.or.mp.Constraint;
import drasys.or.mp.ConstraintI;
import drasys.or.mp.DuplicateException;
import drasys.or.mp.NotFoundException;
import drasys.or.mp.Problem;
import drasys.or.mp.VariableI;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MMultiplicity;
import org.tzi.use.uml.mm.MMultiplicity.Range;
import org.tzi.use.uml.mm.MXor;

/**
 * This class creates a set of inequalities that represent 'xor' objects for conforming semantics.
 * In overriding semantics 'xor' objects are transformed to GS constraints. 
 * @author vitalik
 *
 */
public class XorInequalitiesCreator implements IInequalitiesCreator {

	@Override
	public void addInequalities(MModel model, Problem problem) {
		try {
			for (MXor xor : model.getXorSet()) {
				//creatreInequality(problem, xor);
				
				create(problem, xor);
			}
		}catch (DuplicateException e) {
			System.err.println(e);
			e.printStackTrace();
		} catch (NotFoundException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	
	private void create(Problem problem, MXor xor) throws DuplicateException, NotFoundException {
		
		createUpperBound(problem, xor);
		
		createLowerBound(problem, xor);
		
		
	}

	// r1/max(1) + r2/max(2) + ... + rn/max(n) < c
	private void createUpperBound(Problem problem, MXor xor) throws NotFoundException, DuplicateException {
		ConstraintI constraint;
		String var_name;
		int max;

		constraint = problem.newConstraint("upper bound for xor " + xor.name());
		
		constraint.setRightHandSide(0);
		constraint.setType(ConstraintI.LESS);
		problem.setCoefficientAt(constraint.getName(), "class var " + xor.getSourceClass().name(), -1);
		for (MAssociationEnd end : xor.getEnds()){
			
			//if true - max is "*" (a star)
			
			if ((max = end.multiplicity().getRange().getUpper()) < 0) {
				//TODO: We need to chose another number
				max = 100000;
			}
			
			var_name = "class var " + end.cls().name();
			
			problem.setCoefficientAt(constraint.getName(), var_name, 1f/max);
		}
		
		
	}

	//r1+r2+....+rn > C
	private void createLowerBound(Problem problem, MXor xor) throws DuplicateException, NotFoundException {
		ConstraintI constraint;
		String var_name;
		
		constraint = problem.newConstraint("lower bound for xor " + xor.name());
		
		constraint.setRightHandSide(0);
		constraint.setType(ConstraintI.GREATER);
		problem.setCoefficientAt(constraint.getName(), "class var " + xor.getSourceClass().name(), -1);
		for (MAssociationEnd end : xor.getEnds()){
			var_name = "class var " + end.cls().name();
			problem.setCoefficientAt(constraint.getName(), var_name, 1);
		}		
	}


	private void creatreInequality(Problem problem, MXor xor) 
	throws DuplicateException, NotFoundException {
		
		//Create constraints for virtual classes
		createVclassConsraints(problem, xor);
		
		//Create constraint for virtual GS
		createVGSConstrains(problem, xor);
		
		
		//Create association constraint between virtual and real classes.
		createVAssociationConstraints(problem, xor);
	}
	
	
	private void createVAssociationConstraints(Problem problem, MXor xor) throws DuplicateException {
		//src - virtual class ; tar - real class from xor ends. 
		Range srcRange, tarRange;
		String srcClsName, tarClsName; 
		MAssociationEnd srcEnd;

		VariableI var;
		
		for (MAssociationEnd end : xor.getEnds()) {
			var = problem.newVariable("vassociation for "+ xor.name() + "and end " + end.name());
			
			srcEnd = end.getAllOtherAssociationEnds().get(0);
			srcRange = srcEnd.multiplicity().getRange();
			tarRange = end.multiplicity().getRange();
			
			srcClsName = "vclass var for "+xor.name() + " " + end.cls().name();
			tarClsName = "class var "+end.cls().name();
			
			addUpperMultiplicityConstraint(problem, var, srcRange,tarClsName,"1");
			addUpperMultiplicityConstraint(problem, var, tarRange,srcClsName,"2");
			addLowerMultiplicityConstraint(problem, var, srcRange,tarClsName,"1");
			addLowerMultiplicityConstraint(problem, var, tarRange,srcClsName,"2");
		}


		
	}

	private void createVGSConstrains(Problem problem, MXor xor) throws DuplicateException, NotFoundException {
		ConstraintI constraint;
		String var_name;
		
		constraint = problem.newConstraint("vgs for xor " + xor.name());
		
		constraint.setRightHandSide(0);
		constraint.setType(ConstraintI.EQUAL);
		problem.setCoefficientAt(constraint.getName(), "class var " + xor.getSourceClass().name(), 1);
		for (MAssociationEnd end : xor.getEnds()){
			var_name = "vclass var for " + xor.name() + " " + end.cls().name();
			problem.setCoefficientAt(constraint.getName(), var_name, -1);
		}		
	}

	private void createVclassConsraints(Problem problem, MXor xor) throws DuplicateException {
		VariableI var;
		ConstraintI constraint;
		
		for (MAssociationEnd end : xor.getEnds()) {
			var = problem.newVariable("vclass var for "+xor.name() + " " + end.cls().name());
			constraint = problem.newConstraint("vclass constraint for " 
					+ xor.name() 
					+ "  " 
					+ end.cls().name());	
			//set minimum size to 1 to ensure non empty instance			 
			constraint.setRightHandSide(1);
			constraint.setType(Constraint.GREATER);			
			problem.setCoefficientAt(constraint.getRowIndex(), var.getColumnIndex(), 1);
			var.setObjectiveCoefficient(1);  //objective function to estimate instances number
		}


		
	}

	/**
	 * The method adds constraints for the lower bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the lower bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the lower range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	private void addLowerMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName, String time) {
		/** When a new constraint created a name is concatenated with string length in order
		to prevent throwing of duplicate exception , but still put association name in constraint name
		as an identifier.**/
		try{
			ConstraintI constraint = problem.newConstraint(var.getName() + " Lower " + time );
			constraint.setRightHandSide(0);
			constraint.setType(Constraint.GREATER);
			problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
			problem.setCoefficientAt(constraint.getName(), oppositeClassName, range.getLower() * (-1));
		}catch(NotFoundException e) {
			e.printStackTrace();
		}
		catch(DuplicateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method adds constraints for the upper bound in the multiplicity constraints
	 * @param problem is the linear programming problem to be extended
	 * @param var is the variable for the current association being investigated
	 * @param range is the range, from which the upper bound is taken
	 * @param oppositeClassName is the opposite class name that its corresponding variable name should
	 * be multiplied by the upper range
	 * @param time is the time this method called. Needed to distinguish the different constraints names
	 */
	public void addUpperMultiplicityConstraint(Problem problem, VariableI var,
			Range range, String oppositeClassName,String time)  {
		//Only if upper bound is bounded a new constraint created
		if (range.getUpper() != MMultiplicity.MANY){
			/** When a new constraint created a name is concatenated with string length in order
			to prevent throwing of duplicate exception , but still put association name in constraint name
			as an identifier.**/
			try {
				ConstraintI constraint = problem.newConstraint(var.getName() + " Higher " +time);
				constraint.setRightHandSide(0);
				constraint.setType(Constraint.LESS);
				problem.setCoefficientAt(constraint.getName(), var.getName(), 1);
				problem.setCoefficientAt(constraint.getName(), oppositeClassName, range.getUpper() * (-1));
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch(DuplicateException e) {
				e.printStackTrace();
			}
		}
	}

}
