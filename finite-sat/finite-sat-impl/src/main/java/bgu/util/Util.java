package bgu.util;

import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.util.StringUtil;

import java.io.*;
import java.nio.CharBuffer;
import java.util.Scanner;

public class Util {
	
	private static Util u;
	private PrintStream out;
	private InputStream in;


	public static Util getUtil() {
		if (u == null) {
			u = new Util();
		}
		return u;
	}
	
	private Util() {
		out = System.out;
        in = System.in;
	}
	
	public boolean checkFileExists(String fileName) {
		File f = new File(fileName);
		return f.exists();
	}
	
	public String print(String s) {
            out.println(s);

            return s;
	}

    public String read(){
        String msg ="";
        byte[] buf = new byte[1024];

       // Scanner sc = new Scanner(in);
        try {
            int len = in.read(buf);
            msg = new String(buf, 0,len);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return msg.toString();

    }
	
	public void setOut(PrintStream out) {
		this.out = out;
	}
	public void setIn(InputStream in) {
        this.in = in;}

	public PrintStream getOut() {
		return out;
	}
	public InputStream getIn() { return in;}

	public MModel compile(Reader r) {
		MModel model = USECompiler.compileSpecification(r,
				null, new PrintWriter(out),
				new ModelFactory());
		return model;

	}
	
	public MModel compile(String fileName) throws FileNotFoundException {
		Reader r = new BufferedReader(new FileReader(fileName));
		return compile(r);
	}

}
