package bgu.util;

import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAssociationClass;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MGeneralizable;
import org.tzi.use.uml.mm.MGeneralization;
import org.tzi.use.uml.mm.MGeneralizationSet;
import org.tzi.use.uml.mm.MInvalidModelException;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MXor;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.uml.ocl.type.EnumType;
import org.tzi.use.uml.ocl.type.TypeFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;



public abstract class DeepCopyMModel {

	protected MModel src_model;
	protected MModel tar_model;
	protected final ModelFactory factory;


	public DeepCopyMModel() {
		factory = new ModelFactory();
	}

	public DeepCopyMModel(MModel model) {
		this();
		src_model = model;
		tar_model = factory.createModel(src_model.name());
	}

	public void copyClasses(MModel newModel, MModel model) throws MInvalidModelException {
		this.setOldModel(model);
		this.setNewModel(newModel);
		this.copyClasses();
	}

	public void copyClasses() throws MInvalidModelException {
		for (final Object obj : src_model.classes()) {
			final MClass cls = (MClass) obj;
			final MClass newCls;
			if (obj instanceof MAssociationClass){
				newCls = this.factory.createAssociationClass(cls.name(), cls.isAbstract());
            }
			else {
				newCls = this.factory.createClass(cls.name(), cls.isAbstract());
            }
            newCls.setVirtualClass(cls.isVirtualClass());
            newCls.setFlag(cls.getFlag());
            newCls.setDeletedClass(cls.isDeletedClass());
			this.tar_model.addClass(newCls);

			for (Object a : cls.allAttributes()) {
				MAttribute atr = (MAttribute) a;
				if (cls.attribute(atr.name(), false) != null)
					newCls.addAttribute(this.factory.createAttribute(atr
							.name(), atr.type()));
			}

			for (Object o : cls.allOperations()) {
				MOperation op = (MOperation) o;
				if (cls.operation(op.name(), false) != null)
					newCls.addOperation(this.factory.createOperation(op
							.name(), op.paramList(), op.resultType()));
			}
		}
	}


	public void copyGEneralizationSets() {
		MGeneralizationSet newGs;
		for (MGeneralizationSet gs : src_model.getGeneralizationSets()) {
			newGs = factory.createGeneralizationSet(gs.getGSName());
			newGs.setSuperClass(tar_model.getClass(gs.getSuperClass().name()));
			newGs.setType(gs.getType());
			for (MClass cls : gs.getSubClasses()) {
				newGs.addSubClass(tar_model.getClass(cls.name()));
			}
			tar_model.addGeneralizationSet(newGs);
		}
	}


	public void copyGeneralizations() throws MInvalidModelException {
		MGeneralization gen;
		MGeneralizable child, parent;
		Iterator<MGeneralization> it = src_model.generalizationGraph().edgeIterator();


		while (it.hasNext()) {
			gen = it.next();
			if (gen.child() instanceof MAssociationClass) {
				child = tar_model.getAssociationClass(gen.child().name());
				parent = tar_model.getAssociationClass(gen.parent().name());
			}
			else if (gen.child() instanceof MAssociation) {
				child = tar_model.getAssociation(gen.child().name());
				parent = tar_model.getAssociation(gen.parent().name());
			}
			else {
				child = tar_model.getClass(gen.child().name());
				parent = tar_model.getClass(gen.parent().name());
			}
			tar_model.addGeneralization(factory.createGeneralization(child, parent));
		}
	}


	public void copyAssociations() throws MInvalidModelException {

		//key - from src_model, val - from tar_model
		Map<MAssociationEnd, MAssociationEnd> assocEndMap = new HashMap<MAssociationEnd, MAssociationEnd>(); 

		for (final Object obj : src_model.associations())
		{
			final MAssociation assoc = (MAssociation) obj;
			if (assoc instanceof MAssociationClass)
			{
				final MAssociationClass src_assocls = (MAssociationClass) assoc;
				MAssociationClass tar_assocClass = this.tar_model.getAssociationClass(src_assocls.name());

				for (Object o : src_assocls.associationEnds())
				{
					MAssociationEnd ae = (MAssociationEnd) o;
					copyAssociationEnd(tar_assocClass, ae, assocEndMap);

				}

				addAssociationToTar_model(tar_assocClass);
			}
			else {
				final MAssociation newAssoc = this.factory
						.createAssociation(assoc.name());
				for (Object o : assoc.associationEnds())

					{
					MAssociationEnd ae = (MAssociationEnd) o;
					copyAssociationEnd(newAssoc, ae, assocEndMap);
				}
                newAssoc.setDeleteAssoc(assoc.isDeleteAssoc());
                newAssoc.setVirtualAssoc(assoc.isVirtualAssoc());
                newAssoc.setFlag(assoc.getFlag());
				addAssociationToTar_model(newAssoc);
			}

		}
		this.copySubsetRedefine(assocEndMap);
	}

	private void addAssociationToTar_model(MAssociation newAssoc) throws MInvalidModelException {
		tar_model.addAssociation(newAssoc);
		tar_model.redefineAssociationsGraph().add(newAssoc);
		tar_model.subsetAssociationsGraph().add(newAssoc);
		tar_model.generalizationGraph().add(newAssoc);
	}

	private void copyAssociationEnd (MAssociation tar_assoc, MAssociationEnd src_assocEnd, Map<MAssociationEnd, MAssociationEnd> assocEndMap) throws MInvalidModelException {
		MAssociationEnd new_end = this.factory.createAssociationEnd(
				this.tar_model.getClass(src_assocEnd.cls().name()), 
				src_assocEnd.nameAsRolename(), 
				src_assocEnd.multiplicity().clone(), 
				src_assocEnd.aggregationKind(), 
				src_assocEnd.isOrdered());

		new_end.setQualifier(src_assocEnd.getQualifier());
		tar_assoc.addAssociationEnd(new_end);

		assocEndMap.put(src_assocEnd, new_end);
	}

	public void copyXors() {

		MAssociation assoc = null;
		for (MXor xor : src_model.getXorSet()) {
			List<MAssociationEnd> ends = new ArrayList<MAssociationEnd>(xor.numberOfEnds());
			for (MAssociationEnd end : xor.getEnds()) {
				assoc = tar_model.getAssociation(end.association().name());
				for (MAssociationEnd assocEnd : (List<MAssociationEnd>)assoc.associationEnds()) {
					if (assocEnd.cls().name().equals(end.cls().name())) {
						ends.add(assocEnd);
						break;
					}
				}
			}
			tar_model.addXor(factory.createXor(xor.name(), 
					tar_model.getClass(xor.getSourceClass().name()), 
					ends));
		}
	}



	private void copySubsetRedefine(Map<MAssociationEnd, MAssociationEnd> assocEndMap) {
		MAssociationEnd tarEnd = null;
		for (MAssociationEnd srcEnd : assocEndMap.keySet()) {
			tarEnd = assocEndMap.get(srcEnd);
			for (MAssociationEnd srcSubsetedEnd : srcEnd.subset().getSubseted()) {
				tarEnd.subset().addSubsettedAssocEnd(assocEndMap.get(srcSubsetedEnd));
			}

			for (MAssociationEnd srcRedefinedEnd : srcEnd.redefine().getRedefined()) {
				tarEnd.redefine().addRedefinedAssocEnd(assocEndMap.get(srcRedefinedEnd));
			}
		}
	}

	public void copyEnums() throws MInvalidModelException {
		for (EnumType e : (Set<EnumType>)src_model.enumTypes()) {
			EnumType res;
			String name = e.name();

			// map token list to string list
			List<String> literalList = new ArrayList<String>();
			Iterator it = e.literals();
			String s;
			while (it.hasNext() ) {
				s = (String) it.next();
				literalList.add(s);
			}
			
			res = TypeFactory.mkEnum(name, literalList);
			tar_model.addEnumType(res);
		}



	}

	/*
	 * 
	   EnumType res;
        String name = fName.getText();

        // map token list to string list
        List literalList = new ArrayList();
        Iterator it = fIdList.iterator();
        while (it.hasNext() ) {
            MyToken tok = (MyToken) it.next();
            literalList.add(tok.getText());
        }

        try {
            res = TypeFactory.mkEnum(name, literalList);

	 */

	protected MModel getNewModel() {
		return tar_model;
	}

	protected void setNewModel(MModel newModel) {
		this.tar_model = newModel;
	}

	protected MModel getOldModel() {
		return src_model;
	}

	protected void setOldModel(MModel model) {
		this.src_model = model;
	}

	public abstract MModel createNewModel();
}
