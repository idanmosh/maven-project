package bgu.translation;

import bgu.util.DeepCopyMModel;
import com.sun.swing.internal.plaf.synth.resources.synth_sv;
import org.tzi.use.uml.mm.*;
import org.tzi.use.uml.ocl.type.EnumType;
import org.tzi.use.uml.ocl.type.Type;

import java.util.*;

/**
 * Created by user on 11/10/14.
 */
public class Translate extends DeepCopyMModel {

    private static Translate _mt = null;

    private Translate(){

    }

    public static Translate getInstance() {
        if (_mt == null) {
            _mt = new Translate();
        }
        return _mt;
    }

    /**
     * given a model with association-classes ans qualifiers translate it to a new model
     * without those constraints.
     * @param model     the model to be translated
     * @return          nre model whith out association class and qualifier constraints.
     */
    public MModel translate(MModel model){
        MModel newModel = factory.createModel(model.name());
        super.setNewModel(newModel);
        super.setOldModel(model);
        try {
            copyEnums();
            copyClasses();
            copyAssociations();
            copyGeneralizations();
            copyGEneralizationSets();
            copyXors();
            translation();
        }
        catch (MInvalidModelException e) {
            System.err.println("Error: " + e.getMessage() + " can't continue...");
            System.exit(-1);
        }
        return  super.tar_model;
    }

    private void translation(){
        //tar_model and src_model should be the same
        //translation of association classes
        Collection<MAssociationClass> ca = super.tar_model.getAssociationClassesOnly();
        Iterator<MAssociationClass> it1 = ca.iterator();
        while(it1.hasNext()){
            MAssociationClass mac = it1.next();
            try {
                _translateAssocClass(mac);
            } catch (MInvalidModelException e) {
                e.printStackTrace();
            }
        }
        //tarnslation of qualifiers
        Collection<MAssociation> c = super.tar_model.associations(true);
        Iterator<MAssociation> it2 = c.iterator();
        while(it2.hasNext()){
            MAssociation ass = it2.next();
            List<MAssociationEnd> ma = ass.associationEnds();
            if ( ((MAssociationEnd)ma.get(0)).isQualified() || ((MAssociationEnd)ma.get(1)).isQualified()){
                ass.setDeleteAssoc(true);
                ass.setFlag(1);
                _translateQualifier(ass);
            }
        }

    }
    //new
    private void _translateAssocClass(MAssociationClass ass) throws MInvalidModelException{
        MAssociation assoc1,assoc2;
        MMultiplicity mult;
        MAssociationEnd assocEnd;
        //delete ass from the model classe set
        //super.tar_model.classes().remove(ass);
        ass.setDeletedClass(true);
        ass.setFlag(2);
        // create new virtual class
        MClass cls = super.factory.createClass(ass.name()+"_Class", false);//create class name as above
        cls.setVirtualClass(true); // mark the class has virtual
        cls.setFlag(2);//mark has created from association class


        //get the two originals classes
        List<MAssociationEnd> list = ass.associationEnds();
        MAssociationEnd s1 = list.get(0); //"left"
        MAssociationEnd s2 = list.get(1);//"right"

        // create 2 new association
        assoc1 = super.factory.createAssociation(s1.cls().name()+ass.name()); //create two virtual association between
        assoc2 = super.factory.createAssociation(s2.cls().name()+ass.name());//the virtual class to the two original classes
        assoc1.setVirtualAssoc(true);
        assoc2.setVirtualAssoc(true);

        //create the multiplicity of the associationEnds
        //"left"
        mult = factory.createMultiplicity();
        mult.addRange(1,1);
        assocEnd = super.factory.createAssociationEnd(s1.cls(), gensym(), mult , MAggregationKind.NONE, false);
        assoc1.addAssociationEnd(assocEnd);

        mult = factory.createMultiplicity();
        mult.addRange(s2.multiplicity().getRange().getLower(), s2.multiplicity().getRange().getUpper());
        assocEnd = super.factory.createAssociationEnd(cls, s2.cls().name()+"_"+gensym(), mult , MAggregationKind.NONE, false);
        assoc1.addAssociationEnd(assocEnd);

        //"right"
        mult = factory.createMultiplicity();
        mult.addRange(1,1);
        assocEnd = super.factory.createAssociationEnd(s2.cls(), gensym(), mult , MAggregationKind.NONE, false);
        assoc2.addAssociationEnd(assocEnd);

        mult = factory.createMultiplicity();
        mult.addRange(s1.multiplicity().getRange().getLower(), s1.multiplicity().getRange().getUpper());
        assocEnd = super.factory.createAssociationEnd(cls, s1.cls().name()+"_"+gensym(), mult , MAggregationKind.NONE, false);
        assoc2.addAssociationEnd(assocEnd);

        super.tar_model.addClass(cls); //add the virtual class
        super.tar_model.addAssociation(assoc1);
        super.tar_model.generalizationGraph().add(assoc1);
        super.tar_model.addAssociation(assoc2);
        super.tar_model.generalizationGraph().add(assoc2);

        //find all association related to ass and change them thus they will point to the virtual class
        Set<MAssociation> relatedassociation = ass.associations();
        Iterator<MAssociation> it = relatedassociation.iterator();
        while(it.hasNext()){
            MAssociation ma = it.next();
            //change the relevant assocoationEnd to point to to virtual class
            if(cls.name().equals(((MAssociationEnd)ma.associationEnds().get(0)).cls().name()+"_Class")){
                MAssociationEnd mae = (MAssociationEnd)ma.associationEnds().remove(0); //remove the relevant associationend
                assocEnd = super.factory.createAssociationEnd(cls, mae.name(), mae.multiplicity() , MAggregationKind.NONE, false);
                ma.addAssociationEnd(assocEnd);
            }
            else if(cls.name().equals(((MAssociationEnd)ma.associationEnds().get(1)).cls().name()+"_Class")){
                MAssociationEnd mae = (MAssociationEnd)ma.associationEnds().remove(1); //remove the relevant associationend
                assocEnd = super.factory.createAssociationEnd(cls, mae.name(), mae.multiplicity() , MAggregationKind.NONE, false);
                ma.addAssociationEnd(assocEnd);
            }
        }
    }
    //new
    private void _translateQualifier(MAssociation ass){
        // create new virtual class
        MClass cls = super.factory.createClass(ass.name().toUpperCase(), false);//create class name as above
        cls.setVirtualClass(true); // mark the class has virtual
        cls.setFlag(1);//mark has created from qualifier

        //get the two originals classes
        List<MAssociationEnd> list = ass.associationEnds();
        MAssociationEnd s1 = list.get(0); //"left"
        MAssociationEnd s2 = list.get(1);//"right"

        if (s1.isQualified()){
            try{
                something(ass, s1,s2,cls);
            }catch (MInvalidModelException e) {
                e.printStackTrace();
            }
        }
        else{
            try{
                something(ass, s2,s1,cls);
            }catch (MInvalidModelException e){
                e.printStackTrace();
            }
        }
    }
    //new
    /**
     * create to new virtual association based on the qualified side of the assocoition
     * @param ass the qualified association
     * @param s1 the qiulifier side of the association ass
     * @param s2 the other side of the association ass
     * @param cls the virtual class created
     */
    private void something(MAssociation ass,MAssociationEnd s1,MAssociationEnd s2,MClass cls) throws MInvalidModelException{
        MAssociation assoc1,assoc2;
        MMultiplicity mult;
        MAssociationEnd assocEnd;
        assoc1 = super.factory.createAssociation(s1.cls().name().toLowerCase()+ass.name()+"_qualifier"); //create two virtual association between
        assoc2 = super.factory.createAssociation(s2.cls().name().toLowerCase()+ass.name());//the virtual class and the two original classes
        assoc1.setVirtualAssoc(true);
        assoc2.setVirtualAssoc(true);

        mult = factory.createMultiplicity();
        mult.addRange(1,1);
        assocEnd = super.factory.createAssociationEnd(s1.cls(), gensym(), mult , MAggregationKind.NONE, false);
        assoc1.addAssociationEnd(assocEnd);
        int k = 0;
        //iterator on all Enums in the model
        Iterator<EnumType> it = tar_model.enumTypes().iterator();
        while(it.hasNext()){
            EnumType s = it.next();
            //loop on Attributes associate the End side (who is qualifier)
            for (int i=0 ;  i < s1.getQualifier().getAttributes().size() ; i++){
                if (s1.getQualifier().getAttributes().get(i).type().toString().equals(s.toString()))
                    k = s.getEnumerationSize();
            }
        }
        mult = factory.createMultiplicity();
        mult.addRange(k,k);
        assocEnd = super.factory.createAssociationEnd(cls, gensym(), mult , MAggregationKind.NONE, false);
        assoc1.addAssociationEnd(assocEnd);

        mult = factory.createMultiplicity();
        mult.addRange(s2.multiplicity().getRange().getLower(),s2.multiplicity().getRange().getUpper());
        assocEnd = super.factory.createAssociationEnd(s2.cls(), gensym(), mult , MAggregationKind.NONE, false);
        assoc2.addAssociationEnd(assocEnd);

        mult = factory.createMultiplicity();
        mult.addRange(s1.multiplicity().getRange().getLower(),s1.multiplicity().getRange().getUpper());
        assocEnd = super.factory.createAssociationEnd(cls, gensym(), mult , MAggregationKind.NONE, false);
        assoc2.addAssociationEnd(assocEnd);

        super.tar_model.addClass(cls); //add the virtual class
        super.tar_model.addAssociation(assoc1);
        super.tar_model.generalizationGraph().add(assoc1);
        super.tar_model.addAssociation(assoc2);
        super.tar_model.generalizationGraph().add(assoc2);

    }

    private static int count=111;
    private String gensym(){
        String res = "gensym_"+count;
        count++;
        return res;
    }

    /**
     * given a model with association-classes ans qualifiers translate it to a new model
     * without those constraints.
     * @param model     the model to be translated
     * @return          nre model whith out association class and qualifier constraints.
     */
    public MModel translateMinus1(MModel model){
        MModel newModel = factory.createModel(model.name());
        super.setNewModel(newModel);
        super.setOldModel(model);
        try {
            copyEnums();
            copyClasses();
            copyAssociations();
            copyGeneralizations();
            copyGEneralizationSets();
            copyXors();
            translationMinus1();
        }
        catch (MInvalidModelException e) {
            System.err.println("Error: " + e.getMessage() + " can't continue...");
            System.exit(-1);
        }
        return  super.tar_model;
    }

    //new
    private MModel translationMinus1(){
        Collection<MClass> classes = super.tar_model.classes();// return all classes in the model
        Iterator<MClass> it = classes.iterator();//over all the classes!
        while(it.hasNext()){
            MClass cls = it.next();
            if(cls.isVirtualClass() && cls.getFlag()==2){
                try {
                    translateMinus1AssocClass(cls);
                } catch (MInvalidModelException e) {
                    e.printStackTrace();
                }
            }
            else if(cls.isVirtualClass() && cls.getFlag()==1){
                translateMinus1Qualified(cls);
            }
        }
        delVirtAssociations();
        delVirtClasses();
        return  super.tar_model;
    }

    //new
    private void translateMinus1Qualified(MClass cls){
        MMultiplicity.Range r;
        Set<MAssociation> assos;
        assos = cls.associations();//return all association related to this class
        //model.classes().remove(cls);//no need of this class in the real model

        Set<MClass> lmc = new HashSet<MClass>();//Set to save the originals classes connected in source model
        List<MMultiplicity> lm = new ArrayList<MMultiplicity>(2);//the multiplicity of the two classes
        Iterator<MAssociation> iterator = assos.iterator();
        while(iterator.hasNext()){//passing all assosiation related to the virtual class
            MAssociation ma = iterator.next();
            lmc.add(((MAssociationEnd)(ma.associationEnds().get(0))).cls());
            if (ma.name().indexOf("qualifier") == -1){//if this is not the qualifier "side"
                //save the multiplicity of both sides
                lm.add(((MAssociationEnd)(ma.associationEnds().get(0))).multiplicity());//original
                lm.add(((MAssociationEnd)(ma.associationEnds().get(1))).multiplicity());//virtual
            }

            ma.setDeleteAssoc(true);//not relevent any more
        }
        Set<MAssociation> assocBetween = super.tar_model.getAssociationsBetweenClasses(lmc);//return all association between the two original classes we
        //including deleted association (a.k.a: DeleteAssoc==true)
        iterator = assocBetween.iterator();
        while(iterator.hasNext()){
            MAssociation ma = iterator.next();
            if (ma.isDeleteAssoc() && ma.getFlag() == 1){//if this is the deleted association between the two classes
                ma.setDeleteAssoc(false);//return the assoc to the model

                List<MAssociationEnd> lme = ma.associationEnds();
                if (lme.get(0).isQualified()){//which associationEnds is the qualified side

                    //update of the ranges, both sides
                    r = lme.get(0).multiplicity().getRange();//old range
                    lme.get(0).multiplicity().addRange(lm.get(1).getRange().getLower(),lm.get(1).getRange().getUpper());
                    lme.get(0).multiplicity().getRanges().remove(r);

                    r = lme.get(1).multiplicity().getRange();
                    lme.get(1).multiplicity().addRange(lm.get(0).getRange().getLower(),lm.get(0).getRange().getUpper());
                    lme.get(1).multiplicity().getRanges().remove(r);
                }
                else{
                    //update of the ranges, both sides
                    r = lme.get(1).multiplicity().getRange();//old range
                    lme.get(1).multiplicity().addRange(lm.get(1).getRange().getLower(),lm.get(1).getRange().getUpper());
                    lme.get(1).multiplicity().getRanges().remove(r);

                    r = lme.get(0).multiplicity().getRange();
                    lme.get(0).multiplicity().addRange(lm.get(0).getRange().getLower(),lm.get(0).getRange().getUpper());
                    lme.get(0).multiplicity().getRanges().remove(r);
                }
                break;//we found what we looking for, no need to proceed
            }
        }
//        delVirtClasses();
//        delVirtAssociations();
    }




    //new
    private void translateMinus1AssocClass(MClass cls) throws MInvalidModelException {
        //find the association with the same name as the virtual class
        MAssociation mas1=null;
        Collection<MAssociation> associations = super.tar_model.associations();
        Iterator<MAssociation> it = associations.iterator();
        boolean found=false;
        while(it.hasNext()&& !found){
            mas1 = it.next();
            if(cls.name().equals(mas1.name()+"_Class")){
                found =true;
            }
        }
        //ma is the association we need to change it's multiplicity
        if(found==false)
            return;

        MAssociationEnd ma0=((MAssociationEnd)mas1.associationEnds().get(0));//one end off the association
        MAssociationEnd ma1=((MAssociationEnd)mas1.associationEnds().get(1));//another end of the association

        Set<MAssociation> assos = cls.associations();//the association related to the virtual class
        it = assos.iterator();
        while (it.hasNext()){
            MAssociation mas2 = it.next();
            MAssociationEnd ma2= ((MAssociationEnd)mas2.associationEnds().get(0));
            MAssociationEnd ma3= ((MAssociationEnd)mas2.associationEnds().get(1));
            if(mas2.isVirtualAssoc() && mas2.getFlag()==2){
                //remove the virtual assoc
                mas2.setDeleteAssoc(true);
                //change the multiplicity
                MMultiplicity.Range r;
                if(ma2.name().startsWith(ma0.cls().name())){
                    r = ma0.multiplicity().getRange();
                    ma0.multiplicity().addRange(ma2.multiplicity().getRange().getLower(),ma2.multiplicity().getRange().getUpper());
                    ma0.multiplicity().getRanges().remove(r);
                    //ma1.multiplicity<-ma3.muliplicity
                }
                else if(ma3.name().startsWith(ma0.cls().name())){
                    r = ma0.multiplicity().getRange();
                    ma0.multiplicity().addRange(ma3.multiplicity().getRange().getLower(),ma3.multiplicity().getRange().getUpper());
                    ma0.multiplicity().getRanges().remove(r);
                    //ma1.multiplicity<-ma2.multiplicity
                }
                if(ma2.name().startsWith(ma1.cls().name())){
                    r = ma1.multiplicity().getRange();
                    ma1.multiplicity().addRange(ma2.multiplicity().getRange().getLower(),ma2.multiplicity().getRange().getUpper());
                    ma1.multiplicity().getRanges().remove(r);
                    //ma1.multiplicity<-ma3.muliplicity
                }
                else if(ma3.name().startsWith(ma1.cls().name())){
                    r = ma1.multiplicity().getRange();
                    ma1.multiplicity().addRange(ma3.multiplicity().getRange().getLower(),ma3.multiplicity().getRange().getUpper());
                    ma1.multiplicity().getRanges().remove(r);
                    //ma0.multiplicity<-ma2.muliplicity
                }
            }
            else{
                if(ma0.cls().name().equals(ma2.cls().name()) || ma1.cls().name().equals(ma2.cls().name())){
                    mas2.associationEnds().remove(ma3);
                    MAssociationEnd assocEnd =super.factory.createAssociationEnd(((MClass)mas1),ma3.name(), ma3.multiplicity() , MAggregationKind.NONE, false);
                    mas2.addAssociationEnd(assocEnd);
                }
                else if(ma0.cls().name().equals(ma3.cls().name()) ||ma1.cls().name().equals(ma3.cls().name()) ){
                    mas2.associationEnds().remove(ma2);
                    MAssociationEnd assocEnd =super.factory.createAssociationEnd(((MClass)mas1),ma2.name(), ma2.multiplicity() , MAggregationKind.NONE, false);
                    mas2.addAssociationEnd(assocEnd);
                }

            }
        }
//        delVirtClasses();
//        delVirtAssociations();

    }

    private void delVirtClasses(){
        Collection<MClass> classes = super.tar_model.classes();
        Iterator<MClass> it = classes.iterator();
        int i;
        while(it.hasNext()){
            MClass cls = it.next();
            if (cls.isVirtualClass())
                it.remove();
        }
    }

    private void delVirtAssociations() {
        Collection<MAssociation> classes = super.tar_model.associations();
        Iterator<MAssociation> it = classes.iterator();
        int i;
        while(it.hasNext()){
            MAssociation cls = it.next();
            if (cls.isVirtualAssoc() || cls.isDeleteAssoc())
                it.remove();
        }
    }

    @Override
    public MModel createNewModel() {
        return null;
    }


}
